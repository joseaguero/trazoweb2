<div class="ctndr100">
    <div class="footer">
        <div class="filaNewsletter">
            <form>
                <input type="text" name="newsletter" class="campoNewsletter" placeholder="Ingresa tu correo para recibir nuestras novedades" />
                <a href="javascript:void(0)" class="btnNewsletter">SUSCRÍBETE</a>
            </form>
        </div>
        <div class="filaMenuFooter">
            <div class="menuFooterIzquierdo">
                <a href="<?php echo $url_base; ?>nuestra-tienda">TIENDA</a>
                <a href="<?php echo $url_base; ?>terminos-y-condiciones">TÉRMINOS Y CONDICIONES</a>
                <!--<a href="<?php echo $url_base; ?>politicas-de-envio">POLÍTICAS DE ENVÍO</a>
                <a href="<?php echo $url_base; ?>cambios-y-devoluciones">CAMBIOS Y DEVOLUCIONES</a>
                <a href="">CÓMO COMPRAR</a>-->
            </div>
            <div class="menuFooterDerecho">
                <a class="redes" href="https://www.instagram.com/tiendaforastero/" target="_blank"><img src="img/instagram.jpg" width="22" height="23" /></a>
                <a class="redes" href="https://www.facebook.com/tiendaforastero/" target="_blank"><img src="img/facebook.jpg" width="22" height="23" /></a>
                <a class="telefonoFooter" href="tel:+56945732364">tel +562 2219 1224</a>
                <a href="javascript:void(0)" target="_blank">Horarios: Lunes a Viernes de 10:00 a 20:00 hrs., Sabado 10:00 a 14:30 hrs.</a>
                <a href="https://goo.gl/maps/JcvZAjLrGCK2" target="_blank">AV.JUAN XXIII 6190, VITACURA</a>
            </div>
        </div><!--Fin filaMenuFooter -->
        <div class="filaWebpay ctndr100"><img src="img/iconoWebpayFooter.jpg" width="161" /></div>
        <div class="filaFirma ctndr100">
            <span>Todos los derechos reservados. FORASTERO, 2016</span> 
            <a href="http://www.moldeable.com" target="_blank">
                <img src="img/firma.jpg" width="26" />
            </a>
        </div>
        <div style="clear:both"></div>
    </div>
</div>