<?php 
	$id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
	$nombre = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;
	$destacados = consulta_bd("titulo,sku_productos","banner_home","id=$id","");
?>
<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="javascript:void(0)" class="">destacados</a> <span>></span> <a href="javascript:void(0)" class="actual"><?php echo strip_tags($destacados[0][0]); ?></a>
    </div>
    <div class="filtros">
    	
    </div>
    <div class="titulo">
    	<h2><?php echo strip_tags($destacados[0][0]); ?></h2>
    </div>
</div>

<?php 
//echo $destacados[0][1];
    $cadena = strip_tags($destacados[0][1]);

    $cadena = str_replace(" ","",$cadena);
    $cadena = str_replace("&nbsp;","",$cadena);
	
$productos = explode(",", $cadena);
	//$productos[1];

//echo $cadena;
	$array[]=""; 

	$productosPrincipales = '';
    foreach($productos as $clave){
        $clave2 = trim($clave);
		$productoId = consulta_bd("producto_id","productos_detalles","sku='$clave2'","");
        $cant = mysqli_affected_rows($conexion);
		//echo $productoId[0][0].'--'.$clave.'<br/>';
        if($cant > 0){
			$productosPrincipales .= $productoId[0][0].',';
			if (!in_array($productoId[0][0], $array)) {
				$array[] = $productoId[0][0];
			}
		}
	}
    
    //echo $productosPrincipales.'<br/>';
?>

<div class="productosDestacados content_products ctndr100 grid-5">
    <?php for($i=1; $i<sizeof($array); $i++){ 
		$idArreglo = trim($array[$i]);
		$prod = consulta_bd("p.id, p.nombre, p.thumbs","productos p, productos_detalles pd","p.publicado=1 and p.id=$idArreglo and p.id = pd.producto_id and pd.publicado=1", "p.id");
		$cantProd = mysqli_affected_rows($conexion);
		//echo $prod[$i][1];
		if($prod[0][2] == '' || $prod[0][2] == 'NULL'){
			$grilla = $url_base."img/sinImagenGrilla.jpg";
		} else {
			$grilla = $prod[0][2];
		}
		if($cantProd > 0){
	?>
    <div class="grillaProducto">
        <div class="intGrilla">
            <a href="<?php echo $url_base.'ficha/'.$prod[0][0].'/'.url_amigables($prod[0][1]); ?>" class="contImgGrilla">
                <?php echo ofertaGrilla($prod[0][0]); ?>
                <img src="<?php echo $grilla ?>" width="100%" alt="<?php echo $prod[0][1]; ?>" />
            </a>
            <a href="<?php echo $url_base.'ficha/'.$prod[0][0].'/'.url_amigables($prod[0][1]); ?>"><?php echo $prod[0][1]; ?></a>
            <a href="<?php echo $url_base.'ficha/'.$prod[0][0].'/'.url_amigables($prod[0][1]); ?>"><?php echo skuGrilla($prod[0][0]); ?></a>
            <a href="<?php echo $url_base.'ficha/'.$prod[$i][0].'/'.url_amigables($prod[0][1]); ?>" class="valorGrilla"><?php echo valorGrilla($prod[0][0]); ?></a>
        </div>
    </div><!--fin grillaProducto -->
    <?php 
            } 
        } 
    ?>
    
    
</div><!--Fin productosDestacados -->

