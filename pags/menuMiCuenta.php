<div class="menuMiCuenta">
    <a class="<?php if($op == "mi-cuenta"){echo "current";}?>" href="mi-cuenta">Mis Datos</a>
    <a class="<?php if($op == "mis-direcciones"){echo "current";}?>" href="mis-direcciones">Mis Direcciones</a>
    <a class="<?php if($op == "mis-pedidos" || $op == "detalle-pedido"){echo "current";}?>" href="mis-pedidos">Mis Pedidos</a>
    <a class="<?php if($op == "productos-guardados"){echo "current";}?>" href="productos-guardados">Productos Guardados</a>
    
    <a href="logout">Cerrar sesión</a>
</div>