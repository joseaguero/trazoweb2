<div class="header ctndr100">
	<a href="javascript:void(0)" class="btnMenu"><i class="material-icons">&#xE5D2;</i></a>
    <div class="barraTop ctndr100">
    	
        <div class="contCarroHeader">
            <a href="carro" class="cantItems"><?php echo qty_pro(); ?></a>
            <a href="carro" class="carro"><i class="material-icons">&#xE8CC;</i></a>
            <a href="carro" class="totalHeader">$<?php echo number_format(totalCart(),0,",","."); ?></a>
            <div id="contMiniCart">
            	<?php echo miniCart(); ?><!--Fin resumenCarroHeader-->
            </div>

            <div id="popup-producto"></div>
			
        </div>
        
        <div class="contuser">
            <?php if (!isset($_SESSION['user_account'])): ?>
                <a href="login">Mi cuenta</a>
            <?php else: ?>
                <a href="mi-cuenta"><i class="fas fa-user"></i> <?= info_user($_SESSION['user_account'], 'fullname') ?></a>
            <?php endif ?>
        </div>
        
        <a href="<?php echo $url_base; ?>nuestra-tienda" class="menuTipo">NUESTRA TIENDA</a>
        <form id="buscador" method="get" action="busquedas">
            <input type="submit" name="enviar" value=" " class="btnBuscar" id="lupa" />
            <input type="text" name="busqueda" placeholder="Ingrese lo que desea buscar" class="campoBuscar" id="input-buscador" />  
        </form>
        <a class="buscar"><span>BUSCAR</span></a>
    </div>
    <h1 class="contLogo">
        <a href="<?php echo $url_base; ?>home"></a>
    </h1>
</div>

<script type="text/javascript">
    $(document).ready(function () {
    $('#input-buscador').hide()
});
 
$(".btnBuscar, .buscar").click(function (e) {
    $('#input-buscador').animate({
        width: 'toggle'
    }, "slow");
    if ($('#input-buscador').val().trim() == "") {
        e.preventDefault();
    }
});


</script>