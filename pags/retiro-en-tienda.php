<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Retiro en tienda</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>Retiro en tienda</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">
		<h4>COMPRA ONLINE, RETIRA EN TIENDA</h4>
		<p>Tu tiempo es valioso para nosotros. Retira rápido y sin costo de despacho,
independiente del monto de la compra.</p>
		<h4>¿CÓMO FUNCIONA EL RETIRO EN TIENDA?</h4>
		<img src="img/pag_retiroentienda.png">
	</div><!--fin contTextosFijos -->
</div><!--Fin productosDestacados -->

