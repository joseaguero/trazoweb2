<?php 

    require_once 'paginador/paginadorofertas.class.php';
    
    /*$campos="p.id, p.nombre, p.thumbs, pd.nombre, pd.id, MIN(if(pd.descuento > 0, pd.descuento,pd.precio)) as valorMenor, pd.sku, c.nombre, sc.nombre";
    $tablas="productos p, subcategorias sc, categorias_productos cp, productos_detalles pd, categorias c";
    $where="p.publicado=1 and p.id=cp.producto_id and sc.id=cp.subcategoria_id and c.id = cp.categoria_id and p.id = pd.producto_id and pd.descuento <> 0";*/

    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 1;
    $ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 20;

    $campos="distinct(pd.producto_id), p.id, p.nombre, p.thumbs, pd.nombre, pd.id, pd.descuento, pd.sku";
    $tablas="productos p, productos_detalles pd";
    $where="p.publicado=1 and p.id = pd.producto_id and pd.publicado = 1 and pd.descuento <> 0;";
    
    $resultados = consulta_bd($campos,$tablas,$where,"");
    $cant_resultados = mysqli_affected_rows($conexion);

    $filtro_cat = (isset($_GET[id_cat])) ? mysqli_real_escape_string($conexion, $_GET[id_cat]) : 0;
    $filtro_sub = (isset($_GET[id_sub])) ? mysqli_real_escape_string($conexion, $_GET[id_sub]) : 0;
    
    if ($filtro_cat > 0) {
        $filtro['categoria'] = $filtro_cat;
    }
    if ($filtro_sub > 0) {
        $filtro['subcategoria'] = $filtro_sub;
    }

    $filtro['oferta'] = true;
    $ofertas = productos($filtro);

    $rutaRetorno = 'ofertas';
    

?>


<div class="filaCategorias ctndr100">
    <div class="breadCrumbs">
        <a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Ofertas</a>
    </div>
    <div class="filtros">
        
    </div>
    <div class="titulo">
        <h2>OFERTAS</h2>
    </div>
</div>

<div class="menu-lateral">
    <ul>
        <li class="menu-title">OFERTAS</li>
        <?php 
        // $categorias = consulta_bd('id, nombre', 'categorias', 'publicada = 1', 'id asc');
        $categorias = consulta_bd('c.id, c.nombre',
                            'categorias c 
                            JOIN categorias_productos cp ON cp.categoria_id = c.id
                            JOIN productos p ON p.id = cp.producto_id
                            JOIN productos_detalles pd ON pd.producto_id = p.id',
                            'c.publicada = 1 AND pd.descuento > 0 AND p.publicado = 1 AND pd.publicado = 1 GROUP BY c.id', 'c.id asc');
        foreach ($categorias as $categoria) { 
            $url_categorias = 'ofertas/'.$ipp.'/pagina-1/'.$categoria[0].'/'.url_amigables($categoria[1]);
            $active = ($categoria[0] == $filtro_cat) ? 'class="menu-lateral-active"': ''; ?>
            <li><a href="<?= $url_categorias ?>" <?= $active ?>><?php echo $categoria[1]; ?><i class="fas fa-angle-down"></i></a>
                <?php if ($filtro_cat > 0): 
                    if ($filtro_cat == $categoria[0]): ?>
                        <ul class="suboff">
                            <?php $subcats = consulta_bd('s.id, s.nombre', 
                                'subcategorias s
                                JOIN categorias_productos cp ON cp.subcategoria_id = s.id
                                JOIN productos p ON p.id = cp.producto_id
                                JOIN productos_detalles pd ON p.id = pd.producto_id', 
                                "s.categoria_id = $categoria[0] AND pd.descuento > 0 AND p.publicado = 1 AND pd.publicado = 1 GROUP BY s.id", 
                                's.id asc'); 
                            foreach ($subcats as $sub): 
                                $url_sub = $url_categorias . '/' . $sub[0] . '/' . url_amigables($sub[1]); 
                                $active = ($sub[0] == $filtro_sub) ? 'class="active"': ''; ?>
                                <li><a href="<?= $url_sub ?>" <?= $active ?>><?= $sub[1] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
            </li>
        <?php } ?>
    </ul>
    
</div>

<div class="productosDestacados content_products">
    <?php if ($ofertas != null) {
        echo grilla_ofertas($ofertas, $page, $ipp, $rutaRetorno, $filtro); 
    }else{
        echo 'No se encontraron productos';
    } ?>

<script type="text/javascript">

    $(window).scroll(function(){
        var windowHeight = $(window).scrollTop(); 
        var contenido2 = $(".content_products").offset(); 
        contenido2 = contenido2.top - 100;
        console.log(windowHeight);
  
        if(windowHeight >= contenido2  ){ 
         $('.menu-lateral').addClass('positionFixed'); 

        }else{ 
         $('.menu-lateral').removeClass('positionFixed'); 
        } 
    });
            
</script>
