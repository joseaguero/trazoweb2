<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a>  <span>&gt;</span> <a href="javascript:void(0)" class="actual">Mi Carro</a>
    </div>
</div>

<?php unset($_SESSION['stock_validado']); ?>

<?php if (isset($_SESSION['error_stock'])): ?>
    <script type="text/javascript">
        alertify.alert('Aviso importante', 'No todos los productos del carro cuentan con stock actualmente.');
    </script>
<?php unset($_SESSION['error_stock']); endif ?>

<div class="ctndr100">
	<?php
	if (qty_pro() == 0) {
		echo '<h2 class="titulos_interiores tituloCarro" style="position:relative;">EL CARRO ESTÁ VACÍO</h2>';
	}else{
	?>
    
    <h2 class="titulos_interiores tituloCarro" style="position:relative;">CARRO DE COMPRAS</h2><!--fin titulos_interiores -->
    
    <div class="carroLateral">
    	<div class="filaTitulos">
			<div class="espacioImagen"></div>
			<div class="columnaTituloNombre">Item</div>
			<div class="columnaTituloPrecioUnitario">Precio unitario</div>
			<div class="columnaTituloCantidad">Cantidad</div>
			<div class="columnaTituloTotalFila">Total</div>
			<div class="columnaTituloEliminar"></div>
		</div><!--FIn filaTitulos --> 
		<?php 

		$productos = lista_carro();
		?>
		<?php foreach ($productos['producto'] as $producto): 
			$url = 'ficha/'.$producto['id'].'/'.$producto['nombre_seteado'];
			?>
			<!--Fila para el ciclo -->
	        <div class="filasProductos">
	        	<a class="imgFilaProducto" href="<?=$url?>">
	            	<img src="<?= $producto['imagen_grilla'] ?>" width="100%" />
	            </a>
	            <div class="contCamposTxt">
	            	<div class="columnaNombre">
	                	<span class="nombreResponsive">Item</span><span><?= $producto['nombre'] ?><br><br><?= $producto['nombre_hijo'] ?><br>
	                		<?php if ($producto['terminacion'] != 0) {
	                			$terminacion = consulta_bd('nombre', 'terminaciones', "id = {$producto[terminacion]}", '');
	                			echo strtoupper($terminacion[0][0]);
	                		}else{
	                			echo '';
	                		} ?></span>
	                </div>
	                <div class="columnaPrecioUnitario">
	                	<span class="nombreResponsive">Precio Unitario</span><span>$<?=number_format($producto['precio'],0,",",".")?></span>
	                </div>
	                <div class="columnaCantidad">
	                	<span class="nombreResponsive">Cantidad</span>
	                    <div class="spinner">
	                        <input type="text" id="campoSpinner_<?=$producto['id']?>" name="cantidad" value="<?=$producto['cantidad']?>" class="campoSpinner" data-id="<?=$producto['id']?>" data-sku="<?=$producto['sku']?>"> 
	                        <a href="#" class="spinn spinner_mas" data-action="add"></a>
	                        <a href="#" class="spinn spinner_menos" data-action="remove"></a>
	                    </div>
	                </div>
	                <div class="columnaTotalFila">
	                	<span class="nombreResponsive">Total</span><span>$<?=number_format(round($producto['precio']*$producto['cantidad']),0,",",".")?></span>
	                </div>
	                <div class="columnaEliminar">
						<a href="#" class="btn-delete" data-id="<?=$producto['id']?>"><img src="img/cruz.jpg" width="21" /></a>
					</div>
	            </div><!--fin contCamposTxt -->
	        </div> <!--Finn filasPriductos --> 
		<?php endforeach ?>
		
    </div>
    
    <div class="resumenCarroDerecha">
    	<div class="contTotales">
		   <h4>Resumen de compra</h4>
		   <!--<h5>¿Tienes un código de descuento?</h5>-->
		   <div id="contValoresResumen">
			  <!--<div class="ancho100 filaValoresResumen">
				 <div class="ancho50">Sub-total</div>
				 <div class="ancho50 valoresResumen">$'.number_format($total_neto,0,",",".").'</div>
			  </div>-->
			  <!--<div class="ancho100 filaValoresResumen">
				 <div class="ancho50">Valor IVA</div>
				 <div class="ancho50 valoresResumen">$'.number_format($iva,0,",",".").'</div>
			  </div>-->
			  <div class="ancho100 filaValoresResumen">
				 <div class="ancho50">Sub total</div>
				 <div class="ancho50 valoresResumen">$<?=number_format($productos['totales']['subtotal'],0,",",".")?></div>
			  </div>
		   </div>
		   <div class="ancho100 filatoolTip">*El valor del envío se definirá en el tercer paso del carro</div>
		   <div class=""></div>
		   <div class=""></div>
		   <a href="javascript:void(0)" onclick="validate()" class="btnCompletarCompra">Completar compra</a>
		   <a href="home" class="btnAgregarMasProductos">agregar mas productos</a>
		</div>
    </div><!--fin resumenCarroDerecha -->
<?php } ?>

</div>
<div class="pop_delete">
    <p>¿Estás seguro de querer eliminar este producto del carro de compras?</p>
    <div class="buttons_confirm">
        <a href="#" class="btn_confirm" data-action="cancel">Cancelar</a>
        <a href="#" class="btn_confirm" data-action="accept">Aceptar</a>
    </div>
</div>