<?php 
	$idProd = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
	$nombre = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;
	
	$producto = consulta_bd("p.nombre,p.descripcion_seo,p.envios,p.especificacion,c.nombre, c.id,sc.nombre,sc.id, p.marca_id, p.dificil, p.pack, p.codigos_pack","productos p, categorias c, subcategorias sc, categorias_productos cp, productos_detalles pd","p.publicado=1 and pd.publicado = 1 and cp.producto_id=p.id and cp.categoria_id = c.id and cp.subcategoria_id=sc.id and pd.producto_id = p.id and p.id = $idProd","");
    if ($producto == NULL) {
        echo '<script>location.href = "home";</script>';
    }
	$marca_id = $producto[0][8];
	$monto = consulta_bd("MIN(if(descuento > 0, descuento,precio)) as valorMenor","productos_detalles","producto_id=$idProd and publicado = 1","");
	
	$productoPrincipal = consulta_bd("id,sku,precio,descuento,imagen1,imagen2,imagen3,imagen4,imagen5,nombre,terminacion_id, ancho, alto, largo, stock","productos_detalles","publicado = 1 and producto_id = $idProd and (precio =".$monto[0][0]." or descuento =".$monto[0][0].") ","");

    $producto_dificil = $producto[0][9];

    $pack = $producto[0][10];
    $sku_pack = $producto[0][11];

    if ($pack == 1) {

        $imagenes = array();

        $explode_sku = explode(',', $sku_pack);
        $skus = array();
        foreach ($explode_sku as $item) {
            $skus[$item] = (isset($skus[$item])) ? $skus[$item] + 1 : 1;
        }
        
        foreach ($skus as $item => $qty) {

            $sku_item = trim($item);
            $imagen_producto = consulta_bd('imagen1', 'productos_detalles', "sku = '$sku_item'", '');

            $imagenes[] = $imagen_producto[0][0];

        }

    }
?>

<div class="filaCategorias ctndr100">
	<div class="breadCrumbs fichaBread">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="<?php echo $url_base."categorias/".$producto[0][5]."/".url_amigables($producto[0][4]); ?>"><?php echo $producto[0][4]; ?></a> <span>></span> <a href="<?php echo $url_base."subcategorias/".$producto[0][7]."/".url_amigables($producto[0][6]); ?>"><?php echo $producto[0][6]; ?></a> <span>></span> <a href="javascript:void(0)" class="actual"><?php echo $producto[0][0]; ?></a>
    </div>
    <div class="titulo"></div>
</div>








<div class="contProducto ctndr100">
	<div class="galProducto">
    	<div class="colThumbs">
            <?php if ($pack == 1): ?>
                <?php foreach ($imagenes as $img_gallery): ?>
                    <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $img_gallery; ?>">
                        <img src="<?php echo $img_gallery; ?>" width="96" />
                    </a>
                <?php endforeach ?>
            <?php else: ?>
                
                <?php if($productoPrincipal[0][4] != '' and $productoPrincipal[0][4] != "NULL"){ ?>
                <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][4]; ?>">
                    <img src="<?php echo $productoPrincipal[0][4]; ?>" width="96" />
                </a>
                <?php } ?>
                <?php if($productoPrincipal[0][5] != '' and $productoPrincipal[0][5] != "NULL"){ ?>
                <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][5]; ?>">
                    <img src="<?php echo $productoPrincipal[0][5]; ?>" width="96" />
                </a>
                <?php } ?>
                <?php if($productoPrincipal[0][6] != '' and $productoPrincipal[0][6] != "NULL"){ ?>
                <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][6]; ?>">
                    <img src="<?php echo $productoPrincipal[0][6]; ?>" width="96" />
                </a>
                <?php } ?>
                <?php if($productoPrincipal[0][7] != '' and $productoPrincipal[0][7] != "NULL"){ ?>
                <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][7]; ?>">
                    <img src="<?php echo $productoPrincipal[0][7]; ?>" width="96" />
                </a>
                <?php } ?>
                <?php if($productoPrincipal[0][8] != '' and $productoPrincipal[0][8] != "NULL"){ ?>
                <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][8]; ?>">
                    <img src="<?php echo $productoPrincipal[0][8]; ?>" width="96" />
                </a>
                <?php } 
                
                if($productoPrincipal[0][4] == '' ||  $productoPrincipal[0][4] == 'NULL' and $productoPrincipal[0][5] == '' ||  $productoPrincipal[0][5] === 'NULL' and $productoPrincipal[0][6] == '' ||  $productoPrincipal[0][6] === 'NULL' and $productoPrincipal[0][7] == '' ||  $productoPrincipal[0][7] === 'NULL' and $productoPrincipal[0][8] == '' ||  $productoPrincipal[0][8] === 'NULL') { ?>
                <a class="thumbsGal" href="javascript:void(0)" rel="img/sinImagen.jpg">
                    <img src="img/sinImagen.jpg" width="96" />
                </a>
                <?php } ?>

            <?php endif ?>
        	
        </div>
        <div class="imgGal">
            <?php if ($pack == 1):
                $imgActual = $imagenes[0];
            else: ?>
                <?php if($productoPrincipal[0][4] != '' and $productoPrincipal[0][4] != 'NULL'){ 
                    $imgActual = $productoPrincipal[0][4];
                } else if($productoPrincipal[0][5] != '' and $productoPrincipal[0][5] != 'NULL'){ 
                    $imgActual = $productoPrincipal[0][5];
                } else if($productoPrincipal[0][6] != '' and $productoPrincipal[0][6] != 'NULL'){ 
                    $imgActual = $productoPrincipal[0][6];
                } else if($productoPrincipal[0][7] != '' and $productoPrincipal[0][7] != 'NULL'){ 
                    $imgActual = $productoPrincipal[0][7];
                } else if($productoPrincipal[0][8] != '' and $productoPrincipal[0][8] != 'NULL'){ 
                    $imgActual = $productoPrincipal[0][8];
                } else {
                    $imgActual = "img/sinImagen.jpg";
                }
                ?>
            <?php endif ?>
            
        	<a id="imgGal" class="fancybox" href="<?php echo $imgActual; ?>">
            	<img id="imgGrandeGal" src="<?php echo $imgActual; ?>" width="100%" />
            </a>
        </div>
    </div><!--fin galProducto -->
    <div class="datosProducto">
    	<h2 class="nombreProducto"><?php echo $producto[0][0]; ?></h2>
        <h6>cod: <span id="contCodigo"><?php echo $productoPrincipal[0][1]; ?></span></h6>

        <div class="clear-fix"></div>
        <div class="opciones_producto">
            <div class="row-block"><img src="img/despacho.png"> <span>Disponible para despacho</span></div>
            
            <?php if ($producto_dificil == 0): ?>
                <div class="row-block"><img src="img/tienda.png"> <span>Disponible para retiro</span></div>
            <?php endif ?>
            
        </div>

        <div class="producto_guardado">
            <?php if (isset($_SESSION['user_account'])):
                $existe = producto_guardado($productoPrincipal[0][0], $_SESSION['user_account']); ?>
                <?php if ($existe): ?>
                    <div class="red-color action_guardados" data-id="<?= $productoPrincipal[0][0] ?>" data-action="delete"><i class="fas fa-heart"></i> Producto guardado</div>
                <?php else: ?>
                    <div class="action_guardados" data-id="<?= $productoPrincipal[0][0] ?>" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>
                <?php endif ?>  
            <?php else: ?>
                <div class="action_guardados" data-id="<?= $productoPrincipal[0][0] ?>" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>
            <?php endif ?>
        </div>
        
<?php 
	$medidas = consulta_bd("distinct(nombre), ancho, alto, largo","productos_detalles","producto_id=$idProd","nombre"); 
	$cantMedidas = mysqli_affected_rows($conexion);
	if($cantMedidas > 1){$estilo ='';} else { $estilo = 'style="display:none;"';}
?>
        <div class="radiosButon" <?php echo $estilo; ?>>
        	<h5>Selecciona la Medida que necesitas</h5>
            <div class="contR">
                <?php for($i=0; $i<sizeof($medidas); $i++){ ?>
                <div class="contTerm">
                    <input type="radio" class="cambioMedidas" rel="<?php echo $idProd; ?>" name="medida" <?php if($medidas[$i][0]==$productoPrincipal[0][9]){echo 'checked="checked"';}?> value="<?php echo $medidas[$i][0] ?>" /><div class="nomMedida"><?php echo $medidas[$i][0] ?> CM.</div>
                </div>
                <?php } ?>
            </div>
        </div><!--fin radiosButon -->
        

<?php 
$nomProdTer = $productoPrincipal[0][9];
$texturasDispobibles = consulta_bd("t.id, t.nombre, t.thumbs, t.thumbs_hover, pd.id, pd.nombre", "terminaciones t join productos_detalles pd on pd.terminacion_id = t.id", "pd.nombre = '$nomProdTer' and pd.producto_id = {$idProd} and pd.publicado = 1", "");
$cantTerminaciones = mysqli_affected_rows($conexion);
	if($cantTerminaciones > 0){$estilo2 ='';} else { $estilo2 = 'style="display:none;"';}
	
?>       
        <div class="radiosButon" <?php echo $estilo2; ?>>
        	<h5>Selecciona la Terminación</h5>
            <div class="ctndr100 texturas">
            	<?php for($i=0; $i<sizeof($texturasDispobibles); $i++){ ?>
                <input type="radio" class="cambioProducto" name="terminacion" id="<?php echo url_amigables($texturasDispobibles[$i][1]) ?>" value="<?php echo $texturasDispobibles[$i][0]; ?>" <?php if($texturasDispobibles[$i][0] == $productoPrincipal[0][10]){echo 'checked="checked"';}?> rel="<?php echo $texturasDispobibles[$i][4]; ?>" />
                <?php } ?>
            </div>
        </div><!--fin radiosButon -->
        
        <div class="datosCarro">
        	<div class="valorFicha"><?php echo precioProductoBase($productoPrincipal[0][0]); ?></div>
            
            <?php //if($_GET[opcion] == 'desarrollo'){ ?>
            <div class="agregar">
            	<a href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][0]; ?>" class="btnAgregar" id="btn_agregarCarro">AGREGAR AL CARRO</a>
                <div class="spinner">
                	<input type="text" rel="<?php echo $productoPrincipal[0][14]; ?>" name="cantidad" value="1" class="campoSpinner" />
                    <a href="javascript:void(0)" class="masProductos"></a> 
                    <a href="javascript:void(0)" class="menosProductos"></a> 
                </div>
            </div>
            <?php //} ?>
        </div>
        
        
        <div class="acordion">
        	<?php if($producto[0][1] != ""){?>
            <a class="itemAcordeon" href="javascript:void(0)">Descripción del producto</a>
            <div style="display:block;">
            	<p>
				<?php echo strip_tags($producto[0][1]); ?>
                </p>
            </div>
            <?php } ?>
            
            
            <?php if($producto[0][3] != ""){?>
            <a class="itemAcordeon" href="javascript:void(0)">Especificaciones</a>
            <div>
            	<p>
                    <span class="ancho"><strong>Ancho:</strong><?php echo $productoPrincipal[0][11]; ?> Cm.<br /></span>
                    <span class="alto"><strong>Alto:</strong><?php echo $productoPrincipal[0][12]; ?> Cm.<br /></span>
                    <span class="largo"><strong>Largo:</strong><?php echo $productoPrincipal[0][13]; ?> Cm.</span>
                </p>
                <?php echo $producto[0][3]; ?>
            </div>
            <?php } else {?>
            	<a class="itemAcordeon" href="javascript:void(0)">Especificaciones</a>
                <div>
                    <p>
                        <span class="ancho"><strong >Ancho:</strong><?php echo $productoPrincipal[0][11]; ?> Cm.<br /></span>
                        <span class="alto"><strong>Alto:</strong><?php echo $productoPrincipal[0][12]; ?> Cm.<br /></span>
                        <span class="largo"><strong>Largo:</strong><?php echo $productoPrincipal[0][13]; ?> Cm.</span>
                    </p>
                </div>
            <?php } ?>
        </div><!--Fin acordion -->
        
        
        <div class="ctndr100">
        		<div class="burdeo" style="margin-top:10px">

				<?php 
				$cantidad_dias = despachoDias($idProd);
                echo 'En Santiago: estamos en condiciones de entregarte este producto o que lo retires en nuestra tienda a partir de '. $cantidad_dias .' días hábiles. La fecha exacta se especificará en el paso final de compra.';
			?>
				
				</div>
        </div>
        
        
    </div><!--Fin datosProducto -->
</div><!--Fin contProducto -->








<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	
    </div>
    <div class="filtros">
    	
    </div>
    <div class="titulo">
    	<h2>PRODUCTOS RELACIONADOS</h2>
    </div>
</div>
<div class="productosDestacados content_products productos-100 grid-5">
    
    <?php 
    $filtro['subcategoria'] = $producto[0][7];
    $filtro['limite'] = 5;
    $filtro['relacionados'] = $idProd;
    $filtro['random'] = true;
    $relacionados = productos($filtro);

    echo grilla_busqueda($relacionados);

	?>
        
</div><!--Fin productosDestacados -->

