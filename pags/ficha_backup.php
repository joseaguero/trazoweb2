<?php 
	$idProd = (isset($_GET[id])) ? mysql_real_escape_string($_GET[id]) : 0;
	$nombre = (isset($_GET[nombre])) ? mysql_real_escape_string($_GET[nombre]) : 0;
	
	$producto = consulta_bd("p.nombre,p.descripcion_seo,p.envios,p.especificacion,c.nombre, c.id,sc.nombre,sc.id, p.marca_id","productos p, categorias c, subcategorias sc, categorias_productos cp","p.publicado=1 and cp.producto_id=p.id and cp.categoria_id = c.id and cp.subcategoria_id=sc.id and p.id = $idProd","");
	
	$monto = consulta_bd("MIN(if(descuento > 0, descuento,precio)) as valorMenor","productos_detalles","producto_id=$idProd and publicado = 1","");
	
	$productoPrincipal = consulta_bd("id,sku,precio,descuento,imagen1,imagen2,imagen3,imagen4,imagen5,nombre,terminacion_id, ancho, alto, largo, stock","productos_detalles","producto_id = $idProd and (precio =".$monto[0][0]." or descuento =".$monto[0][0].")","");
?>

<div class="filaCategorias ctndr100">
	<div class="breadCrumbs fichaBread">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="<?php echo $url_base."categorias/".$producto[0][5]."/".url_amigables($producto[0][4]); ?>"><?php echo $producto[0][4]; ?></a> <span>></span> <a href="<?php echo $url_base."subcategorias/".$producto[0][7]."/".url_amigables($producto[0][6]); ?>"><?php echo $producto[0][6]; ?></a> <span>></span> <a href="javascript:void(0)" class="actual"><?php echo $producto[0][0]; ?></a>
    </div>
    <div class="filtros">
    	
    </div>
    <div class="titulo"></div>
</div>







<div class="contProducto ctndr100">
	<div class="galProducto">
    	<div class="colThumbs">
        	<?php if($productoPrincipal[0][4] != '' and $productoPrincipal[0][4] != "NULL"){ ?>
            <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][4]; ?>">
            	<img src="<?php echo $productoPrincipal[0][4]; ?>" width="96" />
            </a>
            <?php } ?>
            <?php if($productoPrincipal[0][5] != '' and $productoPrincipal[0][5] != "NULL"){ ?>
            <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][5]; ?>">
            	<img src="<?php echo $productoPrincipal[0][5]; ?>" width="96" />
            </a>
            <?php } ?>
            <?php if($productoPrincipal[0][6] != '' and $productoPrincipal[0][6] != "NULL"){ ?>
            <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][6]; ?>">
            	<img src="<?php echo $productoPrincipal[0][6]; ?>" width="96" />
            </a>
            <?php } ?>
            <?php if($productoPrincipal[0][7] != '' and $productoPrincipal[0][7] != "NULL"){ ?>
            <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][7]; ?>">
            	<img src="<?php echo $productoPrincipal[0][7]; ?>" width="96" />
            </a>
            <?php } ?>
            <?php if($productoPrincipal[0][8] != '' and $productoPrincipal[0][8] != "NULL"){ ?>
            <a class="thumbsGal" href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][8]; ?>">
            	<img src="<?php echo $productoPrincipal[0][8]; ?>" width="96" />
            </a>
            <?php } 
			
			if($productoPrincipal[0][4] == '' ||  $productoPrincipal[0][4] == 'NULL' and $productoPrincipal[0][5] == '' ||  $productoPrincipal[0][5] === 'NULL' and $productoPrincipal[0][6] == '' ||  $productoPrincipal[0][6] === 'NULL' and $productoPrincipal[0][7] == '' ||  $productoPrincipal[0][7] === 'NULL' and $productoPrincipal[0][8] == '' ||  $productoPrincipal[0][8] === 'NULL') { ?>
            <a class="thumbsGal" href="javascript:void(0)" rel="img/sinImagen.jpg">
            	<img src="img/sinImagen.jpg" width="96" />
            </a>
            <?php } ?>
        </div>
        <div class="imgGal">
        	<?php if($productoPrincipal[0][4] != '' and $productoPrincipal[0][4] != 'NULL'){ 
            	$imgActual = $productoPrincipal[0][4];
            } else if($productoPrincipal[0][5] != '' and $productoPrincipal[0][5] != 'NULL'){ 
            	$imgActual = $productoPrincipal[0][5];
            } else if($productoPrincipal[0][6] != '' and $productoPrincipal[0][6] != 'NULL'){ 
            	$imgActual = $productoPrincipal[0][6];
            } else if($productoPrincipal[0][7] != '' and $productoPrincipal[0][7] != 'NULL'){ 
            	$imgActual = $productoPrincipal[0][7];
            } else if($productoPrincipal[0][8] != '' and $productoPrincipal[0][8] != 'NULL'){ 
            	$imgActual = $productoPrincipal[0][8];
            } else {
				$imgActual = "img/sinImagen.jpg";
			}
			?>
            
        	<a id="imgGal" class="fancybox" href="<?php echo $imgActual; ?>">
            	<img id="imgGrandeGal" src="<?php echo $imgActual; ?>" width="100%" />
            </a>
        </div>
    </div><!--fin galProducto -->
    <div class="datosProducto">
    	<h2 class="nombreProducto"><?php echo $producto[0][0]; ?></h2>
        <h6>cod: <span id="contCodigo"><?php echo $productoPrincipal[0][1]; ?></span></h6>
        
<?php 
	$medidas = consulta_bd("distinct(nombre), ancho, alto, largo","productos_detalles","producto_id=$idProd","nombre"); 
	$cantMedidas = mysql_affected_rows();
	if($cantMedidas > 1){$estilo ='';} else { $estilo = 'style="display:none;"';}
?>
        <div class="radiosButon" <?php echo $estilo; ?>>
        	<h5>Selecciona la Medida que necesitas</h5>
            <div class="contR">
                <?php for($i=0; $i<sizeof($medidas); $i++){ ?>
                <div class="contTerm">
                    <input type="radio" class="cambioMedidas" rel="<?php echo $idProd; ?>" name="medida" <?php if($medidas[$i][0]==$productoPrincipal[0][9]){echo 'checked="checked"';}?> value="<?php echo $medidas[$i][0] ?>" /><div class="nomMedida"><?php echo $medidas[$i][0] ?> CM.</div>
                </div>
                <?php } ?>
            </div>
        </div><!--fin radiosButon -->
        

<?php 
$nomProdTer = $productoPrincipal[0][9];
$texturasDispobibles = consulta_bd("distinct(t.id),t.nombre, t.thumbs,t.thumbs_hover, pd.id, pd.nombre","productos_detalles pd, terminaciones t","pd.nombre='$nomProdTer' and pd.producto_id=$idProd and t.id = pd.terminacion_id","t.nombre"); 
$cantTerminaciones = mysql_affected_rows();
	if($cantTerminaciones > 1){$estilo2 ='';} else { $estilo2 = 'style="display:none;"';}

?>       
        <div class="radiosButon" <?php echo $estilo2; ?>>
        	<h5>Selecciona la Terminación</h5>
            <div class="ctndr100 texturas">
            	<?php for($i=0; $i<sizeof($texturasDispobibles); $i++){ ?>
                <input type="radio" class="cambioProducto" name="terminacion" id="<?php echo url_amigables($texturasDispobibles[$i][1]) ?>" value="<?php echo $texturasDispobibles[$i][0]; ?>" <?php if($texturasDispobibles[$i][0] == $productoPrincipal[0][10]){echo 'checked="checked"';}?> rel="<?php echo $texturasDispobibles[$i][4]; ?>" />
                <?php } ?>
            </div>
        </div><!--fin radiosButon -->
        
        <div class="datosCarro">
        	<div class="valorFicha"><?php echo precioProductoBase($productoPrincipal[0][0]); ?></div>
            
            <?php //if($_GET[opcion] == 'desarrollo'){ ?>
            <div class="agregar">
            	<a href="javascript:void(0)" rel="<?php echo $productoPrincipal[0][0]; ?>" class="btnAgregar" id="btn_agregarCarro">AGREGAR AL CARRO</a>
                <div class="spinner">
                	<input type="text" rel="<?php echo $productoPrincipal[0][14]; ?>" name="cantidad" value="1" class="campoSpinner" />
                    <a href="javascript:void(0)" class="masProductos"></a> 
                    <a href="javascript:void(0)" class="menosProductos"></a> 
                </div>
            </div>
            <?php //} ?>
        </div>
        
        
        <div class="acordion">
        	<?php if($producto[0][1] != ""){?>
            <a class="itemAcordeon" href="javascript:void(0)">Descripción del producto</a>
            <div style="display:block;">
            	<p>
				<?php echo strip_tags($producto[0][1]); ?>
                </p>
            </div>
            <?php } ?>
            
            
            <a class="itemAcordeon" href="javascript:void(0)">Envíos</a>
            <div>
            	<p>Estamos en condiciones de enviarte este producto en <strong><?php if($producto[0][8] == 1){echo '10';} else{echo '4';} ?></strong> dias hábiles.</p>
            </div>
            
            
            <?php if($producto[0][3] != ""){?>
            <a class="itemAcordeon" href="javascript:void(0)">Especificaciones</a>
            <div>
            	<?php echo $producto[0][3]; ?>
            </div>
            <?php } else {?>
            	<a class="itemAcordeon" href="javascript:void(0)">Especificaciones</a>
                <div>
                    <p>
                    	<strong>Ancho:</strong><?php echo $productoPrincipal[0][11]; ?> Cm.<br />
                    	<strong>Alto:</strong><?php echo $productoPrincipal[0][12]; ?> Cm.<br />
                        <strong>Largo:</strong><?php echo $productoPrincipal[0][13]; ?> Cm.
                    </p>
                </div>
            <?php } ?>
        </div><!--Fin acordion -->
        
        
        <div class="ctndr100">
        	<span class="iconTienda"><img src="img/iconoTienda.jpg" width="28" /></span>
            <span class="nIcon burdeo">Disponible para retiro en tienda</span>
        </div>
        
        
    </div><!--Fin datosProducto -->
</div><!--Fin contProducto -->








<?php /*?><div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	
    </div>
    <div class="filtros">
    	
    </div>
    <div class="titulo">
    	<h2>PRODUCTOS RELACIONADOS</h2>
    </div>
</div><?php */?>
<?php /*?><div class="productosDestacados ctndr100">
    
    <?php 
	$relacionados = consulta_bd("p.id,p.nombre,p.thumbs","productos p, subcategorias sc, categorias_productos cp","p.publicado=1 and p.id =cp.producto_id and cp.subcategoria_id = sc.id and sc.id=".$producto[0][7],"p.posicion asc limit 5"); 
	for($i=0; $i<sizeof($relacionados); $i++){ 
	if($relacionados[$i][2] == ''|| $relacionados[$i][2] == 'NULL'){
			$grilla = $url_base."img/sinImagenGrilla.jpg";
		} else {
			$grilla = $relacionados[$i][2];
		}
	?>
    <div class="grillaProducto">
        <div class="intGrilla">
            <a href="<?php echo $url_base.'ficha/'.$relacionados[$i][0].'/'.url_amigables($relacionados[$i][1]); ?>" class="contImgGrilla">
                <?php echo ofertaGrilla($relacionados[$i][0]); ?>
                <img src="<?php echo $grilla; ?>" width="100%" alt="<?php echo $relacionados[$i][1]; ?>" />
            </a>
            <a href="<?php echo $url_base.'ficha/'.$relacionados[$i][0].'/'.url_amigables($relacionados[$i][1]); ?>"><?php echo $relacionados[$i][1]; ?></a>
            <a href="<?php echo $url_base.'ficha/'.$relacionados[$i][0].'/'.url_amigables($relacionados[$i][1]); ?>"><?php echo skuGrilla($relacionados[$i][0]); ?></a>
            <a href="<?php echo $url_base.'ficha/'.$relacionados[$i][0].'/'.url_amigables($relacionados[$i][1]); ?>" class="valorGrilla"><?php echo valorGrilla($relacionados[$i][0]); ?></a>
        </div>
    </div><!--fin grillaProducto -->
    <?php } ?>
        
</div><?php */?><!--Fin productosDestacados -->

