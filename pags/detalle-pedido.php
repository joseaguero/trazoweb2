<?php 
  if (!isset($_SESSION['user_account']) OR is_null($_SESSION['user_account'])) {
    echo '<script>location.href="login"</script>';
  }
?>

<?php 
$oc = mysqli_real_escape_string($conexion, $_GET['oc']);
$pedidos = get_pedido($oc);

$tipo_pago = tipo_pago($pedidos[0]['tipo_pago'], $pedidos[0]['cuotas']);

?>

<div class="filaCategorias ctndr100">
    <div class="breadCrumbs">
        <a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Mi cuenta</a>
    </div>
</div>

<div class="cont100">
    <div class="container">
        <div class="titulo2 hidden-dash title-dash"><span>Mi cuenta</span></div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="cont100 datosCliente">
              <div class="back-dash"><a href="dashboard"><img src="images/back-dash.png"> Volver</a></div>
            	<h3 class="subtitulo">Detalle pedido Nº: <?= $oc ?></h3>
              
              <?php $i = 0; foreach ($pedidos as $item): ?>
                <div class="row_pedidos">
                  <div class="head_pedidos">
                    <div class="col">
                      <span class="title">Orden de compra</span>
                      <span><?= $item['oc'] ?></span>
                    </div>
                    <div class="col">
                      <span class="title">Fecha del pedido</span>
                      <span><?= $item['fecha'] ?></span>
                    </div>  
                    <div class="col">
                      <span class="title">Fecha entrega</span>
                      <span><?= $item['fecha_entrega'] ?></span>
                    </div>
                    <div class="col">
                      <span class="title fleft">Total pagado</span>
                      <span>$<?= number_format($item['total_pagado'], 0, ',', '.') ?></span>
                      <a href="mis-pedidos" class="v_details">Volver</a>
                    </div>
                  </div>
                  <div class="grid_info">
                    <div class="col">
                      <div class="title"><?= ($item['retiro_tienda'] == 1) ? 'Retiro en tienda' : 'Despacho' ?></div>
                      <div class="row"><?= $item['direccion'] ?>, <?= $item['comuna'] ?>, <?= $item['region'] ?></div>
                    </div>
                    <div class="col">
                      <div class="title">Pago</div>
                      <div class="row">Medio de pago: Webpay</div>
                      <div class="row">Tipo de transacción: Venta</div>
                      <div class="row">Nº de Cuotas: <?= $tipo_pago['cuota'] ?></div>
                      <div class="row">Tipo de cuota: <?= $tipo_pago['tipo_cuota'] ?></div>
                      <div class="row">Tarjeta terminada en: **** **** **** <?= $item['card_number'] ?></div>
                      <div class="row">Tipo de pago: <?= $tipo_pago['tipo_pago'] ?></div>
                      <div class="row">Fecha y hora de transacción: <?= date("d/m/Y | H:i:s", strtotime($item['fecha_webpay'])) ?></div>
                      <div class="row">Valor pagado: $<?= number_format($item['total_pagado'], 0, ',', '.') ?></div>
                      <div class="row">Código de autorización: <?= $item['codigo_auth'] ?></div>
                      <div class="row">Tipo de moneda: CLP / Peso chileno</div>
                    </div>
                    <div class="col">
                      <div class="title">Totales</div>
                      <div class="row">Subtotal <span>$<?= number_format($item['total'], 0, ',', '.') ?></span></div>
                      <div class="row">Descuentos <span>$<?= number_format($item['descuento'], 0, ',', '.') ?></span></div>
                      <div class="row">Envío <span>$<?= number_format($item['valor_despacho'], 0, ',', '.') ?></span></div>
                      <div class="row">Total cancelado <span>$<?= number_format($item['total_pagado'], 0, ',', '.') ?></span></div>
                    </div>
                  </div>
                  <?php foreach ($pedidos[$i]['productos'] as $aux): ?>
                    <div class="body_pedidos">
                      <div class="col">
                        <img src="<?= $aux['imagen'] ?>">
                      </div>
                      <div class="col">
                        <div class="nombre"><?= $aux['nombre'] ?></div>
                        <div class="sku"><?= $aux['sku'] ?></div>
                        <div class="qty">Cantidad: <?= $aux['cantidad'] ?></div>
                      </div>
                      <div class="col">
                        <div class="nombre">VALOR PAGADO</div>
                        <div class="sku">$<?= number_format($aux['total'], 0, ',', '.') ?></div>
                      </div>
                    </div>
                  <?php endforeach ?>
                </div>
              <?php $i++; endforeach ?>

            </div>
            
            <div class="cont100 direcciones">
            </div>
        
        </div><!--fin contenidoMiCuenta-->
               
    </div>
</div>
