<?php 
	require_once 'paginador/paginator.class.php';

    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 0;
	$ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 15;
	
	$idCat = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
	$nombreCat = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;
	
	$filtro_precio = (isset($_GET[filtro_precio])) ? mysqli_real_escape_string($conexion, $_GET[filtro_precio]) : 0;
	$filtro_material = (isset($_GET[filtro_material])) ? mysqli_real_escape_string($conexion, $_GET[filtro_material]) : 0;
	
	$categoria = consulta_bd("id,nombre","categorias","id=$idCat","");
	
	if($filtro_precio == "0"){
		$orden = 'p.id desc';
		//echo "<h1>".$orden."</h1>";
	} else if($filtro_precio == "precio-desc"){
		$orden = 'valorMenor desc';
		//echo "<h1>".$orden."</h1>";
	} else if($filtro_precio == "precio-asc"){
		$orden = 'valorMenor asc';
		//echo "<h1>".$orden."</h1>";
	}
	//echo "<h1>".$filtro_precio."</h1>";
	
	
	if($filtro_material == 0){
		$terminacion = '';
	} else if($filtro_material == '1'){
		$terminacion = 'valorMenor desc';
	} else if($filtro_material == '2'){
		$terminacion = 'valorMenor asc';
	}
	
	
	$filas = consulta_bd("p.id, MIN(if(pd.descuento > 0, pd.descuento,pd.precio)) as valorMenor","productos p, categorias c, categorias_productos cp, productos_detalles pd","p.publicado=1 and p.id=cp.producto_id and c.id=cp.categoria_id and p.id = pd.producto_id and cp.categoria_id=$idCat GROUP BY( p.id)","$orden");
    $total=mysqli_affected_rows($conexion);
	
	$pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = $ipp; 
	$rutaRetorno = "categorias/$idCat/".url_amigables($categoria[0][1])."/".$filtro_precio."/".$filtro_material;
    $pages->paginate($rutaRetorno);

	$campos="p.id, p.nombre, p.thumbs, pd.nombre, pd.id, MIN(if(pd.descuento > 0, pd.descuento,pd.precio)) as valorMenor";
	$tablas="productos p, categorias c, categorias_productos cp, productos_detalles pd";
	$where="p.publicado=1 and p.id=cp.producto_id and c.id=cp.categoria_id and p.id = pd.producto_id and cp.categoria_id=$idCat GROUP BY p.id";
	
	$productos = consulta_bd($campos,$tablas,$where,"$orden $pages->limit");
    $cant_productos = mysqli_affected_rows($conexion);
	
	
?>
<script type="text/javascript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>


<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="javascript:void(0)" class="actual"><?php echo $categoria[0][1]; ?></a>
    </div>
    <div class="filtros">
    	<form id="filtroPrecio">
        	<select name="jumpMenu" id="selectFiltroPrecio" onchange="MM_jumpMenu('parent',this,0)">
            	<option value="<?php echo $url_base; ?>categorias/<?php echo $idCat.'/'.url_amigables($categoria[0][1]); ?>/0/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?php if($filtro_precio == 0){echo 'selected="selected"';}?>>Filtrar por precio</option>
                <option value="<?php echo $url_base; ?>categorias/<?php echo $idCat.'/'.url_amigables($categoria[0][1]); ?>/precio-desc/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?php if($filtro_precio == 'precio-desc'){echo 'selected="selected"';}?>>Precio mas alto</option>
                <option value="<?php echo $url_base; ?>categorias/<?php echo $idCat.'/'.url_amigables($categoria[0][1]); ?>/precio-asc/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?php if($filtro_precio == 'precio-asc'){echo 'selected="selected"';}?>>Precio mas bajo</option>
            </select>
        </form>
        <!--<form id="filtroMaterialidad">
        	<select name="materialidad" id="selectFiltroMaterialidad">
            	<option value="" selected="selected">Filtrar por materialidad</option>
                <option value="">Precio mas bajo</option>
            </select>
        </form>-->
        
      
    </div>
    <div class="titulo">
    	<h2><?php echo $categoria[0][1]; ?></h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <?php for($i=0; $i<sizeof($productos); $i++){ 
	if($productos[$i][2] == ''|| $productos[$i][2] == 'NULL'){
			$grilla = $url_base."img/sinImagenGrilla.jpg";
		} else {
			$grilla = $productos[$i][2];
		}
	?>
    <div class="grillaProducto">
        <div class="intGrilla">
            <a href="<?php echo $url_base.'ficha/'.$productos[$i][0].'/'.url_amigables($productos[$i][1]); ?>" class="contImgGrilla">
                <?php echo ofertaGrilla($productos[$i][0]); ?>
                <img src="<?php echo $grilla; ?>" width="100%" alt="<?php echo $productos[$i][1]; ?>" />
            </a>
            <a href="<?php echo $url_base.'ficha/'.$productos[$i][0].'/'.url_amigables($productos[$i][1]); ?>"><?php echo $productos[$i][1]; ?></a>
            <a href="<?php echo $url_base.'ficha/'.$productos[$i][0].'/'.url_amigables($productos[$i][1]); ?>"><?php echo skuGrilla($productos[$i][0]); ?></a>
            <a href="<?php echo $url_base.'ficha/'.$productos[$i][0].'/'.url_amigables($productos[$i][1]); ?>" class="valorGrilla"><?php echo valorGrilla($productos[$i][0]); ?></a>
        </div>
    </div><!--fin grillaProducto -->
    <?php } ?>
    
    
    
</div><!--Fin productosDestacados -->


<div class="contBtnMas ctndr100">
	<!--<a href="javascript:void(0)">cargar más +</a>-->
    <?php
		echo $pages->display_pages();
	?>
</div>