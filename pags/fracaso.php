<?php
    unset($_SESSION['ocRechazo']);
	//$oc = ($_POST) ? $_POST[oc] : $_GET[oc];
    $oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
    $actionoc = (isset($_GET[actionoc])) ? mysqli_real_escape_string($conexion, $_GET[actionoc]) : 0;
	
    $PA = consulta_bd("id, estado_id, oc","pedidos","oc='$oc'","");
	$cant = mysqli_affected_rows($conexion);
	if($oc == 'anulada'){
        update_bd("pedidos","estado_id = 3","oc='$oc'");
    } else if ($cant == 0){
		echo '<script>parent.location = "'.$url_base.'404";</script>';
	} else {
		update_bd("pedidos","estado_id = 3","oc='$oc' and payment_type_code IS NULL");
	}
	
?>

<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a>  <span>&gt;</span> <a href="javascript:void(0)" class="actual">Fracaso</a>
    </div>
</div>

<div class="ctndr100 compraFianl">
	<h2 class="titulos_interiores" style="position:relative;">FRACASO EN LA COMPRA</h2><!--fin titulos_interiores -->

	<div class="top-identificacion" style="margin-bottom:20px;">
        <div>Transacción Rechazada (<span style="font-size:16px; color:#9b2534;"><?php echo $oc; ?></span>)<br />
        Las posibles causas de este rechazo son:
    </div>
    
        <div class="txt-exito"> - Error en el ingreso de los datos de su tarjeta de crédito o Debito (fecha y/o código de seguridad).</div>
        <div class="txt-exito"> - Su tarjeta de crédito o debito no cuenta con el cupo necesario para cancelar la compra.</div>
        <div class="txt-exito"> -Tarjeta aún no habilitada en el sistema financiero. </div>
        
    </div>
    
    <div class="cont-btns4" style="float:left; width:100%;">
        <div class="btns-paso4">
            <a href="<?php echo $url_base; ?>home">SEGUIR EN EL SITIO</a>
        </div>
    </div>

</div>


<script type="text/javascript">
	ga('ec:setAction','checkout', {'step': 4});
</script>