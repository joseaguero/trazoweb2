<?php 
    $home = consulta_bd("titulo, link, titulo_boton, imagen, texto","home","publicado=1",""); 

if (is_array($home)) { ?>
    <div class="paralax ctndr100 parallax-window" data-parallax="scroll" data-image-src="imagenes/home/<?php echo $home[0][3]; ?>">
        <div class="infoParalax">
            <h2><?php echo $home[0][0]; ?></h2>
            <div>
                <?php echo $home[0][4]; ?>
            </div>
            <?php if ($home[0][1] != ''){ ?>
            <p>
                <a href="<?php echo $home[0][1]; ?>"><?php echo $home[0][2]; ?></a>
            </p>
            <?php } ?>
        </div><!--Fin infoParalax -->
    </div>
<?php } ?>