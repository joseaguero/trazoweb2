<?php 
  if (!isset($_SESSION['user_account']) OR is_null($_SESSION['user_account'])) {
    echo '<script>location.href="login"</script>';
  }
?>

<div class="filaCategorias ctndr100">
    <div class="breadCrumbs">
        <a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Mi cuenta</a>
    </div>
</div>

<div class="cont100">
    <div class="container">
        <div class="titulo2 hidden-dash title-dash"><span>Mi cuenta</span></div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="cont100 datosCliente">
              <div class="back-dash"><a href="dashboard"><img src="images/back-dash.png"> Volver</a></div>
            	<h3 class="subtitulo">Mis direcciones <a href="javascript:void(0)" id="nuevaDireccion" class="nuevaDireccion">agregar nueva direccion</a></h3>
              
              <?php $direcciones = consulta_bd('cd.nombre, r.nombre, ci.nombre, c.nombre, l.nombre, cd.calle, cd.id', 'clientes_direcciones cd JOIN regiones r ON r.id = cd.region_id JOIN ciudades ci ON ci.id = cd.ciudad_id JOIN comunas c ON c.id = cd.comuna_id JOIN localidades l ON l.id = cd.localidad_id', "cd.cliente_id = {$_SESSION[user_account]}", 'cd.id desc'); 

              ?>

              <div class="content_address">
                <?php foreach ($direcciones as $item): ?>
                  <div class="col">
                    <span class="titulo_dir"><?= $item[0] ?></span>
                    <span class="dir_dir"><?= $item[3] ?>, <?= $item[1] ?></span>

                    <div class="action_dir">
                      <a href="javascript:void(0)" class="edit_address" data-id="<?= $item[6] ?>">Editar</a> | <a href="javascript:void(0)" class="delete_address" data-id="<?= $item[6] ?>">Eliminar</a>
                    </div>
                  </div>
                <?php endforeach ?>
              </div>
            </div>
            
            <div class="cont100 direcciones">
            </div>
        
        </div><!--fin contenidoMiCuenta-->
               
    </div>
</div>

<div class="bg_change_password"></div>
<div class="content_change">
  <h2>Agregar nueva dirección</h2>
  <div class="form_group">
    <label for="password_actual">Nombre asignado</label>
    <input type="text" name="nombre">
  </div>

  <div class="form_group">
    <?php $regiones = consulta_bd("id, nombre","regiones","","id desc");?>
    <label for="password_actual">Región</label>
    <select name="region" id="region" class="region_select">
      <option value="0">Seleccionar región</option>
      <?php for($i=0; $i<sizeof($regiones); $i++){ 
          if($regiones[$i][0] > 0){?>
      <option <?php if($regiones[$i][0] == $region_id){echo 'selected="selected"';} ?> value="<?php echo $regiones[$i][0]; ?>"><?php echo $regiones[$i][1]; ?></option>
      <?php } } ?>
    </select>
  </div>

  <div class="form_group">
    <label for="password_actual">Comuna</label>
    <select name="comuna" id="comuna">
      <option value="0">Seleccionar comuna</option>
    </select>
  </div>

  <div class="form_group">
    <label for="localidad" class="lbLocalidad">Localidad</label>
    <select name="localidad" id="localidad">
        <option value="0">Localidades</option>
    </select>
  </div>

  <div class="form_group">
    <label for="direccion">Dirección</label>
    <input type="text" name="direccion">
  </div>

  <div class="form_group">
    <a href="javascript:void(0)" class="btn-address" data-id="<?= $_SESSION['user_account'] ?>" data-action="create" data-dir="0">Agregar</a>
  </div>
</div>

<?php if (isset($_SESSION['alert_message'])): ?>
  <?php if ($_SESSION['alert_message']['status'] == 'error'): ?>
    <script type="text/javascript">
      alertify.error("<?= $_SESSION['alert_message']['message'] ?>");
    </script>
  <?php else: ?>
    <script type="text/javascript">
      alertify.success("<?= $_SESSION['alert_message']['message'] ?>");
    </script>
  <?php endif ?>
<?php 
unset($_SESSION['alert_message']);
endif ?>
