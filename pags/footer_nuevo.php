<div class="ctndr100">
    
    <footer>
        <div class="content_foot">
            <div class="filaF">
                <div class="colFooter1">
                    <h3>Trazo</h3>
                    <ul>
                        <li><a href="quienes-somos">Quiénes somos</a></li>
                        <li><a href="terminos-y-condiciones">Términos y condiciones</a></li>
                    </ul>
    
                    <h3 class="siguenos">Síguenos</h3>
                    <a href="https://www.instagram.com/tiendaforastero/" target="_blank" class="icon-newsletter" style="margin-right: 10px;"><img src="img/logo_insta.png" width="31"></a>
                    <a href="https://www.facebook.com/tiendaforastero/" class="icon-newsletter" target="_blank"><img src="img/logo_face.png"></a>
                </div><!-- AMOBLE -->
    
                <div class="colFooter2">
                    <h3>Ayuda</h3>
                    <ul>
                        <?php echo ($isCyber) ? '<li><a href="cyberday">CYBERDAY</a></li>' : ''; ?>
                        <li><a href="cambios-y-devoluciones">Cambios y devoluciones</a></li>
                        <li><a href="envios">Métodos y costos de envío</a></li>
                        <li><a href="asegura-tu-despacho">MIDE: Asegura tu despacho</a></li>
                        <li><a href="retiro-en-tienda">Retiro en tiendas</a></li>
                        <li><a href="preguntas-frecuentes">Preguntas frecuentes</a></li>
                    </ul>
                </div><!-- Te ayudamos -->
            </div>

			<div class="filaF filaF2">
                <div class="colFooter3">
                    <h3>Contacto</h3>
                    <p>
                        Av. Juan XXIII 6190, Vitacura <br/>
                        (+56 2) 2219 1224 <br/>
                        Lunes a Viernes 10:00 a 20:00 hrs. <br/>
                        Sábado 10:00 a 19:00 hrs. <br/>
                        contacto@forastero.life <br/>
                    </p>    
                </div><!-- Contacto -->
    
                <div class="colFooter4">
                    <form>
                        <input type="text" name="newsletter" class="campoNewsletter" placeholder="Ingresa tu correo para recibir nuestras novedades" />
                        <a href="javascript:void(0)" class="btnNewsletter">SUSCRÍBETE</a>
                    </form>
                    <div class="sello_wp">
                        <div class="contImgFooter1">
                            <img src="img/ssl_logo.png" width="100%" />
                        </div>
                        <div class="contImgFooter3">
                            <img src="img/webpay.png" width="100%" class="" />
                        </div>
                        
                    </div>
                    <?php echo ($isCyber) ? '<img src="img/cyberdaylogo.jpg" class="logocyberfooter">' : '';?>                
                </div><!-- Neswletter -->
            </div>
        </div>
    </footer>
    <!-- <div class="filaMenuFooter">
            <div class="menuFooterIzquierdo">
                <a href="<?php echo $url_base; ?>nuestra-tienda">TIENDA</a>
                <a href="<?php echo $url_base; ?>terminos-y-condiciones">TÉRMINOS Y CONDICIONES</a>
                <a href="<?php echo $url_base; ?>politicas-de-envio">POLÍTICAS DE ENVÍO</a>
                <a href="<?php echo $url_base; ?>cambios-y-devoluciones">CAMBIOS Y DEVOLUCIONES</a>
                <a href="">CÓMO COMPRAR</a>
            </div>
            <div class="menuFooterDerecho">
                <a class="telefonoFooter" href="tel:+56945732364"> (+56 2) 2341 5075</a>
                <a href="https://goo.gl/maps/JcvZAjLrGCK2" target="_blank">Av.Italia 1372, Providencia </a><br>
                 <a href="javascript:void(0)" target="_blank" class="altura">Horarios: Lunes a Viernes de 10:30 a 19:30 hrs., Sábado 10:30 a 19:00 hrs.</a><br>
    
                 <div style="clear:both"></div>
                <a class="telefonoFooter" href="tel:+56945732364"> (+56 2) 2905 6095 </a>
                <a href="https://goo.gl/maps/JcvZAjLrGCK2" target="_blank">Av.Juan XXIII 6152, Vitacura </a><br>
                <a href="javascript:void(0)" target="_blank">Horarios: Lunes a Viernes de 10:30 a 20:00 hrs., Sábado 10:30 a 16:30 hrs.</a>
            </div>
        </div>Fin filaMenuFooter
        <div class="filaWebpay ctndr100"><img src="img/iconoWebpayFooter.jpg" width="161" /></div> -->
        <!--<div class="filaFirma ctndr100">
            <span>Todos los derechos reservados. FORASTERO, 2016</span> 
            <a href="http://www.moldeable.com" target="_blank">
                <img src="img/firma.jpg" width="26" />
            </a>
        </div> -->
</div>