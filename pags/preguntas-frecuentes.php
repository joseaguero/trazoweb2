<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Preguntas frecuentes</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>Preguntas frecuentes</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">
		<h4>1. ¿Despachan a todo Chile?</h4>
		<p>Sí, ¡despachamos a todo Chile! Actualmente trabajamos con 3 empresas
de despacho: Ecocargo (Sur), Aedo Cargo (Norte), Pullman Cargo
(Sur). Estos Couriers se asignan dependiendo de tu comuna y región. Ellos
retiran en nuestra bodega central y se encargan de dejarlo en la puerta de
tu casa.</p>

		<h4>2. ¿Cuáles son los costos de envío?</h4>
		<p>Los costos de envío para regiones varían dependiendo del producto y la
región en la cual te encuentras. Dependerá de la localidad u de las
dimensiones de los productos que compres.</p>
<p>En la Región Metropolitana el costo de envío es fijo, independiente de la
cantidad de productos que haya en el pedido. Los productos pequeños
tienen un costo de envío de $10.000 y los de gran tamaño de $20.000. En
caso de comprar varios productos, se cobrará sólo por un producto, el que
tenga mayor costo de despacho.</p>

<h4>3. ¿Cuál es el horario de entrega?</h4>
<p>El despacho es dentro del día acordado, sin especificación. El transporte
llama aproximadamente una hora antes de llegar. Si no se logra poner en
contacto vía teléfono, se le manda un mail, y se va a dejar el pedido a su
dirección.</p>
<p>Los plazos de entrega dependen de la región de destino, y varían entre 4 y
15 días hábiles, a excepción de Coyhaique y Punta Arenas, que
demora 25 días hábiles. Las entregas se realizan de lunes a viernes
desde 9:00 a 17:00 horas, y éstas pueden realizarse a terceros (familiares o
conserjes).</p>
<p>Es responsabilidad del cliente proveer una dirección en la que haya gente
para recibir el o los paquetes en el horario de entrega. Las demoras
causadas por la ausencia del cliente al momento de la entrega: información
de registro incorrecta, la negativa del cliente para aceptar el envío y el
cambio de domicilio, no son responsabilidad de Forastero.</p>
<p>De no encontrarse nadie en la dirección para recibir el producto este volverá
a nuestro centro de distribución y se coordinará para organizar y pagar
nuevamente el servicio de despacho. Para coordinar estas opciones debe
escribir a contacto@forastero.life o clientes@forastero.life</p>
<h4>4. ¿El despacho incluye la instalación de los productos?</h4>
<p>Sí, de venir piezas desarmadas, se instalan a menos de que el cliente pida
lo contrario (Ej: sofás, comedores), siempre y cuando, los productos entren
por el ascensor (si es que es un departamento). Además los transportistas
retiran todos los escombros y cartones del producto entregado. El cliente
tiene la responsabilidad de tener el lugar despejado para la instalación del
producto.</p>
<h4>5. Quiero agregar un mensaje a mi regalo</h4>
<p>¡No hay problema! Puedes dejar un mensaje que irá impreso en una tarjeta
especial, sólo recuerda escribirlo al momento de seleccionar que tu pedido
es para regalo.</p>
<h4>6. ¿Es seguro comprar en Forastero.life?</h4>
<p>¡Totalmente! En Forastero trabajamos con altos estándares de seguridad y
toda la información ingresada en nuestro sitio se mantiene de forma
estrictamente confidencial. Sólo te pedimos que ingreses la información
necesaria para el pago y envío de tu compra. Forastero NO guarda la
información de tu tarjeta de crédito ni de tu cuenta bancaria.</p>
<h4>7. ¿Qué medio de pago puedo usar?</h4>
<p>Puedes pagar con tarjetas de crédito y débito emitidas en Chile y/o
transferencia electrónica a nuestra cuenta.</p>
<h4>8. ¿Puedo pedir Boleta o Factura?</h4>
<p>Sí, se puede pedir boleta o factura, tienes que especificarlo al momento de
pagar.</p>
<h4>9. Me equivoqué en mi dirección, ¿lo puedo arreglar?</h4>
<p>Te recomendamos prestar mucha atención al escribirla y revisarla muy bien
antes de continuar con la compra. Si tuviste un error, escríbenos a
clientes@forastero.life lo antes posible, informando el error y tu número de
orden, así lo podemos arreglar antes de que sea despachado.</p>
<h4>10. ¿Qué días despachamos?</h4>
<p>Todos nuestros despachos son de Lunes a Viernes de 09:00 a 17:00.
Cualquier urgencia o atraso lo comunicaremos siempre a través de correo
electrónico o teléfono. El transporte encargado de entregar tu pedido le
llamará aproximadamente 1 hora antes de la entrega. Los días Martes se
entrega en sector Colina - Lampa, los días jueves en sector La Reina -
Peñalolén.</p>
<h4>11. Nadie puede recibir por mí, ¿Puedo comprar y retirar en alguna parte?</h4>
<p>Sí, ¡no hay problema! Elige RETIRO EN TIENDA en el método de envío.
Especifica en qué tienda retirarás y espera a la fecha que te indiquemos
para pasar a buscar tu producto.</p>
<h4>12. Ya pasó el plazo de entrega de mi compra y no ha llegado.</h4>
<p>Escríbenos con total confianza a nuestro correo clientes@forastero.life,
indicando tu número de orden. Revisaremos lo antes posible qué pasó con
tu compra y te contactaremos de vuelta.</p>
<h4>13. CAMBIOS, DEVOLUCIONES Y GARANTÍAS</h4>
<p>¿Necesitas realizar un cambio o solicitar la devolución de tu compra? Visita
la página de CAMBIOS Y DEVOLUCIONES para revisar el procedimiento.</p>
	</div><!--fin contTextosFijos -->
</div><!--Fin productosDestacados -->

