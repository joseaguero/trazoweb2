<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Métodos y costos de envío</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>Métodos y costos de envío</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">
        <p>Estamos en condiciones de entregar el producto en nuestra tienda o realizamos
repartos a domicilio a lo largo de Chile a través de empresas de transporte propias o
externas, dependiendo de la localidad.</p>
<p>Compra Online Retira en Tienda:</p>
<p>Si elige retirar el producto en una tienda, podrá hacerlo en el horario en que ésta se
encuentre abierta al público.</p>
<p>Los retiros deberán ser hechos dentro de un plazo determinado, el cual deberá ser
debidamente indicado en cada una de las opciones ofrecidas. Por medio del correo
electrónico registrado o vía llamado telefónico, se informará que el producto está a
disposición y el plazo para ser retirado. Los comprobantes de esta gestión quedarán
disponibles durante 30 días. Si el producto no fuera retirado dentro del plazo
especificado, la transacción quedará sin efecto, haciéndose la devolución de la cantidad
pagada.</p>
<p>Despacho de Productos a Domicilio:</p>
<p>Realizamos repartos a domicilio a lo largo de Chile a través de empresas de transporte
propias o externas, dependiendo de la localidad. El costo del despacho dependerá del
tamaño del producto, ciudad, comuna e incluso localidad (por ejemplo, la localidad de
Farellones que es de la comuna de Lo Barnechea).</p>
<p>En caso de comprar dos o más productos, se realizará un solo despacho con la totalidad
de la orden considerando como fecha de entrega el producto con mayores días de
fabricación o tránsito.</p>
<p>Nuestros despachos se realizan de Lunes a Viernes en horario hábil y en caso de que el
cliente lo acuerde con nuestro equipo, podemos realizar despachos el día sábado
pagando un cargo extra.</p>
<p>De no encontrarse nadie en la dirección para recibir el producto este volverá a nuestro
centro de distribución y se coordinará para organizar y pagar nuevamente el servicio de
despacho. Para coordinar estas opciones debe escribir a contacto@trazo.cl o
clientes@trazo.cl</p>
<p>Para la entrega de productos se considera que estos quepan en el ascensor y/o por los
accesos principales como puertas y pasillos. Si el tamaño del producto impide que la
entrega se haga en el lugar estipulado por el cliente, éste deberá aceptar la recepción
conforme en otro lugar del domicilio que lo permita. En caso de entregas en
departamentos y que el producto no entre en el ascensor, se podrán subir por la
escalera con un costo adicional que se acordará con el personal de la tienda
contacto@trazo.cl o clientes@trazo.cl, y variará según el número de pisos y
complejidad de la maniobra. En caso de que ese trabajo adicional lo realice un equipo
interno o una empresa especializada en este tipo de trabajos, serán responsabilidad y a
cargo del cliente.</p>

</div><!--fin contTextosFijos -->
</div><!--Fin productosDestacados -->

