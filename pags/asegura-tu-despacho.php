<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">MIDE: Asegura tu despacho</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>MIDE: Asegura tu despacho</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">
<p>Para la entrega de productos se considera que estos quepan en el ascensor y/o por los
accesos principales como puertas y pasillos. Si el tamaño del producto impide que la
entrega se haga en el lugar estipulado por el cliente, éste deberá aceptar la recepción
conforme en otro lugar del domicilio que lo permita.</p>
<p>En caso de entregas en departamentos y que el producto no entre en el ascensor, se
podrán subir por la escalera con un costo adicional que se acordará con el personal de la
tienda contacto@trazo.cl o clientes@trazo.cl, y variará según el número de
pisos y complejidad de la maniobra. En caso de que ese trabajo adicional lo realice un
equipo interno o una empresa especializada en este tipo de trabajos, serán
responsabilidad y a cargo del cliente.</p>
<p>Las puertas estrechas, los pasillos angostos y las escaleras pueden generar un problema
al coordinar la entregan de muebles. Asegúrate de que lo que compraste entre sin
problemas en tu casa antes de realizar su pedido. Aquí te ayudamos a hacerlo:</p>
<blockquote>
	1. ¿Cómo escoger el mueble adecuado? <br>
	<p>Asegúrate de que el mueble quepa en tu espacio, las dimensiones publicadas en
nuestra página web te ayudarán a descubrirlo.</p>
	<p>Para poder visualizarlo, mapea el área dónde iría el mueble, usando masking tape
para suelos lisos, y pedazos de papel en alfombras.</p>
	2. ¿Cómo medir tu casa? <br>
	<p>Es importante asegurarse de que el producto pase por todas las entradas antes de
realizar tu pedido. </p>
	<p>Mide la altura (A) y el ancho (B) de todas sus puertas y pasillos.</p>
	<p>Determina el mejor camino desde el exterior de tu casa a donde instalarás el
producto.</p>
<img src="img/despacho/img_1.png" class="img_pagestatica" />
	<p>TOMA NOTA: Al medir, ten en cuenta accesorios, molduras decorativas, paredes interiores, escaleras y ascensores, ya que pueden ser un obstáculo.</p>
	<p>3. ¿Cómo medir el mueble?</p>
	<p>Localiza el ancho (W), la profundidad (D) y la altura (H) del producto</p>
	<p>Si es un sofá o silla, también ten en cuenta la profundidad diagonal (DD), que determinará si el producto puede entrar en ángulo a través de accesos más estrechos.</p>
	<img src="img/despacho/img_2.png" class="img_pagestatica">
	<p>TOMA NOTA: Los sofás seccionales se entregan en piezas individuales.</p>
	<p>4. ¿Cómo asegurarse de que entre en tu casa?</p>
	<p>PARA SOFÁS + SILLAS</p>
	<p>El ancho de la entrada (B) debe superar al menos una de las siguientes medidas: profundidad del producto (D), profundidad diagonal (DD) o altura (H).</p>
	<img src="img/despacho/img_3.png" class="img_pagestatica2">
	<p>Si la longitud total de separación de muros (C) excede el ancho del producto (W), entonces puedes moverlo horizontalmente.</p>
	<img src="img/despacho/img_4.png" class="img_pagestatica2">
	<p>Si la longitud total de separación de muros (C) es menor que el ancho del producto (W), entonces tendrá que intentar moverlo verticalmente, lo que significa que la altura de entrada (A) debe exceder el ancho del producto (W).</p>
	<img src="img/despacho/img_5.png" class="img_pagestatica2">
	<p>ATENCIÓN: El producto no entrará en tu casa si ocurre alguna de estas situaciones:</p>
	<p>a) El ancho del producto (W) excede tanto a la longitud total de separación de muros (C) como el de altura (A).</p>
	<p>b) Si el ancho de la entrada (B) es menor que las tres medidas siguientes: profundidad del producto (D), profundidad diagonal (DD) y altura (H).</p>
	<p>PARA VITRINAS + REPISAS</p>
	<p>El ancho de la entrada (B) debe exceder la profundidad de la pieza (D).</p>
	<img src="img/despacho/img_6.png" class="img_pagestatica2">
	<p>Si la longitud total de separación de muros (C) excede la altura de la pieza (H), entonces usted puede moverlo horizontalmente.</p>
	<img src="img/despacho/img_7.png" class="img_pagestatica2">
	<p>Si la longitud total de separación de muros (C) es menor que la altura del producto (H), entonces tendrás que intentar moverlo verticalmente, lo que significa que la altura de la entrada (A) debe exceder la altura del producto (H).</p>
	<img src="img/despacho/img_8.png" class="img_pagestatica2">
	<p>ATENCIÓN: El producto no entrará en tu casa si la altura de este (H) excede tanto la longitud total de separación de muros (C), como la altura de la puerta (A).</p>
</blockquote>

</div><!--fin contTextosFijos -->
</div><!--Fin productosDestacados -->

