<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Cambios y devoluciones</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>Cambios y devoluciones</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">
        <p>La compra de cualquier producto para tu casa es una compra importante y poco
frecuente. Queremos que tengas la tranquilidad que haremos todo lo posible para que
quedes conforme con tu compra y, en caso de que esto no ocurra, amparados en los
plazos y condiciones de la Ley del Consumidor, podrás realizar la devolución de los
productos adquiridos.</p>
        <p>Para realizar un cambio o devolución de un producto adquirido a través de la página
web de Trazo, debes ir a la tienda con su respectiva boleta con el
producto sin uso, en perfectas condiciones y con su embalaje original hasta 30 días
después de la fecha en que este haya sido recibido. En caso de que el producto haya
sido entregado en otro lugar, sólo te cobraremos el mismo monto del despacho de ida
para retirarlo. En ese caso nuestro equipo se reserva el derecho de evaluar su condición
al momento de revisar el producto.</p>

<p>Adicionalmente realizaremos el cambio del producto cuando:</p>
<blockquote>
    • El producto comprado llega dañado o incompleto, o con deficiencias en su fabricación,
materiales, partes, y estructura se realizará el cambio de manera inmediata sin otro
cobro de despacho. <br>
    • El producto recibido no corresponde al producto comprado o no tiene las
características técnicas mencionadas en la página.
</blockquote>

<p>Si el producto presenta fallas dentro de los 6 primeros meses a la fecha de realizada la
compra del producto, se puede solicitar la visita de un técnico para una reparación,
restitución o su devolución, siempre que el deterioro no sea imputable al cliente.
Trazo utiliza maderas recicladas en algunos de sus productos y otros son productos
únicos de fabricación artesanal no en serie lo cual genera variación entre el producto
recibido y la exhibición en tienda o la foto referencial de la página web.</p>

<p>En caso de haber realizado una compra de un producto de segunda selección, usado,
abierto o con alguna deficiencia, a precio rebajado, no realizaremos cambios ni
devoluciones.</p>

<p>En caso de que tu compra haya sido un producto rústico en el cual maderas recicladas
son utilizadas en su fabricación, no procederá el cambio ni la devolución del producto.
De la misma manera, si el producto comprado era una exhibición de nuestra tienda, de
segunda selección, con algún desperfecto, usado.</p>
        
    </div><!--fin contTextosFijos -->
    
</div><!--Fin productosDestacados -->

