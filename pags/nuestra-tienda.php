<?php
    $tienda = consulta_bd("titulo, txt1, txt2","tiendas","id=1","");
    $imgTienda = consulta_bd("archivo","img_tiendas","tienda_id=1","posicion asc");
?>
<div class="slider2 ctndr100 cycle-slideshow">
    <?php for($i=0; $i<sizeof($imgTienda); $i++){ ?>
    <img src="imagenes/tiendas/<?= $imgTienda[$i][0]; ?>" width="100%" />
    <?php } ?>
</div><!--Fin slider -->

<div class="columnaAbsolutaLeft">
	<div class="cuadroTexto1">
    	<?= $tienda[0][2]; ?>
    </div>
    <div class="mapaTienda">
    	<img src="img/mapaTienda.jpg" width="100%" />
    </div>
</div><!--Fin columnaAbsolutaLeft -->


<div class="columnaAbsolutaRight">
	<div class="colTxtGris">
    	<h4><?= $tienda[0][0]; ?></h4>
        <?= $tienda[0][1]; ?>
    </div>
    <div class="formularioTienda">
    	<h5>¿TIENES ALGUNA CONSULTA?</h5>
        <form method="post" action="mailsender.php">
        	<label for="Nombre">Nombre y Apellidos</label>
            <input type="text" name="Nombre" value="" class="campoInterior" />
            
            <label for="Email">Email</label>
            <input type="text" name="Email" value="" class="campoInterior" />
            
            <label for="Mensaje">Mensaje</label>
            <textarea name="Mensaje" class="campoInterior2"></textarea>
            <input type="hidden" name="validador" value="" />
            <input type="submit" name="enviar" value="ENVIAR" class="btnForm" />
            
        </form>
    </div>
</div><!--Fin columnaAbsolutaRight -->