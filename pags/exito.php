<?php
	$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
	$PA = consulta_bd("id, estado_id, oc","pedidos","oc='$oc'","");
	$cant = mysqli_affected_rows($conexion);
	if($cant > 0 and $PA[0][1] != 2){
		echo '<script>parent.location = "'.$url_base.'fracaso?oc='.$oc.'";</script>';
	} else if($oc=="" || $cant == 0){
	    echo '<script>parent.location = "'.$url_base.'404";</script>';
 	}else{

		unset($_SESSION['cart_alfa_cm']); //elimino la sesion del carro de compras
		$campos = "id,nombre,direccion,email,telefono,";//0-4
		$campos.= "regalo,total,valor_despacho,transaction_date,";//5-8
		$campos.= "amount, card_number,shares_number,authorization_code,payment_type_code,cliente_id,";//9-14
		$campos.= "comuna,region,ciudad,estado_id,";//15-18
		$campos.= "factura, direccion_factura, giro, email_factura, rut_factura, fecha_modificacion, region,ciudad,estado_id, rut, retiro_en_tienda,fecha_despacho";//19-30

		$pedido = consulta_bd($campos,"pedidos","oc='$oc'","");

	 	 //detalle comprador
	     $direccion_cliente = $pedido[0][2];
	     $comuna_cliente    = $pedido[0][15];
	     $region_cliente    = $pedido[0][16];
	     $ciudad_cliente    = $pedido[0][17];
		 $localidad_cliente    = $pedido[0][29];

	     //detalle pedido
		 $pedido_id 	   = $pedido[0][0];
	     $total_pedido 	   = $pedido[0][6];
	     $total_despacho   = $pedido[0][7];
	     $fecha 		   = $pedido[0][8];
	     $hora_pago		   = $pedido[0][9];

	     //detalle transbank
	     $num_tarjeta 	   = $pedido[0][10];
	     $num_cuotas 	   = $pedido[0][11];
	     $cod_aurizacion   = $pedido[0][12];
	     $tipo_pago 	   = $pedido[0][13];

	     //USER
	     $id_usuario	   = $pedido[0][14];
	     //$info = info_user($id_usuario);

	     //validar si esta pagado
	     $validador = $pedido[0][18];
	     $contador_validador = count($validador);

	     //total
   		 $total_productos = $pedido[0][6];
   		 $total_pagado = number_format($total_pedido,0,",",".");

   		 //enviar comprobante
	    
		//funcion tipo pago
   		$tipo_pago = tipo_pago($tipo_pago,$num_cuotas);

   		//detalle productos
   		$detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario","productos_pedidos","pedido_id=".$pedido[0][0],"");

      $ex_despacho = explode('-', $pedido[0][30]);

      if ($pedido[0][29] == 1) {
        $fecha_despacho = calculoFestivos(1, strtotime($pedido[0][30]));
        $explode = explode(" ", $fecha_despacho);
        $fecha_despacho = $explode[2];
      }else{
        $fecha_despacho = date("d/m/Y", strtotime($pedido[0][30]));
      }
 }
?>


<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a>  <span>&gt;</span> <a href="javascript:void(0)" class="actual">Éxito</a>
    </div>
</div>

<div class="ctndr100">
    
    <h2 class="titulos_interiores" style="position:relative;">TRANSACCIÓN EXITOSA</h2><!--fin titulos_interiores -->
    
    
    
    
    <div class="top-identificacion">
        <div>¡MUCHAS GRACIAS POR COMPRAR CON TRAZO!</div>
    
        Su número de orden de compra es:
        <div class="codigo-4"><?php echo $oc; ?></div>
        
        <?php if($pedido[0][29] == 1){?>
                <div class="caja-fecha" rel="<?php echo $pedido[0][29]; ?>">
                  *Fecha estimada de retiro en tienda: <span><?= $fecha_despacho ?></span>
                </div>
        <?php } else { ?>
                <div class="caja-fecha" rel="<?php echo $pedido[0][29]; ?>">
                  *Fecha estimada de envio: <span><?= $fecha_despacho ?></span>
                </div>
        <?php  } ?>
        
    </div>
    
    <div class="cont-btns4">
        <div class="btns-paso4">
            <a href="tienda/boucher/boucherCompra.php?oc=<?php echo $oc; ?>" target="_blank">VER BOUCHER DE COMPRA</a>
             <a href="<?php echo $url_base; ?>home">SEGUIR EN EL SITIO</a>
        </div>
        
    </div>
    
    <div class="columna-paso4 alpha">
      <div class="t-paso4">INFORMACIÓN PERSONAL</div>
      <div class="t2-paso4">Nombre:</div>
      <div class="dato-paso4"><?php echo $pedido[0][1] ?></div>
    
      <div class="t2-paso4">Teléfono:</div>
      <div class="dato-paso4"><?php echo $pedido[0][4] ?></div>
    
      <!--<div class="t2-paso4">Rut:</div>
      <div class="dato-paso4">11.234.234-5</div> -->
    
      <div class="t2-paso4">Email:</div>
      <div class="dato-paso4"><?php echo $pedido[0][3] ?></div>
    </div>
    
    
    
    <div class="columna-paso4">
      <div class="t-paso4">INFORMACIÓN DE ENVÍO</div>
      <div class="t2-paso4">Dirección;</div>
      <div class="dato-paso4" style="margin-bottom:15px;"><?php echo $direccion_cliente ?>, <?php echo $localidad_cliente; ?></div>
    
      <div class="dato-paso4"><?php echo $comuna_cliente ?>, <?php echo $region_cliente ?>,  <?php echo $ciudad_cliente ?></div>
    
    </div>
    
    
    
    <?php if ($pedido[0][19] == 'boleta'){ ?>
    <div class="columna-paso4">
        <div class="t-paso4">INFORMACIÓN DE FACTURACIÓN</div>
        <div class="t2-paso4">Dirección;</div>
        <div class="dato-paso4" style="margin-bottom:15px;"><?php echo $direccion_cliente ?>, <?php echo $localidad_cliente; ?></div>
        <div class="dato-paso4"><?php echo $comuna_cliente ?>, <?php echo $region_cliente ?>,  <?php echo $ciudad_cliente ?></div>
    </div>
    <?php } else {?>
    
    <div class="columna-paso4">
      <div class="t-paso4">INFORMACIÓN DE FACTURACIÓN</div>
      <div class="t2-paso4">Nombre:</div>
      <div class="dato-paso4"><?php echo $pedido[0][1] ?></div>
    
      <div class="t2-paso4">Direccion:</div>
      <div class="dato-paso4"><?php echo $pedido[0][20] ?>, <?php echo $pedido[0][24] ?></div>
    
      <div class="t2-paso4">Rut:</div>
      <div class="dato-paso4"><?php echo $pedido[0][23] ?></div>
    
    </div>
    <?php } ?>
    
    <div class="caja-resumen">
      <div class="resumen1">INFORMACIÓN DE PAGO</div>
    
      <div class="resumen2">
        <div class="resumen-in1">
          <div>Tipo de transición:</div>
          Venta
        </div>
    
        <div class="resumen-in1">
          <div>N° de Cuotas:</div>
          <?php echo $tipo_pago[cuota]; ?>
        </div>
    
        <div class="resumen-in2">
          <div>Tipo de cuota:</div>
          <?php echo $tipo_pago[tipo_cuota]; ?>
        </div>
      </div><!-- resumen2 -->
    
      <div class="resumen2">
        <div class="resumen-in1">
          <div>Tarjeta terminada en:</div>
          **** **** **** <?php echo $num_tarjeta; ?>
        </div>
    
        <div class="resumen-in1">
          <div>Tipo de pago:</div>
          <?php echo $tipo_pago['tipo_pago']; ?>
        </div>
    
        <div class="resumen-in2">
          <div>Fecha y hora de transacción:</div>
          <?php echo $fecha ?> | <?php echo $hora_pago ?>
        </div>
      </div>
    
      <div class="resumen2">
        <div class="resumen-in1">
          <div>Valor pagado:</div>
          $<?php echo $total_pagado; ?>
        </div>
    
        <div class="resumen-in1">
          <div>Código de autorización de transacción:</div>
          <?php echo $cod_aurizacion; ?>
        </div>
    
        <div class="resumen-in2">
          <div>Tipo de moneda:</div>
          CLP / PESO Chileno
        </div>
      </div>
    
      <div class="resumen3">
        <div class="resumen-in1">
          <div>Nombre/URL Comercio:</div>
          Forastero | www.trazo.cl
        </div>
    
      </div>
    </div><!-- caja-resumen -->
    
    
    
    
    <div class="cont-datos">
        <!--<div class="columna-datos1">
            <div class="titulo-datos">
                <a href="<?php echo $url_base; ?>despachos" style="text-decoration:none;color:#666666;" target="blank">VER CONDICIONES DE ENVÍO</a>
            </div>
        </div>-->
        <div class="columna-datos2">
    
            <div class="titulo-datos">
                <a href="<?php echo $url_base; ?>terminos-y-condiciones" style="text-decoration:none;color:#666666;" target="blank">VER TÉRMINOS Y CONDICIONES</a>
            </div>
        </div>
    </div><!-- cont-datos -->
    
    <?php echo showCartExito($oc); ?>
    <?php /*?><div class="caja-fecha">
      *Fecha estimada de entrega:
      <?php $id_locaclidad = consulta_bd("id","localidades","nombre = '$localidad_cliente'","");
        $localidadFuncion = $id_locaclidad[0][0];
        $regionFuncion = 1;
      ?>
      <span><?php echo fechaDespacho($regionFuncion, $localidadFuncion); ?></span>
    </div>
<?php */?>
</div>
<script type="text/javascript">
     
<?php
for ($i=0; $i <sizeof($detalle_productos) ; $i++) {
		$pro_id = $detalle_productos[$i][0]; 
		$details_pro = consulta_bd("nombre,producto_id,id","productos_detalles","id=$pro_id","");
		$total_item = round($detalle_productos[$i][2])*$detalle_productos[$i][1];
		
		$marca = consulta_bd("m.nombre","productos p, marcas m","p.id=".$details_pro[0][1]." and p.marca_id = m.id","");
		$categoria = consulta_bd("sc.nombre","categorias_productos cp, categorias c, subcategorias sc","cp.producto_id=".$details_pro[0][1]." and cp.categoria_id = c.id and sc.id = cp.subcategoria_id and cp.subcategoria_id <> ''","");
		
?>							
    ga('ec:addProduct', {
      'id': '<?php echo $details_pro[0][2]; ?>',
      'name': '<?php echo $details_pro[0][0]; ?>',
      'category': '<?php echo $categoria[0][0] ?>',
      'brand': '<?php echo $marca[0][0] ?>',
      'variant': '<?php echo $details_pro[$i][0]; ?>',
      'price': <?php echo round($detalle_productos[$i][2]); ?>,
      'quantity': <?php echo $detalle_productos[$i][1]; ?>
    });
<?php } ?>

// Transaction level information is provided via an actionFieldObject.
ga('ec:setAction', 'purchase', {
  'id': '<?php echo $oc; ?>',
  'affiliation': 'Forastero',
  'revenue': <?php echo round($pedido[0][6]); ?>,
  'tax': <?php echo round(($pedido[0][6]/1.19) * 0.19); ?>,
  'shipping': <?php echo $total_despacho; ?>
});


//ga('ec:setAction','checkout', {'step': 4});
</script>