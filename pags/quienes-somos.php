<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Quiénes somos</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>Quiénes somos</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">

        <p>Platón hace muchos años se preguntó quizás la pregunta más importante de todas:
        ¿Cómo debemos vivir? En tiempos donde las agendas están muy apretadas, la presión
        por rendir, el &quot;éxito&quot; y un consumo sin sentido nos han quitado calidad de vida: esa
        capacidad de vivir en el presente y disfrutar con las cosas sencillas, las no planificadas,
        como una buena conversación, una salida a comer, a bailar, ayudar a alguien que lo
        necesita o conocer un lugar nuevo.</p>
        
        <h4>AMBIENTES QUE CONMUEVAN, INSPIREN Y CONECTEN</h4>
        
        <p>Las cosas no tienen valor hasta que se usan y por eso en Forastero, hemos decidido
traer muebles y objetos de decoración que permitan armar ambientes cálidos,
acogedores, sencillos que inspiren y que puedan ser vividos y aprovechados al máximo
por cada uno de nosotros con la familia y amigos.</p>
        <p>Con productos de más de 10 países, hay un esfuerzo por mostrar las capacidades
técnicas de diferentes culturas y así poder mostrar, al menos una parte, de la riqueza de
viajar y ofrecer de manera permanente colecciones exclusivas.</p>
        <p>De estructura en fierro y hormigón a la vista, la tienda Forastero tiene más de 400
metros cuadrados donde se muestran no sólo de muebles, alfombras, lámparas y
objetos de de decoración, sino también muebles de terraza exhibidos durante todo el
año.</p>
<p>Ven a conocernos en Juan XXIII 6190, Vitacura.</p>
    </div><!--fin contTextosFijos -->
    
</div><!--Fin productosDestacados -->

