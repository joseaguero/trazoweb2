<?php 
	if (!isset($_SESSION['user_account']) OR is_null($_SESSION['user_account'])) {
		echo '<script>location.href="login"</script>';
	}
?>

<div class="filaCategorias ctndr100">
    <div class="breadCrumbs">
        <a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Mi cuenta</a>
    </div>
</div>

<div class="cont100">
    <div class="container">
        <div class="titulo2 hidden-dash title-dash"><span>Mi cuenta</span></div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
<?php $cliente = consulta_bd("id, nombre, apellido, rut, telefono, email","clientes","id=".$_SESSION['user_account'],""); ?>        	
            <div class="cont100 datosCliente">
              <div class="back-dash"><a href="dashboard"><img src="images/back-dash.png"> Volver</a></div>
            	<h3 class="subtitulo">Datos personales</h3>
                <form action="ajax/actualizarDatos.php" method="post" id="formularioActualizar">
                  <div class="grid-form">
                    <div class="col">
                        
                       <div class="form_group">
                           <label for="nombre" class="label_dash">Nombre</label>
                           <input type="text" id="nombre" name="nombre" class="campoGrande" value="<?= $cliente[0][1]; ?>" />
                       </div>

                       <div class="form_group">
                           <label for="apellido" class="label_dash">Apellido</label>
                           <input type="text" id="apellido" name="apellido" class="campoGrande" value="<?= $cliente[0][2]; ?>" />
                       </div>

                       <div class="form_group">
                           <label for="rut" class="label_dash">Rut</label>
                           <input type="text" id="Rut" name="rut" class="campoGrande" value="<?= $cliente[0][3]; ?>" />
                       </div>
                       
                       <div style="clear:both"></div>
                       
                    </div><!--iniciar sesion -->
                    
                    <div class="col">
                        
                      <div class="form_group">
                           <label for="telefono" class="label_dash">Teléfono</label>
                           <input type="text" id="telefono" name="telefono" class="campoGrande" value="<?= $cliente[0][4]; ?>" />
                       </div>

                       <div class="form_group">
                           <label for="email" class="label_dash">Email</label>
                           <input type="text" id="email" name="email" class="campoGrande" value="<?= $cliente[0][5]; ?>" />
                       </div>
                      
                      <div class="form_group">
                        <a href="javascript:void(0);" class="btn-change-pass">Cambiar contraseña</a>
                      </div>

                       <div style="clear:both"></div>
                       
                    </div><!--iniciar sesion -->
                  </div>
                  
                  <button id="btnActualizar">Actualizar datos</button>
                    
                </form>
            </div>
            
            <div class="cont100 direcciones">
            </div>
        
        </div><!--fin contenidoMiCuenta-->
               
    </div>
</div>

<div class="bg_change_password"></div>
<div class="content_change">
  <h2>Cambiar contraseña</h2>
  <div class="form_group">
    <label for="password_actual">Contraseña actual</label>
    <input type="password" name="password_actual">
  </div>

  <div class="form_group">
    <label for="password_actual">Nueva contraseña</label>
    <input type="password" name="password_nueva">
  </div>

  <div class="form_group">
    <label for="password_actual">Repetir nueva contraseña</label>
    <input type="password" name="password_nueva_re">
  </div>

  <div class="form_group">
    <a href="javascript:void(0)" class="btn-change-password" data-id="<?= $cliente[0][0] ?>">Cambiar contraseña</a>
  </div>
</div>

<?php if (isset($_SESSION['alert_message'])): ?>
  <?php if ($_SESSION['alert_message']['status'] == 'error'): ?>
    <script type="text/javascript">
      alertify.error("<?= $_SESSION['alert_message']['message'] ?>");
    </script>
  <?php else: ?>
    <script type="text/javascript">
      alertify.success("<?= $_SESSION['alert_message']['message'] ?>");
    </script>
  <?php endif ?>
<?php 
unset($_SESSION['alert_message']);
endif ?>
