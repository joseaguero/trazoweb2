<?php  $slider = consulta_bd("imagen, url","slider","publicado=1","posicion asc"); ?>
<div class="slider ctndr100 cycle-slideshow" data-cycle-fx="fade" data-cycle-timeout="2000" data-cycle-slides="> a">
    <?php for($i=0; $i<sizeof($slider); $i++){ ?>
    <a href="<?= $slider[$i][1] ?>">
        <img src="imagenes/slider/<?php echo $slider[$i][0]; ?>" width="100%" />
    </a>
    <?php } ?>
</div><!--Fin slider -->



<?php  
    $destacados = consulta_bd("DISTINCT(p.id), p.nombre,p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and p.publicado=1 and p.destacado=1","p.posicion asc limit 10"); 
    $cantDestacados = mysqli_affected_rows($conexion);

    $filtro['destacados'] = true;
    $filtro['limite'] = 5;
    $destacado = productos($filtro);

    // var_dump($destacado);
    if($cantDestacados > 0){
?>
<h2>PRODUCTOS DESTACADOS</h2>
<div class="productosDestacados content_products productos-100 grid-5">
    <?php echo grilla_destacados($destacado); ?>
</div><!--Fin productosDestacados -->
<?php } ?>

<?php $banners = consulta_bd("id,titulo,fecha_creacion,imagen, link","banner_home","","posicion asc"); ?>
<!--Categorias destacadas -->
<div class="catDestacados ctndr100">
    <div class="colCatDest1">
        <?php 
        if($banners[0][4] != ""){
                $url1 = $banners[0][4];
            } else {
                $url1 = $url_base.'destacados/'.$banners[0][0].'/'.url_amigables($banners[0][1]);
            }
        ?>
        <a href="<?= $url1; ?>">
            <div class="tituloCatDest posicion1"><?php echo $banners[0][1]; ?></div>
            <img src="imagenes/banner_home/<?php echo $banners[0][3]; ?>" width="100%" />
        </a>
    </div>
    <div class="colCatDest1">
        <div class="colCatDest2">
            <?php 
            if($banners[1][4] != ""){
                    $url2 = $banners[1][4];
                } else {
                    $url2 = $url_base.'destacados/'.$banners[1][0].'/'.url_amigables($banners[1][1]);
                }
            ?>
            <a href="<?= $url2; ?>">
                <div class="tituloCatDest posicion2"><?php echo $banners[1][1]; ?></div>
                <img src="imagenes/banner_home/<?php echo $banners[1][3]; ?>" width="100%" />
            </a>
        </div>
        <div class="colCatDest2">
            <?php 
            if($banners[2][4] != ""){
                    $url3 = $banners[2][4];
                } else {
                    $url3 = $url_base.'destacados/'.$banners[2][0].'/'.url_amigables($banners[2][1]);
                }
            ?>
            <a href="<?= $url3; ?>">
                <div class="tituloCatDest posicion3"><?php echo $banners[2][1]; ?></div>
                <img src="imagenes/banner_home/<?php echo $banners[2][3]; ?>" width="100%" />
            </a>
        </div>
    </div>
    <div class="colCatDest1">
        <?php 
        if($banners[3][4] != ""){
                $url4 = $banners[3][4];
            } else {
                $url4 = $url_base.'destacados/'.$banners[3][0].'/'.url_amigables($banners[3][1]);
            }
        ?>
        <a href="<?= $url4; ?>">
            <div class="tituloCatDest posicion4"><?php echo $banners[3][1]; ?></div>
            <img src="imagenes/banner_home/<?php echo $banners[3][3]; ?>" width="100%" />
        </a>
    </div>
</div>