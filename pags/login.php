<?php 

include("../admin/conf.php");
include("../funciones.php");
// unset($_SESSION['cart_alfa_cm']);

require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
include("../php-html-css-js-minifier.php");

if (isset($_SESSION['user_account'])) {
	header('Location: home');
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php 
		ob_start(); # apertura de bufer
		include("../includes/head.php");
		$head = ob_get_contents();
		ob_end_clean(); # cierre de bufer
		echo minify_html($head);
	?>
	<title>Acceder</title>

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

	<?php 
		$estilos = file_get_contents('../css/estilos.css');
		echo '<style type="text/css">';
		echo minify_css($estilos);
		echo '</style>';
		
		$carroCSS = file_get_contents('../tienda/carro.css');
		echo '<style type="text/css">';
		echo minify_css($carroCSS);
		echo '</style>';
		
		$agent = file_get_contents('../css/agent.css');
		echo '<style type="text/css">';
		echo minify_css($agent);
		echo '</style>';
		
		$responsive = file_get_contents('../css/responsive.css');
		echo '<style type="text/css">';
		echo minify_css($responsive);
		echo '</style>';
		
	?>

	<link rel="stylesheet" href="css/alertify.min.css" />
	<script src="js/alertify.min.js"></script>
	<!-- include a theme -->
	<?php 
	    $default = file_get_contents('../css/themes/default.min.css');
		echo '<style type="text/css">';
		echo minify_css($default);
		echo '</style>';
	    
	    $fancybox = file_get_contents('../css/jquery.fancybox.css');
		echo '<style type="text/css">';
		echo minify_css($fancybox);
		echo '</style>';
	?>

	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:376677,hjsv:5};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-65024256-39', 'auto');
	  ga('require', 'ec');
	  ga('set', '&cu', 'CLP');
	  
	  function productClick(sku, nombre_producto,categoria, variante, posicion, lista, marca ) {
		  ga('ec:addProduct', {
		    'id': sku,
		    'name': nombre_producto,
		    'category': categoria,
		    'brand': marca,
		    'variant': variante,
		    'position': posicion
		  });
		  ga('ec:setAction', 'click', {list: lista});
		  // Send click with an event, then send user to product page.
		  ga('send', 'event', 'UX', 'click', 'Results', {
		      hitCallback: function() {
		      }
		  });
		}
	  
		function addToCart(sku, nombre_producto,categoria, variante, precio, cantidad, marca) {
		 ga('ec:addProduct', {
		    'id': sku,
		    'name': nombre_producto,
		    'category': categoria,
		    'brand': marca,
		    'variant': variante,
		    'price': precio,
		    'quantity': cantidad
		  });
		  ga('ec:setAction', 'add');
		  ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
		}
		
	</script>
</head>
<body class="body_login">
	<div class="content_center">
		<img class="logo_login" src="img/logowhite.svg">
		<div class="center_login">
			<div class="col">
				
				<form action="login_form" method="post" class="login_form">
					<div class="form_group">
						<label>Email</label>
						<input type="text" name="email" class="email_login">
					</div>
					
					<div class="form_group">
						<label>Contraseña</label>
						<input type="password" name="password" class="pass_login">
					</div>

					<a href="javascript:void(0)" class="rec-password">¿Olvidaste tu contraseña?</a>

					<div class="form_group">
						<input type="submit" name="login" class="btn-form" value="INGRESAR">
					</div>
				</form>
				

			</div>

			<!-- <div class="col">
				<h2>REGÍSTRATE</h2>

				<form action="registro" method="post" class="registro_form">
					<div class="form_group">
						<label>Nombre</label>
						<input type="text" name="nombre">
					</div>

					<div class="form_group">
						<label>Apellido</label>
						<input type="text" name="apellido">
					</div>

					<div class="form_group">
						<label>Email</label>
						<input type="text" name="email">
					</div>

					<div class="form_group">
						<label>Rut</label>
						<input type="text" name="rut">
					</div>

					<div class="form_group">
						<label>Teléfono</label>
						<input type="text" name="telefono">
					</div>
					
					<div class="form_group">
						<label>Contraseña</label>
						<input type="password" name="password">
					</div>

					<div class="form_group">
						<label>Repetir Contraseña</label>
						<input type="password" name="password-rec">
					</div>

					<div class="form_group">
						<input type="submit" name="registro" class="btn-form" value="REGISTRARSE">
					</div>
				</form>
			</div>
		</div> -->
	</div>
</div>

	<div class="bgpop-rec"></div>
	<div class="pop-rec">
		<h2>Recuperar contraseña</h2>
		<div class="form_group">
			<input type="text" name="email" class="email-rec" placeholder="Ingrese su email">
		</div>

		<div class="form_group">
			<input type="submit" name="recuperar" value="Recuperar contraseña" class="btn-rec">
		</div>
	</div>

	<?php if (isset($_SESSION['alert_w'])): ?>
		<script type="text/javascript">
			alertify.alert('Aviso importante', '<?= $_SESSION['alert_w']['message'] ?>');
		</script>
	<?php unset($_SESSION['alert_w']); endif ?>

	<?php if (isset($_SESSION['alert_message'])): ?>
		<?php if ($_SESSION['alert_message']['status'] == 'error'): ?>
			<script type="text/javascript">
				alertify.error("<?= $_SESSION['alert_message']['message'] ?>");
			</script>
		<?php else: ?>
			<script type="text/javascript">
				alertify.success("<?= $_SESSION['alert_message']['message'] ?>");
			</script>
		<?php endif ?>
	<?php 
	unset($_SESSION['alert_message']);
	endif ?>
	
	
	<script type="text/javascript" src="js/parallax.min.js"></script>
	<script type="text/javascript" src="js/jquery.uniform.bundled.js"></script>
	<script type="text/javascript" src="js/jquery.cycle2.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>

	<link rel="stylesheet" type="text/css" href="css/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="css/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="css/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="css/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="css/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
	<script type="text/javascript" src="js/jquery.Rut.js"></script>
	<script type="text/javascript" src="js/js.cookie.min.js"></script>
	<script type="text/javascript" src="js/funciones.js?v=<?=cacheo()?>"></script>
	<script type="text/javascript" src="js/validacionCompraRapida.js?v=<?=cacheo()?>"></script>
	<script type="text/javascript" src="js/agrega_desde_ficha.js?v=<?=cacheo()?>"></script>
	<script type="text/javascript" src="js/agrega_quita_elimina_carro.js?v=<?=cacheo()?>"></script>
	<script type="text/javascript" src="js/codigoDescuento.js?v=<?=cacheo()?>"></script>

	<script>ga('send', 'pageview');</script>

</body>
</html>