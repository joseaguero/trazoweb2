<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Términos y condiciones</a>
    </div>
    
    <div class="filtros"></div>
    <div class="titulo tituloPaginasFijas">
    	<h2>Términos y condiciones</h2>
    </div>
</div>

<div class="productosDestacados ctndr100">
    
    <div class="contTextosFijos">
    	<h4>PRIMERO: GENERALIDADES</h4>
    	<p>Este documento regula los términos y condiciones bajo los cuales Ud. tiene derecho a
acceder y usar los servicios del sitio web www.forastero.life y de cualquier información,
texto, video u otro material comunicado en el sitio web.</p>

<p>En este sitio web podrá usar, sin costo, el software y las aplicaciones para equipos
móviles que le permitan navegar, visitar, comparar y si lo desea, adquirir los bienes o
servicios que se exhiben aquí.</p>

<p>Le recomendamos que lea detenidamente estas condiciones e imprima o guarde una
copia de las mismas en la unidad de disco local para su información.</p>

<p>Estos Términos y Condiciones serán aplicados y se entenderán incorporados en cada
uno de los contratos que celebre con Forastero por medio de este sitio web.</p>

<p>El uso de este sitio web, la aplicación de estos Términos y Condiciones, los actos que
ejecute y los contratos que celebre por medio de este sitio web, se encuentran sujetos y
sometidos a las leyes de la República de Chile y en especial a la ley 19.496 de
protección de los derechos de los consumidores.</p>

<p>Forastero por lo tanto, aplicará estrictamente todos los beneficios, garantías y
derechos reconocidos en favor de los consumidores en la ley 19.496. Además,
Forastero adhiere en todas sus partes al Código de Buenas Prácticas para el Comercio
Electrónico de la Cámara de Comercio de Santiago, el cual se encuentra disponible en el
siguiente link <a href="http://www.ccs.cl/prensa/publicaciones/CodigoBuenasPracticas_02.pdf">http://www.ccs.cl/prensa/publicaciones/CodigoBuenasPracticas_02.pdf</a></p>

<div class="linea"></div>

<h4>SEGUNDO: COMUNICACIONES</h4>
<p>Para la comunicación de cualquier presentación, consulta o reclamo a propósito
del uso de este sitio, o los contratos que en él se lleven a cabo, Forastero designa como
representante especial a don Felipe González, socio, disponible en el correo
contacto@forastero.life; y en el teléfono de Servicio al Cliente (+56 2) 2219 1224
domiciliado para estos efectos en calle Juan XXIII 6190, comuna de Vitacura, ciudad de
Santiago.</p>

<p>Forastero se obliga a que en caso de enviarle información publicitaria o promocional,
por correo electrónico, ésta contendrá al menos la siguiente información:</p>

<blockquote>a) Identificación del mensaje, que lo haga reconocible como publicidad o comunicación
promocional, en el campo de asunto del mensaje. <br>
b) Existencia del derecho del consumidor o usuario a solicitar el cese de envíos de
publicidad de ese tipo por Forastero y la obligación de Forastero de suspender esos
envíos. <br>
c) Un procedimiento simple y eficaz para que el consumidor pueda hacer esa solicitud,
indicando una dirección electrónica para estos efectos. <br>
d) La Identificación del anunciante con su denominación comercial.
</blockquote>

<p>Tratándose de publicidad teaser, se cumplirá con las obligaciones señaladas
precedentemente en la medida que se identifique la agencia que realiza la misma.</p>

<p>Forastero cesará el envío de los mensajes publicitarios o promocionales por correo
electrónico u otros medios de comunicación individual equivalentes, a toda persona que
hubiere solicitado expresamente la suspensión de esos envíos.</p>

<div class="linea"></div>

<h4>TERCERO: LIBERTAD DE NAVEGACIÓN y ACEPTACIÓN DE LOS TÉRMINOS Y
CONDICIONES</h4>

<p>La mera visita de este sitio no impone ningún tipo de obligación para el usuario, a menos
que éste exprese de forma inequívoca, por medio de actos positivos, su voluntad de
contratar con la empresa para adquirir bienes o servicios, en la forma indicada en estos
términos y condiciones.</p>

<p>Para aceptar estos Términos y Condiciones, el usuario deberá hacer click donde el sitio
web de Forastero ofrezca esta opción en la interfaz del usuario con la frase “he leído y
aceptado” u otra equivalente que permita dar su consentimiento inequívoco respecto
de la aceptación.</p>

<h4>CUARTO: CÓMO COMPRAR</h4>

<p>Para hacer compras o contratar servicios en este sitio deberá seguir los siguientes
pasos haciendo click en el campo correspondiente:</p>

<p>1. Seleccione el producto o servicio que le interesa y agréguelo a su “carro de compra”.</p>
<p>2. Ingresa tu correo electrónico para acceder al paso de pago y envío.</p>
<p>3. Seleccione el tipo de despacho y entrega entre las alternativas disponibles en el sitio:
despacho a domicilio o retiro en tienda.</p>
<p>4. Antes de ir a pagar, aparecerá en la pantalla:</p>
<blockquote>
    a. Una descripción del producto o servicio contratado. <br>
    b. Precio. <br>
    c. Indicación de costo de envío si corresponde. <br>
    d. Fecha de entrega según el tipo de despacho escogido. <br>
    e. Medio de pago. <br>
    f. Valor total de la operación. <br>
    g. Demás condiciones de la orden.
</blockquote>

<p>5. Para iniciar el proceso de compra es necesario que confirme haber leído y aceptado
estos Términos y Condiciones.</p>

<div class="linea"></div>
<h4>QUINTO: MEDIOS DE PAGO</h4>
<p>Salvo que se señale un medio diferente para casos u ofertas específicos, los productos y
servicios que se informan en este sitio sólo podrán ser pagados por medio de:</p>
<p>1. Tarjeta de crédito bancarias Visa, Mastercard, Dinners Club International o
American Express, emitidas en Chile o en el extranjero, siempre que mantengan un
contacto vigente para este efecto con Transbank.</p>
<p>2. Tarjetas de débito bancarias acogidas al sistema Redcompra, emitidas en Chile por
bancos nacionales, que mantengan un contrato vigente para tales efectos con
Forastero.</p>
<p>El pago con tarjetas de débito se realizará a través de WebPay, que es un sistema de
pago electrónico que se encarga de hacer el cargo automático a la cuenta bancaria del
usuario.</p>
<p>Los usuarios declaran que entienden que estos medios de pago o portales de pago
pertenecen a terceras empresas proveedoras de estos servicios, independientes y no
vinculadas a Forastero, por lo que la continuidad de su prestación de servicios en el
tiempo, así como el correcto funcionamiento de sus herramientas y botones de pago en
línea, será de exclusiva responsabilidad de las empresas proveedoras de estos servicios
y en ningún caso de Forastero.</p>

<div class="linea"></div>

<h4>SEXTO: DERECHOS y GARANTÍAS</h4>
<p>Cambios y devoluciones</p>
<p>Si el producto comprado no lo satisface, este puede ser cambiado o devuelto sin
problemas hasta 30 días después de la fecha en que este haya sido recibido, en
cualquier sucursal de la empresa, o si fue despachado, la empresa puede ir a buscarlo al
mismo lugar, pagando sólo el valor del transporte que será el mismo valor que el
despacho.</p>
<p>Para poder hacer cambio o devolución es necesario que el producto esté sin uso, con
todos sus accesorios, embalajes originales.</p>
<p>Para cambio o devolución deberá presentarse la boleta, guía de despacho, ticket de
cambio, u otro comprobante de compra.</p>
<p>Esta garantía no aplica a productos a pedido o confeccionados a la medida. En el caso de
productos que sean informados como usados, abiertos, de segunda selección o con
alguna deficiencia, que sean comprados habiéndose informado esta situación en el sitio
y contando con un precio reducido, no será aplicable el cambio ni devolución.</p>
<p>En caso de devolución de dinero la empresa realizará un abono en el medio de pago que
haya utilizado en un período no superior a 5 días hábiles de haberse aceptado la
devolución, cuestión que será informada a través del correo electrónico que se hubiere
registrado.</p>
<p>Derecho de Retracto</p>
<p>El usuario tendrá un plazo de 10 días para poner término al contrato celebrado por
medios electrónicos desde que reciba el producto o desde que contrate el servicio,
antes que éste sea prestado.</p>
<p>Para ejercer este derecho deberá utilizar los mismos medios que empleó para celebrar
el contrato y siempre que Forastero le haya enviado comunicación de haberse
perfeccionado el contrato, copia íntegra, acceso claro, comprensible e inequívoco de las
condiciones generales del mismo y la posibilidad de almacenarlos o imprimirlos.</p>
<p>Si Forastero no ha dado cumplimiento al envío de la comunicación escrita
anteriormente mencionada el plazo se extenderá a 90 días.</p>
<p>No podrá ejercerse este derecho cuando el bien, materia del contrato, se haya
deteriorado por hecho imputable al consumidor.</p>
<p>Forastero tendrá la obligación de devolverle las sumas abonadas, sin retención de
gastos, y a la mayor brevedad posible y, en cualquier caso, antes de 45 días a la
comunicación del retracto. Tratándose de servicios, la devolución sólo comprenderá
aquellas sumas abonadas que no correspondan a servicios ya prestados a la fecha del
retracto. Usted deberá restituir en buen estado los elementos de embalaje, los
manuales de uso, las cajas, elementos de protección y todo otro elemento que viniera
con el bien.</p>
<p>Garantías Legales</p>
<p>En caso de que el producto no cuente con las características técnicas informadas, si
estuviera dañado o incompleto, podrá ser cambiado de inmediato. Si presentara fallas o
defectos dentro de los 3 meses siguientes a la fecha en que éste fue recibido, puede
optarse entre su reparación gratuita, o previa restitución, su cambio o la devolución de
la cantidad pagada, siempre que el producto no se hubiera deteriorado por un hecho
imputable al consumidor. Se pide contactar al correo
electrónico clientes@forastero.life para gestionar la garantía o en la tienda misma.</p>
<p>Se considerará como falla o defecto:</p>
<blockquote>
    1. Productos sujetos a normas de seguridad o calidad de cumplimiento obligatorio que
no cumplan con las especificaciones. <br>
    2. Si los materiales, partes, piezas, elementos, sustancias o ingredientes que constituyan
o integren los productos no correspondan a las especificaciones que ostenten o a las
menciones del rotulado. <br>
    3. Productos que por deficiencias de fabricación, elaboración, materiales, partes, piezas,
elementos, sustancias, ingredientes, estructura, calidad o condiciones sanitarias, en su
caso, no sea enteramente apto para el uso o consumo al que está destinado o al que el
proveedor hubiese señalado en su publicidad. <br>
    4. Si el proveedor y consumidor hubieren convenido que los productos objeto del
contrato deban reunir determinadas especificaciones y esto no ocurra. <br>
    5. Si después de la primera vez de haberse hecho efectiva la garantía y prestado el
servicio técnico correspondiente, subsistieren las deficiencias que hagan al bien inapto
para el uso o consumo a que se refiere el numeral 3. Este derecho subsistirá para el
evento de presentarse una deficiencia a la que fue objeto del servicio técnico, o volviere
a presentarse la misma.
</blockquote>

<div class="linea"></div>
<h4>SÉPTIMO: ENTREGA DE PRODUCTOS</h4>
<p>Despacho de Productos</p>
<p>Las condiciones de despacho y entrega de los productos adquiridos podrán ser
escogidas de entre las opciones ofrecidas en el sitio.</p>
<p>Compra Online retira en tienda</p>
<p>Si elige retirar el producto en una tienda, podrá hacerlo en el horario en que ésta se
encuentre abierta al público.</p>
<p>Los retiros deberán ser hechos dentro de un plazo determinado, el cual deberá ser
debidamente indicado en cada una de las opciones ofrecidas. Por medio del correo
electrónico registrado o vía llamado telefónico, se informará que el producto está a
disposición y el plazo para ser retirado. Los comprobantes de esta gestión quedarán
disponibles durante 30 días. Si el producto no fuera retirado dentro del plazo
especificado, la transacción quedará sin efecto, haciéndose la devolución de la cantidad
pagada.</p>
<p>Despacho de Productos a Domicilio:</p>
<p>Realizamos repartos a domicilio a lo largo de Chile a través de empresas de transporte
propias o externas, dependiendo de la localidad. El costo del despacho dependerá del
tamaño del producto, ciudad, comuna e incluso localidad (por ejemplo, la localidad de
Farellones que es de la comuna de Lo Barnechea).</p>
<p>En caso de comprar dos o más productos, se realizará un solo despacho con la totalidad
de la orden considerando como fecha de entrega el producto con mayores días de
fabricación o tránsito.</p>
<p>Nuestros despachos se realizan de Lunes a Viernes en horario hábil y en caso de que el
cliente lo acuerde con nuestro equipo, podemos realizar despachos el día sábado
pagando un cargo extra.</p>
<p>De no encontrarse nadie en la dirección para recibir el producto este volverá a nuestro
centro de distribución y se coordinará para organizar y pagar nuevamente el servicio de
despacho. Para coordinar estas opciones debe escribir a contacto@forastero.life o
clientes@forastero.life</p>
<p>Para la entrega de productos se considera que estos quepan en el ascensor y/o por los
accesos principales como puertas y pasillos. Si el tamaño del producto impide que la
entrega se haga en el lugar estipulado por el cliente, éste deberá aceptar la recepción
conforme en otro lugar del domicilio que lo permita. En caso de entregas en
departamentos y que el producto no entre en el ascensor, se podrán subir por la
escalera con un costo adicional que se acordará con el personal de la tienda
contacto@forastero.life o clientes@forastero.life, y variará según el número de pisos y
complejidad de la maniobra. En caso de que ese trabajo adicional lo realice un equipo
interno o una empresa especializada en este tipo de trabajos, serán responsabilidad y a
cargo del cliente.</p>

<div class="linea"></div>

<h4>OCTAVO: RESPONSABILIDAD</h4>
<p>En ningún caso Forastero responderá por:</p>
<blockquote>
    1. La utilización indebida que Usuarios o visitantes de El Sitio puedan hacer de los
materiales exhibidos, de los derechos de propiedad industrial y de los derechos de
propiedad intelectual. <br>
    2. Daños o eventuales daños y perjuicios que se le puedan causar a los Compradores
y/o Usuarios por el funcionamiento de las herramientas de búsqueda y de los errores
que se generen por los elementos técnicos de El Sitio o motor de búsqueda. <br>
    3. Contenidos de las páginas a las que los Compradores o Usuarios puedan acceder con
o sin autorización de Forastero. <br>
    4. Pérdida, mal uso o uso no autorizado de su código de validación, ya sea por parte del
Usuario y/ o comprador Compradores, o de terceros, luego de realizada la compra en la
forma expresada en los Términos y Condiciones. Asimismo, las partes reconocen y
dejan constancia que la plataforma computacional proporcionada por Forastero no es
infalible, y por tanto, durante la vigencia del presente Contrato pueden verificarse
circunstancias ajenas a la voluntad de Forastero, que impliquen que El Sitio o la
plataforma computacional no se encuentren operativos durante un determinado
periodo de tiempo. <br>
    5. Información de Forastero o sus servicios que se encuentre en sitios distintos a
www.forastero.life
</blockquote>

<p>En tales casos, Forastero procurará restablecer El Sitio y el sistema computacional con
la mayor celeridad posible, sin que por ello pueda imputársele algún tipo de
responsabilidad.</p>
<p>Forastero no garantiza la disponibilidad y continuidad del funcionamiento de El Sitio y
tampoco que, en cualquier momento y tiempo, los Usuarios puedan acceder a las
promociones y Ofertas del El Sitio.</p>
<p>Forastero no se hace responsable por los virus ni otros elementos en los documentos
electrónicos almacenados en los sistemas informáticos de los Usuarios. Forastero no
responderá de los perjuicios ocasionados al Cliente, provenientes del uso inadecuado
de las tecnologías puestas a disposición de éste, cualquiera sea la forma en la cual se
utilicen inadecuadamente estas tecnologías. Forastero no responderá de los daños
producidos a El Sitio por el uso indebido y de mala fe de los Usuarios y/o Compradores.
No obstante, en el evento de realizarse un doble pago por un Usuario o Comprador en
El Sitio, Forastero devolverá la suma del sobrepago, dentro de los 5 días siguientes a la
recepción del respectivo reclamo escrito del Usuario o Comprador, en el que se anexen
los originales de los comprobantes del pago adicional a lo adquirido.</p>
<p>En Login, registro y comunicación con empresas de medios de pago, Forastero usa
certificados digitales de seguridad (SSL), con el fin de encriptar comunicación.
Forastero no manipula ni almacena datos financieros de sus clientes.</p>
<p>En todo caso, la responsabilidad de Forastero, contractual, extracontractual o legal, con
los Usuarios, Compradores o visitantes de El Sitio no excederá del precio efectivamente
pagado por el Comprador en contraprestación por el producto o servicio, sin perjuicio
de lo que determinen los Tribunales de Justicia.</p>

<div class="linea"></div>

<h4>NOVENO: SEGURIDAD DE DATOS Y CLAVE SECRETA</h4>

<p>Responsabilidad de los Usuarios respecto de la información registrada en el sitio</p>
<p>Forastero adoptará las medidas necesarias y prudentes para resguardar la seguridad de
los datos y clave secreta, como sistemas de encriptación de información, certificados de
seguridad u otros que la empresa estime pertinente. En caso de detectarse cambios en
la información que hayas registrado en el sitio, o bien, ante cualquier irregularidad en
las transacciones relacionadas con su identificación o la del medio de pago, o
simplemente como medida de protección a su identidad, nuestros ejecutivos podrán
contactarlo por vía telefónica o correo electrónico, a fin de corroborar sus datos e
intentar evitar posibles fraudes.</p>
<p>En caso de no poder establecer el contacto en un plazo de 72 hrs, por su propia
seguridad, su orden de compra efectuada en nuestro sitio no podrá ser confirmada. Le
informaremos vía telefónica o por correo electrónico que su orden ha quedado sin
efecto por no poder confirmar su identidad o el medio de pago ofrecido. Además, los
comprobantes de las gestiones realizadas para contactarte y poder confirmar la
operación, estarán disponibles en nuestras oficinas durante 30 días, para que puedas
confirmar la orden de compra. Cualquier consulta puede ser efectuada a
clientes@forastero.life</p>
<p>Sin embargo, los Usuarios y/o Compradores son exclusivamente responsables por la
pérdida, mal uso o uso no autorizado del código de validación, ya sea por parte de los
mismos o de terceros, luego de realizada la compra en la forma expresada en los
Términos y Condiciones.</p>
<p>Datos personales:</p>
<p>Los Usuarios y/o Compradores garantizan que la información que suministran para la
celebración del contrato es veraz, completa, exacta y actualizada.</p>
<p>De conformidad con la Ley 19.628 los datos personales que suministren en el Sitio Web
pasarán a formar parte de una base de datos de Forastero y serán destinados única y
exclusivamente en para ser utilizados en los fines que motivaron su entrega y
especialmente para la comunicación en general entre la empresa y sus clientes, validar
los datos de la compra, concretar el despacho y responder sus consultas. Los datos no
serán comunicados a otras empresas sin la expresa autorización de su titular ni serán
transferidos internacionalmente.</p>
<p>Forastero jamás solicita datos personales o financieros a través de correo electrónico.</p>
<p>Forastero presume que los datos han sido incorporados por su titular o por persona
autorizada por éste, así como que son correctos y exactos. Los Usuarios y/o
Compradores con la aceptación de los presentes Términos y Condiciones manifiestan
que los datos de carácter personal que aporte a través de los formularios online en la
página web de Forastero pueden ser utilizados para Ofertas posteriores y distintas a las
ofrecidas en El Sitio.</p>
<p>Sin perjuicio de lo anterior, Forastero garantiza a los usuarios el libre ejercicio de sus
derechos de información, modificación, cancelación y bloqueo de sus datos personales
establecidos en la Ley 19.628. Por consiguiente, los compradores podrán realizar
requerimientos que digan relación con dichos derechos, y en un plazo máximo de dos
días corridos, Forastero deberá dar respuesta e implementar efectivamente esta
solicitud.</p>
<p>Documentos Electrónicos:</p>
<p>El usuario en su calidad de receptor manual de documentos electrónicos, de
conformidad con la Resolución Exenta N° 11 del 14 de febrero de 2003 del Servicio de
Impuestos Internos (que estableció el procedimiento para que contribuyentes
autorizados para emitir documentos electrónicos puedan también enviarlos por estos
medios a receptores manuales), declara y acepta lo siguiente:</p>
<p>Al aprobar estos términos y condiciones, el usuario autoriza a la empresa Forastero
Diseño SPA, 76.582.736-1, para que el documento tributario correspondiente de esta
transacción, le sea entregada solamente por un medio electrónico. De igual forma,
autoriza que el aviso de publicación del documento tributario me enviado mediante
correo electrónico.</p>
<p>De conformidad con la normativa indicada, y en caso que el usuario lo requiera para
respaldar la información contable, asume en relación a dichos documentos tributarios,
las siguientes obligaciones:</p>
<blockquote>
    1. Imprimir los documentos recibidos en forma electrónica, para cada período
tributario, en forma inmediata a su recepción desde el emisor. <br>
    2. Imprimir el documento en el tamaño y forma que fue generado. <br>
    3. Utilizar papel blanco tipo original de tamaño mínimo 21,5 cm x 14 cm (1/2 carta) y de
tamaño máximo 21,5 x 33 cm (oficio). <br>
    4. Imprimir en una calidad que asegure la permanencia de la legibilidad del documento
durante un periodo mínimo de seis años, conforme lo establece la legislación vigente
sobre la materia. Esta impresión se hará hecha usando impresión láser o de inyección
de tinta, excepto que se establezca una autorización o norma distinta al respecto.
</blockquote>

<div class="linea"></div>

<h4>DÉCIMO: ALCANCE DE LAS CONDICIONES INFORMADAS EN EL SITIO</h4>
<p>Forastero no modificará las condiciones bajo las cuales haya contratado con los
consumidores en este sitio. Mientras aparezcan en este sitio, los precios informados
estarán a disposición del usuario, aunque no sean los mismos que se ofrezcan en otros
canales de venta de Forastero, como tiendas físicas, catálogos, televisión, radio, u otros.</p>
<p>Con todo, los precios son aplicables para la ciudad de entrega o despacho. Si una vez
ingresado un producto al carro de compras, es cambiada la dirección de entrega o
despacho a una ciudad diferente, cambiará el precio total del producto de acuerdo a los
costos de envío a la nueva ciudad registrada.</p>
<p>Cualquier cambio en las informaciones publicadas en este sitio, incluyendo las referidas
a mercaderías, servicios, precios, existencias y condiciones, promociones y ofertas,
tendrá lugar antes de recibir una orden de compra y solo se referirá a operaciones
futuras, sin afectar, en caso alguno, derechos adquiridos por los consumidores.</p>
<p>Las promociones ofrecidas en el sitio no necesariamente serán las mismas que
Forastero ofrezca por otros canales de venta. En las promociones que consistan en la
entrega gratuita o rebajada de un producto por la compra de otro, el despacho del bien
que se entregue gratuitamente o a precio rebajado se hará en el mismo lugar al cual se
despacha el producto comprado, salvo que el adquirente solicite, al aceptar la oferta,
que los productos se remitan a direcciones distintas, en cuyo caso deberá pagar el valor
del despacho de ambos productos. No se podrá participar en estas promociones sin
adquirir conjuntamente todos los productos comprendidos en ellas.</p>

<div class="linea"></div>
<h4>DÉCIMO PRIMERO: PROPIEDAD INTELECTUAL</h4>
<p>Todos los contenidos incluidos en este sitio, como textos, material gráfico, logotipos,
íconos de botones, códigos fuente, imágenes, audio clips, descargas digitales y
compilaciones de datos, son propiedad de Forastero o de sus proveedores de
contenidos, y están protegidos por las leyes chilenas e internacionales sobre propiedad
intelectual. Los materiales gráficos, logotipos, encabezados de páginas, frases
publicitarias, iconos de botones, textos escritos y nombres de servicios incluidos en
este sitio son marcas comerciales, creaciones o imágenes comerciales de propiedad de
Forastero en Chile y en otros países. Dichas marcas, creaciones e imágenes comerciales
no se pueden usar en relación a ningún producto o servicio que pueda causar confusión
entre los clientes y en ninguna forma que desprestigie o desacredite a Forastero. Las
demás marcas comerciales que no sean de propiedad de Forastero y que aparezcan en
este sitio pertenecen a sus respectivos dueños.</p>
<p>Todos los derechos no expresamente otorgados en estos Términos y Condiciones son
reservados por Forastero o sus cesionarios, proveedores, editores, titulares de
derechos u otros proveedores de contenidos. Ningún producto, imagen o sonido
pueden ser reproducidos, duplicados, copiados, vendidos, revendidos, visitados o
explotados para ningún fin, en todo o en parte, sin el consentimiento escrito previo de
Forastero. No se puede enmarcar o utilizar técnicas de enmarcación para encerrar
alguna marca comercial, logotipo u otra información registrada o patentada (incluyendo
imágenes, texto, disposición de páginas, o formulario) de Forastero, sin nuestro
consentimiento escrito previo. Tampoco se puede usar meta etiquetas ni ningún otro
“texto oculto” que use el nombre o marcas comerciales de Forastero, sin autorización
escrita previa de esta empresa. Se prohíbe hacer un uso indebido de este sitio o de
estas marcas, licencias o patentes. Lo anterior, sin perjuicio de las excepciones
expresamente señaladas en la ley.</p>
<div class="linea"></div>
<h4>DÉCIMO SEGUNDO: LEGISLACIÓN APLICABLE Y COMPETENCIA</h4>
<p>Los presentes términos y condiciones se rigen por las leyes de la República de Chile.
Cualquier controversia o conflicto derivado de la utilización del sitio web de Forastero,
sus Términos y Condiciones y las Políticas de Privacidad, su validez, interpretación,
alcance o cumplimiento, será sometida a las leyes aplicables de la República de Chile.</p>

       
</div><!--fin contTextosFijos -->    
</div><!--Fin productosDestacados -->

