<?php 
  if (!isset($_SESSION['user_account']) OR is_null($_SESSION['user_account'])) {
    echo '<script>location.href="login"</script>';
  }
?>

<?php 
$productos = productos_guardados($_SESSION['user_account']);

?>

<div class="filaCategorias ctndr100">
    <div class="breadCrumbs">
        <a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Mi cuenta</a>
    </div>
</div>

<div class="cont100">
    <div class="container">
        <div class="titulo2 hidden-dash title-dash"><span>Mi cuenta</span></div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="cont100 datosCliente">
              <div class="back-dash"><a href="dashboard"><img src="images/back-dash.png"> Volver</a></div>
            	<h3 class="subtitulo">Productos guardados</h3>
              
              <?php $i = 0; foreach ($productos as $item): ?>
                <div class="row_pedidos">
                    <div class="body_pedidos">
                      <div class="col">
                        <img src="<?= $item[4] ?>">
                      </div>
                      <div class="col">
                        <div class="nombre"><?= $item[1] ?></div>
                        <div class="sku"><?= $item[2] ?></div>
                        <a href="javascript:void(0)" class="valor_prd">$<?= number_format($item[3], 0, ',', '.') ?></a>
                        <div class="qty"><a href="javascript:void(0)" data-id="<?= $item[0] ?>" class="delete_guardado">Eliminar</a></div>
                      </div>
                      <div class="col grid-center">
                        <a href="javascript:void(0)" class="btn_mover_cart" data-id="<?= $item[0] ?>" data-sku="<?= $item[2] ?>">Mover al carro</a>
                      </div>
                    </div>
                </div>
              <?php $i++; endforeach ?>

            </div>
            
            <div class="cont100 direcciones">
            </div>
        
        </div><!--fin contenidoMiCuenta-->
               
    </div>
</div>
