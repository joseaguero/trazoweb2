<?php 
	$busqueda = (isset($_GET[busqueda])) ? mysqli_real_escape_string($conexion, $_GET[busqueda]) : 0;

	$filtro['busqueda'] = $busqueda;
	$productos = productos($filtro);
    $cant_resultados = $productos['productos']['total'][0];
	

?>


<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Busquedas</a>
    </div>
    <div class="filtros">
    	
    </div>
    <div class="titulo">
    	<h2>RESULTADO DE BÚSQUEDA</h2>
    </div>
</div>

<div class="ctndr100">
	<p class="tituloBusquedas">Se han encontrado <span class="cantResultados"><?php echo $cant_resultados; ?></span> productos con el nombre <span class="burdeo"><?php echo $busqueda; ?></span>.</p>
</div>

<div class="productosDestacados content_products ctndr100 grid-5">
   <?php echo grilla_busqueda($productos); ?>
</div><!--Fin productosDestacados -->
