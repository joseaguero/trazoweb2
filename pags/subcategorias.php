<?php 
	require_once 'paginador/paginator.class.php';

    $page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 1;
	$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 20;
	
	$idSubcat = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
	$nombreSubat = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;
	
	$filtro_precio = (isset($_GET['filtro_precio'])) ? mysqli_real_escape_string($conexion, $_GET['filtro_precio']) : 0;
	$filtro_material = (isset($_GET['filtro_material'])) ? mysqli_real_escape_string($conexion, $_GET['filtro_material']) : 0;
	
	$categoria = consulta_bd("c.id,c.nombre,sc.id,sc.nombre","categorias c, subcategorias sc","c.id = sc.categoria_id and sc.id =$idSubcat","");
    $cantSubcategoria = mysqli_affected_rows($conexion);
    if($cantSubcategoria < 1){
    echo '<script>location.href ="404";</script>';
    }

	if($filtro_precio == "0"){
    	$orden = NULL;
    	$sel_null = "selected";
	} else if($filtro_precio == "precio-desc"){
		$orden = 'IF(pd.descuento > 0, pd.descuento, pd.precio) desc';
		$sel_desc = "selected";
	} else if($filtro_precio == "precio-asc"){
		$orden = 'IF(pd.descuento > 0, pd.descuento, pd.precio) asc';
		$sel_asc = "selected";
	}

    $filtro['subcategoria'] = $idSubcat;
    $filtro['orden'] = $orden;

    $prds = productos($filtro);
    $rutaRetorno = "{$url_base}subcategorias/{$idSubcat}/".$nombreSubat."/".$filtro_precio."/".$filtro_material;
	
?>
<script type="text/javascript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>


<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a> <span>></span> <a href="<?php echo $url_base; ?>categorias/<?php echo $categoria[0][0]; ?>/<?php echo url_amigables($categoria[0][1]); ?>"><?php echo $categoria[0][1]; ?></a> <span>></span> <a href="javascript:void(0)" class="actual"><?php echo $categoria[0][3]; ?></a>
    </div>
    <div class="filtros">
    	<!-- <form id="filtroPrecio">
        	<select name="jumpMenu" id="selectFiltroPrecio" onchange="MM_jumpMenu('parent',this,0)">
                <option value="<?php echo $url_base; ?>subcategorias/<?php echo $idSubcat.'/'.url_amigables($categoria[0][3]); ?>/precio-desc/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?= ($filtro_precio == 'precio-desc') ? 'selected' : ''; ?>>Precio mas alto</option>
                <option value="<?php echo $url_base; ?>subcategorias/<?php echo $idSubcat.'/'.url_amigables($categoria[0][3]); ?>/precio-asc/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?= ($filtro_precio == 'precio-asc') ? 'selected' : ''; ?>>Precio mas bajo</option>
                <option value="<?php echo $url_base; ?>subcategorias/<?php echo $idSubcat.'/'.url_amigables($categoria[0][3]); ?>/0/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?= (isset($sel_null)) ? 'selected' : ''; ?>>Lo más nuevo</option>
            </select>
        </form> -->
        <div class="menuSelectFiltro">
        	<ul>
            	<li><span>
				<?php 
				if($filtro_precio === "0" || $filtro_precio === 0){
					echo 'Lo más nuevo';
				} else if($filtro_precio === "precio-desc"){
					echo 'Precio mas alto';
					} else if($filtro_precio === 'precio-asc'){
						echo 'Precio mas bajo';
						} ?>
				</span>
                	<ul>
                    	<li class="<?php if($filtro_precio === "0" || $filtro_precio === 0){echo 'seleccionado';}?>">
                        	<a href="<?php echo $url_base; ?>subcategorias/<?php echo $idSubcat.'/'.url_amigables($categoria[0][3]); ?>/0/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?= (isset($sel_null)) ? 'selected' : ''; ?>">Lo más nuevo</a>
                        </li>
                        
                        <li class="<?php if($filtro_precio === 'precio-desc'){echo 'seleccionado';}?>">
                        	<a href="<?php echo $url_base; ?>subcategorias/<?php echo $idSubcat.'/'.url_amigables($categoria[0][3]); ?>/precio-desc/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?= ($filtro_precio == 'precio-desc') ? 'selected' : ''; ?>">Precio mas alto</a>
                        </li>
                        
                        <li class="<?php if($filtro_precio === 'precio-asc'){echo 'seleccionado';}?>">
                        	<a href="<?php echo $url_base; ?>subcategorias/<?php echo $idSubcat.'/'.url_amigables($categoria[0][3]); ?>/precio-asc/<?php echo $filtro_material; ?>/<?php echo $ipp ?>/pagina-1" <?= ($filtro_precio == 'precio-asc') ? 'selected' : ''; ?>">Precio mas bajo</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
      
    </div>
    <div class="titulo">
    	<h2><?php echo $categoria[0][3]; ?></h2>
    </div>
</div>
<div class="menu-lateral">
	<ul>
		<li class="menu-title"><?=$categoria[0][1]?></li>
		<?php $subcategorias_rel = consulta_bd("sb.id, sb.nombre", "subcategorias sb JOIN categorias c ON sb.categoria_id = c.id", "c.id = {$categoria[0][0]} AND sb.publicada = 1", "sb.id ASC"); 

		foreach ($subcategorias_rel as $sub) {	

			$active = ($idSubcat == $sub[0]) ? 'menu-lateral-active' : '';
			$nombre_amg = url_amigables($sub[1]);

			echo "<li><a href='subcategorias/{$sub[0]}/{$nombre_amg}' class='{$active}'>{$sub[1]}</a></li>";
		}
		?>

	</ul>
	
</div>
<div class="productosDestacados content_products">
	<?php echo get_grilla($prds, $page, $ipp, $rutaRetorno, $filtro) ?>
<!--Fin productosDestacados -->


<!-- <div class="contBtnMas ctndr100">
	<?php if($prds['productos']['total'] > 12){ ?>
    <a id="cargarMasCategorias" href="javascript:void(0)" rel="2">cargar más +</a>
    <?php } ?>
</div> -->

<?php if ($prds): ?>
	<script type="text/javascript">

		$(window).scroll(function(){
			var windowHeight = $(window).scrollTop(); 
			var contenido2 = $(".content_products").offset(); 
			contenido2 = contenido2.top - 100;
	  
			if(windowHeight >= contenido2  ){ 
			 $('.menu-lateral').addClass('positionFixed'); 

			}else{ 
			 $('.menu-lateral').removeClass('positionFixed'); 
			} 
		});
				
	</script>
<?php endif ?>