<?php 

/*unset($_SESSION['precio']);
unset($_SESSION['descuento']);*/

if (!isset($_SESSION['stock_validado'])) {
    echo '<script>location.href="carro"</script>';
}

unset($_SESSION['stock_validado']);

$producto_dificil = producto_dificil();

$login_user = (isset($_SESSION['user_account'])) ? $_SESSION['user_account'] : null;

if (!is_null($login_user)) {
    $usuario = consulta_bd('nombre, apellido, email, telefono, rut', 'clientes', "id = $login_user", '');

    $nombre = $usuario[0][0];
    $apellido = $usuario[0][1];
    $email = $usuario[0][2];
    $telefono = $usuario[0][3];
    $rut = $usuario[0][4];

    $disabled = true;

}else{
    $disabled = false;
}
?>
<div class="filaCategorias ctndr100">
	<div class="breadCrumbs">
    	<a href="<?php echo $url_base; ?>home">Home</a>  <span>&gt;</span> <a href="javascript:void(0)" class="actual">Envío y pago</a>
    </div>
    
   
</div>

<form method="post" action="tienda/procesar_compra.php" id="formCompra">
<div class="contenedorPasos">
<?php 
    $datosFactura = consulta_bd("razon_social, direccion_factura, rut_factura, email_factura, giro","pedidos","email='$email' and factura=1","id desc");
    $cf = mysqli_affected_rows($conexion);
?> 
        <div class="contPasosValidacion fila100">
            <div class="col1">
                <div class="col-left">

                <!-- COLUMNA DATOS -->
            	<div class="colForm1">
                    <div class="acceso1 titulo2 tituloColumnas">Completa sus datos</div>
                    <div class="contFondoGris">
                    <?php if (!is_null($login_user)): ?>
                        <a id="modificarDatos" href="mi-cuenta">
                            <i class="material-icons">&#xE22B;</i>
                        </a>
                    <?php endif ?>
                        <div class="contCampos double">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>" class="campoPasoFinal" <?= ($disabled) ? 'disabled="disabled"': '' ?>/>
                        </div>

                        <div class="contCampos double double-right">
                            <label for="nombre">Apellido</label>
                            <input type="text" name="apellido" id="apellido" value="<?php echo $apellido; ?>" class="campoPasoFinal" <?= ($disabled) ? 'disabled="disabled"': '' ?> />
                        </div>
                        
                        <div class="contCampos">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="campoPasoFinal" <?= ($disabled) ? 'disabled="disabled"': '' ?> />
                        </div>
                        
                        <div class="contCampos">
                            <label for="telefono">Teléfono</label>
                            <input type="text" name="telefono" id="telefono" value="<?php echo $telefono; ?>" class="campoPasoFinal" <?= ($disabled) ? 'disabled="disabled"': '' ?> />
                        </div>
                        
                        <div class="contRut contCampos">
                            <label for="rut">Rut</label>
                            <input type="text" name="rut" id="Rut" value="<?php echo $rut; ?>" class="campoPasoFinal" <?= ($disabled) ? 'disabled="disabled"': '' ?> />
                        </div>
                        
                        <!-- <div class="mensajeCaluga">Tus datos seran encriptados en la siguiente compra, para una mejor seguridad.</div> -->
                    </div>
                </div> <!--fin colForm1-->
                
                <!-- FIN COLUMNA DATOS -->
                <div class="contFactura">
                    
                    <?php if($idCliente != '' and $cf > 0) { ?>
                        <a id="modificarDatos2" class="fancybox22" href="javascript:void(0)">
                            <i class="material-icons">&#xE22B;</i>
                        </a>
                     <?php } ?>
                     
                    <div class="opcionFactura">
                        <input type="radio" name="factura" value="no" id="facturaNo" checked="checked" />
                        <span class="tituloRadio">Necesito Boleta</span>
                        
                        <input type="radio" name="factura" value="si" id="facturaSi" />
                        <span class="tituloRadio">Necesito Factura</span>
                    </div>
                    <div class="contDatosFactura">
                        <span class="mensajeFactura">*Todos los campos son obligatorios</span>
                        <div class="form50Izquierda relativo">
                            <?php if($cf > 0){ ?>
                            <div class="campoFantasma"><?php echo previewDatos($datosFactura[0][0]); ?></div>
                            <?php } ?>
                            <label for="razon_social">Razón social</label>
                            <input type="text" name="razon_social" value="<?php echo $datosFactura[0][0] ?>" id="razonSocial" class="campoPasoFinal" <?php if($cf > 0){echo 'disabled="disabled"';}; ?> />
                        </div>

                        <div class="form50Derecha relativo">
                            <?php if($cf > 0){ echo '<div class="campoFantasma">'.previewDatos($datosFactura[0][2]).'</div>';} ?>                                   <label for="rut_factura">Rut</label>
                            <input type="text" name="rut_factura" value="<?php echo $datosFactura[0][2]; ?>" id="RutFactura" class="campoPasoFinal" <?php if($cf > 0){echo 'disabled="disabled"';}; ?> />
                        </div>
                        <div class="form100 relativo">
                            <?php if($cf > 0){ echo '<div class="campoFantasma">'.previewDatos($datosFactura[0][4]).'</div>';} ?>
                            <label for="giro_factura">Giro</label>
                            <input type="text" name="giro_factura" value="<?php echo $datosFactura[0][4]; ?>" id="giro" class="campoPasoFinal" <?php if($cf > 0){echo 'disabled="disabled"';}; ?> maxlength="40" />
                        </div>

                        <div class="form50Izquierda relativo">
                        <?php if($cf > 0){ echo '<div class="campoFantasma">'.previewDatos($datosFactura[0][1]).'</div>'; }; ?>
                            <label for="direccion_factura">Dirección</label>
                            <input type="text" name="direccion_factura" id="direccionFactura" value="<?php echo $datosFactura[0][1]; ?>" class="campoPasoFinal" <?php if($cf > 0){echo 'disabled="disabled"';}; ?> />
                        </div>
                        
                        <div class="form50Derecha relativo">
                            <?php if($cf > 0){ echo '<div class="campoFantasma">'.previewDatos($datosFactura[0][3]).'</div>';} ?>
                            <label for="email_factura">Email de facturación</label>
                            <input type="text" name="email_factura" value="<?php echo $datosFactura[0][3]; ?>" id="emailFactura" class="campoPasoFinal" <?php if($cf > 0){echo 'disabled="disabled"';}; ?> />
                        </div>
                    </div><!--Fin contDatosFactura -->
                </div><!--fin contFactura -->
            </div> <!-- FIN COL LEFT -->
            <div class="col-right">
            <!-- COLUMNA ENVIOS -->
                <div class="colForm1 colForm2">
                    <div class="acceso1 titulo2 tituloColumnas">Datos de envío</div>
                    <div class="contFondoGris">
                    	<div class="selectorTabs">
                        	<div class="direccionDespachoTabs">
                                <input type="radio" name="envio" id="tienda2" value="direccionCliente" checked="checked"/>
                                <span class="tituloTabsDirecciones">Servicio de envío</span>
                            </div>
                            <?php if ($producto_dificil): ?>
                                <div class="direccionDespachoTabs">
                                    <input type="radio" name="envio" id="tienda1" value="retiroEnTienda" />
                                    <span class="tituloTabsDirecciones">Retirar en tienda</span>
                                </div>
                            <?php endif ?>
                        </div>
                    	<div class="tabsCompraRapida" id="tabsCompra2">	
                            <div class="direccionDespacho">
                                <input type="radio" name="retiro" id="tienda2" value="D1" checked="checked" />
                                <div class="direccion">
                                    <strong>Casa Matriz</strong><br />
                                    Juan XXIII 6190 • VITACURA • SANTIAGO <br />
                                    <a href="https://goo.gl/maps/w4ywAyWUxvP2" target="_blank">Ver mapa</a>
                                </div>
                            </div>
                        </div>
						
                        <div class="tabsCompraRapida" id="tabsCompra1">	

                            <?php if (!is_null($login_user)): 
                                $direcciones = consulta_bd('id, nombre', 'clientes_direcciones', "cliente_id = {$_SESSION['user_account']}", 'id desc'); ?>
                                <label for="direccioes" class="lbdireccioes">Mis direcciones</label>
                                <select name="direcciones" id="direcciones">
                                    <option value="0">Seleccionar direccion</option>
                                    <?php foreach ($direcciones as $direccion): ?>
                                        <option value="<?= $direccion[0] ?>"><?= $direccion[1] ?></option>
                                    <?php endforeach ?>
                                </select>
                            <?php endif ?>
                            
                            <div>
                                <?php $regiones = consulta_bd("id, nombre","regiones","","id desc");?>
                                <div class="cont_input">
                                    <label for="región">Región</label>
                                    <select name="region" id="region">
                                        <option>Seleccione region</option>
                                        <?php for($i=0; $i<sizeof($regiones); $i++){ 
                                            if($regiones[$i][0] > 0){?>
                                        <option value="<?php echo $regiones[$i][0]; ?>"><?php echo $regiones[$i][1]; ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>

                                <div class="cont_input">
                                    <label for="comuna">Comuna</label>
                                    <select name="comuna" id="comuna">
                                        <option>Seleccione Comuna</option>
                                    </select>
                                </div>
                            </div>

                            <label for="localidad">Localidad</label>
                            <select name="localidad" id="localidad">
                                <option>Seleccione Localidad</option>
                            </select>
                            
                            
                            <label for="direccion">Dirección</label>
                            <input type="text" name="direccion" id="direccion" value="<?php if($calle != ''){ echo $calle.','.$numero;}; ?>" class="campoPasoFinal" />
                            <label for="direccion">Comentarios</label>
                            <input type="text" name="comentarios" id="comentarios" class="campoPasoFinal" />
                            
                            <!-- <label for="quien_recibe">Comentarios</label>
                            <textarea name="quien_recibe" id="quien_recibe" value="" class="campoPasoFinalText" /></textarea>
                            
                            <div class="mensajeCaluga mensajeCalugaDireccion" style="display:none;">
                                <input type="checkbox" name="regalo" id="regalo" value="regalo" />
                                <span class="iconoRegalo">Lo quiero como regalo</span>
                            </div> -->
                    	</div><!--fin tabsCompraRapida-->
                    </div><!--Fin fondo gris-->
                </div> <!--fin colForm1-->
            <!-- FIN COLUMNA ENVIO -->
            </div>
            </div><!--Fin col1 -->
            
            
            
            <div class="col2">
            	<div class="acceso1 titulo2 tituloColumnas">Resumen de productos</div>
                <div class="contResumenCompra">
                	<div id="contDatosCompra">
						<?php echo resumenCompraShort(); ?>
                        <div class="mensajesDespacho"></div>
                    </div>
                    <div class="compraYAceptaTerminos">
                        <div class="txtTerminosAcepto">
                        <input type="radio" name="terminos" id="terminos">
                        He leído y acepto los <a href="<?php echo $url_base; ?>terminos-y-condiciones">términos y condiciones</a> </div>
                    	<a href="javascript:void(0)" id="comprar" class="desabilitado" />Pagar</a>
                        <div class="contImgTranbank">
                        	<img src="tienda/transbankCompraRapida.jpg" width="226" />
                        </div>
                    </div>
                    
                </div>
            </div><!--Fin col2 -->
        
        
        </div><!--Fin fila100 -->
        
    </div>
</form>     

<script type="text/javascript">
<?php
    $cart = $_SESSION['cart_alfa_cm'];    
    $items = explode(',',$cart);
    foreach ($items as $item) {
        $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
    }
    
    foreach ($contents as $prd_id=>$qty) {
        $filas = consulta_bd("pd.id, pd.sku, pd.nombre, m.nombre, pd.precio, pd.descuento","productos p, productos_detalles pd, marcas m","p.id = pd.producto_id and pd.id = ".$prd_id." and p.marca_id = m.id","");
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] < $filas[0][4] and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
            ga('ec:addProduct', {
              'id': '<?php echo $filas[0][0]; ?>',
              'name': '<?php echo $filas[0][2]; ?>',
              'brand': '<?php echo $filas[0][3]; ?>',
              'price': '<?php echo $valor; ?>',
              'quantity': <?php echo $qty; ?>
            });
		<?php
    }
?>
</script>
<script type="text/javascript">
	ga('ec:setAction','checkout', {'step': 2});
</script>