<?php 
  if (!isset($_SESSION['user_account']) OR is_null($_SESSION['user_account'])) {
    echo '<script>location.href="login"</script>';
  }
?>

<?php 
require_once 'paginador/paginator.class.php';
$pedidos = get_pedidos($_SESSION['user_account'], null);
$count = count($pedidos);

// Paginador
$pages = new Paginator;
$pages->items_total = $count;
$pages->mid_range = 3;
$rutaRetorno = "{$url_base}mis-pedidos";

$pages->paginate_pedidos($rutaRetorno);

$pedidos = get_pedidos($_SESSION['user_account'], $pages->limit);

?>

<div class="filaCategorias ctndr100">
    <div class="breadCrumbs">
        <a href="">Home</a> <span>></span> <a href="javascript:void(0)" class="actual">Mi cuenta</a>
    </div>
</div>

<div class="cont100">
    <div class="container">
        <div class="titulo2 hidden-dash title-dash"><span>Mi cuenta</span></div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="cont100 datosCliente">
              <div class="back-dash"><a href="dashboard"><img src="images/back-dash.png"> Volver</a></div>
            	<h3 class="subtitulo">Mis pedidos</h3>
              
              <?php $i = 0; foreach ($pedidos as $item): ?>
                <div class="row_pedidos">
                  <div class="head_pedidos">
                    <div class="col">
                      <span class="title">Orden de compra</span>
                      <span><?= $item['oc'] ?></span>
                    </div>
                    <div class="col">
                      <span class="title">Fecha del pedido</span>
                      <span><?= $item['fecha'] ?></span>
                    </div>  
                    <div class="col">
                      <span class="title">Fecha entrega</span>
                      <span><?= $item['fecha_entrega'] ?></span>
                    </div>
                    <div class="col">
                      <span class="title fleft">Total pagado</span>
                      <span>$<?= number_format($item['total_pagado'], 0, ',', '.') ?></span>
                      <a href="detalle-pedido?oc=<?= $item['oc'] ?>" class="v_details">Ver detalle</a>
                    </div>
                  </div>
                  <?php foreach ($pedidos[$i]['productos'] as $aux): ?>
                    <div class="body_pedidos">
                      <div class="col">
                        <img src="<?= $aux['imagen'] ?>">
                      </div>
                      <div class="col">
                        <div class="nombre"><?= $aux['nombre'] ?></div>
                        <div class="sku"><?= $aux['sku'] ?></div>
                        <a href="javascript:void(0)" class="repetir_compra" data-id="<?= $aux['id'] ?>" data-sku="<?= $aux['sku'] ?>" data-qty="<?= $item['cantidad'] ?>">Volver a comprar</a>
                        <div class="qty">Cantidad: <?= $aux['cantidad'] ?></div>
                      </div>
                      <div class="col">
                        <div class="nombre">VALOR PAGADO</div>
                        <div class="sku">$<?= number_format($aux['total'], 0, ',', '.') ?></div>
                      </div>
                    </div>
                  <?php endforeach ?>
                </div>
              <?php $i++; endforeach ?>

            </div>
            
            <div class="cont100 direcciones">
            </div>

            <div class="clearfix"></div>
              <div class="contBtnMas paginacion">
              <!-- <?php if($cant_productos > 10){ ?>
                <a id="cargarMasCategorias" href="javascript:void(0)" rel="2">cargar más +</a>
                <?php } ?> -->
              <?php
                echo $pages->display_pages();
              ?>
            </div>
        
        </div><!--fin contenidoMiCuenta-->
               
    </div>
</div>
