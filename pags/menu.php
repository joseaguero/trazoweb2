<?php $categorias = consulta_bd("id,nombre","categorias","publicada=1","posicion asc"); ?>
<div class="menuFantasma"></div>
    <div class="menu ctndr100">
        <div class="contMenuFixed">
            <a class="menu2 logo2" href="<?php echo $url_base; ?>home">
                <img src="img/logo.svg" height="46" />
            </a>
            <div class="menu2 finalMenu">
                <a href="carro" class="cantItems"><?php echo qty_pro(); ?></a>
                <a href="carro" class="carro"><i class="material-icons">&#xE8CC;</i></a>
                <a href="carro" class="totalHeader">$<?php echo number_format(totalCart(),0,",","."); ?></a>
            </div>
            <ul>
                <?php for($i=0; $i<sizeof($categorias); $i++) {?>
                <li><a href="<?php echo $url_base."categorias/".$categorias[$i][0]."/".url_amigables($categorias[$i][1]); ?>"><?php echo $categorias[$i][1]; ?></a><a href="javascript:void(0)" class="i_men"><i class="fas fa-angle-down icon_menu"></i>
                <?php $mSubcategorias = consulta_bd("id,nombre","subcategorias","categoria_id=".$categorias[$i][0]." and publicada=1","nombre asc"); 
                        $cantSubcat = mysqli_affected_rows($conexion);
                        if($cantSubcat > 0){
                ?>
                    <ul class="submenu">
                        <?php for($j=0; $j<sizeof($mSubcategorias); $j++) {?>
                        <li><a href="<?php echo $url_base."subcategorias/".$mSubcategorias[$j][0]."/".url_amigables($mSubcategorias[$j][1]); ?>"><?php echo $mSubcategorias[$j][1]; ?></a></li>
                        <?php } ?>
                        <li><a href="<?php echo $url_base."categorias/".$categorias[$i][0]."/".url_amigables($categorias[$i][1]); ?>">ver todos</a></li>
                    </ul>
                    <?php } ?>
                </li>
                <?php } ?>
                <li><a href="ofertas">OFERTAS</a></li>
            </ul>
            <div class="buscador_fixed">
                <form id="buscador-fix" method="get" action="busquedas">
                    <input type="submit" name="enviar" value=" " class="btnBuscar-fix" id="lupa" />
                    <input type="text" name="busqueda" placeholder="Ingrese lo que desea buscar" class="campoBuscar" id="input-buscador-fix" />  
                </form>
                <a class="buscar"><span>BUSCAR</span></a>
            </div>
        </div><!--Fin menu fixxed -->
    </div>
<script>
$(function(){
    $('.i_men').click(function() {
        console.log('assasa');
        if($(this).parent().find('.submenu').hasClass('sub_active')){
            $(this).parent().find('.submenu').slideUp();
            $(this).parent().find('.submenu').removeClass('sub_active');
        }else{
            $(this).parent().find('.submenu').slideDown();
            $(this).parent().find('.submenu').addClass('sub_active');
        }
    });
});
$('.btnBuscar-fix').click(function (e) {
    $('#input-buscador-fix').animate({
        width: 'toggle'
    }, "slow");
    if ($('#input-buscador-fix').val().trim() == "") {
        e.preventDefault();
    }
});
</script>