<?php 
include("admin/conf.php");
include("funciones.php");
// unset($_SESSION['cart_alfa_cm']);

require_once('admin/includes/tienda/cart/inc/functions.inc.php');


if (!isset($_SESSION['user_account'])) {
	header('Location: login');
}


	if (isset($_GET[op]))
	{
		$op = $_GET[op];
	}
	else
	{
		$op = 'home';
	}
	// $url_base = 'http://clientes-moldeable.com/proyectos/forastero/';
	include("php-html-css-js-minifier.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<?php 
	ob_start(); # apertura de bufer
	include("includes/head.php");
	$head = ob_get_contents();
	ob_end_clean(); # cierre de bufer
	echo minify_html($head);
?>
<title><?php include("titulos.php"); ?></title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
<?php 
	$estilos = file_get_contents('css/estilos.css');
	echo '<style type="text/css">';
	echo minify_css($estilos);
	echo '</style>';
	
	$carroCSS = file_get_contents('tienda/carro.css');
	echo '<style type="text/css">';
	echo minify_css($carroCSS);
	echo '</style>';
	
	$agent = file_get_contents('css/agent.css');
	echo '<style type="text/css">';
	echo minify_css($agent);
	echo '</style>';
	
	$responsive = file_get_contents('css/responsive.css');
	echo '<style type="text/css">';
	echo minify_css($responsive);
	echo '</style>';
	
?>
<!-- include the style -->
<link rel="stylesheet" href="css/alertify.min.css" />
<script src="js/alertify.min.js"></script>
<!-- include a theme -->
<?php 
    $default = file_get_contents('css/themes/default.min.css');
	echo '<style type="text/css">';
	echo minify_css($default);
	echo '</style>';
    
    $fancybox = file_get_contents('css/jquery.fancybox.css');
	echo '<style type="text/css">';
	echo minify_css($fancybox);
	echo '</style>';
?>
    <!--<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css?v=2.1.5" media="screen" />-->
    <!--<link rel="stylesheet" href="css/themes/default.min.css" />-->
<!-- Hotjar Tracking Code for http://forastero.life -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:376677,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65024256-39', 'auto');
  ga('require', 'ec');
  ga('set', '&cu', 'CLP');
  
  function productClick(sku, nombre_producto,categoria, variante, posicion, lista, marca ) {
	  ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'position': posicion
	  });
	  ga('ec:setAction', 'click', {list: lista});
	  // Send click with an event, then send user to product page.
	  ga('send', 'event', 'UX', 'click', 'Results', {
	      hitCallback: function() {
	      }
	  });
	}
  
	function addToCart(sku, nombre_producto,categoria, variante, precio, cantidad, marca) {
	 ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'price': precio,
	    'quantity': cantidad
	  });
	  ga('ec:setAction', 'add');
	  ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
	}
	
</script>
</head>

<body>
	<?php 
		ob_start(); # apertura de bufer
		include("pags/buscador.php");
		$buscador = ob_get_contents();
		ob_end_clean(); # cierre de bufer
		echo minify_html($buscador);
	?>
	<div class="contenedor">
    	<?php 
			ob_start(); # apertura de bufer
			include("pags/header.php");
			$header = ob_get_contents();
			ob_end_clean(); # cierre de bufer
			echo minify_html($header);
			//include("pags/header.php");
			
			ob_start(); # apertura de bufer
			include("pags/menu.php");
			$menu = ob_get_contents();
			ob_end_clean(); # cierre de bufer
			echo minify_html($menu);
			//include("pags/menu.php");
			
			ob_start(); # apertura de bufer
			include("pags/$op.php");
			$contOP = ob_get_contents();
			ob_end_clean(); # cierre de bufer
			echo minify_html($contOP);
			//include("pags/$op.php");
		?>
    	<div style="clear:both"></div>
    </div><!--Fin contenedor -->
    
    <?php if($op == 'home'){
		ob_start(); # apertura de bufer
		include("pags/paralax-home.php");
		$paralax = ob_get_contents();
		ob_end_clean(); # cierre de bufer
		echo minify_html($paralax);
		//include("pags/paralax-home.php");
	} ?>
    <?php 
		ob_start(); # apertura de bufer
		include("pags/footer_nuevo.php");
		$footer = ob_get_contents();
		ob_end_clean(); # cierre de bufer
		echo minify_html($footer);
		//include("pags/footer.php"); 
	
	?><!--Fin footer -->
    
</body>


<script type="text/javascript" src="js/parallax.min.js"></script>
<script type="text/javascript" src="js/jquery.uniform.bundled.js"></script>
<script type="text/javascript">
	$(function() {
		'use strict';
		var $uniformed = $("select, :radio").not(".skipThese");
		$uniformed.uniform();
	});
</script>
<script type="text/javascript" src="js/jquery.cycle2.js"></script>

<?php if($op == 'ficha'){ ?>
<!--Crear un listado con los estilso de las texturas cambiando el id y la imagen de fondo-->
<style type="text/css">
	<?php 
	$terminaciones = consulta_bd("id, nombre, thumbs, thumbs_hover","terminaciones","","id"); 
	for($i=0; $i<sizeof($terminaciones); $i++) {?>
	#uniform-<?php echo url_amigables($terminaciones[$i][1]); ?>{width:34px;	height:34px; border:solid 1px #ccc; float:left;}
	#uniform-<?php echo url_amigables($terminaciones[$i][1]); ?> span{width:34px; height:34px; background:url(imagenes/terminaciones/<?php echo $terminaciones[$i][2]; ?>) center center no-repeat;}
	#uniform-<?php echo url_amigables($terminaciones[$i][1]); ?> span.checked{background:url(imagenes/terminaciones/<?php echo $terminaciones[$i][3]; ?>) center center no-repeat;}
	#uniform-<?php echo url_amigables($terminaciones[$i][1]); ?> input{width:34px;height:34px; cursor:pointer;}
	<?php } ?>
</style>
<?php } ?>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>

<link rel="stylesheet" type="text/css" href="css/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="css/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<link rel="stylesheet" type="text/css" href="css/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="css/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="css/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
</script>

<script type="text/javascript" src="js/jquery.Rut.js"></script>
<script type="text/javascript">
$("#Rut").Rut({
   on_error: function(){ 
   				alertify.error('El rut ingresado es incorrecto'); 
				$("#Rut").addClass("incompleto");
				$("#Rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#Rut").removeClass("incompleto");} 
});
$("#RutFactura").Rut({
   on_error: function(){ 
   				alertify.error('El rut ingresado es incorrecto'); 
				$("#RutFactura").addClass("incompleto");
				$("#RutFactura").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#RutFactura").removeClass("incompleto");} 
   
})
</script>
<script type="text/javascript" src="js/js.cookie.min.js"></script>
<script type="text/javascript" src="js/funciones.js?v=<?=cacheo()?>"></script>
<script type="text/javascript" src="js/validacionCompraRapida.js?v=<?=cacheo()?>"></script>
<script type="text/javascript" src="js/agrega_desde_ficha.js?v=<?=cacheo()?>"></script>
<script type="text/javascript" src="js/agrega_quita_elimina_carro.js?v=<?=cacheo()?>"></script>
<script type="text/javascript" src="js/codigoDescuento.js?v=<?=cacheo()?>"></script>

<script>ga('send', 'pageview');</script>
</html>