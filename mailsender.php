<?php 
header('Content-type: text/html; charset=utf-8');
/********************************************************\
|  Alfastudio Ltda - Mail sender V0.5			 		 |
|  Fecha Modificacion: 16/06/2011		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total, 					 |
|  venta, comercializacion o distribucion				 |
|  http://www.alfastudio.cl/                             |
\********************************************************/

//******************Cambiar sólo estos valores*************************//


$nombre_sitio = "Forastero";
$url_sitio = "http://www.forastero.life.";
$url_logo = "http://www.forastero.life/img/logo.jpg";
$campo_oculto = 'validador';
$para = 'clientes@trazodiseno.cl, mercedes@amoble.cl, rafa@trazodiseno.cl, contacto@forastero.life, mercedes.morande@forastero.life, mariapaz.barros@forastero.life';
$asunto = "Contacto desde sitio web $nombre_sitio";
$ruta_retorno = $url_base.'nuestra-tienda';
$campo_mensaje = 'Mensaje';
$obligatorios = "Nombre, Email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";
$file_field = ""; //Nombre del campo del archivo adjunto, dejar vacÃ­o si no se va a usar
$from = "no-reply@forastero.life";
$save_in_db = false;

//Campos del formulario correspondiente a nombre e email.
$nombre_cl = $_POST['Nombre'];
$email_cl = $_POST['Email'];

//******************Comienzo del código, no modificar*********************//
function get_post($except)
	{
		$excepciones = explode(',',$except);
		$excepciones[] = "posicion_img";
		foreach ($_POST as $key=>$val)
		{
			$ok = true;
			foreach($excepciones as $no)
			{
				if ($key == trim($no))
				{
					$ok = false;
				}
			}
			if ($ok)
			{
				if (!is_array($val)) {
					$valurl = urlencode($val);
					$variables .= "&$key=$valurl";
				}
			}
		}
		return $variables;
	}
function comprobar_mail($email)
	{ 
	    $mail_correcto = 0; 
	    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
	       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {  
	          if (substr_count($email,".")>= 1){ 
	             $term_dom = substr(strrchr ($email, '.'),1); 
	             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){  
	                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
	                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
	                if ($caracter_ult != "@" && $caracter_ult != "."){ 
	                   $mail_correcto = 1; 
	                } 
	             } 
	          } 
	       } 
	    } 
	    if ($mail_correcto) 
	       return 1; 
	    else 
	       return 0; 
	} 
if ($_POST['enviar'] and $_POST[$campo_oculto] == '')
{
	$vars = get_post('');
	$obligatorios = explode(',', $obligatorios);
	foreach($obligatorios as $o)
	{
		$campo = trim($o);
		if ($_POST[$campo] == '')
		{
			$error = ($lang == 'esp') ? "Los campos marcados con * son necesarios." : "* indicates requiered fields.";
			die(header("location:$ruta_retorno?lang=$lang&error=$error"));
			//die('1');
		}
	}

	$comp_mail = comprobar_mail($email_cl);
	if ($comp_mail == 1)
	{
		$mail = $email_cl;
	}
	else
	{
		$error = ($lang == 'esp') ? "Correo Inválido $email." : "Invalid email.";
		die(header("location:$ruta_retorno?lang=$lang&error=$error"));
		//die('2');
	}
	
	//Veo si hay archivos para adjuntar
	if ($_FILES["$file_field"]['tmp_name'] != '')
	{
		$file_tmp_name  = $_FILES["$file_field"]['tmp_name']; 
		$file_type  = $_FILES["$file_field"]['type'];   
		$file_ext = strtolower(substr(strrchr($_FILES["$file_field"]['name'], '.'), 1));
		$file_name =  $_FILES["$file_field"]['name'];
		
		if ($file_ext == 'exe' OR $file_ext == 'bat' OR $file_ext == 'dmg')
		{
			$error = ($lang == 'esp') ? "Archivo no aceptado." : "Invalid file extension.";
			header ("location:$ruta_retorno?lang=$lang&error=$error");
			//die('5');
			break;
		}
		
		$semi_rand = md5(time());    
	 	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
	 	
		$header = "From: $nombre_sitio <$from>\nReply-To:$email\n";
		$header .= "X-Mailer:PHP/".phpversion()."\n";
		$header .= "Mime-Version: 1.0\n";
		$header .= "Content-Type: multipart/mixed;\n boundary=\"{$mime_boundary}\"";

		$msg2 =  "This is a multi-part message in MIME format.\n\n";
        $msg2 .= "--{$mime_boundary}\n";
        $msg2 .= "Content-Type: text/html; charset='UTF-8'\n";    
        $msg2 .= "Content-Transfer-Encoding: 7bit\n";
	}
	else
	{
		$header = "From: $nombre_sitio <$from>\nReply-To:$email\n";
		$header .= "X-Mailer:PHP/".phpversion()."\n";
		$header .= "Mime-Version: 1.0\n";
		$header .= "Content-Type: text/html; charset=UTF-8";
	}

	
	$msg2 .= '
	<html>
		<head>
		<title>'.$nombre_sitio.'</title>
			<style type="text/css">
				body,td { 
					color:#000000; 
					font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; 
					background-color: #ffffff; 
				}
				a {
					color: #33ccff;
				}
			</style>
		</head>
		<body>
		<div style="font:12px/1.35em Verdana, Arial, Helvetica, sans-serif;">
		    <table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
		        <tr>
		            <td align="center" valign="top">
		                <table cellspacing="0" cellpadding="0" border="0" width="650">
		                    <tr>
		                        <td valign="top">
		                            <p><a href="'.$url_sitio.'" style="color:#8CC63F;"><img src="'.$url_logo.'" alt="'.$nombre_sitio.'" border="0"/></a></p></td>
		                    </tr>
		                </table>
						<br/><br/>
		                <table cellspacing="0" cellpadding="0" border="0" width="650">
		                    <tr>
		                		<td valign="top">
									<p><strong>Estimado:</strong></p>
									<p>Se ha enviado una consulta a trav&eacute;s de la p&aacute;gina web de '.$nombre_sitio.'. <br />Los datos de la persona que contacta son:<br /></p>
									<ul>
										';
											foreach($_POST as $key=>$val)
											{
												if ($key != 'enviar' AND $key != $campo_mensaje AND $key != '')
												{
													$nombre = ucwords($key);
													$msg2 .= "<li style='height:30px;'>".$nombre.": ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
												}
											}
									$msg2 .= '</ul>
									<p>El cliente ha dejado el siguiente mensaje: <br /><br />
										<em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
									</p>
									<p>Rogamos contactarse con el cliente lo antes posible.</p>
									<p>Muchas gracias<br /> Atte,</p>
									<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		</div>
		</body>
	</html>
	';
	
	
	if ($file_tmp_name != '')
	{
		//Se lee el archivo para adjuntarlo
		$file = fopen($file_tmp_name,'rb');    
 		$data_tmp = fread($file,filesize($file_tmp_name));    
 		fclose($file);
		$data = chunk_split(base64_encode($data_tmp));

 		//Se adjunta el archivo al mensaje
		$msg2 .= "\n\n--{$mime_boundary}\n" .    
		             "Content-Type: {$file_type};\n" .    
		             " name=\"{$file_name}\"\n" .    
		             "Content-Disposition: attachment;\n" .    
		             " filename=\"{$fileatt_name}\"\n" .    
		             "Content-Transfer-Encoding: base64\n\n" .    
		             $data . "\n\n" .    
		             "--{$mime_boundary}--\n";
	}

	$envia_mail = @mail($para, $asunto, $msg2,$header);
	
	if ($save_in_db)
	$insert = insert_entry("enviar", "contactos");
	
	//Respuesta a cliente
	
	$header_cl = "From: $nombre_sitio <$from>\nReply-To:$email\n";
	$header_cl .= "X-Mailer:PHP/".phpversion()."\n";
	$header_cl .= "Mime-Version: 1.0\n";
	$header_cl .= "Content-Type: text/html; charset=UTF-8";
		
	if ($lang == 'esp')
	{
		$asunto = "Gracias por visitar $nombre_sitio";
		$msg3 = '
		<html>
			<head>
			<title>'.$nombre_sitio.'</title>
				<style type="text/css">
					body,td { 
						color:#000000; 
						font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; 
						background-color: #ffffff; 
					}
					a {
						color: #33ccff;
					}
				</style>
			</head>
			<body>
			<div style="font:12px/1.35em Verdana, Arial, Helvetica, sans-serif;">
			    <table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
			        <tr>
			            <td align="center" valign="top">
			                <table cellspacing="0" cellpadding="0" border="0" width="650">
			                    <tr>
			                        <td valign="top">
			                            <p><a href="'.$url_sitio.'" style="color:#8CC63F;"><img src="'.$url_logo.'" alt="'.$nombre_sitio.'" border="0"/></a></p></td>
			                    </tr>
			                </table>
							<br/><br/>
			                <table cellspacing="0" cellpadding="0" border="0" width="650">
			                    <tr>
			                		<td valign="top">
										<p><strong>Estimado '.htmlentities($nombre_cl,ENT_QUOTES,"UTF-8").':</strong></p>
										<p>Muchas gracias por contactarse con '.$nombre_sitio.'!, <br />le enviaremos una respuesta lo antes posible.<br /></p>
										<p>Saludos,</p>
										<p><strong>Equipo de '.$nombre_sitio.'.</strong></p>
			                        </td>
			                    </tr>
			                </table>
			            </td>
			        </tr>
			    </table>
			</div>
			</body>
		</html>
		';
	}
	else
	{
		$asunto = "Thanks for visiting $nombre_sitio";
		$msg3 = '
		<html>
			<head>
			<title>'.$nombre_sitio.'</title>
				<style type="text/css">
					body,td { 
						color:#000000; 
						font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; 
						background-color: #ffffff; 
					}
					a {
						color: #33ccff;
					}
				</style>
			</head>
			<body>
			<div style="font:12px/1.35em Verdana, Arial, Helvetica, sans-serif;">
			    <table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
			        <tr>
			            <td align="center" valign="top">
			                <table cellspacing="0" cellpadding="0" border="0" width="650">
			                    <tr>
			                        <td valign="top">
			                            <p><a href="'.$url_sitio.'" style="color:#8CC63F;"><img src="'.$url_logo.'" alt="'.$nombre_sitio.'" border="0"/></a></p></td>
			                    </tr>
			                </table>
							<br/><br/>
			                <table cellspacing="0" cellpadding="0" border="0" width="650">
			                    <tr>
			                		<td valign="top">
										<p><strong>Dear '.htmlentities($nombre_cl,ENT_QUOTES,"UTF-8").':</strong></p>
										<p>Thanks you for reaching '.$nombre_sitio.'!, <br />we will contact you as soon as posible<br /></p>
										<p>Best regards,</p>
										<p><strong>Team of '.$nombre_sitio.'.</strong></p>
			                        </td>
			                    </tr>
			                </table>
			            </td>
			        </tr>
			    </table>
			</div>
			</body>
		</html>
		';
	}
	$envia_mail_cliente = mail($email_cl, $asunto, utf8_decode($msg3) ,$header_cl);
	
	
	if ($envia_mail)
	{
		$error = ($lang == 'esp') ?  "Su mensaje fue enviado. Muchas gracias por contactarnos." : "Your message has been sent, we'll answer you asap. Thank You.";
		//die('3');
	}
	else
	{
		$error = ($lang == 'esp') ? "Error enviando el correo, por favor inténtelo nuevamente." : "We had some dificulties sending the email, please try again later.";
		//die('4');
	}
	header ("location:$ruta_retorno?lang=$lang&error=$error");
	//die('121212');
}
else
{
	$error = ($lang == 'esp') ? "Error al enviar el mensaje" : "Error sending the email.";
	$vars = get_post('');
	//die("6");
	header ("location:$ruta_retorno?lang=$lang&error=$error");
}

?>