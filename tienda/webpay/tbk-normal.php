<?php
include("../../admin/conf.php");
require_once('../../admin/includes/tienda/cart/inc/mysql.class.php');
require_once('../../admin/includes/tienda/cart/inc/global.inc.php');
require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

/**
 * @author     Allware Ltda. (http://www.allware.cl)
 * @copyright  2015 Transbank S.A. (http://www.tranbank.cl)
 * @date       Jan 2015
 * @license    GNU LGPL
 * @version    1.0
 */

require_once( 'libwebpay/webpay.php' );
require_once( 'certificates/cert-normal.php' );

/* Configuracion parametros de la clase Webpay */
//$sample_baseurl = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

$monto = round($_POST[TBK_MONTO]);  
$oc = $_POST[TBK_ORDEN_COMPRA];  
$idSesion = $_POST[TBK_ID_SESION];  
$result = $_POST[URL_RETURN];  
$final = $_POST[URL_FINAL];
$exito = "https://forastero.life/exito?oc=";
$fracaso = "https://forastero.life/fracaso?oc=";


if(isset($_POST[TBK_ORDEN_COMPRA]) and $_POST[TBK_ORDEN_COMPRA] != ''){
    $_SESSION['ocRechazo'] = $_POST[TBK_ORDEN_COMPRA];
   // echo $_SESSION['ocRechazo']."Siexiste";
} else {
    //echo $_SESSION['ocRechazo']."noexiste";
}


//echo '<h1>'.$_SESSION['ocRechazo'].'<h1>';
//var_dump($_SESSION['ocRechazo']);

function write_log($tipo,$cadena){
	if (file_exists("log/TBK-WS_".date("Y-m-d").".log")) {
        //si existe no hago nada
    } else {
        //si no existe lo creo
        $fileName = "log/TBK-WS_".date("Y-m-d").".log";
        $contenidoInicial = "Log transbank ".date("Y-m-d")."\n";
        file_put_contents($fileName, $contenidoInicial);
    }
    $arch = fopen("log/TBK-WS_".date("Y-m-d").".log", "a+"); 
    fwrite($arch, $tipo.": ".$cadena."\n");
	fclose($arch);
}



$webpay_settings = array(
	"MODO" => "PRODUCCION",
	"PRIVATE_KEY" => $certificate['private_key'],
	"PUBLIC_CERT" => $certificate['public_cert'],
	"WEBPAY_CERT" => $certificate['webpay_cert'],
	"COMMERCE_CODE" => $certificate['commerce_code'],
	"URL_RETURN" => $result,
	"URL_FINAL" => $final
);

/* Creacion Objeto Webpay */
$webpay = new WebPaySOAP($webpay_settings); // Crea objeto WebPay
$webpay = $webpay->getNormalTransaction(); // Crea Transaccion Normal




$action = isset($_GET["action"]) ? $_GET["action"]: 'init';
$montoAPagar = consulta_bd("total_pagado","pedidos","oc='$oc'","");

switch ($action) {
    default: 
 		$tx_step = "Init";
  	     $request = array(
  			"amount"    => $monto,      // monto a cobrar
  			"buyOrder"  => $oc,    // numero orden de compra
  			"sessionId" => $idSesion, // idsession local
  		);

        //validamos el monto enviado v/s el monto de la bd
        if($montoAPagar[0][0] != $monto){
            $message = "Estas intentando pagar un monto diferente al real, oc = $oc, monto a pagar: $monto, monto que deberia pagar: ".$montoAPagar[0][0];
            write_log("Intento de hackeo: ","$message");
            header("Location: $fracaso".$_SESSION['ocRechazo']);
            break;
        }
        
        // Iniciamos Transaccion
 		$result = $webpay->initTransaction($request["amount"], $request["sessionId"], $request["buyOrder"]);
        $webpay_token = $result["token_ws"];
        
        
        //Valido que la oc no este repetida
        /*$oc2 = $_SESSION['ocRechazo'];
        $existeYActivo = consulta_bd("id, estado_id, oc","pedidos","oc='$oc2' and estado_id = 2","");
        $cantActivoOC = mysql_affected_rows();
        if($cantActivoOC > 0){
            header("Location: $fracaso".$_SESSION['ocRechazo']);
        }*/
        
       // Verificamos respuesta de inicio en webpay
		if (strlen($webpay_token)) {
			$message = "Sesion iniciada con exito en Webpay";
			$next_page = $result["url"];
            write_log("     Response Mensaje","$message");
        } else {
			$message = "webpay no disponible";
            write_log("     Response Mensaje","$message");
            //al no coinidir los certificados retorno al fracaso
            header("Location: $fracaso".$_SESSION['ocRechazo']);
		}

		break;

	case "result":
 		$tx_step = "Get Result";
		if (!isset($_POST["token_ws"])) break;
		
		$webpay_token = $_POST["token_ws"];
		$request = array( 
			"token"  => $_POST["token_ws"]
		);

		// Rescatamos resultado y datos de la transaccion
		$result = $webpay->getTransactionResult($request["token"], $fracaso);  
       
        //datos que retornan de transbank
        $accountingDate = $result->accountingDate;
            $buyOrder = $result->buyOrder;
            $cardNumber = $result->cardDetail->cardNumber;
            $cardExpirationDate = $result->cardDetail->cardExpirationDate;
            
            $authorizationCode = $result->detailOutput->authorizationCode;
            $paymentTypeCode = $result->detailOutput->paymentTypeCode;
            $responseCode = $result->detailOutput->responseCode;
            $sharesNumber = $result->detailOutput->sharesNumber;
            $amount = $result->detailOutput->amount;
            $commerceCode = $result->detailOutput->commerceCode;
            
            $transactionDate = $result->transactionDate;   
            $VCI = $result->VCI;
            $token = $_POST["token_ws"];
        
        
		// Verificamos resultado del pago
        //si el pago es exitoso
		if ($result->detailOutput->responseCode===0) {
			$message = "Pago ACEPTADO por webpay (se deben guardatos para mostrar voucher)";
			$next_page = $result->urlRedirection;
			
            write_log("Resultado","");
            write_log("     Exito","$message");
            
            $next_page_title = "Finalizar Pago";
            
            $estado = 2;
            $fecha_despacho = despachoDateExito($buyOrder);
            $date  = explode("/", $fecha_despacho);
            $new_date = $date[2] . "-" . $date[1] ."-".$date[0];
            //actualizao la bd con los datos recibidos
            update_bd("pedidos","accounting_date = '$accountingDate', card_number = '$cardNumber', card_expiration_date = '$cardExpirationDate', authorization_code = '$authorizationCode', payment_type_code = '$paymentTypeCode', response_code = '$responseCode', shares_number = '$sharesNumber', amount = '$amount', commerce_code = '$commerceCode', transaction_date = '$transactionDate', vci = '$VCI', token = '$token', estado_id = $estado, fecha_despacho = '$new_date'","oc='$buyOrder'");
            
            //obtengo el id del pedido
            $id_pedido = consulta_bd("id, nombre, amount, cant_productos, fecha, direccion, comuna, ciudad, region, telefono, email, transaction_date, factura, razon_social, codigo_descuento","pedidos","oc='$buyOrder'","");
            $reducirStock = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id=".$id_pedido[0][0],"");
            //redusco el stock de los productos comprados.
            for($i=0; $i<sizeof($reducirStock); $i++){
                $cantActual = consulta_bd("id,stock","productos_detalles","id=".$reducirStock[$i][0],"");
                $qtyFinal = $cantActual[0][1]- $reducirStock[$i][1];
				
				//si no hay productos disponibles despublico la entrada.
				if($qtyFinal == 0){
					$despublico = ', publicado = 0';
				}
                update_bd("productos_detalles","stock=$qtyFinal $despublico","id=".$reducirStock[$i][0]);
            }

            if($id_pedido[0][14] != NULL AND $id_pedido != ''){
                $codigo = $id_pedido[0][14];
                $desactivoCodigo = update_bd("descuento_productos","usados = usados + 1, fecha_modificacion = NOW()","codigo = '$codigo'");
            }

            //funciones para avisar al cliente y al administrador de la venta
            enviarComprobanteCliente($buyOrder);
            enviarComprobanteAdmin($buyOrder);
            
            $url = "https://pos.trazo.cl/ingresa_pedido_web/".$buyOrder."/forastero";
            $curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			$result = curl_exec($curl);
			curl_close($curl);
            
		} else {
			$message = "Pago RECHAZADO por webpay - ".utf8_decode($result->detailOutput->responseDescription);
            write_log("Resultado","");
            write_log("     Rechazo","$message");
            
            
            $next_page= $fracaso.$_SESSION['ocRechazo'];
            $estado = 3;
            update_bd("pedidos","accounting_date = '$accountingDate', card_number = '$cardNumber', card_expiration_date = '$cardExpirationDate', authorization_code = '$authorizationCode', payment_type_code = '$paymentTypeCode', response_code = '$responseCode', shares_number = '$sharesNumber', amount = '$amount', commerce_code = '$commerceCode', transaction_date = '$transactionDate', vci = '$VCI', token = '$token', estado_id = $estado","oc='$buyOrder'");
            //header("Location: $fracaso.$buyOrder");
		}

		break;
		

	case "end":
        $token_ws = $_POST[token_ws];
        $oc = consulta_bd("oc","pedidos","token='$token_ws' and vci <> ''","");
        $cantresult = mysqli_affected_rows($conexion);
        if($cantresult > 0){
            
            $tx_step = "End";
            write_log("Transaccion exitosa","$tx_step");

            $request= '';
            $result = $_POST;
            $message = "Transacion Finalizada";
            $next_page = $exito.$oc[0][0];
            //header("Location: $exito".$oc[0][0]);
            break;
        } else {
            $tx_step = "End";
            write_log("Transaccion Cancelada","$tx_step");
            $request= '';
            $result = $_POST;
            $message = "Transacion Finalizada";
            $next_page= $fracaso.$_SESSION['ocRechazo'];
            
            //header("Location: $fracaso".$_POST[TBK_ORDEN_COMPRA]);
            break;
        }
       
}


?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!--<h2><?php echo "Step: ".$tx_step ?> <?php echo $_SESSION['ocRechazo']; ?></h2>

<div style="background-color:lightyellow;">
	<h3>request</h3>
	<?php  //var_dump($request); ?>
</div>
<div style="background-color:lightgrey;">
	<h3>result</h3>
	<?php  //var_dump($result); ?>
</div>
<p><samp><?php  echo $message; ?></samp></p>-->
<?php 
    if (strlen($next_page)) {    
?>
<form action="<?php echo $next_page; ?>" method="post" id="formProceso">
	<input type="hidden" name="token_ws" value="<?php echo $webpay_token; ?>">
	<input type="submit" value="Continuar &raquo;">
</form>
<?php } ?>


<script type="text/javascript">
    $(function(){
        $('#formProceso').submit();	
    });
</script>
