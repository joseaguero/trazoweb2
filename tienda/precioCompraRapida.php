<?php include_once("../admin/conf.php");?>
<?php include("../includes/funciones.php");

// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
?>

<?php
	
//$id_address = $_POST[precio];

$id_address = (is_numeric($_POST[localidad])) ? mysqli_real_escape_string($conexion, $_POST[localidad]) : 0;
	
if($id_address == 'D1'){
    
 //    $msj = 'Puede retirarlo a partir de '.diasDespacho().' días hábiles a partir de hoy.';
	// $_SESSION["precio"] = 0;
	//die("1");
} else if($id_address == 'D2'){
    
 //    $msj = 'Puede retirarlo a partir de '.diasDespacho().' días hábiles a partir de hoy.';
	// $_SESSION["precio"] = 0;
	//die("1");
} else {
    
	$id_localidad = $id_address;

	$envio = valorDespacho($id_localidad);

	$comuna_id = consulta_bd('c.id', 'comunas c JOIN localidades l ON l.comuna_id = c.id', "l.id = $id_localidad", '');
	
	$_SESSION["precio"] = $envio;
    
  
    $msj = 'El costo de envío actual es de: <span class="blue bold"><span id="precio_top">$'.number_format($_SESSION["precio"],0,",",".").'</span> y la fecha estimada de envio es  '.despachoDate(NULL, $comuna_id[0][0]).'.</span>';
	   //$_SESSION["precio"] = $precio_comuna[0][0];
        //die("3");
   
}
	
	if ($codigo_descuento != NULL) {

		$descuento_producto = codigo_descuento($codigo_descuento, $id_address);

		$codigo = $_SESSION['descuento']['codigo'];
		$descuentoProducto = $_SESSION['descuento']['valor'];

		$response = array(
			'valor' => "$".number_format($_SESSION["precio"],0,",","."),
			'msj' => $msj,
			'valorEnvio' => $_SESSION["precio"],
			'descuento' => "$".number_format($descuentoProducto,0,",","."),
			'totalActual' => "$".number_format(round(get_total_price())+$_SESSION["precio"]-$descuentoProducto,0,",","."),
			'arreglo' => '1'
		);

	}elseif (isset($_SESSION["descuento"])){
		$codigo = $_SESSION["descuento"]['codigo'];
		$descuentoProducto = $_SESSION['descuento']['valor'];
		
		$response = array(
			'valor' => "$".number_format($_SESSION["precio"],0,",","."),
			'msj' => $msj,
			'valorEnvio' => $_SESSION["precio"],
			'descuento' => "$".number_format($descuentoProducto,0,",","."),
			'totalActual' => "$".number_format(round(get_total_price())+$_SESSION["precio"]-$descuentoProducto,0,",","."),
			'arreglo' => '2'
		);
	
	} else {
		$response = array(
			'valor' => "$".number_format($_SESSION["precio"],0,",","."),
			'msj' => $msj,
			'valorEnvio' => $_SESSION["precio"],
			'totalActual' => "$".number_format(round(get_total_price())+$_SESSION["precio"],0,",","."),
			'arreglo' => '3',
			'codigo' => $codigo_descuento
		);
	
	}
					
    
	 echo(json_encode($response));
	 
?>
