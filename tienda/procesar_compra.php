<?php
//conf
include('../admin/conf.php');

//funciones carro
// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

$login_user = (isset($_SESSION['user_account'])) ? $_SESSION['user_account'] : null;

if (!is_null($login_user)) {

	$datos_cliente = consulta_bd('nombre, apellido, email, telefono, rut', 'clientes', "id = {$login_user}", '');

	$info_email = $datos_cliente[0][2];
	$info_tele  = $datos_cliente[0][3];
	$info_name  = $datos_cliente[0][0];
	$info_apellido = $datos_cliente[0][1];
	$rut  = $datos_cliente[0][4];

	$current_user = $login_user;

}else{
	$info_email = (isset($_POST[email])) ? mysqli_real_escape_string($conexion, $_POST[email]) : 0;
	$info_tele  = (isset($_POST[telefono])) ? mysqli_real_escape_string($conexion, $_POST[telefono]) : 0;
	$info_name  = (isset($_POST[nombre])) ? mysqli_real_escape_string($conexion, $_POST[nombre]) : 0;
	$info_apellido = (isset($_POST[apellido])) ? mysqli_real_escape_string($conexion, $_POST[apellido]) : 0;
	$rut  = (isset($_POST[rut])) ? mysqli_real_escape_string($conexion, $_POST[rut]) : 0;

	$info_email = trim(strtolower($info_email));
	$clientes = consulta_bd("id","clientes","LOWER(email)='$info_email'","");

	$cantClientes = mysqli_affected_rows($conexion);
	if($cantClientes > 0){
		$_SESSION["cliente_id"] = $clientes[0][0];
		$current_user = $_SESSION["cliente_id"];
	} else {
		$inserCliente = insert_bd("clientes","nombre,rut,email,telefono,apellido","'$info_name','$rut','$info_email','$info_tele', '$info_apellido'");
		$idClienteInsert = mysqli_insert_id($conexion);
		$_SESSION["cliente_id"] = $idClienteInsert;
		$current_user = $_SESSION["cliente_id"];

	}
}

//die();
//GET OC
//$rand = rand(1000, 9999);
//$date = date('sdmY');
$date = date('Ymdhis');
$oc = "OC_$date";
//$oc = "OC_20161215030818";

//GET ID ADDRESS
$id_direccion = mysqli_real_escape_string($conexion, $_POST[address_id]);
$tiendaRetiro = mysqli_real_escape_string($conexion, $_POST[retiro]);



if($_POST[envio] == 'retiroEnTienda'){
	if($tiendaRetiro == 'D1'){
		//tienda las hualtatas
		$calle 		= 'AV. Juan XXIII';
		$numero 	= '6190';
		$ciudad 	= 'Santiago';
		$comuna     = 'Vitacura';
		$region    	= 'Metropolitana';
		$direccion = $calle." #".$numero;
		//DESPACHO
		$despacho = 0;
		$despacho_id = 1;
		$retiroEnTienda = 1;
	} else if($tiendaRetiro == 'D2'){
		//tienda concepcion
		$calle 		= 'Camino El Venado';
		$numero 	= '560 Local A-2';
		$ciudad 	= 'Concepción';
		$comuna     = 'San Pedro de la Paz';
		$region    	= 'Región del Bio Bio';
		$direccion = $calle." #".$numero;
		//DESPACHO
		$despacho_id = 2;
		$despacho = 0;
		$retiroEnTienda = 1;
	} else if($tiendaRetiro == 'D3'){
		//Tienda santa rosa
		$calle 		= 'Av. Santa Rosa';
		$numero 	= '5320, Local 2';
		$ciudad 	= 'Santiago';
		$comuna     = 'San Joaquin';
		$region    	= 'Metropolitana';
		$direccion = $calle." #".$numero;
		//DESPACHO
		$despacho_id = 3;
		$despacho = 0;
		$retiroEnTienda = 1;
	}
} else {
	//var_dump($_POST);
	$comuna_id = (is_numeric($_POST[comuna])) ? mysqli_real_escape_string($conexion, $_POST[comuna]) : 0;
	$localidad_id = (is_numeric($_POST[localidad])) ? mysqli_real_escape_string($conexion, $_POST[localidad]) : 0;
	$campos = "r.nombre, ciu.nombre, c.nombre, c.id, r.id, ciu.id, l.nombre, l.id";
	$tablas = "ciudades ciu, comunas c, regiones r, localidades l";
	$where = "r.id = ciu.region_id and ciu.id = c.ciudade_id and c.id = l.comuna_id and l.id = $localidad_id";
	$address = consulta_bd($campos,$tablas,$where,"");
	
	$calle = (isset($_POST[direccion])) ? mysqli_real_escape_string($conexion, $_POST[direccion]) : 0;
	$numero 	= '';//$address[0][1];
	$region    	= $address[0][0];
	$ciudad 	= $address[0][1];
	$comuna     = $address[0][2];
	$localidad  = $address[0][6];
	$direccion = $calle." #".$numero;
	$ciudad_id = $address[0][5];
	$region_id = (is_numeric($_POST[region])) ? mysqli_real_escape_string($conexion, $_POST[region]) : 0;
	
	//ingreso la direccion del cliente para futuras compras
	/*if($cantClientes == 0){
		$inserCliente = insert_bd("clientes_direcciones","cliente_id,region_id,ciudad_id,comuna_id, localidad_id, calle","$idClienteInsert, $region_id, $ciudad_id, $comuna_id, $localidad_id, '$calle'");
	}*/
	
	//DESPACHO
	$despacho = valorDespacho($localidad_id);
	//die("$despacho");
	$retiroEnTienda = 0;
    $despacho_id = 0;
}


//REGALO
$regalo = (isset($_POST[regalo])) ? mysqli_real_escape_string($conexion, $_POST[regalo]) : 0;//mysqli_real_escape_string($conexion, $_POST[regalo]);
$name_regalo = 0;//mysqli_real_escape_string($conexion, $_POST[name_regalo]);
$tel_regalo = 0;//mysqli_real_escape_string($conexion, $_POST[tel_regalo]);


//facturacion
	$factura  = (isset($_POST[factura])) ? mysqli_real_escape_string($conexion, $_POST[factura]) : 0;
	$datosFacturaHBT = consulta_bd("giro, rut","clientes","email='$info_email'","");
		if($factura == 'no'){
			$giro = 'Particular';
			$factura = 0;
		}else{
			$giro = (isset($_POST[giro_factura])) ? mysqli_real_escape_string($conexion, $_POST[giro_factura]) : 0;
			$factura = 1;
		}
		$razon_social = (isset($_POST[razon_social])) ? mysqli_real_escape_string($conexion, $_POST[razon_social]) : 0;
		$rut_factura = (isset($_POST[rut_factura])) ? mysqli_real_escape_string($conexion, $_POST[rut_factura]) : 0;
		$email_factura = (isset($_POST[email_factura])) ? mysqli_real_escape_string($conexion, $_POST[email_factura]) : 0;
		$direccion_factura = (isset($_POST[direccion_factura])) ? mysqli_real_escape_string($conexion, $_POST[direccion_factura]) : 0;
		
		$id_cliente_hbt = $datosFacturaHBT[0][2];
		
		

$subtotal = round(get_total_price());


//descuento
if($_SESSION["descuento"]){
	$codigo = $_SESSION["descuento"]['codigo'];
	$descuentoProducto = $_SESSION['descuento']['valor'];
} else {
	$descuentoProducto = 0;
	$codigo = '';
}

//TOTAL
$total = $subtotal+$despacho-$descuentoProducto;

//CART EXPLODE AND COUNT ITEMS
$cart = $_SESSION['cart_alfa_cm'];
$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}

$nombre_completo_cliente = $info_name . ' ' . $info_apellido;

if ($_SESSION['cart_alfa_cm']!="") {
	
	$estado_pendiente = 1;
	$campos = "oc, 
	cliente_id, 
	id_hbt, 
	fecha, 
	estado_id, 
	codigo_descuento, 
	descuento, 
	total, 
	valor_despacho, 
	total_pagado, 
	regalo, 
	nombre, 
	telefono, 
	email, 
	direccion, 
	comuna, 
	region, 
	ciudad, 
	giro, 
	rut, 
	retiro_en_tienda, 
	retiro_en_tienda_id,
	factura, razon_social, 
	direccion_factura, 
	rut_factura, 
	email_factura";
	$values = "'$oc', '$current_user', '$id_cliente_hbt', NOW(), $estado_pendiente, '$codigo', '$descuentoProducto', $subtotal, $despacho, $total, 1, '$nombre_completo_cliente', '$info_tele', '$info_email', '$direccion', '$comuna', '$region', '$ciudad', '$giro', '$rut', $retiroEnTienda, $despacho_id, $factura, '$razon_social', '$direccion_factura', '$rut_factura', '$email_factura'";
			$generar_pedido = insert_bd("pedidos","$campos","$values");
			
		$pedido_id = mysqli_insert_id($conexion);
		$cantTotal = 0;
		
		
		foreach ($contents as $id=>$qty) {

			$es_pack = consulta_bd('p.pack, pd.sku', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $id", '');
			$pack = $es_pack[0][0];

			if ($pack == 0) {
				//consulto el precio segun la lista de precios
				$prod = consulta_bd("pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
				if($prod[0][1] > 0){
					$precio_final = $prod[0][1];
				}else{
					$precio_final = $prod[0][0];
				}
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario * $qty;

				$all_sku = consulta_bd("sku","productos_detalles","id=$id","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */

				$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion";
				$values = "$pedido_id,'$id','$qty','$precioUnitario','$sku', $precioTotal, NOW()";
				$detalle = insert_bd("productos_pedidos","$campos","$values");
				$cantTotal = $cantTotal + $qty;
			}else{
				$sku_pack = $es_pack[0][1];
				$productos_pack = consulta_bd('p.codigos_pack', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $id", '');
				$explode_pack = explode(',', $productos_pack[0][0]);
				$skus = array();
	            foreach ($explode_pack as $item) {
	                $skus[$item] = (isset($skus[$item])) ? $skus[$item] + 1 : 1;
	            }
				foreach ($skus as $item => $cantidad_pack) {
					$prod = consulta_bd("pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.sku = '$item'","");
					
					if($prod[0][1] > 0){
						$precio_final = $prod[0][1];
					}else{
						$precio_final = $prod[0][0];
					}
					

					$precioUnitario = $precio_final;
					$precioTotal = $precioUnitario * $qty;

					$all_sku = consulta_bd("sku","productos_detalles","sku='$item'","");
					$sku = $all_sku[0][0];
					/* $precio = $all_sku[0][1]; */

					$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion,codigo_pack,cantidad_pack";
					$values = "$pedido_id,'$id', $cantidad_pack,'$precioUnitario','$sku', $precioTotal, NOW(), '$sku_pack', $qty";
					$detalle = insert_bd("productos_pedidos","$campos","$values");
				}

				$cantTotal = $cantTotal + $qty;
			}
		}

		$qtyFinal = update_bd("pedidos","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$pedido_id");

		unset($_SESSION['cart_alfa_cm']);
		unset($_SESSION["descuento"]);
		unset($_SESSION["cliente_hbt"]);
	
}else{
	header("location:404");
}
?>

<?php /* ?>
<div style="margin: 0 auto; width: 100%;text-align:center;font-family:verdana;font-size: 12px;padding-top:200px;">
	<img src="<?php echo $url_base; ?>tienda/cargador.gif" border="0" /><br /><br />
		Estamos procesando su compra, espere por favor...<br />
        cant productos = <?php echo $cantTotal; ?><br />
        total = <?php echo $total ?>
</div>
<?php */ ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<form id="form" action="webpay/tbk-normal.php" method="post" >
    <input type="hidden" name="TBK_MONTO" value="<?php echo $total; ?>" />
    <input type="hidden" name="TBK_ORDEN_COMPRA" value="<?php echo $oc;?>" />
    <input type="hidden" name="TBK_ID_SESION" value="<?php echo $oc;?>" />
    <input type="hidden" name="URL_RETURN" value="https://forastero.life/tienda/webpay/tbk-normal.php?action=result" />
    <input type="hidden" name="URL_FINAL" value="https://forastero.life/tienda/webpay/tbk-normal.php?action=end" />
</form>

<script type="text/javascript">
$(function(){
	//setTimeout(function() {
		$('#form').submit();
	//},3000);
});
</script>
