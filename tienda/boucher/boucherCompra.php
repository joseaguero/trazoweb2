<?php
include("../../admin/conf.php");
require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
$eoc = consulta_bd("oc","pedidos","oc='$oc'","");
$existeOC = mysqli_affected_rows($conexion);
	
if($existeOC == 0){
	    echo '<script>parent.location = "'.$url_base.'404";</script>';
 }else{

		$campos = "id,nombre,direccion,email,telefono,";//0-4
		$campos.= "regalo,total,valor_despacho,transaction_date,";//5-8
		$campos.= "amount, card_number,shares_number,authorization_code,payment_type_code,cliente_id,";//9-14
		$campos.= "comuna,region,ciudad,estado_id,";//15-18
		$campos.= "factura, direccion_factura, giro, email_factura, rut_factura, fecha_modificacion, region,ciudad,estado_id, rut";//19-30

		$pedido = consulta_bd($campos,"pedidos","oc='$oc'","");

	 	 //detalle comprador
	     $direccion_cliente = $pedido[0][2];
	     $comuna_cliente    = $pedido[0][15];
	     $region_cliente    = $pedido[0][16];
	     $ciudad_cliente    = $pedido[0][17];
		 $localidad_cliente    = $pedido[0][29];

	     //detalle pedido
		 $pedido_id 	   = $pedido[0][0];
	     $subtotal 	   = $pedido[0][6];
		 $total_pedido 	   = $pedido[0][9];
	     $total_despacho   = $pedido[0][7];
	     $fecha 		   = $pedido[0][8];
	     

	     //detalle transbank
	     $num_tarjeta 	   = $pedido[0][10];
	     $num_cuotas 	   = $pedido[0][11];
	     $cod_aurizacion   = $pedido[0][12];
	     $tipo_pago 	   = $pedido[0][13];

	     //USER
	     $id_usuario	   = $pedido[0][14];
	     //$info = info_user($id_usuario);

	     //validar si esta pagado
	     $validador = $pedido[0][18];
	     $contador_validador = count($validador);

	     //total
   		 $total_productos = $pedido[0][6];
   		 $total_pagado = number_format($total_pedido,0,",",".");

   		 //enviar comprobante
	    
		//funcion tipo pago
   		$tipo_pago = tipo_pago($tipo_pago,$num_cuotas);

   		//detalle productos
   		$detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario","productos_pedidos","pedido_id=".$pedido[0][0],"");
		
		
		
 }





$html = '<body>
	<div class="contenedor">
    	<div class="header"><img src="logoCliente.jpg" width="187" /></div>
        <h2>¡MUCHAS GRACIAS POR COMPRAR CON NOSOTROS!</h2>
        <h3>SU ORDEN DE COMPRA ES:</h3>
        <h4>'.$oc.'</h4>
        
        <div class="titulo"><span>DATOS DE COMPRA</span></div>
        
        <div class="filaDato">
        	<div class="tituloDato">Nombre comprador:</div>
            <div class="valorDato">'.$pedido[0][1].'</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Rut:</div>
            <div class="valorDato">'.$pedido[0][28].'</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Fecha y hora de Venta:</div>
            <div class="valorDato">'.$fecha.'</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Tipo de transacción:</div>
            <div class="valorDato">Venta</div>
        </div>
        
        <div class="filaDato">
        	<div class="tituloDato">Tipo de pago:</div>
            <div class="valorDato">'.$tipo_pago['tipo_pago'].'</div>
        </div>
        
        <div class="filaDato filaGris">
        	<div class="tituloDato">Tarjeta terminada en:</div>
            <div class="valorDato">**** **** **** '.$num_tarjeta.'</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Número de coutas:</div>
            <div class="valorDato">'.$tipo_pago['cuota'].'</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Valor pagado:</div>
            <div class="valorDato">$'.$total_pagado.'</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Código de autorización:</div>
            <div class="valorDato">'.$cod_aurizacion.'</div>
        </div>
        <div class="totales">
         	<div class="filaDato">
            	<div class="valorDato">'.$pedido[0][31].'</div>
                <div class="tituloDato">cantidad de productos:</div>
            </div>
			
		
		 
            <div class="filaDato">
            	<div class="valorDato">$'.number_format($subtotal,0,",",".").'</div>
                <div class="tituloDato">Sub total:</div>
            </div>';
			if($descuento != 0){
				$html .= '<div class="filaDato">
							<div class="valorDato">$'.number_format($descuento,0,",",".").'</div>
							<div class="tituloDato">Descuento:</div>
						</div>';
			}
            
			
			if($giftcard != 0){
				$html .= '<div class="filaDato">
							<div class="valorDato">$'.number_format($giftcard,0,",",".").'</div>
							<div class="tituloDato">Giftcard:</div>
						</div>';
			}
			
			
  $html .= '<div class="filaDato">
            	<div class="valorDato">$'.number_format($total_despacho,0,",",".").'</div>
                <div class="tituloDato">Valor envío:</div>
            </div>
            <div class="filaDato">
            	<div class="valorDato total">$'.number_format($total_pedido,0,",",".").'</div>
                <div class="tituloDato total">Total</div>
            </div>
        </div><!--fin totales-->
        
        
        <div class="footer">
        	Todos los derechos reservados. TRAZO
        </div>
        
    </div>
</body>';
//==============================================================
//==============================================================
//==============================================================

include("mpdf60/mpdf.php");

$mpdf=new mPDF(); 

$mpdf->SetDisplayMode('fullpage');

// LOAD a stylesheet
$stylesheet = file_get_contents('estilos.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html);
$mpdf->Output();

exit;
//==============================================================
//==============================================================
//==============================================================

?>