<?php 
//Funciones
function valorGrilla($id){
	$monto = consulta_bd("precio,descuento","productos_detalles","producto_id=$id and publicado = 1","precio asc");

	if(!is_array($monto)){
		return "vacio";
	}

	$monto = consulta_bd("MIN(if(descuento > 0, descuento,precio)) as valorMenor","productos_detalles","producto_id=$id and publicado = 1","");
	$montoFinal = consulta_bd("precio,descuento","productos_detalles","producto_id = $id and (precio =".$monto[0][0]." or descuento =".$monto[0][0].")","");
		
	if($cantInt > 1){
		$desde = 'Desde ';
	} else {
		$desde = '';
	}
	
	if($montoFinal[0][1] > 0){
		$valores = '<span class="ahora">$'.number_format($montoFinal[0][1],0,",",".").'</span> - <span class="antes">$'.number_format($montoFinal[0][0],0,",",".").'</span>';
	} else {
		$valores = $desde.'$'.number_format($montoFinal[0][0],0,",",".").'';
	}
	
	return $valores;
}



//Funciones
function precioProductoBase($id){
	$monto = consulta_bd("precio,descuento","productos_detalles","id=$id and publicado = 1","");
	
	if(!is_array($monto)){
		return "vacio";
	}
	
	if($monto[0][1] > 0){
		$valores = '<span class="ahoraFicha">$'.number_format($monto[0][1],0,",",".").'</span> - <span class="antesFicha">$'.number_format($monto[0][0],0,",",".").'</span>';
	} else {
		$valores = '$'.number_format($monto[0][0],0,",",".").'';
	}
	
	return $valores;
}



function ofertaGrilla($id){
	$monto = consulta_bd("MIN(if(descuento > 0, descuento,precio)) as valorMenor","productos_detalles","producto_id=$id and publicado = 1","");

	if(!is_array($monto)){
		return "vacio";
	}
	$montoFinal = consulta_bd("precio,descuento","productos_detalles","producto_id= $id and (precio =".$monto[0][0]." or descuento =".$monto[0][0].")","");
	
	if($montoFinal[0][1] > 0 and $montoFinal[0][1] < $montoFinal[0][0]){
		$valorDescuento = (1-($montoFinal[0][1] / $montoFinal[0][0]))*100;
		$valores = '<span class="triangulo"><span>-'.round($valorDescuento).'%</span></span>';
		return $valores;
	} else {
		$valores = '';
		return $valores;
	}
}

function skuGrilla($id){
	$monto = consulta_bd("MIN(if(descuento > 0, descuento,precio)) as valorMenor","productos_detalles","producto_id=$id and publicado = 1","");
	$montoFinal = consulta_bd("sku","productos_detalles","producto_id= $id and (precio =".$monto[0][0]." or descuento =".$monto[0][0].")","");
	$sku = $montoFinal[0][0];
	return $sku;
}

function agregarDesdeGrilla($id){
	$consulta = consulta_bd("count(id)", "productos_detalles", "producto_id = {$id}", "");
	if ($consulta[0][0] == 1) {
		return "<a href='' class='add-grilla mostrar'>Agregar al carro</a>";
	}
}

function cantMedidas($id){
	$consulta = consulta_bd("id", "productos_detalles", "producto_id = {$id} and publicado = 1 GROUP BY nombre", "");
	if (is_array($consulta)) {
		$cont = sizeof($consulta);	
	}else{
		$cont = 1;
	}
	return (int)$cont;
}

function ultimasUnidades($id){
	$consulta = consulta_bd("id", "productos_detalles", "producto_id = {$id} and publicado = 1 and stock <= 2", "");
	if (is_array($consulta)) {
		$cont = sizeof($consulta);
	}else{
		$cont = 0;
	}

	return (int)$cont;
}
?>