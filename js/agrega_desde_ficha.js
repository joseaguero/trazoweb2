$(function() {
    $('#btn_agregarCarro').click(function() {
        $(".cont_loading").fadeIn(200);
        var id_pro = $(this).attr('rel');
        var cantidad = parseInt($(".campoSpinner").val());
        console.log(cantidad);
        var cantActual = $(".cantItems").html();
        var sku = $('#contCodigo').html();
        $.ajax({
            type: 'POST',
            url: 'ajax/ws_stock.php',
            dataType: 'json',
            data: {
                sku: sku,
                qty: cantidad
            },
            cache: false,
            beforeSend: function(){
                $('#btn_agregarCarro').html('VERIFICANDO STOCK...');
            },
            success: function(resp) {
                console.log(resp);
                if (resp['status'] == 'success') {
                    $.ajax({
                        type: 'GET',
                        url: 'tienda/addCarro.php',
                        data: {
                            id: id_pro,
                            action: "add",
                            qty: cantidad
                        },
                        cache: false,
                        success: function() {
                            var cantTotal = parseInt(cantActual) + parseInt(cantidad);
                            $(".cantItems").html(cantTotal);
                            $(".totalHeader").load("tienda/totalCart.php");
                            $("#contMiniCart").load("tienda/miniCart.php");
                            $(".cont_loading").fadeOut(100);
                            alertify.success("Producto agregado con éxito.");
                            $('#btn_agregarCarro').html('AGREGAR AL CARRO');
                            $('#popup-producto').load('includes/popup-carro.php?id='+id_pro);
                            $('#popup-producto').fadeIn();
                            setTimeout(function(){
                                $('#popup-producto').fadeOut();
                            }, 3000);
                        }
                    });
                } else {
                    alertify.error("stock insuficiente");
                    $('#btn_agregarCarro').html('AGREGAR AL CARRO');
                    $(".cont_loading").fadeOut(100);
                };
            }
        }).done(function() {
            $(".cargador").fadeOut(200);
        }).fail(function(resp){
            console.log(resp);
        });
    });
})