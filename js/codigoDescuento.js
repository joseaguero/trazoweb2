$(".descuento").on('keyup', function(){
    var codigo = $(this).val();
    var localidad_id = $('#localidad').val();
    // console.log(localidad_id);
	// $("#loadingCheckDescuento").fadeIn(100);
	$.ajax({
		url         : "tienda/ajax_codigo_descuento.php",
		type        : "post",
		async       : true,
		data		: {codigo:codigo, localidad_id: localidad_id},
		cache: false,
		success     : function(resp){
			if(resp == 1){
				// console.log("codigo correcto");
				$(".codigoOK").addClass("aceptado");
				$(".codigoOK").html('<img src="img/aceptado.png" width="20" />');
				$(".descuento").attr('disabled', "disabled");
				var retiro;
				if ($('#tienda2').is(':checked')) {
					retiro = 0;
				}else{
					retiro = 1
				}

				$("#contDatosCompra").load("tienda/carroRetiroTienda.php?r="+retiro);
                
			}else if(resp == 2){
				$(".codigoOK").addClass("aceptado");
				$(".codigoOK").html('<img src="img/aceptado.png" width="20" />');
				$(".descuento").attr('disabled', "disabled");

				$("#contDatosCompra").load("tienda/carroRetiroTienda.php?r=0");
			}else {
				// console.log("codigo incorrecto");
				$(".codigoOK").removeClass("aceptado");
				$(".codigoOK").html('<img src="img/cancelado.png" width="20" />');
			};
		}
	});
});