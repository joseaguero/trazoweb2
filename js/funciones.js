// JavaScript Document




$(function(){
	//galeria en ficha
	$(".thumbsGal").click(function(){
		var imgActual = $(this).attr("rel");
		$("#imgGal").attr("href", imgActual);
		$("#imgGrandeGal").attr("src", imgActual);
	});

	$('input[name="rut"]').Rut({
	   on_error: function(){ 
	   				alertify.error('El rut ingresado es incorrecto'); 
					$('input[name="rut"]').addClass("incompleto");
					$('input[name="rut"]').val("");
				},
	   format_on: 'keyup',
	   on_success: function(){ $('input[name="rut"]').removeClass("incompleto");} 
	});
	
	
	//acordeon ficha
	/*$(".itemAcordeon").click(function(){
		$(".itemAcordeon").next("div").hide(300);
		$(this).next().show(300);
	});*/
	
	//spinner mas
	$(".masProductos").click(function(){
		var cant = parseInt($(".campoSpinner").val());
		var stock = parseInt($(".campoSpinner").attr("rel"));
		if(cant == stock){
		} else{
			$(".campoSpinner").val(cant+1);
		}
		
	});
	
	//spinner menos
	$(".menosProductos").click(function(){
		var cant = parseInt($(".campoSpinner").val());
		if(cant == 1){
		} else{
			$(".campoSpinner").val(cant-1);
		}
		
	});
	
	
	//Newsletter
	$(".btnNewsletter").click(function(){
		var correo = $(".campoNewsletter").val();
		
		if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
			alertify.error("Debe introducir un correo válido.");
		} else {
			$.ajax({
				url         : "ajax/newsletter.php",
				type        : "POST",
				async       : true,
				data		: {correo:correo},
				cache: false,
				success     : function(resp){
					if(resp == 1){
						alertify.success("Correo ingresado correctamente");
					} else {
						alertify.error("Su correo ya existe dentro de nuestros registros");
					}
				}
			});
		}
		
		
	});
	//Newsletter
	
	
	
	//cambios de productos dentro de una ficha por las medidas
	$(".cambioMedidas").click(function(){
		console.log("paso");
		var nombreFiltro = $(this).val();
		var producto_id = $(this).attr("rel");
		console.log(nombreFiltro +' - '+producto_id);
		$.ajax({
			url: 'ajax/cambioProductoMedidas.php',
			type        : "post",
			dataType	: "json",
			async       : true,
			data: {producto_id:producto_id, nombreFiltro:nombreFiltro},
			cache: false,
			success     : function(respuesta){
				
				var json_string = JSON.stringify(respuesta);
				//convertir el texto a un nuevo objeto
				var obj = $.parseJSON(json_string);

				// Producto guardado
				if (obj.producto_guardado) {
					$('.producto_guardado').html('<div class="red-color action_guardados" data-id="'+obj.id_prod+'" data-action="delete"><i class="fas fa-heart"></i> Producto guardado</div>');
				}else{
					$('.producto_guardado').html('<div class="action_guardados" data-id="'+obj.id_prod+'" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>');
				}

				reset_guardados();
				 
				$('#contCodigo').html(obj.sku);
				$(".valorFicha").html(obj.precio);
				$(".texturas").html(obj.terminaciones);
				$('#btn_agregarCarro').attr('rel', obj.id_prd);
				//if(obj.cantImg == 1){
					$('.galProducto').html(obj.galeria);
				//}
				$('.ancho').html('<strong>Ancho:</strong>'+obj.ancho+' Cm. <br>');
				$('.alto').html('<strong>Alto:</strong>'+obj.alto+' Cm. <br>');
				$('.largo').html('<strong>Largo:</strong>'+obj.largo+' Cm. <br>');
				
			}
		}).done(function(){
				$(".thumbsGal").click(function(){
					var imgActual = $(this).attr("rel");
					$("#imgGal").attr("href", imgActual);
					$("#imgGrandeGal").attr("src", imgActual);
				});
				var $uniformed = $("select, :radio").not(".skipThese");
				$uniformed.uniform();
				activaCambioTerminacion();
			});//fin ajax
	});
	//cambios de productos dentro de una ficha por las medidas
	
	
	//cambios de productos dentro de una ficha por la textura
	$(".texturas .radio .cambioProducto").click(function(){
		var idProducto = $(this).attr("rel");
		console.log(idProducto);
			
		$.ajax({
			url: 'ajax/cambioProducto.php',
			type        : "post",
			dataType	: "json",
			async       : true,
			data: {id:idProducto},
			cache: false,
			success     : function(respuesta){
				
				var json_string = JSON.stringify(respuesta);
				//convertir el texto a un nuevo objeto
				var obj = $.parseJSON(json_string);

				// Producto guardado
				if (obj.producto_guardado) {
					$('.producto_guardado').html('<div class="red-color action_guardados" data-id="'+obj.id_prod+'" data-action="delete"><i class="fas fa-heart"></i> Producto guardado</div>');
				}else{
					$('.producto_guardado').html('<div class="action_guardados" data-id="'+obj.id_prod+'" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>');
				}

				reset_guardados();
				 
				$('#contCodigo').html(obj.sku);
				$(".valorFicha").html(obj.precio);
				if(obj.cantImg == 1){
					$('.galProducto').html(obj.galeria);
				}
				$('#btn_agregarCarro').attr('rel', obj.id_prd);
				
			}
		}).done(function(){
				$(".thumbsGal").click(function(){
					var imgActual = $(this).attr("rel");
					$("#imgGal").attr("href", imgActual);
					$("#imgGrandeGal").attr("src", imgActual);
				});
				
			});//fin ajax
		
		
	});
	//cambios de productos dentro de una ficha por la textura
	
	function activaCambioTerminacion(){
		$(".texturas .radio .cambioProducto").click(function(){
		var idProducto = $(this).attr("rel");
		console.log(idProducto);
			
		$.ajax({
			url: 'ajax/cambioProducto.php',
			type        : "post",
			dataType	: "json",
			async       : true,
			data: {id:idProducto},
			cache: false,
			success     : function(respuesta){
				console.log("respuesta");
				var json_string = JSON.stringify(respuesta);
				//convertir el texto a un nuevo objeto
                
				var obj = $.parseJSON(json_string);

				if (obj.producto_guardado) {
					$('.producto_guardado').html('<div class="red-color action_guardados" data-id="'+obj.id_prod+'" data-action="delete"><i class="fas fa-heart"></i> Producto guardado</div>');
				}else{
					$('.producto_guardado').html('<div class="action_guardados" data-id="'+obj.id_prod+'" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>');
				}
				 
				$('#contCodigo').html(obj.sku);
				$(".valorFicha").html(obj.precio);
				if(obj.cantImg == 1){
					$('.galProducto').html(obj.galeria);
				}

				$('#btn_agregarCarro').attr('rel', obj.id_prd);
				
			}
		}).done(function(){
				$(".thumbsGal").click(function(){
					var imgActual = $(this).attr("rel");
					$("#imgGal").attr("href", imgActual);
					$("#imgGrandeGal").attr("src", imgActual);
				});
			});//fin ajax
		
		
	});
	}//fin funcion
	
	
	//Mostrar los destacados del home, 5 siguientes.
	$("#masDestacadosHome").click(function(){
		$(".destacadosOcultos").fadeIn(100);
		$("#masDestacadosHome").fadeOut(10);
	});
	
	
	//Oculto el buscador al hacer click en cualquier parte
	$('.buscar').click(function() {
	   $('.contBuscador').fadeIn(200);
	});
	
	$('.contBuscador').click(function() {
	   $('.contBuscador').fadeOut(200);
	});
	$('#buscador').click(function(event){
	   event.stopPropagation();
	});
	
	/*
$(".contBuscador").click(function(){
		$(".contBuscador").fadeOut(200);
	});
*/


	//paginador
	$("#cargarMasCategorias").click(function(){
		var pagina = parseInt($(this).attr("rel"));
		var x = 0;
		$(".page-"+pagina).each(function() {
		  x = x+1;
		  $(this).fadeIn(400);
		});
		$(this).attr("rel", pagina+1);
		if(x < 10){
			$(this).fadeOut(100);
		}
	});
	
	
	
	$(".btnMenu").click(function(){
			console.log("sdbfhtdlijacs");
			$(".menu").slideToggle(100);
			$(this).addClass("menuHover");
			
		});
	$(".menuHover").click(function(){
		$(this).removeClass("menuHover");
		$(".menu").slideToggle(100);
	});


	$('.close_menu').click(function(){

		$(".menuHover").removeClass("menuHover");
		$(".menu").slideToggle(100);

	});
		
	
	var ventana_alto = $(window).height();
	
	
	

	
	
});

//dejo fijo el header cuando el ancho es mayor a 480px;
		$(window).scroll(function(){
			var ventana_ancho = $(window).width();
			if(ventana_ancho > 480){
				//console.log(window.pageYOffset);
				if (window.pageYOffset >= 173) {
					$(".menuFantasma").css("display","block");
					$('.menu').addClass("menuFixed");
				} else {
					$('.menu').removeClass("menuFixed");
					$(".menuFantasma").css("display","none");
				}
			};
		});
		//fin de la funcion windows scroll

$('.add-grilla').on('click', function(e){
	e.preventDefault();
	var id_producto = $(this).attr('data-ide');
	var sku = $(this).attr('data-sku');
	var cantidad = 1;
	var cantActual = $(".cantItems").html();
	$.ajax({
		url: 'ajax/ws_stock.php',
		type: 'post',
		dataType: 'json',
		data: {sku: sku, qty: cantidad},
		success: function(re){
			if (re['status'] == 'success') {
				$.ajax({
					url: 'tienda/addCarro.php',
					type: 'GET',
					data: {
						id: id_producto,
						action: 'add',
						qty: cantidad
					},
					cache: false,
					success: function(re){
						var cantTotal = parseInt(cantActual) + parseInt(cantidad);
			            $(".cantItems").html(cantTotal);
			            $(".totalHeader").load("tienda/totalCart.php");
			            $("#contMiniCart").load("tienda/miniCart.php");
			            $('#popup-producto').load('includes/popup-carro.php?id='+id_producto);
			            $('#popup-producto').fadeIn();
			            setTimeout(function(){
							$('#popup-producto').fadeOut();
						}, 3000);
					}
				});
			}else{
				alertify.error('Error: '+ re['message']);
			}
		}
	});	
});

$('.spinn').on('click', function(e){
	e.preventDefault();
	var action = $(this).attr('data-action');
	var sku = $(this).parent().find('input').attr('data-sku');
	var input = $(this).parent().find('input');
	var qty = 1;
	var id_producto = $(this).parent().find('input').attr('data-id');
	var cantActual = $(".cantItems").html();
	if (action == 'add') {
		$.ajax({
			url: 'ajax/ws_stock.php',
			type: 'post',
			dataType: 'json',
			data: {sku: sku},
			success: function(re){
				if (re['status'] == 'success') {
					$.ajax({
						url: 'tienda/addCarro.php',
						type: 'GET',
						data: {
							id: id_producto,
							action: 'add',
							qty: qty
						},
						cache: false,
						success: function(re){
							var cantTotal = parseInt(cantActual) + parseInt(qty);
				            $(".cantItems").html(cantTotal);
				            $(".totalHeader").load("tienda/totalCart.php");
				            $("#contMiniCart").load("tienda/miniCart.php");

				            input.val(parseInt(input.val()) + 1);
				            
				            alertify.success('Producto agregao.');
				            location.reload();
						}
					});
				}else{
					alertify.error(re['message']);
				}
			}
		});
	}else if(action == 'remove'){
		if (parseInt(input.val()) > 1) {
			$.ajax({
				url: 'tienda/addCarro.php',
				type: 'GET',
				data: {
					id: id_producto,
					action: 'remove',
					qty: 1
				},
				cache: false,
				success: function(re){
					var cantTotal = parseInt(cantActual) - parseInt(qty);
		            $(".cantItems").html(cantTotal);
		            $(".totalHeader").load("tienda/totalCart.php");
		            $("#contMiniCart").load("tienda/miniCart.php");

		            input.val(parseInt(input.val()) - 1);
		            
		            alertify.success('Producto eliminado.');
		            location.reload();
				}
			});
		}
		
	}else if(action == 'delete'){
		$.ajax({
			url: 'tienda/addCarro.php',
			type: 'GET',
			data: {
				id: id_producto,
				action: 'delete',
				qty: 1
			},
			cache: false,
			success: function(re){
				var cantTotal = parseInt(cantActual) - parseInt(qty);
	            $(".cantItems").html(cantTotal);
	            $(".totalHeader").load("tienda/totalCart.php");
	            $("#contMiniCart").load("tienda/miniCart.php");

	            location.reload();
	            
	            alertify.success('Producto eliminado.');
			}
		});
	}
	
});

$('.btn-delete').on('click', function(e){
	e.preventDefault();
	var id_producto = $(this).attr('data-id');
	$('.pop_delete').attr('data-id', id_producto);
	$('.pop_delete').fadeIn();
	
});

$('.btn_confirm').on('click', function(e){
	e.preventDefault();
	var id_producto = $('.pop_delete').attr('data-id');
	var action = $(this).attr('data-action');
	if (action == 'accept') {
		$.ajax({
			url: 'tienda/addCarro.php',
			type: 'GET',
			data: {
				id: id_producto,
				action: 'delete',
				qty: 1
			},
			cache: false,
			success: function(re){

	            location.reload();
	            
	            alertify.success('Producto eliminado.');
			}
		});
	}else{
		$('.pop_delete').fadeOut();
	}
	
})

$('.contBuscador').click(function() {
	   $('.contBuscador').fadeOut(200);
	});
	$('#buscador').click(function(event){
	   event.stopPropagation();
	});
$(window).scroll(function(){
	var ventana_ancho = $(window).width();
	if(ventana_ancho > 929){
		//console.log(ventana_ancho);
		//console.log(window.pageYOffset);
		if (window.pageYOffset >= 173) {
			$(".menuFantasma").css("display","block");
			$('.menu').addClass("menuFixed");
			$('.buscador_fixed').css('display', 'block');
		} else {
			$('.menu').removeClass("menuFixed");
			$(".menuFantasma").css("display","none");
			$('.buscador_fixed').css('display', 'none');
		}
	};
});

var popup = setInterval(IsVisible, 3000);

function IsVisible(){
	if ($('.popup-news').is(':visible')){
		$(".bg-popup").click(function() {
			$('.bg-popup').fadeOut();
		    $('.popup-news').fadeOut();
		    Cookies.set('POPUPTRAZO','Newsletter (No Suscrito)',{expires: 14});
		});
	}
	clearInterval(popup);
}


$('.popup-news').click(function (e) {
    e.stopPropagation();
});
$(".popup-news .contenido > .close").on('click', function(e){
	Cookies.set('POPUPTRAZO','Newsletter (No Suscrito)',{expires: 14});
	$('.bg-popup').fadeOut();
    $('.popup-news').fadeOut();
})

$('#suscribe-newsletter').on('click', function(e){
	var nombre = $('#nombre-suscribe').val();
	var email = $('#email-suscribe').val();

	if (nombre.trim() != '' && email.trim() != '') {
		$.ajax({
			url: 'ajax/popup.php',
			type: 'post',
			data: {nombre: nombre, email: email},
			success: function(res){
				Cookies.set('POPUPTRAZO','Newsletter Suscrito',{expires: 90});
				$('.bg-popup').fadeOut();
	    		$('.popup-news').fadeOut();
	    		alertify.success('Gracias por suscribirte.');
			}
		}).fail(function(res){
			console.log(res);
		})
	}else{
		alertify.error('Debes ingresar nombre y email.');
	}

})

$('.rec-password').on('click', function(){
	$('.pop-rec').fadeIn(300);
	$('.bgpop-rec').fadeIn(300);
})

$('.bgpop-rec').on('click', function(){
	$('.pop-rec').fadeOut(300);
	$('.bgpop-rec').fadeOut(300);
})

$('.btn-rec').on('click', function(){
	var email = $('.email-rec').val();

	if (email.trim() != '') {

		$.ajax({
			url: 'ajax/recuperar-password.php',
			type: 'post',
			data: { email: email },
			beforeSend: function(){
				$('.btn-rec').val('Enviando...');
			}
		}).done(function(res){
			$('.btn-rec').val('Recuperar contraseña');
			if (res == 1) {
				$('.pop-rec').fadeOut(300);
				$('.bgpop-rec').fadeOut(300, function(){
					alertify.success('Nueva contraseña enviada correctamente');
				})
				
			}else{
				alertify.error('Hubo un error al enviar su nueva contraseña, intente mas tarde.');
			}
		}).fail(function(res){
			console.log(res);
		})

	}else{

		alertify.error('Debe completar todos los campos');

	}
})

$('.login_form').on('submit', function(){
	var email = $('.email_login').val(),
		pass = $('.pass_login').val();

	if (email.trim() != '' && pass.trim() != '') {

		if (validarEmail(email)) {
			return true;
		}else{
			alertify.error('Email ingresado es incorrecto.');
		}

	}else{
		alertify.error('Debes completar todos los campos.');
	}

	return false;
})

$('.registro_form').on('submit', function(){

	var nombre = $(this).find('input[name="nombre"]').val(),
		apellido = $(this).find('input[name="apellido"]').val(),
		email = $(this).find('input[name="email"]').val(),
		rut = $(this).find('input[name="rut"]').val(),
		telefono = $(this).find('input[name="telefono"]').val(),
		password = $(this).find('input[name="password"]').val(),
		password_rec = $(this).find('input[name="password-rec"]').val();

		if (nombre.trim() != '' && apellido.trim() != '' && email.trim() != '' && rut.trim() != '' && telefono.trim() != '' && password.trim() != '' && password_rec.trim() != '') {

			if (validarEmail(email)) {
				if (validarFono(telefono)) {
					return true;
				}else{
					alertify.error("Teléfono ingresar es incorrecto.");
				}
			}else{
				alertify.error("Email ingresado es incorrecto.");
			}

		}else{
			alertify.error("Debe completar todos los campos.");
		}

	return false;
})

$('.btn-change-pass').on('click', function(){
	$('.bg_change_password').fadeIn();
	$('.content_change').fadeIn();
})

$('.bg_change_password').on('click', function(){
	$('.bg_change_password').fadeOut();
	$('.content_change').fadeOut();
})

$('.btn-change-password').on('click', function(){
	var pass_actual = $('input[name="password_actual"]').val(),
		pass_nueva = $('input[name="password_nueva"]').val(),
		pass_nueva_re = $('input[name="password_nueva_re"]').val(),
		id_cliente = $(this).attr('data-id');

	if (pass_actual.trim() != '' && pass_nueva.trim() != '' && pass_nueva_re.trim() != '') {

		if (pass_nueva === pass_nueva_re) {

			$.ajax({
				url: 'ajax/change_password.php',
				type: 'post',
				dataType: 'json',
				data: { password_actual: pass_actual, password_nueva: pass_nueva, password_nueva_re: pass_nueva_re, id_cliente: id_cliente }
			}).done(function(res){
				if (res['status'] == 'success') {
					$('input[name="password_actual"]').val('');
					$('input[name="password_nueva"]').val('');
					$('input[name="password_nueva_re"]').val('');

					$('.bg_change_password').fadeOut();
					$('.content_change').fadeOut();

					alertify.success(res['message']);

				}else{
					alertify.error(res['message']);
				}
			}).fail(function(res){
				console.log(res);
			})

		}else{
			alertify.error('Las contraseñas no coinciden');
		}

	}else{
		alertify.error('Debe completar todos los campos.');
	}
})

$('.nuevaDireccion').on('click', function(){
	$('.bg_change_password').fadeIn();
	$('.content_change').fadeIn();

	$('input[name="nombre"]').val('');
	$('input[name="direccion"]').val('');

	$('select[name="comuna"]').html('<option value="0">Seleccionar comuna</option>');
	$('select[name="localidad"]').html('<option value="0">Seleccionar localidad</option>');

	$('select[name="region"]').val(0);
	$('select[name="comuna"]').val(0);
	$('select[name="localidad"]').val(0);

	$('#uniform-comuna').find('span').html('Seleccionar comuna');
	$('#uniform-localidad').find('span').html('Seleccionar localidad');
	$('#uniform-region').find('span').html('Seleccionar región');

	$('.btn-address').html('Agregar');
	$('.btn-address').attr('data-dir', 0);
	$('.btn-address').attr('data-action', 'create');

	$('.content_change h2').html('Agregar nueva dirección');

})

$('.btn-address').on('click', function(){
	var action = $(this).attr('data-action'),
		user_id = $(this).attr('data-id')
		id_direccion = $(this).attr('data-dir');

	var nombre = $('input[name="nombre"]').val(),
		region_id = $('select[name="region"]').val(),
		comuna_id = $('select[name="comuna"]').val(),
		localidad_id = $('select[name="localidad"]').val(),
		direccion = $('input[name="direccion"]').val();

	if (nombre.trim() != '' && region_id != 0 && comuna_id != 0 && localidad_id != 0 && direccion.trim() != '' ) {

		$.ajax({
			url: 'ajax/direcciones.php',
			type: 'post',
			dataType: 'json',
			data: {  action: action, user_id: user_id, nombre: nombre, region: region_id, comuna: comuna_id, localidad: localidad_id, direccion: direccion, id_direccion: id_direccion }
		}).done(function(res){

			if (res['status'] == 'success') {
				location.reload();
			}else{
				alertify.error('Hubo un error al realizar la acción');
			}

		}).fail(function(res){
			console.log(res);
		})

	}else{
		alertify.error('Debes completar todos los campos.');
	}

})

$('.edit_address').on('click', function(){
	var id = $(this).attr('data-id');

	$.ajax({
		url: 'ajax/direcciones.php',
		type: 'post',
		dataType: 'json',
		data: { action: 'show_dir', id_direccion: id }
	}).done(function(res){
		console.log(res);

		$('input[name="nombre"]').val(res[0]['nombre']);
		$('input[name="direccion"]').val(res[0]['calle']);

		for (var i = 0; i < res[0]['regiones'].length; i++) {
			if (res[0]['regiones'][i]['checked']) {
				$('#uniform-region').find('span').html(res[0]['regiones'][i]['nombre']);
				$('select[name="region"]').val(res[0]['regiones'][i]['id']);
			}
		}

		$('select[name="comuna"]').html('<option value="0">Seleccionar comuna</option>');
		for (var i = 0; i < res[0]['comunas'].length; i++) {
			$('select[name="comuna"]').append('<option value="'+res[0]['comunas'][i]['id']+'">'+res[0]['comunas'][i]['nombre']+'</option>');

			if (res[0]['comunas'][i]['checked']) {
				$('#uniform-comuna').find('span').html(res[0]['comunas'][i]['nombre']);
				$('select[name="comuna"]').val(res[0]['comunas'][i]['id']);
			}
		}

		$('select[name="localidad"]').html('<option value="0">Seleccionar localidad</option>');
		for (var i = 0; i < res[0]['localidades'].length; i++) {
			$('select[name="localidad"]').append('<option value="'+res[0]['localidades'][i]['id']+'">'+res[0]['localidades'][i]['nombre']+'</option>');

			if (res[0]['localidades'][i]['checked']) {
				$('#uniform-localidad').find('span').html(res[0]['localidades'][i]['nombre']);
				$('select[name="localidad"]').val(res[0]['localidades'][i]['id']);
			}
		}

		$('.btn-address').html('Editar');
		$('.btn-address').attr('data-dir', res[0]['id']);
		$('.btn-address').attr('data-action', 'edit');

		$('.content_change h2').html('Editar dirección');

		$('.bg_change_password').fadeIn();
		$('.content_change').fadeIn();

	}).fail(function(res){
		console.log(res);
	})

})

$('.delete_address').on('click', function(){
	var id = $(this).attr('data-id');

	$.ajax({
		url: 'ajax/direcciones.php',
		type: 'post',
		dataType: 'json',
		data: { action: 'delete', id_direccion: id }
	}).done(function(res){
		if (res['status'] == 'success') {
			location.reload();
		}else{
			alertify.error('Error al eliminar la dirección');
		}
	}).fail(function(res){
		console.log(res);
	})
})

$('.repetir_compra').click(function(e) {
    $(".cont_loading").fadeIn(200);
    var id_pro = $(this).attr('data-id');
    var cantidad = $(this).attr('data-qty');
    var sku = $(this).attr('data-sku');
    var cantActual = $(".cantItems").html();
    // console.log(cantidad);
    $.ajax({
        url: 'ajax/ws_stock.php',
        type: 'post',
        dataType: 'json',
        data: {sku: sku, qty: cantidad},
        success: function(re) {
            // console.log(re);
            if (re['status'] == 'success') {
                $.ajax({
                    type: 'GET',
                    url: 'tienda/addCarro.php',
                    data: {
                        id: id_pro,
                        action: "add",
                        qty: cantidad
                    },
                    cache: false,
                    success: function() {
                        var cantTotal = parseInt(cantActual) + parseInt(cantidad);
                        $(".cantItems").html(cantTotal);
                        $(".totalHeader").load("tienda/totalCart.php");
                        $("#contMiniCart").load("tienda/miniCart.php");
                        $(".cont_loading").fadeOut(100);
                        /*abro pop up*/
                        $.ajax({
								type: 'GET',
								url: 'pags/popUpAddToCart.php',
								data: {id:id_pro, qty:cantidad},
								cache: false,
								 success:function(resp){
									 $("#popUp").html(resp);
									 $("#popUp2").html(resp);
									 $(".fondoPopUp").fadeIn(100);
								 }
							 }).done(function(){
								 setTimeout(function(){ $(".fondoPopUp").fadeOut(200); }, 3000);
								 });
                        /*alertify.success("Producto agregado con éxito.");*/
                    }
                });
            } else {
                alertify.error('Error: '+ re['message']);
            };
        }
    }).done(function() {
        $(".cargador").fadeOut(200);
    }).fail(function (res) {
        console.log(res);
    });
});

$('.btn_mover_cart').click(function(e) {
    $(".cont_loading").fadeIn(200);
    var id_pro = $(this).attr('data-id');
    var cantidad = 1;
    var sku = $(this).attr('data-sku');
    var cantActual = $(".cantItems").html();

    var ts = $(this);
    // console.log(cantidad);
    $.ajax({
        url: 'ajax/ws_stock.php',
        type: 'post',
        dataType: 'json',
        data: {sku: sku, qty: cantidad},
        beforeSend: function(){
        	ts.html('Validando stock...');
        },
        success: function(re) {
            // console.log(re);
            if (re['status'] == 'success') {
                $.ajax({
                    type: 'GET',
                    url: 'tienda/addCarro.php',
                    data: {
                        id: id_pro,
                        action: "add",
                        qty: cantidad
                    },
                    cache: false,
                    success: function() {
                        var cantTotal = parseInt(cantActual) + parseInt(cantidad);
                        $(".cantItems").html(cantTotal);
                        $(".totalHeader").load("tienda/totalCart.php");
                        $("#contMiniCart").load("tienda/miniCart.php");
                        $(".cont_loading").fadeOut(100);
                        alertify.success("Producto agregado con éxito.");
                        $('#btn_agregarCarro').html('AGREGAR AL CARRO');
                        $('#popup-producto').load('includes/popup-carro.php?id='+id_pro);
                        $('#popup-producto').fadeIn();
                        setTimeout(function(){
                            $('#popup-producto').fadeOut();
                        }, 3000);
                    }
                });
            } else {
                alertify.error('Error: '+ re['message']);
            };
            ts.html('Mover al carro');
        }
    }).done(function() {
        $(".cargador").fadeOut(200);
    }).fail(function (res) {
        console.log(res);
    });
});

$('.action_guardados').on('click', function(){
	var producto_id = $(this).attr('data-id')
		action = $(this).attr('data-action');

	$.ajax({
		url: 'ajax/producto-guardado.php',
		type: 'post',
		dataType: 'json',
		data: { id: producto_id, action: action }
	}).done(function(res){
		console.log(res);
		if (res['status'] == 'success') {

			if (action == 'save') {
				$('.producto_guardado').html('<div class="red-color action_guardados" data-id="'+producto_id+'" data-action="delete"><i class="fas fa-heart"></i> Producto guardado</div>');
			}else{
				$('.producto_guardado').html('<div class="action_guardados" data-id="'+producto_id+'" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>');
			}

		}else{
			alertify.error(res['message']);
		}
	}).fail(function(res){
		console.log(res);
	})

})

function reset_guardados(){
	$('.action_guardados').on('click', function(){
		var producto_id = $(this).attr('data-id')
			action = $(this).attr('data-action');

		$.ajax({
			url: 'ajax/producto-guardado.php',
			type: 'post',
			dataType: 'json',
			data: { id: producto_id, action: action }
		}).done(function(res){
			console.log(res);
			if (res['status'] == 'success') {

				if (action == 'save') {
					$('.producto_guardado').html('<div class="red-color action_guardados" data-id="'+producto_id+'" data-action="delete"><i class="fas fa-heart"></i> Producto guardado</div>');
				}else{
					$('.producto_guardado').html('<div class="action_guardados" data-id="'+producto_id+'" data-action="save"><i class="far fa-heart"></i> Guardar para después</div>');
				}

			}else{
				alertify.error(res['message']);
			}
		}).fail(function(res){
			console.log(res);
		})

	})
}

$('.delete_guardado').on('click', function(){
	var producto_id = $(this).attr('data-id')
		action = 'delete';

	$.ajax({
		url: 'ajax/producto-guardado.php',
		type: 'post',
		dataType: 'json',
		data: { id: producto_id, action: action }
	}).done(function(res){
		console.log(res);
		if (res['status'] == 'success') {

			location.reload();

		}else{
			alertify.error(res['message']);
		}
	}).fail(function(res){
		console.log(res);
	})

})

$('#direcciones').on('change', function(){
	var id = $(this).val();

	if (id != 0) {
		$.ajax({
			url: 'ajax/direcciones.php',
			type: 'post',
			dataType: 'json',
			data: { action: 'show_dir', id_direccion: id }
		}).done(function(res){
			// console.log(res);

			$('input[name="direccion"]').val(res[0]['calle']);

			for (var i = 0; i < res[0]['regiones'].length; i++) {
				if (res[0]['regiones'][i]['checked']) {
					$('#uniform-region').find('span').html(res[0]['regiones'][i]['nombre']);
					$('select[name="region"]').val(res[0]['regiones'][i]['id']);
				}
			}

			$('select[name="comuna"]').html('<option value="0">Seleccionar comuna</option>');
			for (var i = 0; i < res[0]['comunas'].length; i++) {
				$('select[name="comuna"]').append('<option value="'+res[0]['comunas'][i]['id']+'">'+res[0]['comunas'][i]['nombre']+'</option>');

				if (res[0]['comunas'][i]['checked']) {
					$('#uniform-comuna').find('span').html(res[0]['comunas'][i]['nombre']);
					$('select[name="comuna"]').val(res[0]['comunas'][i]['id']);
				}
			}

			$('select[name="localidad"]').html('<option value="0">Seleccionar localidad</option>');
			for (var i = 0; i < res[0]['localidades'].length; i++) {
				$('select[name="localidad"]').append('<option value="'+res[0]['localidades'][i]['id']+'">'+res[0]['localidades'][i]['nombre']+'</option>');

				if (res[0]['localidades'][i]['checked']) {
					var localidadSeleccionada = res[0]['localidades'][i]['id'];
					$('#uniform-localidad').find('span').html(res[0]['localidades'][i]['nombre']);
					$('select[name="localidad"]').val(res[0]['localidades'][i]['id']);
				}
			}

			$.ajax({
	            type: 'POST',
	            url: 'tienda/precioCompraRapida.php',
	            data: {
	                localidad: localidadSeleccionada
	            },
	            dataType: 'JSON',
	            success: function(data) {}
	        }).done(function(data) {
	            var json_string = JSON.stringify(data);
	            var obj = $.parseJSON(json_string);
	            $('.mensajesDespacho').html(obj.msj);
	            $('#precio_bottom').text(obj.valor);
	            $("#totalConEnvio").html(obj.totalActual);
	            $(".descuento").html(obj.descuento);
	            if (obj.valorEnvio != 0) {
	                $("#totalConEnvio").html(obj.totalActual);
	            }
	        });

		}).fail(function(res){
			console.log(res);
		})
	}else{
		$('#uniform-region').find('span').html('Seleccione region');
		$('#region').val(0);

		$('#uniform-comuna').find('span').html('Seleccione comuna');
		$('#comuna').html('<option value="0">Seleccione comuna</option>');
		$('#comuna').val(0);

		$('#uniform-localidad').find('span').html('Seleccione localidad');
		$('#localidad').html('<option value="0">Seleccione localidad</option>');
		$('#localidad').val(0);
	}
})

function validate(){
	$.ajax({
		url: 'ajax/validarStockCart.php',
		type: 'post',
		dataType: 'json',
		data: { action: 'verificar' },
		beforeSend: function(){
			$('.btnCompletarCompra').html('Verificando stock...');
		}
	}).done(function(res){
		// console.log(res);
		if (res['errores'] == 0) {
			location.href = 'envio-y-pago';
		}else{
			location.reload();
		}
		$('.btnCompletarCompra').html('Completar compra');
	}).fail(function(res){
		console.log(res);
	})
}

function validarEmail(valor) {
	emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	if (emailRegex.test(valor)){
		return true;
	}else {
		return false;
	}
}
function validarFono(valor){
	var re = /^([0-9]){8,12}$/;
	
	if (re.test(valor)) {
		return true;
	}else{
		return false;
	}
}