// JavaScript Document
//validacion de paso compra rapida
$(function(){
	
	//abro el resumen del carro desde el header
	$(".abrirCarro").click(function(){
		$(".resumenCarroHeader").fadeIn(200);
	});
	//Cierro el resumen del carro desde el header
	$(".cerrarMenuCart").click(function(){
		$(".resumenCarroHeader").fadeOut(200);
	});
	
	
	
		//valido correo y creo session con el correo
		//paso 1 identificacion
		$(".btnFormCompraRapida").click(function(){
			var correoCompraRapida = $("#emailRapido").val();
			if(correoCompraRapida.indexOf('@', 0) == -1 || correoCompraRapida.indexOf('.', 0) == -1) {
				//alertify.log("Ingrese un correo válido");
				console.log("Ingrese un correo válido");
				$("#emailRapido").val("");
			} else {
				$.ajax({
					url      : "ajax/correoCompraRapida.php",
					type     : "post",
					async    : true,
					data 	 : {correo : correoCompraRapida},
					success     : function(resp){}
				});// fin ajax	
				$(".formCompraRapida1").submit();
			}
		});




	
	
	
	
	
	$("#uniform-facturaSi").click(function(){
		$(".contDatosFactura").fadeIn(100);
	});
	$("#uniform-facturaNo").click(function(){
		$(".contDatosFactura").fadeOut(100);
	});
	
	/* $("#modificarDatos").click(function(){
		console.log("sdsdsd");
		var idCliente  = parseInt($(this).attr("rel"));
		console.log(idCliente);	
	}); */


	$("#modificarDatos2").click(function(){
		$("#modificarDatos").trigger("click");
	});	


$("#tienda1").click(function(){
	$("#tabsCompra1").fadeOut(100);
	$("#tabsCompra2").fadeIn(100);
	console.log("cargo los valores por ajax");
	$("#contDatosCompra").load("tienda/carroRetiroTienda.php");
	console.log($("#localidad").val());
});


	/* Funciones desde el pop up */
	$("#btnIngresarCompraRapida").click(function(){
		var usuario = $("#usuarioLogin").val();
		var pass = $("#passLogin").val();
		console.log(usuario+" - "+pass);
		if(pass != ''){
			console.log("tomo los datos y los envio por ajax");
			$.ajax({
				type: 'POST',
				url: 'tienda/loginCompraRapida.php',
				data: {correo: usuario, clave:pass},
				dataType: 'JSON',
				success: function(data){
					//console.log(data);
					if(data == 1){
						alertify.success("inicio de sesión exitoso.");
						//desactivo los input y oculto los div que cubren los input
						$(".tipoBlanco").removeClass("tipoBlanco");
						
						//$("input:[name='nombre'], input:[name='telefono'], input:[name='rut'], input:[name='direccion']").removeAttr("disabled");
						$("#nombre, #telefono, #rut, #direccion").removeAttr("disabled");
						$("#razonSocial, #direccionFactura, #RutFactura, #emailFactura, #giro").removeAttr("disabled");
						$(".campoFantasma").fadeOut(10);
						
						$(".campoFantasma").each(function(){
							$(this).css("display", "none");
						});
						$(".contRut").fadeIn(100);
						$("#modificarDatos").fadeOut(10);
						$(".fancybox-close").trigger("click");
					} else {
						alertify.error("usuario o contraseña no coinciden, si no conoce su clave puede solicitar una.");
					}
				}
			});//fin ajax
		}else{
			alertify.error("Debe ingresar una contraseña válida.");
		}
	});
	
	
	
	
	
	$("#solicitarClave").click(function(){
		var usuario = $("#usuarioLogin").val();
		
		$.ajax({
				type: 'POST',
				url: 'tienda/recuperarClaveCompraRapida.php',
				data: {correo: usuario},
				dataType: 'JSON',
				success: function(data){
					//console.log(data);
					if(data == 1){
						alertify.success("Revisa tu correo, te enviamos una nueva clave.");
					} else if (data == 2){
						alertify.error("No fue posible enviarte una nueva clave en estos momentos, intenta más tarde.");
					} else if(data == 3){
						alertify.error("El correo ingresado no se encuentra en nuestros registros.")
					}
				}
			});//fin ajax
	});

/* Fin Funciones desde el pop up */


$("#tienda2").click(function(){
	$("#tabsCompra2").fadeOut(100);
	$("#tabsCompra1").fadeIn(100);
	var localidadSeleccionada = $("#localidad").val();
	console.log("abajo muestro localidad");
	console.log($("#localidad").val());
	if(localidadSeleccionada != '0' && localidadSeleccionada != '' && localidadSeleccionada != 'Localidades' && localidadSeleccionada != 'Seleccione Localidad.'){
		$.ajax({
			type: 'POST',
			url: 'tienda/precioCompraRapida.php',
			data: {localidad:localidadSeleccionada},
			dataType: 'JSON',
			success: function(data){
			}
		}).done(function (data){
			var json_string = JSON.stringify(data);
			var obj = $.parseJSON(json_string);
			$('.mensajesDespacho').html(obj.msj);
			$('#precio_bottom').text(obj.valor);
			/*
if(obj.valor == '$0'){
				$('#precio_bottom').html("<span class='rojo'>$0</span>");
			} else {
				$('#precio_bottom').text(obj.valor);
			}
*/
			$("#totalConEnvio").html(obj.totalActual);
			$(".descuento").html(obj.descuento);
			if(obj.valorEnvio != 0){
				$("#totalConEnvio").html(obj.totalActual);
			}
		});
	}
	
});

// regiones a comunas
$("#region").change(function(){
	var region_id = $(this).val();
	$("#comuna").load("tienda/comunas.php?region_id="+region_id);
	$("#uniform-comuna span").html("Seleccione comuna");
	$("#uniform-localidad span").html("Localidades");
});

// comunas a localidades
$("#comuna").change(function(){
	var comuna_id = $(this).val();
	console.log(comuna_id);
	$("#localidad").load("tienda/localidades.php?comuna_id="+comuna_id);
	$("#uniform-localidad span").html("Seleccione Localidad.");
});


$("#localidad").change(function(){
	var localidadSeleccionada = $(this).val();
	///console.log(comunaSeleccionada);
	$.ajax({
		type: 'POST',
		url: 'tienda/precioCompraRapida.php',
		data: {localidad:localidadSeleccionada},
		dataType: 'JSON',
		success: function(data){
		}
	}).done(function (data){
		var json_string = JSON.stringify(data);
		var obj = $.parseJSON(json_string);
		$('.mensajesDespacho').html(obj.msj);
		$('#precio_bottom').text(obj.valor);
		/*
if(obj.valor == '$0'){
			$('#precio_bottom').html("<span class='rojo'>Por pagar</span>");
		} else {
			$('#precio_bottom').text(obj.valor);
		}
*/
		$("#totalConEnvio").html(obj.totalActual);
		$(".descuento").html(obj.descuento);
		if(obj.valorEnvio != 0){
			$("#totalConEnvio").html(obj.totalActual);
		}
	});
})



	$("#comprar").click(function(){
		//console.log("presiono el boton");
		
		var nombre = $("#nombre").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var run = $("#Rut").val();
		
		var region = $("#region").val();
		var comuna = $("#comuna").val();
		var localidad = $("#localidad").val();
		var direccion = $("#direccion").val();
		var quien_recibe = $("#quien_recibe").val();
		//console.log("Datos comprador: "+nombre +' - '+ email +' - '+ telefono +' - '+ run);
		//console.log("Datos despacho: "+region +' - '+ comuna +' - '+ direccion +' - '+ quien_recibe);
		
		//Consulto por los campos de usuario
		if(nombre !='' && email != '' && telefono != '' && run!= ''){
			//valido el correo
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				//correo no es valido
				console.error("Correo inválido.");
				$("#email").addClass("incompleto");
			}else{
				//Correo de usuario es valido reviso las clases y desactivo los que estan con rojo
				if(nombre !=''){$("#nombre").removeClass("incompleto");} else {$("#nombre").addClass("incompleto");}
				if(email !=''){$("#email").removeClass("incompleto");} else {$("#email").addClass("incompleto");}
				if(telefono !=''){$("#telefono").removeClass("incompleto");} else {$("#telefono").addClass("incompleto");}
				if(run !=''){$("#Rut").removeClass("incompleto");} else {$("#Rut").addClass("incompleto");}
				
				//consulto si se retira en tienda o a direccion
				if($("#tienda2").is(':checked')){
					console.log("activado servicio de despacho");
					console.log(localidad);
					console.log("lkjdhsckjsnfckjsdncksejn" + $("#localidad").val());
					//valido las comunas y regiones de despacho
					if(region != '' && region != 'Seleccione region' && region != '0' && comuna != '0' && comuna != '' && direccion != '' && localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null){
						//estan todos los campos de despacho llenos
						if(direccion !=''){$("#direccion").removeClass("incompleto");} else {$("#direccion").addClass("incompleto");}
						if(localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null){$("#uniform-localidad").removeClass("incompleto");} else {$("#uniform-localidad").addClass("incompleto");}
						if(region != '' && region != 'Seleccione region' && region != '0'){
								console.log("Region seleccionada");
							} else {
								alertify.error("Debe seleccionar una region");
							}
							if(localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null){
								console.log("Locaildad seleccionada");
							} else {
								alertify.error("Debe seleccionar una Localidad.");
							}
						////////////////////////////////////////////////////////////////////////
						//consulto por datos de facturacion
						if($("#facturaSi").is(':checked')){
							//solo factura
								console.log("Cliente quiere factura");
								var razonSocial = $("#razonSocial").val();
								var direccionFactura = $("#direccionFactura").val();
								var rutFactura = $("#RutFactura").val();
								var emailFactura = $("#emailFactura").val();
								var giro = $("#giro").val();
								if(razonSocial != '' && direccionFactura != '' && rutFactura != '' && emailFactura != '' && giro != ''){
									if(emailFactura !=''){$("#emailFactura").removeClass("incompleto");} else {$("#emailFactura").addClass("incompleto");}
									if(razonSocial !=''){$("#razonSocial").removeClass("incompleto");} else {$("#razonSocial").addClass("incompleto");}
									if(direccionFactura !=''){$("#direccionFactura").removeClass("incompleto");} else {$("#direccionFactura").addClass("incompleto");}
									if(rutFactura !=''){$("#RutFactura").removeClass("incompleto");} else {$("#RutFactura").addClass("incompleto");}
									if(giro !=''){$("#giro").removeClass("incompleto");} else {$("#giro").addClass("incompleto");}
									//datos de factura validados
									//valido correo de factura
									if(emailFactura.indexOf('@', 0) == -1 || emailFactura.indexOf('.', 0) == -1) {
										alertify.error("correo de facturacion invalido.");
										if(emailFactura !=''){$("#emailFactura").removeClass("incompleto");} else {$("#emailFactura").addClass("incompleto");}
									} else {
										console.log("Correo de factura validado");
										console.log("Envio formulario");
										//envio formulario a procesar compra
										$("input:disabled").removeAttr("disabled");
										$("#formCompra").submit();
									}
									
								} else {
									//faltan datos de factura que ingresar
									alertify.error("todos los campos de facturación son obligatorios");
									if(emailFactura !=''){$("#emailFactura").removeClass("incompleto");} else {$("#emailFactura").addClass("incompleto");}
									if(razonSocial !=''){$("#razonSocial").removeClass("incompleto");} else {$("#razonSocial").addClass("incompleto");}
									if(direccionFactura !=''){$("#direccionFactura").removeClass("incompleto");} else {$("#direccionFactura").addClass("incompleto");}
									if(rutFactura !=''){$("#RutFactura").removeClass("incompleto");} else {$("#RutFactura").addClass("incompleto");}
									if(giro !=''){$("#giro").removeClass("incompleto");} else {$("#giro").addClass("incompleto");}
								}
							} else {
								//solo boleta
								console.log("Cliente quiere Boleta");
								//envio formulario a procesar compra
								console.log("Envio formulario");
								$("input:disabled").removeAttr("disabled");
								$("#formCompra").submit();
							}
						////////////////////////////////////////////////////////////////////////
					} else {
						//no estan llenos los campos de despacho
						alertify.error("Debe completar todos los datos de despacho");
						if(direccion !=''){$("#direccion").removeClass("incompleto");} else {$("#direccion").addClass("incompleto");}
						if(localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null){$("#uniform-localidad").removeClass("incompleto");} else {$("#uniform-localidad").addClass("incompleto");}
						if(region != '' && region != 'Seleccione region' && region != '0'){
								console.log("Region seleccionada");
							} else {
								alertify.error("Debe seleccionar una región.");
							}
						if(localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null){
								console.log("Localidad seleccionada");
							} else {
								alertify.error("Debe seleccionar una localidad.");
							}
					}
				} else {
					console.log("activado retiro en tienda");
					// no valido el despacho, solo queda las facturas.
					////////////////////////////////////////////////////////////////////////
						//consulto por datos de facturacion
						if($("#facturaSi").is(':checked')){
							//solo factura
								console.log("Cliente quiere factura");
								var razonSocial = $("#razonSocial").val();
								var direccionFactura = $("#direccionFactura").val();
								var rutFactura = $("#RutFactura").val();
								var emailFactura = $("#emailFactura").val();
								var giro = $("#giro").val();
								if(razonSocial != '' && direccionFactura != '' && rutFactura != '' && emailFactura != '' && giro != ''){
									if(emailFactura !=''){$("#emailFactura").removeClass("incompleto");} else {$("#emailFactura").addClass("incompleto");}
									if(razonSocial !=''){$("#razonSocial").removeClass("incompleto");} else {$("#razonSocial").addClass("incompleto");}
									if(direccionFactura !=''){$("#direccionFactura").removeClass("incompleto");} else {$("#direccionFactura").addClass("incompleto");}
									if(rutFactura !=''){$("#RutFactura").removeClass("incompleto");} else {$("#RutFactura").addClass("incompleto");}
									if(giro !=''){$("#giro").removeClass("incompleto");} else {$("#giro").addClass("incompleto");}
									//datos de factura validados
									//valido correo de factura
									if(emailFactura.indexOf('@', 0) == -1 || emailFactura.indexOf('.', 0) == -1) {
										alertify.error("correo de facturacion invalido");
										if(emailFactura !=''){$("#emailFactura").removeClass("incompleto");} else {$("#emailFactura").addClass("incompleto");}
									} else {
										console.log("Correo de factura validado");
										console.log("Envio formulario");
										//envio formulario a procesar compra
										$("input:disabled").removeAttr("disabled");
										$("#formCompra").submit();
									}
									
								} else {
									//faltan datos de factura que ingresar
									alertify.error("todos los campos de facturación son obligatorios");
									if(emailFactura !=''){$("#emailFactura").removeClass("incompleto");} else {$("#emailFactura").addClass("incompleto");}
									if(razonSocial !=''){$("#razonSocial").removeClass("incompleto");} else {$("#razonSocial").addClass("incompleto");}
									if(direccionFactura !=''){$("#direccionFactura").removeClass("incompleto");} else {$("#direccionFactura").addClass("incompleto");}
									if(rutFactura !=''){$("#RutFactura").removeClass("incompleto");} else {$("#RutFactura").addClass("incompleto");}
									if(giro !=''){$("#giro").removeClass("incompleto");} else {$("#giro").addClass("incompleto");}
								}
							} else {
								//solo boleta
								console.log("Cliente quiere Boleta");
								//envio formulario a procesar compra
								console.log("Envio formulario");
								$("input:disabled").removeAttr("disabled");
								$("#formCompra").submit();
							}
						////////////////////////////////////////////////////////////////////////
				}
			}
		} else {
			//si estan vacios obligo a ingresar algo
			alertify.error("Faltan campos por completar.");
			if(nombre !=''){$("#nombre").removeClass("incompleto");} else {$("#nombre").addClass("incompleto");}
			if(email !=''){$("#email").removeClass("incompleto");} else {$("#email").addClass("incompleto");}
			if(telefono !=''){$("#telefono").removeClass("incompleto");} else {$("#telefono").addClass("incompleto");}
			if(run !=''){$("#Rut").removeClass("incompleto");} else {$("#Rut").addClass("incompleto");}
			
		}
		
	});
	//Fin click de validacion
	
	
	
});