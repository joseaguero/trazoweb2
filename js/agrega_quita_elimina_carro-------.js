// JavaScript Document

$(function(){
	//elimino un item desde el mini carro--------------------------------------------------------------------------
	$('.quitarCarro').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var QtyActual = $(".cantItems").html();
		var cantidadAEliminar = $(this).parent().find(".qtyHeader a").html();
		console.log("producto a eliminar;"+id_pro);
		
		var totalFinal = parseInt(QtyActual) - parseInt(cantidadAEliminar);
			
			$.ajax({
				type: 'GET',
				url: 'tienda/addCarro.php',
				data: {id:id_pro, action:"delete"},
				cache: false,
				 success:function() {
					$(".cont_loading").fadeOut(100);
					$("#fila_carro_"+id_pro).fadeOut(300);
					$(".totalHeader").load("tienda/totalCart.php");
					$("#contMiniCart").load("tienda/miniCart.php");
					$(".cantItems").load("tienda/qtyTotal.php");
					alertify.success("Producto eliminado con éxito.");
				},
				error: function() {
					$(".cont_loading").fadeOut(100);
				}
			}).done(function(){
				$(".cont_loading").fadeOut(100);
			});
		});
	
	// fin funcion ç------------------------------------------------------------------------------------------------------------------



	//elimino un item --------------------------------------------------------------------------
	$('.compraOnline .elim-lista').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var QtyActual = $("#cantHeaderCompra").html();
		var cantidadAEliminar = $(this).parent().parent().find("input.spinnerchico").attr("aria-valuenow");
		console.log(cantidadAEliminar);
		
		var totalFinal = parseInt(QtyActual) - parseInt(cantidadAEliminar);
			
			$.ajax({
				type: 'GET',
				url: 'carro-compra',
				data: {id:id_pro, action:"delete"},
				cache: false,
				 success:function() {
					$(".cont_loading").fadeOut(100);
					$("#fila_carro_"+id_pro).fadeOut(300);
					if (totalFinal == 0) {
						$('#contCarroVacio').load("paginas/carro_vacio.php");
					};
					$('#contValoresLateral').load("ajax/cuadro_valores_paso1.php");
					$('#cantHeaderCompra').load("ajax/qty_header.php");
					$('#cantHeaderCompra2').load("ajax/qty_header.php");
				},
				error: function() {
					$(".cont_loading").fadeOut(100);
				}
			}).done(function(){
				$(".cont_loading").fadeOut(100);
			});
		});
	
	// fin funcion ç------------------------------------------------------------------------------------------------------------------



});


//activo las funciones de quitar y agregar por medio de window load, ya que esta con jquery ui el spinner
$(window).load(function() {
  
  //Agrega un producto mas a la sesion
	$('.compraOnline a.ui-spinner-up').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).parent().parent().parent().parent().attr('rel');
		//console.log(id_pro);
		var cantidadAscendente = parseInt($(this).parent().parent().attr("rel"));
		//UPDATE PRICE
		//console.log(cantidadAscendente);
		//precio unitario
		/*var precioDescuento = $(this).parent().parent().parent().find(".precio-fila1").attr("rel");
		var	valores = precioDescuento.split("-");
		var precio = parseInt(valores[0]);
		var descuento = parseInt(valores[1]);
		if	(descuento=="" || descuento==0){
			var nuevoValor = precio;
		}else{
			var nuevoValor = (precio * descuento)/100;
		}*/
		var cantidadActual = parseInt($(this).parent().find(".ui-spinner-input").attr("aria-valuenow"));
		//console.log(cantidadActual);
		//Obtengo la cantidad actual y le sumo la cantidad ascendente 
		//var precioActual = $("#fila_carro_"+id_pro).find(".precio-total-lista").text().replace(/[^a-z0-9\s]/gi, '');
		//var precioActual = parseInt($("#fila_carro_"+id_pro).find(".precio").attr("rel"));
		var precioActual = parseInt($(this).parent().parent().parent().parent().find('.precio').attr('rel'));
		//console.log(precioActual);
		
		//var total_actualizado = formato_numeros(parseInt(nuevoValor) + parseInt(precioActual));
		var total_actualizado = formato_numeros(parseInt(precioActual));
		
		//console.log(total_actualizado);
		
		var totalFila = precioActual*cantidadActual;
		//console.log(totalFila);
		//var precioTotal = $("#precio_total").text().replace(/[^a-z0-9\s]/gi, '');
		//var precioTotalParseado = parseInt(precioTotal);
		//var totalFinal = precioTotalParseado + nuevoValor;
		
		//var cantTotalHeader = parseInt($("a.carro_fixed").text());
			
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, action:"add", cantidad:cantidadAscendente},
				cache: false,
				 success:function(resp) {
					if(resp > cantidadActual){
						// agrego uno mas
						//console.log("Se puede agregar uno mas");
						$.ajax({
							type: 'GET',
							url: 'carro-compra',
							data: {id:id_pro, action:"add", qty:cantidadAscendente},
							cache: false,
							 success:function() {
								//agrego uno a la cantidad seleccionada
								//var nuevaCantidad = cantidadActual +cantidadAscendente;
								//var cantTotal = cantTotalHeader + cantidadAscendente;
								//$("#fila_carro_"+id_pro).find(".cont-cantidad div").text(nuevaCantidad);
								//$("a.carro_fixed").html(cantTotal);
								//$("span#cantHeader").html(cantTotal);
								//console.log(nuevaCantidad);
								// cambio el precio del total de la fila
								//$("#fila_carro_"+id_pro).find(".precio-total-lista").text(total_actualizado);
								//cambio el valor total de la compra
								//$("#precio_total").text(formato_numeros(totalFinal))
								//$("#contCarroHeader").load("ajax/ajax_carro_header.php");
								
								$('#cantHeaderCompra').load("ajax/qty_header.php");
								$('#cantHeaderCompra2').load("ajax/qty_header.php");
								$('#contValoresLateral').load("ajax/cuadro_valores_paso1.php");
								$("#cant_" + id_pro).html(formato_numeros(totalFila));
								alertify.log("Producto agregado con éxito");
							},
							error: function() {
								$(".cont_loading").fadeOut(200);
							}
						});//fin ajax para agregar uno mas
					} else {
						alertify.log("No es posible agregar más de este producto, stock completo");
						$(".cont_loading").fadeOut(200);
					}
				},
				error: function() {
					$(".cont_loading").fadeOut(200);
				}
			}).done(function(){
				$(".cont_loading").fadeOut(200);
			});
			
		});
	
	// fin funcion ç------------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//Quita un producto a la sesion -------------------------------
	$('.compraOnline  a.ui-spinner-down').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).parent().parent().parent().parent().attr('rel');
		
		var cantidadDescontar = parseInt($(this).parent().parent().attr("rel"));
		console.log(cantidadDescontar);
		
		var valor_producto = parseInt($(this).parent().parent().parent().parent().find('.precio').attr('rel'));
		var cant_actual = $(this).parent().find("input").val();
		var precioFinal = valor_producto * cant_actual;
		
		var QtyActual = $("#cantHeaderCompra").html();
		var totalFinal = parseInt(QtyActual) - cantidadDescontar;
		
			//solo si es mayor a uno quito, de lo contrario elimino
			$.ajax({
				type: 'GET',
				url: 'carro-compra',
				data: {id:id_pro, action:"remove", qty:cantidadDescontar},
				cache: false,
				 success:function() {
					 $('#cant_' + id_pro).html()
					//agrego uno a la cantidad seleccionada
					$('#cantHeaderCompra').load("ajax/qty_header.php");
					$('#cantHeaderCompra2').load("ajax/qty_header.php");
					$('#contValoresLateral').load("ajax/cuadro_valores_paso1.php");
					$("#cant_" + id_pro).html(formato_numeros(precioFinal))
					
					if(cant_actual == 0){
						$("#fila_carro_"+ id_pro).fadeOut(100);
					}
					if (totalFinal == 0) {
						$('#contCarroVacio').load("paginas/carro_vacio.php");
					};
				},
				error: function() {
					$(".cont_loading").fadeOut(200);
				}
			}).done(function(){
				$(".cont_loading").fadeOut(200);
			});//aca termina el ajax
		
		
	});
  
  
});