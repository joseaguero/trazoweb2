// JavaScript Document
$(function(){

	//Agrega un producto mas a la sesion desde la ficha
	$('#btn_agregarCarro').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = parseInt($(".campoSpinner").val());
		var cantActual = $(".cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				success:function(resp) {
					console.log(resp);
					if(resp >= 0){
						//si hay stock agrego uno nuevo
						//console.log("stock suficiente - " + resp);
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"add", qty:cantidad},
							cache: false,
							 success:function() {
								//agrego uno a la cantidad seleccionada
								var cantTotal = parseInt(cantActual) + parseInt(cantidad);
								$(".cantItems").html(cantTotal);
								$(".totalHeader").load("tienda/totalCart.php");
								$("#contMiniCart").load("tienda/miniCart.php");
								$(".cont_loading").fadeOut(100);
								alertify.success("Producto agregado con éxito.");
								/*
$(".alerta").html("<div class='cerrar' onclick='javascript:cerrar_alerta()'><img width='9' height='11' src='img/cerrar-alerta.png'></div><div class='icono'><img src='img/icn_exito.png'></div><p class='titulo'>Producto agregado con éxito.</p><div class='aceptar' onclick='javascript:cerrar_alerta()'>ACEPTAR</div>");
								$(".lightbox3").fadeIn(100);
*/
								
							}
						});//fin ajax
					}else{
						//si no hay stock aviso que no se puede agregar mas
						alertify.error("stock insuficiente");
						$(".cont_loading").fadeOut(100);
						/*
$(".alerta").html("<div class='cerrar' onclick='javascript:cerrar_alerta()'><img width='9' height='11' src='img/cerrar-alerta.png'></div><div class='icono'><img src='img/icn_fracaso.png'></div><p class='titulo'>Stock insuficiente para agregar un nuevo producto.</p><div class='aceptar' onclick='javascript:cerrar_alerta()'>ACEPTAR</div>");
						$(".lightbox3").fadeIn(100);
*/
						
					};
				}//fin success
			}).done(function(){
				$(".cargador").fadeOut(200);
			});
		
	});
	
	// fin funcion ç------------------------------------------------------------------------------------------------------------------


})