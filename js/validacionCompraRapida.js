$(function() {
    $(".abrirCarro").click(function() {
        $(".resumenCarroHeader").fadeIn(200);
    });
    $(".cerrarMenuCart").click(function() {
        $(".resumenCarroHeader").fadeOut(200);
    });
    $(".btnFormCompraRapida").click(function() {
        var correoCompraRapida = $("#emailRapido").val();
        if (correoCompraRapida.indexOf('@', 0) == -1 || correoCompraRapida.indexOf('.', 0) == -1) {
            console.log("Ingrese un correo válido");
            $("#emailRapido").val("");
        } else {
            $.ajax({
                url: "ajax/correoCompraRapida.php",
                type: "post",
                async: true,
                data: {
                    correo: correoCompraRapida
                },
                success: function(resp) {}
            });
            $(".formCompraRapida1").submit();
        }
    });
    $("#uniform-facturaSi").click(function() {
        $(".contDatosFactura").fadeIn(100);
    });
    $("#uniform-facturaNo").click(function() {
        $(".contDatosFactura").fadeOut(100);
    });
    $("#modificarDatos2").click(function() {
        $("#modificarDatos").trigger("click");
    });
    $("#tienda1").click(function() {
        $("#tabsCompra1").fadeOut(100);
        $("#tabsCompra2").fadeIn(100);
        console.log("cargo los valores por ajax");
        $("#contDatosCompra").load("tienda/carroRetiroTienda.php?r=1");
        console.log($("#localidad").val());
    });
    $("#btnIngresarCompraRapida").click(function() {
        var usuario = $("#usuarioLogin").val();
        var pass = $("#passLogin").val();
        console.log(usuario + " - " + pass);
        if (pass != '') {
            console.log("tomo los datos y los envio por ajax");
            $.ajax({
                type: 'POST',
                url: 'tienda/loginCompraRapida.php',
                data: {
                    correo: usuario,
                    clave: pass
                },
                dataType: 'JSON',
                success: function(data) {
                    if (data == 1) {
                        alertify.success("inicio de sesión exitoso.");
                        $(".tipoBlanco").removeClass("tipoBlanco");
                        $("#nombre, #telefono, #rut, #direccion").removeAttr("disabled");
                        $("#razonSocial, #direccionFactura, #RutFactura, #emailFactura, #giro, #apellido").removeAttr("disabled");
                        $(".campoFantasma").fadeOut(10);
                        $(".campoFantasma").each(function() {
                            $(this).css("display", "none");
                        });
                        $(".contRut").fadeIn(100);
                        $("#modificarDatos").fadeOut(10);
                        $(".fancybox-close").trigger("click");
                    } else {
                        alertify.error("usuario o contraseña no coinciden, si no conoce su clave puede solicitar una.");
                    }
                }
            });
        } else {
            alertify.error("Debe ingresar una contraseña válida.");
        }
    });
    $("#solicitarClave").click(function() {
        var usuario = $("#usuarioLogin").val();
        $.ajax({
            type: 'POST',
            url: 'tienda/recuperarClaveCompraRapida.php',
            data: {
                correo: usuario
            },
            dataType: 'JSON',
            success: function(data) {
                if (data == 1) {
                    $('.mensaje_login').html('Revisa tu correo, te enviamos una nueva clave.');
                    // alertify.success("Revisa tu correo, te enviamos una nueva clave.");
                } else if (data == 2) {
                    $('.mensaje_login').html('No fue posible enviarte una nueva clave en estos momentos, intenta más tarde.');
                    $('.mensaje_login').css('background', '#e96d6d');
                    $('.mensaje_login').css('border', '#d65b5b');
                    // alertify.error("No fue posible enviarte una nueva clave en estos momentos, intenta más tarde.");
                } else if (data == 3) {
                    $('.mensaje_login').html('El correo ingresado no se encuentra en nuestros registros.');
                    $('.mensaje_login').css('background', '#e96d6d');
                    $('.mensaje_login').css('border', '#d65b5b');
                    // alertify.error("El correo ingresado no se encuentra en nuestros registros.")
                }
                $('.mensaje_login').fadeIn();
            }
        });
    });
    $("#tienda2").click(function() {
        $("#tabsCompra2").fadeOut(100);
        $("#tabsCompra1").fadeIn(100);
        var descuento = $('.descuento').val();
        var localidadSeleccionada = $("#localidad").val();

        if (localidadSeleccionada != '0' && localidadSeleccionada != '' && localidadSeleccionada != 'Localidades' && localidadSeleccionada != 'Seleccione Localidad.') {
            $.ajax({
                type: 'POST',
                url: 'tienda/precioCompraRapida.php',
                data: {
                    localidad: localidadSeleccionada,
                    codigo_descuento: descuento
                },
                dataType: 'JSON',
                success: function(data) {}
            }).done(function(data) {
                var json_string = JSON.stringify(data);
                var obj = $.parseJSON(json_string);
                $('.mensajesDespacho').html(obj.msj);
                $('#precio_bottom').text(obj.valor);
                $("#totalConEnvio").html(obj.totalActual);
                $(".descuento").html(obj.descuento);
                if (obj.descuento) {
                    $("#precio_descuento").css('display', 'block');
                    $('.title_descuento').css('display', 'block');
                }
                if (obj.valorEnvio != 0) {
                    $("#totalConEnvio").html(obj.totalActual);
                }
            });
        }
    });
    $("#region").change(function() {
        var region_id = $(this).val();
        $("#comuna").load("tienda/comunas.php?region_id=" + region_id);
        $("#uniform-comuna span").html("Seleccione comuna");
        $("#uniform-localidad span").html("Localidades");
    });
    $("#comuna").change(function() {
        var comuna_id = $(this).val();
        // console.log(comuna_id);
        $("#localidad").load("tienda/localidades.php?comuna_id=" + comuna_id);
        $("#uniform-localidad span").html("Seleccione Localidad.");
    });
    $("#localidad").change(function() {
        var localidadSeleccionada = $(this).val();
        console.log(localidadSeleccionada);
        $.ajax({
            type: 'POST',
            url: 'tienda/precioCompraRapida.php',
            data: {
                localidad: localidadSeleccionada
            },
            dataType: 'JSON',
            success: function(data) {}
        }).done(function(data) {
            console.log(data);
            var json_string = JSON.stringify(data);
            var obj = $.parseJSON(json_string);
            $('.mensajesDespacho').html(obj.msj);
            $('#precio_bottom').text(obj.valor);
            $("#totalConEnvio").html(obj.totalActual);
            $(".descuento").html(obj.descuento);
            if (obj.valorEnvio != 0) {
                $("#totalConEnvio").html(obj.totalActual);
            }
        });
    })
    $("#comprar").click(function() {
        var nombre = $("#nombre").val();
        var email = $("#email").val();
        var telefono = $("#telefono").val();
        var apellido = $("#apellido").val();
        var run = $("#Rut").val();
        var region = $("#region").val();
        var comuna = $("#comuna").val();
        var localidad = $("#localidad").val();
        var direccion = $("#direccion").val();
        var quien_recibe = $("#quien_recibe").val();
        var terminos = $('#terminos');
        if (nombre != '' && email != '' && telefono != '' && run != '' && apellido != '') {
            if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                console.error("Correo inválido.");
                $("#email").addClass("incompleto");
            } else {
                if (nombre != '') {
                    $("#nombre").removeClass("incompleto");
                } else {
                    $("#nombre").addClass("incompleto");
                }
                if (email != '') {
                    $("#email").removeClass("incompleto");
                } else {
                    $("#email").addClass("incompleto");
                }
                if (telefono != '') {
                    $("#telefono").removeClass("incompleto");
                } else {
                    $("#telefono").addClass("incompleto");
                }
                if (run != '') {
                    $("#Rut").removeClass("incompleto");
                } else {
                    $("#Rut").addClass("incompleto");
                }
                if ($("#tienda2").is(':checked')) {
                    console.log("activado servicio de despacho");
                    console.log(localidad);
                    console.log("lkjdhsckjsnfckjsdncksejn" + $("#localidad").val());
                    if (region != '' && region != 'Seleccione region' && region != '0' && comuna != '0' && comuna != '' && direccion != '' && localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null) {
                        if (direccion != '') {
                            $("#direccion").removeClass("incompleto");
                        } else {
                            $("#direccion").addClass("incompleto");
                        }
                        if (localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null) {
                            $("#uniform-localidad").removeClass("incompleto");
                        } else {
                            $("#uniform-localidad").addClass("incompleto");
                        }
                        if (region != '' && region != 'Seleccione region' && region != '0') {
                            console.log("Region seleccionada");
                        } else {
                            alertify.error("Debe seleccionar una region");
                        }
                        if (localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null) {
                            console.log("Locaildad seleccionada");
                        } else {
                            alertify.error("Debe seleccionar una Localidad.");
                        }
                        if ($("#facturaSi").is(':checked')) {
                            console.log("Cliente quiere factura");
                            var razonSocial = $("#razonSocial").val();
                            var direccionFactura = $("#direccionFactura").val();
                            var rutFactura = $("#RutFactura").val();
                            var emailFactura = $("#emailFactura").val();
                            var giro = $("#giro").val();
                            if (razonSocial != '' && direccionFactura != '' && rutFactura != '' && emailFactura != '' && giro != '') {
                                if (emailFactura != '') {
                                    $("#emailFactura").removeClass("incompleto");
                                } else {
                                    $("#emailFactura").addClass("incompleto");
                                }
                                if (razonSocial != '') {
                                    $("#razonSocial").removeClass("incompleto");
                                } else {
                                    $("#razonSocial").addClass("incompleto");
                                }
                                if (direccionFactura != '') {
                                    $("#direccionFactura").removeClass("incompleto");
                                } else {
                                    $("#direccionFactura").addClass("incompleto");
                                }
                                if (rutFactura != '') {
                                    $("#RutFactura").removeClass("incompleto");
                                } else {
                                    $("#RutFactura").addClass("incompleto");
                                }
                                if (giro != '') {
                                    $("#giro").removeClass("incompleto");
                                } else {
                                    $("#giro").addClass("incompleto");
                                }
                                if (giro.length > 40) {
                                    alertify.error("Giro debe tener como máximo 40 carácteres");
                                } else {
                                    if (emailFactura.indexOf('@', 0) == -1 || emailFactura.indexOf('.', 0) == -1) {
                                        alertify.error("correo de facturacion invalido.");
                                        if (emailFactura != '') {
                                            $("#emailFactura").removeClass("incompleto");
                                        } else {
                                            $("#emailFactura").addClass("incompleto");
                                        }
                                    } else {
                                        console.log("Correo de factura validado");
                                        console.log("Envio formulario");
                                        $("input:disabled").removeAttr("disabled");
                                        $("#formCompra").submit();
                                    }
                                }
                            } else {
                                alertify.error("todos los campos de facturación son obligatorios");
                                if (emailFactura != '') {
                                    $("#emailFactura").removeClass("incompleto");
                                } else {
                                    $("#emailFactura").addClass("incompleto");
                                }
                                if (razonSocial != '') {
                                    $("#razonSocial").removeClass("incompleto");
                                } else {
                                    $("#razonSocial").addClass("incompleto");
                                }
                                if (direccionFactura != '') {
                                    $("#direccionFactura").removeClass("incompleto");
                                } else {
                                    $("#direccionFactura").addClass("incompleto");
                                }
                                if (rutFactura != '') {
                                    $("#RutFactura").removeClass("incompleto");
                                } else {
                                    $("#RutFactura").addClass("incompleto");
                                }
                                if (giro != '') {
                                    $("#giro").removeClass("incompleto");
                                } else {
                                    $("#giro").addClass("incompleto");
                                }
                            }
                        } else {
                            if(terminos.is(':checked')){
                                console.log("Cliente quiere Boleta");
                                console.log("Envio formulario");
                                $("input:disabled").removeAttr("disabled");
                                $("#formCompra").submit();
                            }else{
                                alertify.error("Debe aceptar terminos y condiciones");
                            }
                        }
                    } else {
                        alertify.error("Debe completar todos los datos de despacho");
                        if (direccion != '') {
                            $("#direccion").removeClass("incompleto");
                        } else {
                            $("#direccion").addClass("incompleto");
                        }
                        if (localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null) {
                            $("#uniform-localidad").removeClass("incompleto");
                        } else {
                            $("#uniform-localidad").addClass("incompleto");
                        }
                        if (region != '' && region != 'Seleccione region' && region != '0') {
                            console.log("Region seleccionada");
                        } else {
                            alertify.error("Debe seleccionar una región.");
                        }
                        if (localidad != '0' && localidad != '' && localidad != 'Localidades' && localidad != 'Seleccione Localidad.' && localidad != 'Seleccione Localidad' && localidad != null) {
                            console.log("Localidad seleccionada");
                        } else {
                            alertify.error("Debe seleccionar una localidad.");
                        }
                    }
                } else {
                    console.log("activado retiro en tienda");
                    if ($("#facturaSi").is(':checked')) {
                        console.log("Cliente quiere factura");
                        var razonSocial = $("#razonSocial").val();
                        var direccionFactura = $("#direccionFactura").val();
                        var rutFactura = $("#RutFactura").val();
                        var emailFactura = $("#emailFactura").val();
                        var giro = $("#giro").val();
                        if (razonSocial != '' && direccionFactura != '' && rutFactura != '' && emailFactura != '' && giro != '') {
                            if (emailFactura != '') {
                                $("#emailFactura").removeClass("incompleto");
                            } else {
                                $("#emailFactura").addClass("incompleto");
                            }
                            if (razonSocial != '') {
                                $("#razonSocial").removeClass("incompleto");
                            } else {
                                $("#razonSocial").addClass("incompleto");
                            }
                            if (direccionFactura != '') {
                                $("#direccionFactura").removeClass("incompleto");
                            } else {
                                $("#direccionFactura").addClass("incompleto");
                            }
                            if (rutFactura != '') {
                                $("#RutFactura").removeClass("incompleto");
                            } else {
                                $("#RutFactura").addClass("incompleto");
                            }
                            if (giro != '') {
                                $("#giro").removeClass("incompleto");
                            } else {
                                $("#giro").addClass("incompleto");
                            }
                            if (giro.length > 40) {
                                alertify.error("Giro debe tener como máximo 40 carácteres");
                            } else {
                                if (emailFactura.indexOf('@', 0) == -1 || emailFactura.indexOf('.', 0) == -1) {
                                    alertify.error("correo de facturacion invalido");
                                    if (emailFactura != '') {
                                        $("#emailFactura").removeClass("incompleto");
                                    } else {
                                        $("#emailFactura").addClass("incompleto");
                                    }
                                } else {
                                    if(terminos.is(':checked')){
                                        console.log("Correo de factura validado");
                                        console.log("Envio formulario");
                                        $("input:disabled").removeAttr("disabled");
                                        $("#formCompra").submit();
                                    }else{
                                        alertify.error("Debe aceptar terminos y condiciones");
                                    }
                                }
                            }
                        } else {
                            alertify.error("todos los campos de facturación son obligatorios");
                            if (emailFactura != '') {
                                $("#emailFactura").removeClass("incompleto");
                            } else {
                                $("#emailFactura").addClass("incompleto");
                            }
                            if (razonSocial != '') {
                                $("#razonSocial").removeClass("incompleto");
                            } else {
                                $("#razonSocial").addClass("incompleto");
                            }
                            if (direccionFactura != '') {
                                $("#direccionFactura").removeClass("incompleto");
                            } else {
                                $("#direccionFactura").addClass("incompleto");
                            }
                            if (rutFactura != '') {
                                $("#RutFactura").removeClass("incompleto");
                            } else {
                                $("#RutFactura").addClass("incompleto");
                            }
                            if (giro != '') {
                                $("#giro").removeClass("incompleto");
                            } else {
                                $("#giro").addClass("incompleto");
                            }
                        }
                    } else {
                        if(terminos.is(':checked')){
                            console.log("Cliente quiere Boleta");
                            console.log("Envio formulario");
                            $("input:disabled").removeAttr("disabled");
                            $("#formCompra").submit();
                        }else{
                            alertify.error("Debe aceptar terminos y condiciones.");
                        }
                    }
                }
            }
        } else {
            alertify.error("Faltan campos por completar.");
            if (nombre != '') {
                $("#nombre").removeClass("incompleto");
            } else {
                $("#nombre").addClass("incompleto");
            }
            if (email != '') {
                $("#email").removeClass("incompleto");
            } else {
                $("#email").addClass("incompleto");
            }
            if (telefono != '') {
                $("#telefono").removeClass("incompleto");
            } else {
                $("#telefono").addClass("incompleto");
            }
            if (run != '') {
                $("#Rut").removeClass("incompleto");
            } else {
                $("#Rut").addClass("incompleto");
            }
            if (apellido != '') {
                $("#apellido").removeClass("incompleto");
            } else {
                $("#apellido").addClass("incompleto");
            }
        }
    });
});