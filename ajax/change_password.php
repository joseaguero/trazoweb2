<?php 

include('../admin/conf.php');

$password_actual = mysqli_real_escape_string($conexion, $_POST['password_actual']);
$password_nueva = mysqli_real_escape_string($conexion, $_POST['password_nueva']);
$password_nueva_re = mysqli_real_escape_string($conexion, $_POST['password_nueva_re']);
$cliente_id = (int)mysqli_real_escape_string($conexion, $_POST['id_cliente']);

$pass_hash =  md5($password_actual);
$pass_encrypted = consulta_bd("password_salt", "clientes", "id = $cliente_id", "");

$password_final = hash('md5',$pass_hash.$pass_encrypted[0][0]);
$enter  = consulta_bd("id, nombre", "clientes", "pass ='$password_final' and id = $cliente_id", "");

if(!is_array($enter)){
	$out['status'] = 'error';
	$out['message'] = 'Contraseña actual no es la correcta.';
}else{
	$pass_encrypted = md5($password_nueva);
	$rand = rand(1000, 9999);
    $password_salt = md5($rand);
    $password = hash('md5',$pass_encrypted.$password_salt);

    $update = update_bd('clientes', "pass = '$password', password_salt = '$password_salt'", "id = $cliente_id");

    if ($update) {
    	$out['status'] = 'success';
    	$out['message'] = 'Cambio de contraseña exitoso.';
    }else{
    	$out['status'] = 'error';
		$out['message'] = 'Error al ingresar ';
    }
}

echo json_encode($out);

?>