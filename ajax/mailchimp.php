<?php
 
use DrewM\MailChimp\MailChimp;
use \DrewM\MailChimp\Batch;
 
/**
 * Class MailChimpClient
 */
class MailChimpClient
{
	/**
	* @var
	*/
	protected $_apiKey;

	/**
	* @var MailChimp
	*/
	protected $_mailChimp;

	/**
	* MailChimpClient constructor.
	*
	* @param $apiKey
	*/
	public function __construct($apiKey)
	{
		$this->_apiKey = $apiKey;
		$this->_mailChimp = new MailChimp($this->_apiKey);
	}

	public function subscribeNewsletter($data = [])
	{
		try{
			$url = sprintf('lists/%s/members', $data['listId']);
			$result = $this->_mailChimp->post($url, [
				'email_address' => $data['email'],
				'status'        => 'subscribed',
				'merge_fields'  => array(
		        	'FNAME' => $data['nombre']
		    	)
			]);
			return $result;
	    }
	    catch (Exception $exception)
	    {
	    	return $exception->getMessage();
	    }
	}

	public function subscribe($data = [])
	{
		try{
			$url = sprintf('lists/%s/members', $data['listId']);
			$result = $this->_mailChimp->post($url, [
				'email_address' => $data['email'],
				'status'        => 'subscribed',
				'merge_fields'  => array(
		        'FNAME' => $data['nombre'],
		        'LNAME' => $data['apellido'],
		    )
			]);
			return $result;
	    }
	    catch (Exception $exception)
	    {
	    	return $exception->getMessage();
	    }
	}

}