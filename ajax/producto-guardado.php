<?php 

include('../admin/conf.php');

$producto_id = mysqli_real_escape_string($conexion, $_POST['id']); // Producto detalle id
$cliente_id = (isset($_SESSION['user_account'])) ? mysqli_real_escape_string($conexion, $_SESSION['user_account']) : null;
$action = mysqli_real_escape_string($conexion, $_POST['action']);

if (!is_null($cliente_id)) {

	if ($action == 'save') {
		$insert = insert_bd('productos_guardados', 'cliente_id, productos_detalle_id', "$cliente_id, $producto_id");
		if ($insert) {
			$out['status'] = 'success';
			$out['message'] = 'Producto guardado';
		}else{
			$out['status'] = 'error';
			$out['message'] = 'Error al guardar el producto.';
		}
	}else{
		$del = del_bd_generic_where('productos_guardados', "cliente_id = $cliente_id AND productos_detalle_id = $producto_id");

		if ($del) {
			$out['status'] = 'success';
			$out['message'] = 'Producto eliminado.';
		}else{
			$out['status'] = 'error';
			$out['message'] = 'Error al eliminar el producto guardado.';
		}
	}

}else{
	$out['status'] = 'error';
	$out['message'] = 'Debes iniciar sesión para realizar la acción.';
}

echo json_encode($out);

?>