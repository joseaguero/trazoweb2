<?php 

include('../admin/conf.php');

if (!empty($_POST)) {
	// Login
	
	$email = strtolower(mysqli_real_escape_string($conexion, $_POST['email']));
	$password = mysqli_real_escape_string($conexion, $_POST['password']);

	$pass_hash =  md5($password);
	$pass_encrypted = consulta_bd("password_salt", "clientes", "LOWER(email) = '$email'", "");

	if (is_array($pass_encrypted)) {
		
		$password_final = hash('md5',$pass_hash.$pass_encrypted[0][0]);
		$enter  = consulta_bd("id, nombre", "clientes", "pass ='$password_final' and LOWER(email) ='$email'", "");
		$cant = mysqli_affected_rows($conexion);
		
		if(count($enter) > 0){
			$_SESSION["user_account"] = $enter[0][0];
			$_SESSION['alert_message']['status'] = 'success';
			$_SESSION['alert_message']['message'] = "Bienvenido {$enter[0][1]}";
		} else {
			$_SESSION['alert_message']['status'] = 'error';
			$_SESSION['alert_message']['message'] = 'Email y/o contraseña incorrecta';
		}

	}else{

		$_SESSION['alert_message']['status'] = 'error';
		$_SESSION['alert_message']['message'] = 'No se encontró usuario registrado con el email ingresado.';

	}

	if ($_SESSION['alert_message']['status'] == 'error') {
		header("Location: {$url_global}login");
	}else{
		header("Location: {$url_global}");
	}

	echo json_encode($out);


}else{
	echo 'Acceso denegado.';
}

?>