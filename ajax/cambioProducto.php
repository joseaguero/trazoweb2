<?php 
include("../admin/conf.php");
include("../funciones.php");

$id = (isset($_POST[id])) ? mysqli_real_escape_string($conexion, $_POST[id]) : 0;
$productosDetalles = consulta_bd("id, sku,imagen1,imagen2,imagen3,imagen4,imagen5,stock","productos_detalles","id=$id","id desc");
$precio = precioProductoBase($id);


$galeria = '
				<div class="colThumbs">';
					if($productosDetalles[0][2] != '' and $productosDetalles[0][2] != "NULL"){
	   $galeria .= '<a rel="'.$productosDetalles[0][2].'" href="javascript:void(0)" class="thumbsGal">
						<img width="96" src="'.$productosDetalles[0][2].'">
					</a>';
					}
					if($productosDetalles[0][3] != '' and $productosDetalles[0][3] != "NULL"){
	   $galeria .= '<a rel="'.$productosDetalles[0][3].'" href="javascript:void(0)" class="thumbsGal">
						<img width="96" src="'.$productosDetalles[0][3].'">
					</a>';
					}
					if($productosDetalles[0][4] != '' and $productosDetalles[0][4] != "NULL"){
	   $galeria .= '<a rel="'.$productosDetalles[0][4].'" href="javascript:void(0)" class="thumbsGal">
						<img width="96" src="'.$productosDetalles[0][4].'">
					</a>';
					}
					if($productosDetalles[0][5] != '' and $productosDetalles[0][5] != "NULL"){
	   $galeria .= '<a rel="'.$productosDetalles[0][5].'" href="javascript:void(0)" class="thumbsGal">
						<img width="96" src="'.$productosDetalles[0][5].'">
					</a>';
					}
					if($productosDetalles[0][6] != '' and $productosDetalles[0][6] != "NULL"){
	   $galeria .= '<a rel="'.$productosDetalles[0][6].'" href="javascript:void(0)" class="thumbsGal">
						<img width="96" src="'.$productosDetalles[0][6].'">
					</a>';
					}
//pendiente la condicion de imagen principal galeria
if($productosDetalles[0][2] != '' and $productosDetalles[0][2] != "NULL"){ 
	$imgActual = $productosDetalles[0][2];
	$cantImg = 1;
} else if($productosDetalles[0][3] != '' and $productosDetalles[0][3] != "NULL"){ 
	$imgActual = $productosDetalles[0][3];
	$cantImg = 1;
} else if($productosDetalles[0][4] != '' and $productosDetalles[0][4] != "NULL"){ 
	$imgActual = $productosDetalles[0][4];
	$cantImg = 1;
} else if($productosDetalles[0][5] != '' and $productosDetalles[0][5] != "NULL"){ 
	$imgActual = $productosDetalles[0][5];
	$cantImg = 1;
} else if($productosDetalles[0][6] != '' and $productosDetalles[0][6] != "NULL"){ 
	$imgActual = $productosDetalles[0][6];
	$cantImg = 1;
} else {
	$imgActual = "img/sinImagen.jpg";
	$cantImg = 0;
}
								
	$galeria .='</div>
				<div class="imgGal">
					<a href="'.$imgActual.'" class="fancybox" id="imgGal">
						<img width="100%" src="'.$imgActual.'" id="imgGrandeGal">
					</a>
				</div>
			';




$producto = array(
	'precio'=>$precio,
	'sku'=>$productosDetalles[0][1],
	'stock' => $productosDetalles[0][7],
	'galeria' => $galeria,
	'cantImg' => $cantImg
	);
echo (json_encode($producto));

?>
