<?php 

include('../admin/conf.php');
date_default_timezone_set('America/Santiago');

$fecha_hoy = date("Y-m-d H:i:s", time());

$nombre = (isset($_POST[nombre])) ? mysqli_real_escape_string($conexion, $_POST[nombre]) : 0;
$apellido = (isset($_POST[apellido])) ? mysqli_real_escape_string($conexion, $_POST[apellido]) : 0;
$correo = (isset($_POST[email])) ? mysqli_real_escape_string($conexion, $_POST[email]) : 0;
$rut = (isset($_POST[rut])) ? mysqli_real_escape_string($conexion, $_POST[rut]) : 0;
$pass = (isset($_POST[password])) ? mysqli_real_escape_string($conexion, $_POST[password]) : 0;
$pass_re = (isset($_POST[password-rec])) ? mysqli_real_escape_string($conexion, $_POST[password-rec]) : 0;
$telefono = (isset($_POST[telefono])) ? mysqli_real_escape_string($conexion, $_POST[telefono]) : 0;

$pass_encrypted = md5($pass);
$rand = rand(1000, 9999);
$password_salt = md5($rand);
$password = hash('md5',$pass_encrypted.$password_salt);

//Consulto si el correo ya existe
$existe_usuario = consulta_bd('id', 'clientes', "email = '$correo' and (pass IS NULL OR pass = '')", '');

if (!is_array($existe_usuario)) {
	$existe_usuario = consulta_bd('id', 'clientes', "email = '$correo'", '');

	if (!is_array($existe_usuario)) {
		$insert = insert_bd('clientes', "nombre, apellido, email, rut, telefono, pass, password_salt, fecha_creacion", "'$nombre', '$apellido', '$correo', '$rut', '$telefono', '$password', '$password_salt', '$fecha_hoy'");
		if ($insert) {
			$_SESSION['alert_message']['status'] = 'success';
			$_SESSION['alert_message']['message'] = "Cuenta creada correctamente.";
		}else{
			$_SESSION['alert_message']['status'] = 'error';
			$_SESSION['alert_message']['message'] = "Error al crear cuenta, intente más tarde.";
		}
	}else{
		$_SESSION['alert_message']['status'] = 'error';
		$_SESSION['alert_message']['message'] = "Correo ingresado ya se encuentra asociado a una cuenta.";
	}
}else{
	// Es un cliente que ha comprado pero no ha creado una cuenta.
	$_SESSION['alert_w']['status'] = 'error';
	$_SESSION['alert_w']['message'] = "Se encontraron compras anteriores con el correo ingresado, favor hacer click en ¿Olvidaste tu contraseña? y tendrás tu cuenta lista para utilizar.";

}

header("Location: {$url_global}login");

?>