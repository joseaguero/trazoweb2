<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : null;

$user = consulta_bd('id, nombre', 'clientes', "email = '$email'", '');

if (is_array($user)) {
	$usuario = $user[0][1];
	$id = $user[0][0];

	$nombre_sitio = "Trazo";
    $nombre_corto = "trazo";
    $noreply = "no-reply@".$nombre_corto.".cl";
    $url_sitio="https://".$nombre_corto.".cl";
    $logo = $url_sitio."/img/logo.jpg";
	$color_logo = "#414141";

	$rand = rand(1000, 999999);
	
	$pass_nueva = "FRLF".$rand;
	$pass_encrypted =  md5($pass_nueva);
	$password_salt = md5($rand);
	$password = hash('md5',$pass_encrypted.$password_salt);

	$update = update_bd("clientes","pass = '$password', password_salt = '$password_salt'","id=$id");

	$msg2 = '
		<html>
			<head>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
			<title>'.$nombre_sitio.'</title>
			<style type="text/css">
					p, ul, a { 
						color:#666; 
						font-family: "Open Sans", sans-serif;
						background-color: #ffffff; 
						font-weight:300;
					}
					strong{
						font-weight:600;
					}
					a {
						color:#666;
					}
				</style>
			</head>
			<body style="background:#f9f9f9;">
				<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
				
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
							<tr>
								<th align="left" width="50%">
									<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="214"/>
										</a>
									</p>
								</th>
							</tr>
						</table>
						<br/><br/>
							
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td valign="top">
										<p><strong>Estimado '.$usuario.':</strong></p>
										<p>Se ha solicitado recuperar clave a través de la página web '.$nombre_sitio.'. <br />
										se recomienda cambiarla en cuanto reciba este correo:<br /></p>
										
										<p>Nueva clave: '.$pass_nueva.'</p>
										
										<p>Muchas gracias<br /> Atte,</p>
										<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
									</td>
								</tr>
							</table>
				</div>
			</body>
		</html>';

	$to = "$usuario <$email>";
	$cc = '';

	$send = enviarCorreoJP("Nueva contraseña", $msg2, $to, $cc);

    if (!$send) {
        echo 0;
    } else {
        echo 1;
    }

}

?>