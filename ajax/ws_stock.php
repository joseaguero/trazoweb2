<?php 
include('../admin/conf.php');
$sku = isset($_POST['sku']) ? mysqli_real_escape_string($conexion, $_POST['sku']) : NULL;
$cantidad = isset($_POST['qty']) ? mysqli_real_escape_string($conexion, $_POST['qty']) : 0;
// $sku = 'TSOFTEL0539TELX';
if ($sku != NULL) {
	$file = file_get_contents('http://trazo.cl/ws_elias/product_size/sku/'.$sku);

	$xml = simplexml_load_string($file);
	$stock = $xml->items->stock_disponible; // Stock disponible trazo sin procesar
	$transito = $xml->items->stock_en_transito;

	// Precios
	$precio = $xml->items->price;
	$descuento = $xml->items->sale_price;

	if ($descuento == '') {
		$descuento = 0;
	}

	// var_dump($descuento);

	if ($sku[0] == 'P') {
		if ($file) {
			$stock_disponible = $stock - $transito;
			if ($stock_disponible > 0) {
				// Revisamos si la cantidad es mayor al stock disponible
				if ($stock_disponible >= $cantidad) {
					// Revisaremos si tiene ya productos agregados en el carro
					$cart = $_SESSION['cart_alfa_cm'];
					if ($cart) {
						$items = explode(',',$cart);
						$contents = array();
						foreach ($items as $item) {
							$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
						}
						foreach ($contents as $prd_id=>$qty) {
							$sql = consulta_bd("id, sku","productos_detalles","id=$prd_id","");
							if ($sku == $sql[0][1]) {
								if ($stock_disponible >= $qty+$cantidad) {
									$response['status'] = 'success';
									$response['message'] = 'Producto agregado con exito';
								}else{
									$response['status'] = 'error';
									$response['message'] = 'Stock insuficiente';
								}
								break;
							}else{
								$response['status'] = 'success';
								$response['message'] = 'Producto agregado con exitok';
							}
						}
					}else{
						$response['status'] = 'success';
						$response['message'] = 'Producto agregado con exitok';
					}
				}else{
					$response['status'] = 'error';
					$response['message'] = 'Stock insuficiente';
				}
			}else{
				$response['status'] = 'error';
				$response['message'] = 'Stock insuficiente';
			}	
		}else{
			$response['status'] = 'error';
			$response['message'] = 'No se encontro sku en ws';
		}
	}else{
		$response['status'] = 'success';
		$response['message'] = 'Producto agregado con exito';
	}
}else{
	$response['status'] = 'error';
	$response['message'] = 'No hay sku.';
}

// Actualizo el precio del producto
if ($response['status'] == 'success') {
	update_bd('productos_detalles', "precio = $precio, descuento = $descuento", "sku = '$sku'");
}

echo json_encode($response);

?>