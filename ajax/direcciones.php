<?php 

include('../admin/conf.php');
date_default_timezone_set('America/Santiago');

$fecha_hoy = date("Y-m-d H:i:s", time());

$action = mysqli_real_escape_string($conexion, $_POST['action']);
$user_id = mysqli_real_escape_string($conexion, $_POST['user_id']);

// Variables
$nombre = mysqli_real_escape_string($conexion, $_POST['nombre']);
$region = mysqli_real_escape_string($conexion, $_POST['region']); 
$comuna = mysqli_real_escape_string($conexion, $_POST['comuna']);
$localidad = mysqli_real_escape_string($conexion, $_POST['localidad']);
$direccion = mysqli_real_escape_string($conexion, $_POST['direccion']);

if ($action != 'show_dir' AND $action != 'delete') {
	$ciudad = consulta_bd('ciudade_id', 'comunas', "id = $comuna", '');
	$ciudad_id = $ciudad[0][0];
}

switch ($action) {
	case 'create':
	
		$insert = insert_bd('clientes_direcciones', "cliente_id, nombre, region_id, ciudad_id, comuna_id, localidad_id, calle, fecha_creacion", "$user_id, '$nombre', $region, $ciudad_id, $comuna, $localidad, '$direccion', '$fecha_hoy'");

		if ($insert) {
			echo json_encode( array('status' => 'success') );
		}else{
			echo json_encode( array('status' => 'error') );
		}

	break;

	case 'edit':

		$id_direccion = mysqli_real_escape_string($conexion, $_POST['id_direccion']);

		$update = update_bd('clientes_direcciones', "cliente_id = $user_id, nombre = '$nombre', region_id = $region, ciudad_id = $ciudad_id, comuna_id = $comuna, localidad_id = $localidad, calle = '$direccion', fecha_modificacion = '$fecha_hoy'", "id = $id_direccion");

		if ($update) {
			echo json_encode( array('status' => 'success') );
		}else{
			echo json_encode( array('status' => 'error') );
		}

	break;

	case 'show_dir':

		$id_direccion = mysqli_real_escape_string($conexion, $_POST['id_direccion']);

		$query = consulta_bd('nombre, region_id, ciudad_id, comuna_id, localidad_id, calle, id', 'clientes_direcciones', "id = $id_direccion", '');

		$regiones = consulta_bd('id, nombre', 'regiones', "id > 0", '');

		$comunas = consulta_bd('id, nombre', 'comunas', "ciudade_id = {$query[0][2]} and id > 0", '');

		$localidades = consulta_bd('id, nombre', 'localidades', "comuna_id = {$query[0][3]}", '');

		$contador = 0;

		$out[0]['nombre'] = $query[0][0];
		$out[0]['calle'] = $query[0][5];

		foreach ($regiones as $item) {
			$out[0]['regiones'][$contador]['id'] = $item[0];
			$out[0]['regiones'][$contador]['nombre'] = $item[1];
			$out[0]['regiones'][$contador]['checked'] = ($item[0] == $query[0][1]) ? true : false;
			$contador++;
		}

		$contador = 0;
		foreach ($comunas as $item) {
			$out[0]['comunas'][$contador]['id'] = $item[0];
			$out[0]['comunas'][$contador]['nombre'] = $item[1];
			$out[0]['comunas'][$contador]['checked'] = ($item[0] == $query[0][3]) ? true : false;
			$contador++;
		}

		$contador = 0;
		foreach ($localidades as $item) {
			$out[0]['localidades'][$contador]['id'] = $item[0];
			$out[0]['localidades'][$contador]['nombre'] = $item[1];
			$out[0]['localidades'][$contador]['checked'] = ($item[0] == $query[0][4]) ? true : false;
			$contador++;
		}

		$out[0]['id'] = $id_direccion;

		echo json_encode($out);

	break;

	case 'delete':

		$id_direccion = mysqli_real_escape_string($conexion, $_POST['id_direccion']);

		$del = del_bd('clientes_direcciones', $id_direccion);

		if ($del) {
			echo json_encode( array('status' => 'success') );
		}else{
			echo json_encode( array('status' => 'error') );
		}

	break;

}

?>