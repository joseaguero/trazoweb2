<?php

include(dirname(__FILE__).'/../admin/conf.php');
date_default_timezone_set('America/Santiago');
header('Content-Type: text/html; charset=utf-8');

	$file = file_get_contents('https://trazo.cl/ws/trazo_sub_categories/');    
	
	$xml = simplexml_load_string($file);
	$insertados = 0;
	$actualizados = 0;

	    foreach($xml->system_category as $sub_category){
	
       
			$id = trim($sub_category->id);
			$categoria_id = trim($sub_category->parent_id);
	        $nombre = substr($sub_category->name, 6);
	        $nombre = trim($nombre);

	        echo $nombre . '<br>';
	        
			$subcategorias = consulta_bd("id","subcategorias","id=$id","");
			$cant = mysqli_affected_rows($conexion);
			if($cant > 0){
				//update
				update_bd("subcategorias","nombre='$nombre', categoria_id=$categoria_id, ws=1, publicada=1","id=$id");
				$actualizados = $actualizados +1;
                echo "$id  -  $categoria_id  -  $nombre <br/>";
				
			} else {
				//insert
				insert_bd("subcategorias","id, nombre, categoria_id, fecha_creacion, ws, publicada","$id, '$nombre', $categoria_id, NOW(), 1, 1");
				$insertados = $insertados +1;
			}
			
			
	    	

	    }

     update_bd("subcategorias","publicada=0","ws=0");
	 insert_bd("log_webservices","nombre, fecha","'Subcategorias', NOW()");          
	 update_bd("subcategorias","ws=0","");

	 echo 'Sub-Categorías insertados= '.$insertados.'<br />'; 
	 echo 'Sub-Categorías actualizados= '.$actualizados;   

?>