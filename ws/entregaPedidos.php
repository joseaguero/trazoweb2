<?php
include(dirname(__FILE__).'/../admin/conf.php');
include(dirname(__FILE__).'/../admin/includes/tienda/cart/inc/functions.inc.php');
date_default_timezone_set('America/Santiago');

$id = $_GET[id];
$oc = $_GET[oc];


    /* grab the posts from the db */
    if(isset($_GET[oc])){
		//$query = "SELECT * FROM pedidos WHERE id > $id AND estado_id = 3 ORDER BY id desc";
        $query = "SELECT * FROM pedidos WHERE oc = '$oc' ORDER BY id desc";
        $result = mysqli_query($conexion, $query) or die('Error query:  '.$query);
        $cant = mysqli_affected_rows($conexion);
        
    } else if(isset($_GET[id])){
		//$query = "SELECT * FROM pedidos WHERE id > $id AND estado_id = 3 ORDER BY id desc";
        $query = "SELECT * FROM pedidos WHERE id > $id ORDER BY id desc";
        $result = mysqli_query($conexion, $query) or die('Error query:  '.$query);
        $cant = mysqli_affected_rows($conexion);
        
    }else{
        //$query = "SELECT * FROM pedidos WHERE estado_id = 3 ORDER BY id desc LIMIT 200";
        $query = "SELECT * FROM pedidos ORDER BY id desc";
        $result = mysqli_query($conexion, $query) or die('Error query:  '.$query);
        $cant = mysqli_affected_rows($conexion);
    }
	//var_dump($result);





	/* create one master array of the records */
	$posts = array();
	if(mysqli_num_rows($result)) {
		while($post = mysqli_fetch_assoc($result)) {
			$posts[] = array('Pedido'=>$post);
		}
	}


	$row=mysqli_fetch_array($result);	

	/* output in necessary format */
	header('Content-type: text/xml; charset=utf-8');
	
    echo '<?xml version="1.0" encoding="utf-8"?>';
    echo '<pedidos>';
    echo '<Columnas_afectadas>'.$cant.'</Columnas_afectadas>';
		foreach($posts as $index => $post) {
			if(is_array($post)) {
				foreach($post as $key => $value) {
					echo '<'.$key.'>';
					if(is_array($value)) {
	
                        foreach($value as $tag => $val) {
							if($tag == 'giro'){
								if($val == ''){
									$val = 'Particular';
								}else{
									$val = $val;
								}
							}
                            // if($tag == 'oc'){
                            //       echo '<fechaEntrega>'.utf8_encode(fechaRetiro($val)).'</fechaEntrega>';
                            //     }
                            if($tag == 'fecha_despacho'){
                                  echo '<fechaEntrega>'.$val.'</fechaEntrega>';
                                }
                            
							if($tag == 'estado_id'){
								if($val == 1){
									$estado = 'Pendiente';
								} else if($val == 2){
									$estado = 'Pagado';
								} else if($val == 3){
									$estado = 'Rechazado';
								} else {
                                    $estado = 'Sin estado';
                                }
                                
                                
				                echo '<'.$tag.'>'.utf8_encode($estado).'</'.$tag.'>';
							} else if($tag == 'cant_productos'){
								 $query2 = "SELECT productos_detalle_id, precio_unitario, precio_total, codigo, cantidad, codigo_pack FROM productos_pedidos WHERE pedido_id =".$value['id']." ORDER BY id";
								$result2 = mysqli_query($conexion, $query2) or die('Error query:  '.$query2);
								$cant = mysqli_affected_rows($conexion); 
								echo '<productosComprados>';
									while ($row=mysqli_fetch_array($result2)) {
										echo '<producto>';
											echo '<skuComprado>'.utf8_encode($row['codigo']).'</skuComprado>';
											echo '<qty>'.$row['cantidad'].'</qty>';
											echo '<precioUnitario>'.round($row['precio_unitario']).'</precioUnitario>';
											echo '<precioTotal>'.round($row['precio_total']).'</precioTotal>';
											if (!is_null($row['codigo_pack'])) {
												echo '<codigoPack>'.$row['codigo_pack'].'</codigoPack>';
											}else{
												echo '<codigoPack/>';
											}
										echo '</producto>';
									}
								echo '</productosComprados>';
								
								
							}  else {
								if($val == ''){
									$val = 'vacio';
								} else {
									$val = $val;
								}
								echo '<'.$tag.'>'.utf8_encode($val).'</'.$tag.'>';
							}
							
                        }
					}
					echo '</'.$key.'>';
				}
                
			}
            
		}
		echo '</pedidos>';
	

	/* disconnect from the db */
	@mysqli_close($conexion, $link);
//}



 
?>


