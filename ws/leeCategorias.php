<?php
include(dirname(__FILE__).'/../admin/conf.php');
date_default_timezone_set('America/Santiago');
header('Content-Type: text/html; charset=utf-8');

	$file = file_get_contents('https://trazo.cl/ws/trazo_categories/');    
	
	$xml = simplexml_load_string($file);
	$insertados = 0;
	$actualizados = 0;

	    foreach($xml->system_category as $trazo_category){
	
       
			$id = trim($trazo_category->id);
	        $nombre = substr($trazo_category->name, 3);
	        $nombre = trim($nombre);

	        if ($id != 8 AND $id != 9 AND $id != 10 AND $id != 11 AND $id != 75 AND $id != 81) {
	        	$categorias = consulta_bd("id","categorias","id=$id","");
				$cant = mysqli_affected_rows($conexion);
				if($cant > 0){
					//update
					update_bd("categorias","nombre = '$nombre', ws = 1, publicada = 1","id=$id");
					$actualizados = $actualizados +1;
					
				} else {
					//insert
					insert_bd("categorias","id, nombre, fecha_creacion, publicada, ws","$id, '$nombre', NOW(),1, 1");
					$insertados = $insertados +1;
				}
	        }	

	    }
     update_bd("categorias","publicada=0","ws=0");
	 insert_bd("log_webservices","nombre, fecha","'Categorias', NOW()");   
	 echo 'Categorías insertados= '.$insertados.'<br />'; 
	 echo 'Categorías actualizados= '.$actualizados;   
    update_bd("categorias","ws=0","");

?>