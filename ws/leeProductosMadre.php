<?php
include(dirname(__FILE__).'/../admin/conf.php');
date_default_timezone_set('America/Santiago');
header('Content-Type: text/html; charset=utf-8');


	$file = file_get_contents('https://s3.amazonaws.com/trazo_test/xml/products_trazo.xml');
	
	$xml = simplexml_load_string($file);
	$insertados = 0;
	$actualizados = 0;
	$pp = 0;
	$pd = 0;
    $cont = 0;
        
	    foreach($xml->product as $product){       
			$id = trim($product->id);
	        $marca = trim($product->brand_id);
	        $fecha_creacion = trim($product->created-at);
	        $descripcion = trim(strip_tags($product->description));
	        $descripcion = str_replace("'","",$descripcion);
            $nombre = trim($product->name);	
			
			
			$grilla = trim($product->imagen_grilla_file_name);	
			if($grilla == ''){
				$grilla = 'NULL';
			} else {
				$grilla ="https://s3.amazonaws.com/trazo_test/products/imagenes_grilla/web_thumb/".$grilla;
				$grilla = "$grilla";
			}
			
			
			$publicado = trim($product->published);
			if($publicado == "true"){
				$publicado = 1;
				$pp = $pp +1;
			} else {
				$publicado = 0;
				$pd = $pd+1;
			}
			$seo = trim(strip_tags($product->seo_description));	

			$dificil = trim($product->dificult);

	         if ($dificil == true) {
	         	$dificil = 1;
	         }else{
	         	$dificil = 0;
	         }  

	        
			//consulto si los hijos estan despublicados o hay alguno disponible
			/*$productosInteriores = consulta_bd("id","productos_detalles","publicado = 1 and producto_id=$id","");
			$cantProductosInteriores = mysql_affected_rows();
			if($cantProductosInteriores > 0){
				$publicado = trim($product->published);
			} else {
				$publicado = 0;
			}*/
			
			$producto = consulta_bd("id","productos","id=$id","");
			$cant = mysqli_affected_rows($conexion);
			if($cant > 0){
				//update
				update_bd("productos","marca_id=$marca, descripcion='$descripcion', nombre = '$nombre', publicado=$publicado, descripcion_seo = '$seo', thumbs='$grilla', vacio = 1, webservice=1, dificil = $dificil","id=$id");
				$actualizados = $actualizados +1;
				
			} else {
				//insert
				insert_bd("productos","id, marca_id, descripcion, nombre, publicado, descripcion_seo, destacado, thumbs, vacio, webservice, dificil","$id, $marca, '$descripcion', '$nombre', $publicado, '$seo', 0, '$grilla', 1, 1, $dificil");
				$insertados = $insertados +1;
			}
			
			
	    	$cont = $cont +1;
            //echo $cont." - ".$cant."<br/>";
	    }

     update_bd("productos","publicado=0","webservice = 0");      
     update_bd("productos","webservice = 0","");   
	 insert_bd("log_webservices","nombre, fecha","'Productos Madre', NOW()");
	    
	 echo 'Productos Madre insertados= '.$insertados.'<br />'; 
	 echo 'Productos Madre actualizados= '.$actualizados.'<br />';
	 echo 'Productos Publicados= '.$pp.'<br />'; 
	 echo 'Productos Despublicados= '.$pd;    

?>