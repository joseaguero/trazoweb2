<?php
include(dirname(__FILE__).'/../admin/conf.php');
date_default_timezone_set('America/Santiago');
header('Content-Type: text/html; charset=utf-8');

	$file = file_get_contents('https://trazo.cl/ws/trazo_product_categories');    
	
	$xml = simplexml_load_string($file);
	$insertados = 0;
	$actualizados = 0;

	    foreach($xml->product_system_category as $product_sub_category){
   
	        $producto_id = trim($product_sub_category->product_id);
			$categoria_id = trim($product_sub_category->category_id);
			$subcategoria_id = trim($product_sub_category->sub_category_id);
	        
			$categorias = consulta_bd("id","categorias_productos","producto_id = $producto_id AND categoria_id = $categoria_id AND subcategoria_id = $subcategoria_id","");
			$cant = mysqli_affected_rows($conexion);
			if($cant > 0){
				//update
				update_bd("categorias_productos","categoria_id=$categoria_id, subcategoria_id=$subcategoria_id, producto_id=$producto_id","id={$categorias[0][0]}");
				$actualizados = $actualizados +1;
				
			} else {
				//insert
				insert_bd("categorias_productos","categoria_id, producto_id, subcategoria_id","$categoria_id, $producto_id, $subcategoria_id");
				$insertados = $insertados +1;
			}
			
			
	    	

	    }    
	    
		
		insert_bd("log_webservices","nombre, fecha","'Relaciones', NOW()");   
		
	 echo 'Relaciones insertados= '.$insertados.'<br />'; 
	 echo 'Relaciones actualizados= '.$actualizados;   

?>