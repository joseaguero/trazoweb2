<?php
include(dirname(__FILE__).'/../admin/conf.php');
date_default_timezone_set('America/Santiago');

$insertados = 0;
$actualizados = 0;
	
	
        $file = file_get_contents('https://s3.amazonaws.com/trazo_test/xml/product_sizes_trazo.xml'); 
        $xml = simplexml_load_string($file);

        foreach($xml->product_size as $product){       
			$id = trim($product->id);
	        $producto_id = trim($product->product_id);
			$cbm = trim($product->cbm);
	        
			$principal = trim($product->default);
			if($principal == "true"){
				$principal = 1;
			} else {
				$principal = 0;
			}
			
			$largo = trim($product->depth);
			$alto = trim($product->height);
			$ancho = trim($product->width);
			$nombre = strtolower(trim($product->nombre));
			
			$terminacion = strtolower(trim($product->terminacion));
			$terminacion_id = strtolower(trim($product->terminacion_id));
			if($terminacion_id === ''){
				$terminacion_id = 0;
			}
			$terminacion_codigo = trim($product->terminacion_codigo);
			$publicado = 1;	
			$precio = trim($product->price);
			if($precio == ''){
				$precio = 0;
				//$publicado = 0;
			}
			$descuento = trim($product->sale_price);
			if($descuento == ''){
				$descuento = 0;
			}
			
			$sku = trim($product->sku);
			
			
			$imagen1 = trim($product->imagen_1);
			if($imagen1 == ''){
				$imagen1 = 'NULL';
			} else { $imagen1 = "$imagen1";}
			
			$imagen2 = trim($product->imagen_2);
			if($imagen2 == ''){
				$imagen2 = 'NULL';
			} else { $imagen2 = "$imagen2";}
			
			$imagen3 = trim($product->imagen_3);
			if($imagen3 == ''){
				$imagen3 = 'NULL';
			} else { $imagen3 = "$imagen3";}
			
			$imagen4 = trim($product->imagen_4);
			if($imagen4 == ''){
				$imagen4 = 'NULL';
			} else { $imagen4 = "$imagen4";}
			$imagen5 = trim($product->imagen_5);
			if($imagen5 == ''){
				$imagen5 = 'NULL';
			}else { $imagen5 = "$imagen5";}
					
			$dias_de_fabricacion = trim($product->dias_de_fabricacion);
			if($dias_de_fabricacion == ''){
				$dias_de_fabricacion = 'NULL';
			}
			$in_transit_stock = trim($product->in_transit_stock);
			
			$stock_en_transito = trim($product->in_transit_stock);
			if($stock_en_transito == ''){
				$stock_en_transito = 0;
			}
            
			$stock_disponible = trim($product->stock_disponible);
			if($stock == '' || $stock == 0){
				$stock = 0;
				//$publicado = 0;
			}

			$stock = $stock_disponible - $stock_en_transito;

			$origen = trim($product->origen);
			if ($origen == 'Nacional') {
				$publicado = 1;
			}else if($origen == 'Internacional'){
				if ($stock > 0) {
					$publicado = 1;
				}else{
					$publicado = 0;
				}
			}
			
	       
			//inserto las terminaciones
			if($terminacion_id != ''){
				$term = consulta_bd("*","terminaciones","id=$terminacion_id","");
				$cantTerm = mysqli_affected_rows($conexion);
				if($cantTerm > 0){
					//existe la terminacion
					} else{
						insert_bd("terminaciones","id, nombre, codigo, fecha_creacion","$terminacion_id, '$terminacion', '$terminacion_codigo', NOW()");
					}
			}
			
			$productoDetalle = consulta_bd("id","productos_detalles","id=$id","");
			$cant = mysqli_affected_rows($conexion);
			if($cant > 0){
				//update
				update_bd("productos_detalles","largo = $largo, alto = $alto, ancho = $ancho, nombre= '$nombre', terminacion_id=$terminacion_id, precio = $precio, descuento = $descuento, sku='$sku', stock=$stock, imagen1='$imagen1', imagen2='$imagen2', imagen3='$imagen3', webservice=1, imagen4='$imagen4', imagen5='$imagen5', principal = $principal, cbm = $cbm, dias_de_fabricacion = $dias_de_fabricacion, publicado = $publicado, fecha_modificacion = NOW(), origen = '$origen'","id=$id");
				$actualizados = $actualizados +1;
				
			} else {
				//insert
				insert_bd("productos_detalles","id, producto_id, largo, alto, ancho, nombre, terminacion_id, precio, descuento, sku, stock, imagen1, imagen2, imagen3, imagen4, imagen5, publicado, fecha_creacion, fecha_modificacion, principal, cbm, dias_de_fabricacion,webservice,origen","$id, $producto_id, $largo, $alto, $ancho, '$nombre', $terminacion_id, $precio, $descuento, '$sku', $stock, '$imagen1', '$imagen2', '$imagen3', '$imagen4', '$imagen5', $publicado, NOW(), NOW(), $principal, $cbm, $dias_de_fabricacion,1,'$origen'");
				$insertados = $insertados +1;
			}
		 }







/* C
	update_bd("productos","vacio=0","");
	insert_bd("log_webservices","nombre, fecha","'productos hijo', NOW()");
/* Fin Ciclo */	


    //update_bd("productos","publicado=0","webservice = 0");
    update_bd("productos_detalles","publicado=0","webservice = 0");

    update_bd("productos","webservice = 0","");
    update_bd("productos_detalles","webservice = 0","");


	echo 'Productos Hijo insertados= '.$insertados.'<br />'; 
	echo 'Productos Hijo actualizados= '.$actualizados;   
    
    

    
    
    //despublico madres sin hijos
    //$prod = consulta_bd("id","productos","","");
    
   /* for($i=0; $i<sizeof($prod); $i++){
        $proID = $prod[$i][0];
        $prodDetalle = consulta_bd("id","productos_detalles","producto_id = $proID and publicado=1","");
        $cant = mysqli_affected_rows($conexion);
        if($cant == 0){
            update_bd("productos","publicado = 0","id = $proID");
        }
    }*/

?>