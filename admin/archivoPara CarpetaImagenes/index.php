<?php

include("../../admin/conf.php");

$id = $_SESSION['id'];
$tabla = 'img_'.$_SESSION['tabla'];
$id_tabla = singular($_SESSION['tabla'])."_id";	


/* error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php'); */
//$upload_handler = new UploadHandler();


$options = array(
    'delete_type' => 'POST',
    'db_host' => '127.0.0.1',
    'db_user' => 'root',
    'db_pass' => '',
    'db_name' => 'prueba_cms',
    'db_table' => 'img_'.$_SESSION['tabla'],
	'tabla_id' => $id_tabla,
	'id_entrada_madre' => $_SESSION['id']
);
error_reporting(E_ALL | E_STRICT);

require('../../admin/jQuery-File-Upload/server/php/UploadHandler.php');

class CustomUploadHandler extends UploadHandler {
	 
	protected function initialize() {
        $this->db = new mysqli(
            $this->options['db_host'],
            $this->options['db_user'],
            $this->options['db_pass'],
            $this->options['db_name']
        );
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
        $file->title = @$_REQUEST['title'][$index];
        $file->description = @$_REQUEST['description'][$index];
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index = null, $content_range = null) {
			$file = parent::handle_file_upload(
				$uploaded_file, $name, $size, $type, $error, $index, $content_range
			);
			
			 if (empty($file->error)) {
			 insert_bd($this->options['db_table'],
			 "name, size, type, title, archivo, ".$this->options['tabla_id'],
			 "'".$file->name."', $size, '$type', '$name', '".$file->name."', ".$this->options['id_entrada_madre']);
				$ultimoProducto = mysqli_insert_id($conexion);
				$file->id = $ultimoProducto;
			}  
			return $file;
    }

	//funcion que lista los archivos de las galerias
    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $query2 = consulta_bd("id,type,title,descripcion,name",
									$this->options['db_table'],
									$this->options['tabla_id']."=".$this->options['id_entrada_madre'],
									"posicion asc");
			
			for($i=0; $i<sizeof($query2); $i++) {
				$file->id = $query2[$i][0];
                $file->type = $query2[$i][1];
                $file->title = $query2[$i][2];
                $file->description = $query2[$i][3];
			}
        }
    }

    public function delete($print_response = true) {
        $response = parent::delete(false);
        foreach ($response as $name => $deleted) {
            if ($deleted) {
                del_bd_generic($this->options['db_table'],"name","$name");
			}
        } 
        return $this->generate_response($response, $print_response);
    }

}

$upload_handler = new CustomUploadHandler($options);

?>