<?php

date_default_timezone_set('America/Santiago');
include('../../conf.php');

/**
 * Swiff.Uploader Example Backend
 *
 * This file represents a simple logging, validation and output.
 *  *
 * WARNING: If you really copy these lines in your backend without
 * any modification, there is something seriously wrong! Drop me a line
 * and I can give you a good rate for fancy and customised installation.
 *
 * No showcase represents 100% an actual real world file handling,
 * you need to move and process the file in your own code!
 * Just like you would do it with other uploaded files, nothing
 * special.
 *
 * @license		MIT License
 *
 * @author		Harald Kirschner <mail [at] digitarald [dot] de>
 * @copyright	Authors
 *
 */


// Request log

/**
 * You don't need to log, this is just for the showcase. Better remove
 * those lines for production since the log contains detailed file
 * information.
 */

$result = array();

$result['time'] = date('r');
$result['addr'] = substr_replace(gethostbyaddr($_SERVER['REMOTE_ADDR']), '******', 0, 6);
$result['agent'] = $_SERVER['HTTP_USER_AGENT'];

if (count($_GET)) {
	$result['get'] = $_GET;
}
if (count($_POST)) {
	$result['post'] = $_POST;
}
if (count($_FILES)) {
	$result['files'] = $_FILES;
}

$log = @fopen('script.log', 'a');
if ($log) {
	// we kill an old file to keep the size small
/*
	if (file_exists($result) && date('Y-m-d-H', filemtime($result)) != date('Y-m-d-H')) {
		unlink($result);
	}
*/
	fputs($log, print_r($result, true) . "\n---\n");
	fclose($log);
}


// Validation

$error = false;

if (!isset($_FILES['Filedata']) || !is_uploaded_file($_FILES['Filedata']['tmp_name'])) {
	$error = 'Invalid Upload';
}

/**
 * You would add more validation, checking image type or user rights.
 *

if (!$error && $_FILES['Filedata']['size'] > 2 * 1024 * 1024)
{
	$error = 'Please upload only files smaller than 2Mb!';
}

if (!$error && !($size = @getimagesize($_FILES['Filedata']['tmp_name']) ) )
{
	$error = 'Please upload only images, no other files are supported.';
}

if (!$error && !in_array($size[2], array(1, 2, 3, 7, 8) ) )
{
	$error = 'Please upload only images of type JPEG, GIF or PNG.';
}

if (!$error && ($size[0] < 25) || ($size[1] < 25))
{
	$error = 'Please upload an image bigger than 25px.';
}
*/

if (!$error && $_FILES['Filedata']['size'] > 2 * 1024 * 1024)
{
	$error = 'Por favor suba sólo archivos menores a 2Mb!&tipo=notificacion';
}

// Processing

if (!$error)
{
	$id = $_GET[id];
	$tabla = $_GET[tabla];
	
	//Obtengo las opciones de la tabla
	$opciones = consulta_bd("nombre, valor","opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t","tabla_id = t.id","");
	$i = 0;
	while($i <= (sizeof($opciones)-1))
	{	
		$nombre = $opciones[$i][0];
		$opcion[$nombre] = $opciones[$i][1];
		$i++;
	}
	extract($opcion);
	
	$dir = "../../../imagenes/$tabla";
	if (!file_exists($dir))
	{
		$mk = mkdir($dir);
		if (!$mk)
		die("Error al crear el directorio.");
	}
	
	if ($width_thumb_gal != '')
	{
		$dir_thumb = "$dir/thumbs";
		if (!file_exists("$dir_thumb/"))
		{
			$mk = mkdir($dir_thumb);
			if (!$mk)
			die("Error al crear el directorio thumbs.");
		}
	}
	
	$rand = rand(1,100);
	
	$date = date("dhism");
	$file_ext = strtolower(substr(strrchr($_FILES['Filedata']['name'], '.'), 1));
	
	//Creo imagen grande
	$nombre = $date.'_'.$rand.'.'.$file_ext;
	$dir1 = "$dir/".$_FILES['Filedata']['name'];
	$move = move_uploaded_file($_FILES['Filedata']['tmp_name'], $dir1 );
	
	$dir2 = "$dir/$nombre";
	$new_file = resize_foto_fu($dir1, $height_gal, $width_gal, $dir2);
	
	if ($new_file)
	{
		if (is_numeric($_GET[id]))
		{
			$tabla_s = singular($tabla);
			
			$sql = "INSERT INTO img_$tabla (archivo, ".$tabla_s."_id) VALUES ('$nombre', '$id')";
			$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion));
		}
	}
	
	//creo thumb si es requerido
	if ($width_thumb_gal)
	{	
		$dir2_thumb = "$dir_thumb/$nombre";
		$new_file = resize_foto_fu($dir1, $height_thumb_gal, $width_thumb_gal, $dir2_thumb);
	}
	
	if($new_file)
	{
		unlink($dir1);
	}
}


 /*
 * or
 *
 * $return['link'] = YourImageLibrary::createThumbnail($_FILES['Filedata']['tmp_name']);
 *
 */

if ($error) {

	$return = array(
		'status' => '0',
		'error' => $error
	);

} else {

	$return = array(
		'status' => '1',
		'name' => $_FILES['Filedata']['name']
	);

	// Our processing, we get a hash value from the file
	$return['hash'] = md5_file($dir2);

	// ... and if available, we get image data
	$info = @getimagesize($dir2);

	if ($info) {
		$return['width'] = $info[0];
		$return['height'] = $info[1];
		$return['mime'] = $info['mime'];
	}

}


// Output

/**
 * Again, a demo case. We can switch here, for different showcases
 * between different formats. You can also return plain data, like an URL
 * or whatever you want.
 *
 * The Content-type headers are uncommented, since Flash doesn't care for them
 * anyway. This way also the IFrame-based uploader sees the content.
 */

if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml') {
	header('Content-type: text/xml');
	// Really dirty, use DOM and CDATA section!

	echo '<response>';
	foreach ($return as $key => $value) {
		echo "<$key><![CDATA[$value]]></$key>";
	}
	echo '</response>';

} else {
	header('Content-type: application/json');
	echo json_encode($return);
}



//Cambia el tama̱o de las foto y devuelve el nombre 
//con la imagen guardada en la misma carpeta de este archivo
//410x615
function resize_foto_fu($file, $altura, $ancho, $dir)
{
	$size=getimagesize($file); 
	$width=$size[0]; 
	$height=$size[1];
	
	if ($width > $ancho)
	{
		$p = $ancho/$width;
		$x = $p*$width; 
		$y = $p*$height;

		if ($y > $altura)
		{
			$p = $altura/$y;
			$x = $p*$x; 
			$y = $p*$y;		
		}
		
	}
	elseif ($height > $altura)
	{
		$p = $altura/$height;
		$x = $p*$width; 
		$y = $p*$height;
		
		if ($x > $ancho)
		{
			$p = $ancho/$x;
			$x = $p*$x; 
			$y = $p*$y;		
		}		
	}
	else
	{
		$x = $width;
		$y = $height;
	}
	
	$ext = strtolower(substr(strrchr($file, '.'), 1));
	$date = date("dhis");
/*
	$nombre = $date.'_resized.'.$ext;
	$dir = "$dir2/$nombre";
*/
	
	//crea la nueva imagen (Espacio)
	switch ($ext) { 
	case 'jpg':     // jpg
		$imagen_origen = imagecreatefromjpeg($file);
		$imagen_destino = imagecreatetruecolor($x, $y);
		$res = imagecopyresampled($imagen_destino, $imagen_origen, 0, 0, 0, 0, $x, $y, $width, $height);
		break;
		
	case 'png':     // png
	
		$imagen_destino = imagecreatetruecolor($x, $y);
		imagealphablending($imagen_destino, false);
		imagesavealpha($imagen_destino, true); 
		
		$imagen_origen = imagecreatefrompng($file);
		imagealphablending($imagen_origen, true);
		
		$res = imagecopyresampled($imagen_destino, $imagen_origen, 0, 0, 0, 0, $x, $y, $width, $height);
		
		break;

	case 'gif':     // gif
	
		$imagen_origen = imagecreatefromgif($file);
		$imagen_destino = imagecreatetruecolor($x, $y);
		$transcolor = imagecolortransparent($imagen_origen);

		if($transcolor!=-1)
		{
			$trnprt_color = imagecolorsforindex($imagen_origen, $transcolor);
			$trnprt_indx = imagecolorallocatealpha($imagen_destino, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue'], $trnprt_color['alpha']);
			imagefill($imagen_destino, 0, 0, $trnprt_indx);
			imagecolortransparent($imagen_destino, $trnprt_indx);
		}
		
		$res = imagecopyresampled($imagen_destino, $imagen_origen, 0, 0, 0, 0, $x, $y, $width, $height);
		break;
	}

	switch ($ext) { 
		case 'jpg':     // jpg
			$src = imagejpeg($imagen_destino, $dir, 100);
			break;
		case 'png':     // png
			$src = imagepng($imagen_destino, $dir, 9);
			break;
		case 'gif':     // gif
			$src = imagegif($imagen_destino, $dir, 100);
			break;
	}
	if ($src)
	{
		return true;
	}
	else
	{
		return false;
	}
}


?>