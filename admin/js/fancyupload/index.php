<?php 
include('../../conf.php');
$tabla = $_GET[tabla];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Subir Im&aacute;genes</title>
</head>
<link href="style.css" rel="stylesheet" type="text/css">
<script language="Javascript" type="text/javascript" src="source/mootools.js"></script>
<script language="Javascript" type="text/javascript" src="source/Swiff.Uploader.js"></script>
<script language="Javascript" type="text/javascript" src="source/Uploader.js"></script>
<script language="Javascript" type="text/javascript" src="source/Fx.ProgressBar.js"></script>
<script language="Javascript" type="text/javascript" src="source/FancyUpload2.js"></script>
<script language="Javascript" type="text/javascript" src="script.js"></script>


<body>
<p>
  <?php 
	  if (isset($_GET[error]))
	  {
	  	//echo '<div class="error" id="error">'."$_GET[error]".'</div>';
	  }
	  
	  ?>
</p>
<form action="script.php?<?php 
if (is_numeric($_GET[id]))
{
	echo "id=$_GET[id]&tabla=$_GET[tabla]";
}

?>" method="post" enctype="multipart/form-data" id="form-demo">

	<fieldset id="demo-fallback">
		<legend>File Upload</legend>
		<p>
			This form is just an example fallback for the unobtrusive behaviour of FancyUpload.
			If this part is not changed, something must be wrong with your code.
		</p>
		<label for="demo-photoupload">
			Upload a Photo:
			<input type="file" name="Filedata" />
		</label>
	</fieldset>

	<div id="demo-status" class="hide">
		<p>
			<button  id="demo-browse" class="boton3">Buscar archivos</button> |
			<button id="demo-clear" class="boton3">Limpiar</button> |
			<button id="demo-upload" class="boton3">Subir archivos</button>
		</p>
		<div>
			<strong class="overall-title"></strong><br />
			<img src="assets/progress-bar/bar.gif" class="progress overall-progress" />
		</div>
		<div>
			<strong class="current-title"></strong><br />
			<img src="assets/progress-bar/bar.gif" class="progress current-progress" />
		</div>
		<div class="current-text"></div>
	</div>

	<ul id="demo-list"></ul>

	
</form>


<table width="452" cellspacing="0" cellpadding="5">
<?php 
	if (isset($_GET[id]))
	{
		$id = $_GET[id];
		$singular = singular($tabla);
		$sql = "SELECT * FROM img_$tabla WHERE ".$singular."_id = '$id'";
		$run = mysqli_query($conexion, $sql);
		$cant = mysqli_affected_rows($conexion);
		if ($cant > 0)
		{
		?>
			<tr>
				<td colspan="3"><h1>Im&aacute;genes actuales</h1></td>
			</tr>
		<?php
			while($fila = mysqli_fetch_array($run))
			{
			?>
			
				<tr>
					<td>../imagenes/<?php echo $tabla;?>/<?php echo $fila[archivo];?></td>
					<td><img src="../../../imagenes/<?php echo $tabla;?>/<?php echo $fila[archivo];?>" border="0" height="50px"/></td>
					<td>
					<form id="form2" name="form2" action="delete.php" method="post">
						<button class="boton2">Eliminar</button>
						<input type="hidden" value=<?php echo $tabla;?> name="tabla" />
						<input type="hidden" name="id" value="<?php echo $fila[0];?>"/>
						<input type="hidden" name="row_id" value="<?php echo $id;?>"/>
					</form>
					</td>
				</tr>
			
			<?php	
			}
		}
		else
		{
		?>
			<tr>
				<td colspan="3"><h1><?php echo ucwords($singular);?> sin imágenes.</h1></td>
			</tr>
		<?php
		}
	}
?>
</table>

</body>
</html>