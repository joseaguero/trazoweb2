<?php 

/********************************************************\
|  Moldeable CMS - Imagenes.				             |
|  Fecha Modificación: 14/09/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

	include('../conf.php');
	
	$id = $_POST[id];
	$img_id = $_POST[img_id];
	$tabla = $_POST[tabla];
	$val_op = $_POST[val_op];	
			
	//Obtengo las opciones de la tabla
	$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
	$i = 0;
	while($i <= (sizeof($opciones)-1))
	{	
		$nombre = $opciones[$i][0];
		$opcion[$nombre] = $opciones[$i][1];
		$i++;
	}
	if(sizeof($opciones)>0)
	extract($opcion);
	
	if (is_numeric($_POST[img_id]))
	{
		if (is_numeric($_POST[id]))
		{
			$sql0 = "SELECT archivo FROM img_$tabla WHERE id = '$img_id'";
			$run0 = mysqli_query($conexion, $sql0);
			$res = mysqli_fetch_array($run0);
			$file = $res[0];
			
			$dir = '../../imagenes/'.$tabla.'/'.$file;
			if (file_exists($dir))
			unlink($dir);
			
			$file_thumb = '../../imagenes/'.$tabla.'/thumbs/'.$file;
			if(file_exists($file_thumb))
			unlink($file_thumb);
	
			$sql = "DELETE FROM img_$tabla WHERE id = '$img_id'";
			$run = mysqli_query($conexion, $sql);
			if ($run)
			{
				$error = "Se ha eliminado el archivo&tipo=exito";
			}
			else
			{
				$error = "Error al eliminar el archivo de la Base de Datos&tipo=error";
			}
	
		}
		else
		{
			$error = "Debe seleccionar un archivo&tipo=notificacion";
		}
	}
	
?>

<?php
	if ($error)
	{
		echo "<span style='color:red'>$error</span>";
	}
	
	$rel = (isset($rel1)) ? $rel1 : singular($tabla)."_id";
	$campos = ($texto_galerias) ? "archivo, id, posicion, texto" : "archivo, id, posicion"; 
	$filas = consulta_bd($campos,"img_$tabla","$rel = $id","posicion ASC");
	$file = (isset($width_thumb)) ? "../imagenes/$tabla/thumbs":"../imagenes/$tabla";
	$cant = mysqli_affected_rows($conexion);
	
	if ($cant > 0)
	{
?>
<script>
$(function(){
	$('a.img_galeria').hover(function(){
		$(this).find(".eliminar_galeria").css("display","block");
	}, function(){
		$(this).find(".eliminar_galeria").css("display","none");
	});
});
</script>	
<table width="100%" border="0" cellpadding="5">
	<?php
		$i = 0;
		$j = 1;
		while($i <= (sizeof($filas)-1))
		{	
			if ($i%5==0) {
				echo "<tr>";
			}
			$z = $i+1;
			$id_img = $filas[$i][1];
			$posicion = $filas[$i][2];
			$texto = ($texto_galerias) ? $filas[$i][3] : "";
			
			echo '<td width="20%">
					<div class="galery_wrapper">';
			?>
						<a style="color:#fff" class="img_galeria">
							<img src="pics/delete.png" border="0" class="eliminar_galeria" onclick="delete_img_gal('<?php echo $id ?>','<?php echo $id_img ?>', '<?php echo $tabla ?>', '<?php echo $val_op ?>')" width="30" height="30" />
							<img src=<?php echo $file; ?>/<?php echo $filas[$i][0]; ?> border="0" width="100%" class="volvio" />
						</a>
                        
			<?php
            		echo '<br />Posición 
							<select name="posicion_img['.$id_img.']">';
							for ($counter = 1; $counter <= $cant; $counter++)
							{
								echo '<option value="'.$counter.'"';
								if ($posicion == $counter) echo 'selected="selected" ';
								echo '>'.$counter.'</option>';
							}
					echo '</select>';
					if ($texto_galerias)
					{
						echo '<br ><input type="text" name="texto_img['.$id_img.']" style="width:90%" value="'.$texto.'"/>';
					}
					echo '</div>
				  </td>';
			$i++;
			$j++;
			if ($i%5==0) {
				echo "</tr>";
				$j=1;
			}
		}
		
		if ($j!=5)
		{
			while($j < 6)
			{
				echo "<td width='20%'>&nbsp;</td>";
				$j++;
			}
			echo "</tr>";
		}
	?>
</table>

<?php
	}
	else
	{
		echo "<p>No hay imagenes cargadas.</p>";
	}
?>