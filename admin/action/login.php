<?php 
include ('../conf.php');
require_once('../includes/bcrypt_class.php');
$remote_ip = $_SERVER['REMOTE_ADDR'];
if ($_SESSION[$nombre_sitio."_admin"])
{
	header("location:index.php");
}
if (isset($_POST["username"]) and isset($_POST["pass"])) 
{
	$usuario = mysqli_real_escape_string($conexion, $_POST['username']);
	$pass = mysqli_real_escape_string($conexion, $_POST['pass']);

	$query = consulta_bd("id, perfil_id, password","administradores","username = '$usuario' and active = 1","");
	$res = mysqli_affected_rows($conexion);
	$password_hash = $query[0][2];
	$bcrypt = new Bcrypt(15);

	if ($res == 1) 
	{
		
		#Vaciamos la carpeta 
		$files = glob('../temp/*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
			unlink($file); // delete file
		}
		
		$row = $query[0][0];
		$perfil_id = $query[0][1];
		$rand = rand(1000, 9999);
		$session_hash = md5($rand);
		
		if ($bcrypt->verify($pass, $password_hash))
		{	
			$new_pass = $bcrypt->hash($pass);
			$update = update_bd('administradores', "last_log_in = NOW(), session_hash = '$session_hash', password = '$new_pass', attempt = 0", "id = '$row'");
			
		    /*Log*/
		    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date, remote_ip","'administradores','Inicio de sesión exitoso', '','$row', NOW(), '$remote_ip'");
		    /*Fin Log*/
		
			$nombre_sitio = get_nombre_sitio($base);
			$_SESSION[$nombre_sitio."_admin"] = $row;
			$_SESSION[$nombre_sitio."_admin_sh"] = $session_hash;
			$_SESSION[$nombre_sitio."_perfil_admin"] = $perfil_id;
			
			if ($_SESSION['before_log_in_url'])
			{
				header("location:".$_SESSION['before_log_in_url']);
			}
			else
			{
				header("location:../index.php");
			}
		}
		else
		{
			$query = consulta_bd("id, attempt","administradores","username = '$usuario'","");
			$row = $query[0][0];
			$attempt = $query[0][1];
			if ($attempt >= 2)
			{
				$active = 0;
				$attempt = 0;
				$error = 'Nombre de usuario o password incorrectos. Hemos bloquedado su usuario por razones de seguridad, por favor solicite una nueva contraseña mediante el link ¿Olvidaste tu usuario o password?&tipo=ventana';
			}
			else
			{
				$active = 1;
				$attempt ++;
				$error = "Nombre de usuario o password incorrectos&tipo=error";
			}
		    /*Log*/
		    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date, remote_ip","'administradores','Inicio de sesión fallido', '','$row', NOW(), '$remote_ip'");
		    /*Fin Log*/
			$hash = md5($rand);
			$update = update_bd('administradores', "session_hash = '$hash', attempt = $attempt, active = $active", "id = '$row'");
			
			header("location:../adlogin.php?error=$error");
		}
	}
	else
	{
		$query = consulta_bd("id, attempt, active","administradores","username = '$usuario'","");
		$row = $query[0][0];
		$attempt = $query[0][1];
		$active = $query[0][2];
		$cant = mysqli_affected_rows($conexion);
		
		if ($cant > 0)
		{
			//Usuario existe pero no está activo
			$error = "Su usuario se encuentra bloqueado, por favor genere una nueva contraseña mediante el link ¿Olvidaste tu usuario o password?&tipo=ventana";
		    /*Log*/
		    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date, remote_ip","'administradores','Inicio de sesión con cuenta bloqueada', '','$row', NOW(),'$remote_ip'");
		    /*Fin Log*/
		}
		else
		{
			//Usuario no existe.
			$error = "Nombre de usuario o password incorrectos.&tipo=error";
		}
		
		$hash = md5($rand);
		$update = update_bd('administradores', "session_hash = '$hash'", "id = '$row'");
		
		header("location:../adlogin.php?error=$error");
	}	
}
else
{
		$error = "Falto ingresar pass/user&tipo=notificacion";
		header("location:../adlogin.php?error=$error");
}
?>