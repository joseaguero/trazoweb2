<?php 

include(dirname(__DIR__).'/conf.php');

$producto_id = mysqli_real_escape_string($conexion, $_POST['id']);
$archivo = $_FILES['imagen'];

date_default_timezone_set('America/Santiago');

$name = $archivo['name'];
$tmpfile = $archivo['tmp_name'];
$explode_name = explode('.', $name);
$ext = strtolower(end($explode_name));
$new_file = $producto_id.'-'.md5(time() . $explode_name[0]) . '.' . $ext;

$allowed = array('jpg', 'gif', 'png', 'JPG', 'PNG', 'GIF', 'JPEG', 'jpeg');

if (in_array($ext, $allowed)) {

    $destino = '../../imagenes/packs';

    if (!file_exists($destino)) {
        mkdir($destino, 0775, true);
    }

    $dest_path = $destino . '/' . $new_file;
    if(!move_uploaded_file($tmpfile, $dest_path)){
        $_SESSION['packs']['message'] = 'Error al subir la imagen.';
        $_SESSION['packs']['status'] = 'error';
        header("Location: $_SERVER[HTTP_REFERER]");
        die();
    }else{
        $thumb = $url_global.'imagenes/packs/'.$new_file;
        $thumb_bd = consulta_bd('thumbs, tmp_name_pack', 'productos', "id = $producto_id", '');
        if (($thumb_bd[0][0] != NULL OR $thumb_bd[0][0] != '') AND ($thumb_bd[0][1] != NULL OR $thumb_bd[0][1] != '')) {
            $update = update_bd('productos', "thumbs = NULL, tmp_name_pack = NULL", "id = $producto_id");
            unlink("../../imagenes/packs/{$thumb_bd[0][1]}");
        }

        $update = update_bd('productos', "thumbs = '$thumb', tmp_name_pack = '$new_file'", "id = $producto_id");
    }

}else{
    $_SESSION['packs']['message'] = 'Extensión del archivo no permitida.';
    $_SESSION['packs']['status'] = 'error';
    header("Location: $_SERVER[HTTP_REFERER]");
    die();
}

$_SESSION['packs']['message'] = 'Se ha guardado correctamente.';
$_SESSION['packs']['status'] = 'success';
header("Location: $_SERVER[HTTP_REFERER]");
die();

?>