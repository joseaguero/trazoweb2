<?php 
date_default_timezone_set('America/Santiago');
include('../conf.php');
$query = consulta_bd('
	ped.oc, e.nombre, ped.giro, ped.nombre, ped.rut, ped.email, ped.telefono, ped.retiro_en_tienda, ped.direccion, ped.region, ped.ciudad, ped.comuna, ped.fecha, ped.fecha_despacho, ped.codigo_descuento, ped.descuento, ped.total, ped.valor_despacho, ped.total_pagado, ped.cant_productos, ped.factura, ped.razon_social, ped.direccion_factura, ped.rut_factura, ped.email_factura, ped.transaction_date',
	'pedidos ped JOIN estados e ON ped.estado_id = e.id', '', '');

header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=pedidos_".time()."BD.xls"); 
header("Pragma: no-cache" );
header("Expires: 0"); 

$hoy = date("d/m/Y");

?>
<h3>Base de datos de pedidos, fecha de reporte <?php echo "$hoy";?></h3>
<table border="1">
	<thead>
		<tr>
			<td>OC</td>
			<td>estado</td>
			<td>giro</td>
			<td>nombre</td>
			<td>rut</td>
			<td>email</td>
			<td>telefono</td>
			<td>retiro_en_tienda</td>
			<td>direccion</td>
			<td>region</td>
			<td>ciudad</td>
			<td>comuna</td>
			<td>fecha_web</td>
			<td>fecha despacho</td>
			<td>codigo_descuento</td>
			<td>descuento</td>
			<td>total</td>
			<td>valor_despacho</td>
			<td>total_pagado</td>
			<td>cant_productos</td>
			<td>factura</td>
			<td>razon_social</td>
			<td>direccion_factura</td>
			<td>rut_factura</td>
			<td>email_factura</td>
			<td>webpay_dia</td>
			<td>webpay_mes</td>
			<td>webpay_año</td>
			<td>fecha_webpay</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($query as $item) { 
			$fecha = date('d/m/Y', strtotime($item[25]));
			$split = explode('/', $fecha);
			$dia = $split[0];
			$mes = $split[1];
			$año = $split[2];
		?>
		<tr <?= ($item[1] == 'Pagado') ? 'style="background: #66c878;"' : ''?>>
			<td><?= $item[0] ?></td>
			<td><?= $item[1] ?></td>
			<td><?= $item[2] ?></td>
			<td><?= $item[3] ?></td>
			<td><?= $item[4] ?></td>
			<td><?= $item[5] ?></td>
			<td><?= $item[6] ?></td>
			<td><?= $item[7] ?></td>
			<td><?= $item[8] ?></td>
			<td><?= $item[9] ?></td>
			<td><?= $item[10] ?></td>
			<td><?= $item[11] ?></td>
			<td><?= $item[12] ?></td>
			<td><?= $item[13] ?></td>
			<td><?= $item[14] ?></td>
			<td><?= $item[15] ?></td>
			<td><?= $item[16] ?></td>
			<td><?= $item[17] ?></td>
			<td><?= $item[18] ?></td>
			<td><?= $item[19] ?></td>
			<td><?= $item[20] ?></td>
			<td><?= $item[21] ?></td>
			<td><?= $item[22] ?></td>
			<td><?= $item[23] ?></td>
			<td><?= $item[24] ?></td>
			<td><?= ($item[25] != '' or $item[25] != NULL) ? $dia : '' ?></td>
			<td><?= ($item[25] != '' or $item[25] != NULL) ? $mes : '' ?></td>
			<td><?= ($item[25] != '' or $item[25] != NULL) ? $año : '' ?></td>
			<td><?= ($item[25] != '' or $item[25] != NULL) ? $fecha : '' ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>