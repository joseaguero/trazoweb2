<?php
	include("../conf.php");
	$tabla = $_GET[tabla];
	$id = (is_numeric($_GET[id])) ? 


    ($conexion, $_GET[id]):0;
	$idPadre = (is_numeric($_GET[idPadre])) ? mysqli_real_escape_string($conexion, $_GET[idPadre]):0;
	
	$op = (isset($_GET[op])) ? mysqli_real_escape_string($conexion, $_GET[op]):0;
	$opRetorno = (isset($_GET[op])) ? 


    ($conexion, $_GET[op]):0;
	
	$id_tabla = consulta_bd("id, nombre","tablas","nombre='$tabla'","id desc");
	$id_tabla = $id_tabla[0][0];
	$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $id_tabla","");
	$i = 0;
	while($i <= (sizeof($opciones)-1))
	{	
		$nombre = $opciones[$i][0];
		$opcion[$nombre] = $opciones[$i][1];
		$i++;
	}
	if(sizeof($opciones)>0)
	extract($opcion);
	
	$edit = consulta_bd_por_id("*",$tabla,"id=$id",$id);
	
	$opcionesTiny = consulta_bd("valor","opciones","nombre='tiny_mce'","");
	$tiny_mce = $opcionesTiny[0][0];
?>

<!--<script type="text/javascript" src="../js/jquery-1.8.0.js"></script>
<link href="../css/layout.css" rel="stylesheet" />-->


<div class="contenido">
	<h2 class="titulo_has_many_edit">Edicion: <?php echo $edit[nombre]; ?></h2>
    <form id="edit_has_many" enctype="multipart/form-data" method="post" name="form" action="app/actions.php">
        
		
      
        
        <table width="100%" border="0">
                <?php 	
                //Obtengo los valores según el id
                $columnas = show_columns($tabla,$exclusiones);
                //die($tabla);
                $lista_columnas = implode(",", $columnas);
                $filas = consulta_bd($lista_columnas,$tabla,"id = '$id'","");
        
                $i = 0;
                while($i <= (sizeof($columnas)-1))
                {	
                    $column = get_real_name($columnas[$i]);
                    //Ponemos los * en los campos obligatorios.
                    if ($obligatorios_edit != '')
                    {
                        if (substr_count($opcion['obligatorios_edit'],",")>0)
                        {
                            $obligatorios_edit = explode(',', $opcion['obligatorios_edit']);
                            foreach ($obligatorios_edit as $obl)
                            {
                                $obl = trim($obl);
                                if ($columnas[$i] == $obl)
                                {
                                    $column = "$column *";
                                }
                            }
                        }
                        else
                        {
                            if ($columnas[$i] == trim($obligatorios_edit))
                            {
                                $column = "$column *";
                            }
                        }
                    }
                    
                    $val = ($filas[0][$i]!='') ? $filas[0][$i]: $_GET[$columnas[$i]];
                    $column_id = $columnas[$i];
                    
                    $parent = column_is_related($columnas[$i]);
        
                    //RELACIONES
                    if ($parent)
                    {
                        //Si el padre tiene una relación, el select debe ser dependiente (belongs_to) o se debe establecer una relación many_to_many
                        //Consulto por la opción many_to_many del padre
                        $parent_plural = plural($parent);
                        $many_to_many_q = consulta_bd("ot.nombre, ot.valor","opciones_tablas ot, tablas t","t.nombre = '$parent_plural' AND ot.tabla_id = t.id AND (ot.nombre = 'many_to_many' OR ot.nombre = 'belongs_to')","");
                        $j = 0;
                        while($j <= (sizeof($many_to_many_q)-1))
                        {	
                            $nombre_p = $many_to_many_q[$j][0];
                            $opcion_p[$nombre_p."_parent"] = $many_to_many_q[$j][1];
                            $j++;
                        }
                        
                        if (isset($belongs_to_parent))
                        $belongs_to_parent = NULL;
                        
                        if ($opcion_p != NULL)
                        extract($opcion_p);
                        
                        $opcion_p = NULL;
        
                        if ($belongs_to_parent != '')
                        {
                            //En caso de que sea una entrada nueva muestro al padre lleno y al hijo vacío
                            $sel2 = $columnas[$i];
                            if (!isset($id) OR $val == '')
                            {
                                echo '<tr>';
                                echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
                                echo '<td>';
                                    select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', '');
                                echo '</td>';
                                echo '</tr>';
                                
                                echo '<tr>';
                                echo '<td>'.get_real_name($parent).'</td>';
                                echo '<td>';
                                    select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', '');
                                echo '</td>';
                                echo '</tr>';	    				
                            }
                            else
                            {
                                //Obtengo el id del padre de la relación	
                                $column_parent = singular($belongs_to_parent)."_id";
                                $rel_id_q = consulta_bd($column_parent,plural($parent),"id = $val","");
                                $rel_id = $rel_id_q[0][0];
                                
                                echo '<tr>';
                                echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
                                echo '<td>';
                                    select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', $rel_id);
                                echo '</td>';
                                echo '</tr>';
                                
                                echo '<tr>';
                                echo '<td>'.get_real_name($parent).'</td>';
                                echo '<td>';
                                    select(plural($parent), 'nombre', 'id', $sel2, '', "", '', '');
                                echo '</td>';
                                echo '</tr>';
                            }
                            $btp = $belongs_to_parent;
                            $related_selects_btp[] = $btp;
                            $sel2_btp[] = $sel2;
                        }
                        else
                        {
                            echo '<tr>';
                            echo '<td>'.get_real_name($parent).'</td>';
                            echo '<td>';
                                select(plural($parent), 'nombre', 'id', $columnas[$i], '', "", '', $val);
                            echo '</td>';
                            echo '</tr>';
                        }	
                    }
                    elseif($autocompletar != '')
                    {
                        //Recorro los autocompletar
                        $autocompletar_array = explode(';',$autocompletar);
                        foreach ($autocompletar_array as $autocompletar_item)
                        {
                            $datos_autocompletar = explode(',', $autocompletar_item);
                            $tabla_autocompletar = trim($datos_autocompletar[0]);
                            $columna_autocompletar = trim($datos_autocompletar[1]);
                            
                            if ($column_id === $columna_autocompletar)
                            {
                                
                                ?>
                                    <script>
                                        $(function(){
											alert("entra");
                                            $('#autocomplete_<?php echo $column_id?>').autocomplete({
                                                source: "action/autocomplete.php?tabla=<?php echo $tabla_autocompletar;?>",
                                                minLength: 2,
                                                select: function(event, ui){
                                                    $('#<?php echo $column_id?>').val(ui.item.id);
                                                }
                                            });
                                        });
                                    </script>
                                    <tr>
                                        <td valign="top"><?php echo $column ?></td>
                                        <td>
                                            <input class="campo_texto" type="text" id="autocomplete_<?php echo $column_id; ?>" />
                                            <input type="hidden" id="<?php echo $column_id; ?>" name="<?php echo $tabla.'['.$column_id.']'; ?>" value="" />
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                        
                        //$autocompletar = '';
                        
                    }
                    elseif (stristr($archivos,$columnas[$i]))
                    {
                        echo '<tr>';
                        echo '<td valign="top">'.$column.'</td>';
						if($edit[$column_id] != ''){
							
							echo '<td class="contieneImagen">
							<img class="del" width="30" border="0" height="30"';
							echo 'onclick="delete_img(';
							echo "'".$id."', '".$tabla."', '".$column."')";
							echo '" src="pics/delete.png" style="display: none;">';
							echo "<img src='../imagenes/".$tabla."/".$edit[$column_id]."' width='200' />
							</td>";
						} else {
                        	echo '<td><input type="file" id="'.$column_id.'" name="'.$column_id.'" /></td>';
						}
						echo '</tr>';
                    }
                    elseif (is_boolean(trim($columnas[$i]), $tabla))
                    {
                        $publicado = ($val == 1) ? "checked='checked'": "";
                        echo '<tr>';
                        echo '<td>'.$column.'</td>';
                        echo '<td><input type="checkbox" id="'.$column_id.'" name="'.$column_id.'" '.$publicado.'/></td>';
                        echo '</tr>';
                    }
                    elseif (is_enum(trim($columnas[$i]), $tabla))
                    {
                        $enum_vals =  enum_vals(trim($columnas[$i]), $tabla);
                        echo '<tr>';
                        echo '<td>'.$column.'</td>';
                        echo '<td><select name="'.$tabla.'['.$column_id.']'.'" id="'.$column_id.'"><option>Seleccione</option>';
                        foreach ($enum_vals as $e)
                        {
                            echo ($val == $e) ? "<option value='$e' selected>".ucwords(str_replace("_", " ", $e))."</option>":"<option value='$e'>".ucwords(str_replace("_", " ", $e))."</option>";
                        }
                        echo '</select>';
                        echo '</td>';
                        echo '</tr>';
                    }
                    elseif ($columnas[$i] == 'password')
                    {
                        echo '<tr>';
                        echo '<td>'.$column.'</td>';
                        echo '<td><input class="campo_texto" type="password" id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'"/></td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>'.$column.' Check</td>';
                        echo '<td><input class="campo_texto" type="password" id="'.$column_id.'_check" name="'.$tabla.'['.$column_id.']'.'_check"/></td>';
                        echo '</tr>';
                    }
                    elseif (stristr($tiny_mce,$columnas[$i]))
                    {
                        echo '<tr>';
                        echo '<td valign="top">'.$column.'</td>';
                        echo '<td><textarea id="'.$column_id.'" name="'.$column_id.'">'.$edit[$column_id].'</textarea></td>';
                        echo '</tr>';
                    }
                    else
                    {
                        if ($columnas[$i] == 'rut')
                        {
                            echo '<tr>';
                            echo '<td>'.$column.'</td>';
                            echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'"  onblur="javascript:formato(this.value,'."'$column_id'".');"/></td>';
                            echo '</tr>';
                        }
                        else
                        {
                            echo '<tr>';
                            echo '<td>'.$column.'</td>';
                            echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$column_id.'" value="'.$edit[$column_id].'" /></td>';
                            echo '</tr>';
							
                        }
						
                    }
                    $i++;
                }
                $t++;
            ?>
            
            
        <input type="hidden" name="tabla" value="<?php echo $tabla ?>" />
		<input type="hidden" name="op" value="<?php echo $id_tabla ?>c" />
		<input type="hidden" name="id" value="<?php echo $id ?>" />
		<input type="hidden" name="exception" value="vacio, id, fecha_creacion, fecha_modificacion, modificado_por, creado_por, producto_id" />
        <input type="hidden" name="opRetorno" value="<?php echo $opRetorno; ?>" />
        <input type="hidden" name="idPadre" value="<?php echo $idPadre; ?>" />
        <input type="hidden" name="productoInterior" value="1" />
        
            <tr>
                <td></td>
                <td>
                <input type="submit" name="update_<?php echo $tabla ?>" id="update_<?php echo $tabla ?>"/>
                	<!--<a class="btn_actualizar" href="javascript:void(0)" >Actualizar</a> -->
                </td>
            </tr>
        </table>
    </form>
</div>
	<!-- TinyMCE -->
<script type="text/javascript" src="js/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".contieneImagen").hover(function(){
		$(this).find('img.del').fadeIn(200);
	}, function(){
		$(this).find('img.del').fadeOut(200);
	});
});//fin funcion


	tinymce.init({
		// General options
		language : "es",
		selector: "textarea",
		//mode : "textareas",
		theme : "modern",
		width: 800,
    	height: 150,
		elements : "<?php echo $tiny_mce; if ($tienda) {echo ",valor_opcion_tienda";} ?>",
		content_css: "css/content.css",
		plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "js/tiny_mce/skins/lightgray/content.min.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
	});
	
	function activar_tiny_mc(){
		tinymce.init({
			// General options
			language : "es",
			selector: "textarea",
			//mode : "textareas",
			theme : "modern",
			width: 800,
			height: 150,
			elements : "<?php echo $tiny_mce; if ($tienda) {echo ",valor_opcion_tienda";} ?>",
			content_css: "css/content.css",
			plugins: [
			 "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			 "save table contextmenu directionality emoticons template paste textcolor"
	   ],
	   content_css: "js/tiny_mce/skins/lightgray/content.min.css",
	   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
	   style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		]
		});
	};
</script>
