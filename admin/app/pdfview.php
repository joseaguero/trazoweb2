<?php 
/********************************************************\
|  Moldeable CMS - Vista previa de PDF.				 	 |
|  Fecha Modificación: 25/09/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

$file = consulta_bd_por_id("archivo, nombre",$tabla,"",$id);
$url_sitio = get_option('url_sitio');
$file_url = "$url_sitio/docs/$tabla/".$file['archivo'];
$file_name = $file['nombre'];

?>


<div id="botonera">
	<div id="titulo">
		<h3>Archivo PDF - <?php echo $file_name;?></h3>
	</div>
</div>



<iframe src="http://docs.google.com/viewer?url=<?php echo $file_url; ?>&embedded=true" width="100%" height="550" style="border: none;"></iframe>
