<?php

/********************************************************\
|  Moldeable CMS - Botonera de vista edición.            |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

$real_val_op = $val_op;
$val_op = (is_numeric($_GET[last])) ? mysqli_real_escape_string($conexion, $_GET[last]): $val_op;

?>
<div id="botonera">
	<div id="titulo">
		<h3>
			<?php 
			echo (isset($id)) ? "Editar ": "Agregar ";
			echo get_real_name(singular(get_table_name($real_val_op)));
			?>
		</h3>
	</div>
	<div id="botones">
                <?php
                    if ($sm_table[0][0] != '')
                    {
                        $rid = $_GET[rid];
                        $rel="&r=$submenu_table&id=$rid";
                    } 
$obligatorio = consulta_bd("valor","opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t","nombre = 'obligatorios' AND tabla_id = t.id","");
			$obligatorios = $obligatorio[0][0];
			if ($obligatorios == '')
			{
				$next = 'a';
			} else {
				$next = 'b';
			}
			    ?>
		<button type="button" class="boton3 volver-edit" onclick="javascript:volver('index.php?op=<?php echo (is_numeric($val_op)) ? $val_op."a".$rel:$val_op;?>')">Volver</button>
		<?php if ($duplicable) { ?>
		<button type="button" class="boton3" onclick="javascript:volver('app/duplicate_entry.php?val_op=<?php echo "$val_op&id=$id";?>')">Duplicar</button>
		<?php } if ($create OR !isset($create)) { ?>
		<button type="button" class="boton3 nuevo-edit" onclick="javascript:volver('index.php?op=<?php echo $val_op.$next ?>')"  style="margin-right:20px;color:red">Nuevo</button>	
		<?php } if ($galerias) { 
		$upGal = consulta_bd("valor","opciones","nombre='upload_galerias'","");
		if($upGal[0][0] == 1){
		?>
        <a href="js/fancyupload/index.php?id=<?php echo $id;?>&tabla=<?php echo $tabla;?>" class="boton4 color_box imagenes-edit" >Agregar imágenes</a>
        <?php } else { ?>
			<a href="jQuery-File-Upload/index.php?id=<?php echo $id;?>&tabla=<?php echo $tabla;?>" class="boton4 color_box imagenes-edit" >Agregar imágenes jquery</a>
		<?php }	?>		
			
            
		<?php } if ($create OR !isset($create)) { ?>
		<a onclick="javascript:confirmar('<?php echo $id;?>', 'del', '', '<?php echo $tabla;?>', '<?php echo $rid;?>', '')" class="boton2 eliminar-edit">Eliminar</a>
		<?php } ?>
		<?php if (!isset($save) or $save == 1) { ?>	
		<input type="submit" name="<?php echo (isset($id)) ? "update_$tabla": "add_$tabla";?>" id="<?php echo (isset($id)) ? "update_$tabla": "add_$tabla";?>" value="Guardar" class="guardar-edit"/>
		<?php } ?>
		<input type="hidden" name="tabla" value="<?php echo $tabla; ?>" />
		<input type="hidden" name="op" value="<?php echo $op; ?>" />
		<?php if (isset($id)) { ?>
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?php } ?>
		<input type="hidden" name="exception" value="<?php echo $exception_update; ?>" />
	</div>
</div>
<?php
	$val_op = (is_numeric($_GET[last])) ? mysqli_real_escape_string($conexion, $_GET[last]): 0;
?>