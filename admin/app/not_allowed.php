<?php
/********************************************************\
|  Moldeable CMS - Página no encontrado.		         |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/
?>

<div id="botonera">
	<div id="titulo"><h3>Acceso Restringido / P&aacute;gina no encontrada</h3></div>
	<div id="botones">
		<button type="button" class="boton3" onclick="javascript:back();">Volver</button>	
	</div>
</div>
<br/>
<div id="noticia">     
     	<p>Ud no posee los permisos suficientes para ver esta secci&oacute;n o la p&aacute;gina que buscaba ya no est&aacute; disponible.<br /><br />Por favor comun&iacute;quese con el administrador del sistema para solucionar esta situaci&oacute;n.<br /><br />
  </p>
  <p class="style1">&nbsp;</p>
  <p>&nbsp;</p>
</div>

