<?php

/********************************************************\
|  Moldeable CMS - Excel generator.			             |
|  Fecha Modificación: 16/06/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');
include('../conf.php');
mysqli_query ($conexion, "SET NAMES 'latin1'");

$tabla = mysqli_real_escape_string($conexion, $_GET[tabla]);


header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=".$tabla."_BD.xls"); 
header("Pragma: no-cache" );
header("Expires: 0"); 

$hoy = date("d/m/Y");

?>

<h3>Base de datos de <?php echo $tabla;?>, fecha de reporte <?php echo "$hoy";?></h3>
<table width="700" border="1">
        <thead>
            <tr>
            	<?php 
            		$columnas = show_columns($tabla, '');
            		$lista_columnas = implode(",", $columnas);
            		$i = 0;
            		while($i <= (sizeof($columnas)-1))
            		{	
            		
            			$related[$i] = column_is_related($columnas[$i]);
            			$colum = get_real_name(strtoupper($columnas[$i]));
        				if ($related[$i])
        				{
        					echo '<th><h3>'.strtoupper($related[$i]).'</h3></th>';
        				}
        				else
        				{
        					echo '<th><h3>'.$colum.'</h3></th>';
        				}
            			
            			if (is_inside(get_option('checkboxes'), $columnas[$i]))
            			$id_c_checkboxes = $i;            			
            			
            			if ($columnas[$i] == 'fecha_creacion')
            			$id_c_fechac = $i;
            			
            			$i++;
            		}
            	?>
            </tr>
        </thead>
        <tbody>
			<?php
				
				$order_by = "id DESC";
				$sql = consulta_bd($lista_columnas,$tabla,"",$order_by);
				
				$i = 0;
				while($i <= (sizeof($sql)-1))
				{
					$id = $sql[$i][0];
					
				 	if ($i%2!=0)
					{
					 	echo '<tr>';
					}
					else
					{
					 	echo '<tr bgcolor="#ddd">';
					}
					
					if (isset($id_c_checkboxes))
					{
						if ($sql[$i][$id_c_checkboxes] == 1)
						{
							$val_checkbox = "Si";
						}
						else
						{
							$val_checkbox = "No";
						}
					}

					
					$m = $i+1;
					$fecha = fecha_sql($sql[$i][$id_c_fechac]);
					
					for($j = 0; $j <= sizeof($columnas)-1; $j++)
					{
						if (isset($id_c_checkboxes))
						{
							if ($j == $id_c_checkboxes)
							{
								echo '<td>'.$val_checkbox.'</td>';
							}
							else
							{
								if ($related[$j])
								{
									$id = $sql[$i][$j];
									$val_related = consulta_bd_por_id('nombre', plural($related[$j]), '', $id);
									$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
									echo '<td>'.ucwords($data).'</td>';
								}
								else
								{
									$data = ($sql[$i][$j] == '') ? "&nbsp;": $sql[$i][$j];
									echo '<td>'.$data.'</td>';
								}
							}
						}
						else
						{
							if ($related[$j])
							{
								$id = $sql[$i][$j];
								$val_related = consulta_bd_por_id('nombre', plural($related[$j]), '', $id, '');
								echo '<td>'.ucwords($val_related['nombre']).'</td>';
							}
							else
							{
								echo '<td>'.$sql[$i][$j].'</td>';
							}
						}
						
					}
					
					$i++;
				}
			  ?>
        </tbody>

</table>