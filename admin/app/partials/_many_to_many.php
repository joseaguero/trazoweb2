<?php
if (isset($many_to_many) AND isset($id))
{
	if (is_array($many_to_many))
	{
		foreach ($many_to_many as $many)
		{
		?>
		<div id="tab-<?php echo $t;?>">
			<p>Seleccione <?php echo get_real_name($many);?>:</p>
			<script>
				$(document).ready(function(){
					$("#checkchildren_<?php echo $t;?>").checkboxTree({	initializeChecked: 'expanded', initializeUnchecked: 'collapsed'});
				});
			</script>
			<ul id="checkchildren_<?php echo $t;?>" class="unorderedlisttree">
			<?php
				//Se establece el nombre de la tabla relación
				$mtm_table = $many."_".$tabla;
				
				//Veo si la relacion tiene una tabla belongs_to
				$belongs_to_q = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$many'","");
				$belongs_to_id = $belongs_to_q[0][0];
				
				//Busco los nombres y los id de la relación
				$sql = consulta_bd('id, nombre',"$many",'','id DESC');
				$i = 0;
				while($i <= (sizeof($sql)-1))
				{
					$rel1 = singular($tabla)."_id";
					$rel2 = singular($many)."_id";
					
					$id_mtm = $sql[$i][0];
					$nombre_mtm = str_replace(" ", "_", $sql[$i][1]);
					$col = $nombre_mtm."_".$id_mtm;
					
					echo '
						<li>
							<input type="checkbox" name="'.$col.'" ';
							
							$sql2 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel2 = '".$sql[$i][0]."'","");
							$cant_sql2 = $sql2[0][0];
							if ($cant_sql2 > 0)
							{
								echo 'checked="checked"';
							}
							
					echo '/><label for="'.$col.'">'.$sql[$i][1].'</label>';
					if ($belongs_to_id)
					{
						//Veo que tabla es la belongs_to_id
						$bt_table = strtolower(get_table_name($belongs_to_id));
						$bt_rel = singular($bt_table).'_id';
						$belongs_to_items = consulta_bd("id, nombre","$bt_table","$rel2 = $id_mtm","");
						$j = 0;
						echo '<ul>';
						while($j <= (sizeof($belongs_to_items)-1)) {
							$val = $belongs_to_items[$j][1];
							$id_bt = $belongs_to_items[$j][0];
							$col_bt = $col.'_'.$id_bt;
							echo '<li>';
								echo '<input type="checkbox" name="'.$col_bt.'" id="'.$col_bt.'"';
								$sql3 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $bt_rel = '".$id_bt."'","");
								$cant_sql3 = $sql3[0][0];
								if ($cant_sql3 > 0)
								{
									echo 'checked="checked"';
								}
								echo '/><label for="'.$col_bt.'">'.$val.'</label>';
							echo '</li>';
							$j++;
						}
						echo '</ul>';
					}					
					echo '</li>';
						$i++;
				}
			?>
			</ul>
		</div>
		<?php	
			$t++;
		}
	}
	else
	{
	?>
		<div id="tab-<?php echo $t;?>">
			<p>Seleccione <?php echo get_real_name($many_to_many);?>:</p>
			<ul id="checkchildren" class="unorderedlisttree">
			<?php
				//Se establece el nombre de la tabla relación
				$mtm_table = $many_to_many."_".$tabla;
				
				//Veo si la relacion tiene una tabla belongs_to
				$belongs_to_q = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$many_to_many'","");
				$belongs_to_id = $belongs_to_q[0][0];
				
				//Busco los nombres y los id de la relación
				$sql = consulta_bd('id, nombre',"$many_to_many",'','id ASC');
				$i = 0;
				while($i <= (sizeof($sql)-1))
				{
					$rel1 = singular($tabla)."_id";
					$rel2 = singular($many_to_many)."_id";
					
					$id_mtm = $sql[$i][0];
					$nombre_mtm = str_replace(" ", "_", $sql[$i][1]);
					$col = $nombre_mtm."_".$id_mtm;
					
					echo '
						<li>
							<input type="checkbox" name="'.$col.'" id="'.$col.'" ';
							
							$sql2 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel2 = '".$sql[$i][0]."'","");
							$cant_sql2 = $sql2[0][0];
							if ($cant_sql2 > 0)
							{
								echo 'checked="checked"';
							}
							
					echo '/><label for="'.$col.'">'.$sql[$i][1].'</label>';
					if ($belongs_to_id)
					{
						//Veo que tabla es la belongs_to_id
						$bt_table = obtener_tabla($belongs_to_id);
						$bt_rel = singular($bt_table).'_id';
						$belongs_to_items = consulta_bd("id, nombre","$bt_table","$rel2 = $id_mtm","");
						$j = 0;
						echo '<ul>';
						while($j <= (sizeof($belongs_to_items)-1)) {
							$val = $belongs_to_items[$j][1];
							$id_bt = $belongs_to_items[$j][0];
							$col_bt = $col.'_'.$id_bt;
							echo '<li>';
								echo '<input type="checkbox" name="'.$col_bt.'" id="'.$col_bt.'"';
								$sql3 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $bt_rel = '".$id_bt."'","");
								$cant_sql3 = $sql3[0][0];
								if ($cant_sql3 > 0)
								{
									echo 'checked="checked"';
								}
								echo '/><label for="'.$col_bt.'">'.$val.'</label>';
							echo '</li>';
							$j++;
						}
						echo '</ul>';
					}
					echo '</li>';
						$i++;
				}
				$t++;
			?>
			</ul>
		</div>		
	<?php
	}
}
?>