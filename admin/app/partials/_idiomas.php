<?php

$columnas = show_columns($tabla,$exclusiones);
$lista_columnas = implode(",", $columnas);
$filas = consulta_bd($lista_columnas,$tabla,"id = '$id'","");

if (isset($lista_idiomas))
{
/* 		$lista_idiomas = explode(",", $idiomas); */
	foreach($lista_idiomas as $idioma)
	{	
		?>
		<div id="tab-<?php echo $t; ?>">
			<table width="100%" border="0">
			<?php
				$buscar = "_".trim($idioma);
				$i = 0;
	    		while($i <= (sizeof($columnas)-1))
	    		{	
					$val = ($filas[0][$i]!='') ? $filas[0][$i]: $_GET[$columnas[$i]];
    				$column_id = $columnas[$i];
    				
					if (strpos($columnas[$i], $buscar))
	    			{
	    			
	    				$column = str_replace(trim($idioma), '', $columnas[$i]);
	    				$column = get_real_name($column);
	    			
						if (stristr($tiny_mce,$columnas[$i]))
		    			{
		    				echo '<tr>';
		    				echo '<td valign="top">'.$column.'</td>';
		    				echo '<td><textarea id="'.$column_id.'" name="'.$column_id.'">'.$val.'</textarea></td>';
		    				echo '</tr>';
		    			}
		                elseif (stristr($archivos,trim($columnas[$i])))
		                {
		                    $file_ext = strtolower(substr(strrchr($val, '.'), 1));
		                    
		                   if ($file_ext == 'jpg' or $file_ext == 'gif' or $file_ext == 'png' or $file_ext == 'jpeg')
					{
						$f = $dir.'/'.$val;
						$size=getimagesize($f); 
						$width=$size[0]; 
						$height=$size[1];
						
						if ($width>300)
						{
							$tam = "width='320'";
							$width = 300;
						}
						elseif($height>300)
						{	
							$tam = "height='300'";
						}
						else
						{
							$tam = '';
							$width = $width-40;
						}
						
						echo '<tr class="fila_imagen_'.$columnas[$i].'">';
						echo '<td valign="top">'.$column.' actual:</td>';
						echo '<td>';
						echo	  '<a class="img_admin" href="javascript:void(0)" alt="Borrar" title="Borrar">
										<img class="btn_borrar_imagen" onclick="delete_img(';
										echo "'$id', '$tabla', '$columnas[$i]'";
										echo ')"
											src="pics/delete.png" border="0"/>
										
										<img src="'.$f.'" border="0" '.$tam.' id="cont_img_actual_'.$columnas[$i].'"/>
									</a>';
						echo 	'</td>';
						echo '</tr>';
						
						
					}
					elseif ($file_ext != '')
					{
						echo '<tr>';
					    echo '<td valign="bottom">'.$column.' actual:</td>';
						echo '<td>
								<div id="file_name_for">
									<a href="'.$dir_docs.'/'.$val.'">'.$val.'</a>
									<a href="#" onclick="javascript:delete_file(';
									echo "'$id','$tabla','$columnas[$i]','$val'";
									echo ')" >
									<img src="pics/delete.jpg" border="0" alt="Eliminar Archivo" title="Eliminar Archivo">
								</div>
								</a>
							  </td>';	
						echo '</tr>';
						
					}
					
					
					$paso1 = consulta_bd("id","tablas","nombre='$tabla'","id desc");
					$paso4 = consulta_bd("valor","opciones_tablas","tabla_id=".$paso1[0][0]." and nombre ='crop'","id desc");
					$campos_separados = explode(",", $paso4[0][0]);
					
					if(has_array($campos_separados, $columnas[$i])){
							
							//die("1");
						} else {
							//die("2");
							echo '<tr class="'.$columnas[$i].' '.$oculto.'">';
								echo '<td valign="top">'.$column.'</td>';
								echo '<td><input type="file" id="'.$column_id.'" name="'.$column_id.'" /></td>';
							echo '</tr>';
						}
						/* 
					for($k=0; $k<sizeof($campos_separados); $k++){
						if($campos_separados[$k] != $columnas[$i]){
							if($val != ''){
								$oculto = 'oculto';
							} else {
								$oculto = '';
							}
							echo '<tr class="'.$columnas[$i].' '.$oculto.'">';
								echo '<td valign="top">'.$column.'</td>';
								echo '<td><input type="file" id="'.$column_id.'" name="'.$column_id.'" /></td>';
							echo '</tr>';
						}
					}  */
					
					
					for($k=0; $k<sizeof($campos_separados); $k++){
						if($campos_separados[$k] == $columnas[$i]){
							$medidas = consulta_bd("valor","opciones_tablas","nombre='$campos_separados[$k]' and tabla_id=".$paso1[0][0],"");
							//echo $medidas[0][0];
							if($val != ''){
								$oculto = 'oculto';
							} else {
								$oculto = '';
							}
							$medidas_separadas = explode(",", $medidas[0][0]);
							$ancho_crop = trim($medidas_separadas[0]);
							$alto_crop = trim($medidas_separadas[1]);
							echo '<tr class="'.$columnas[$i].' '.$oculto.'" >
									<td valign="top">'.$column.'</td>
									<td>
										<div id="malla" class="cont_crop" style="float:left; width:'.$ancho_crop.'px; height: '.$alto_crop.'px" >
											<div class="cropContainerModal" id="'.$column_id.'_cropContainerModal" rel="'.$f.'"></div>
										</div>
									</td>
								</tr>';
						}
					}//fin for
					
				} 
				elseif (is_boolean(trim($columnas[$i]), $tabla))
                {
                        $publicado = ($val == 1) ? "checked='checked'": "";
                        echo '<tr>';
                        echo '<td>'.$column.'</td>';
                        echo '<td><input type="checkbox" id="'.$column_id.'" name="'.$column_id.'" '.$publicado.'/></td>';
                        echo '</tr>';
                }
		    			else
		    			{
		    				echo '<tr>';
		    				echo '<td>'.$column.'</td>';
		    				echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$column_id.'" value="'.$val.'"/></td>';
		    				echo '</tr>';
		    			}
	    			}
	    			$i++;
	    		}
			?>
			</table>
		</div>
		<?php
		$t++;
	}				
}
?>