<?php

/********************************************************\
|  Moldeable CMS - Duplicador de entradas.		         |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

include('../conf.php');

$id = $_GET[id];
$val_op = $_GET[val_op];
$table = consulta_bd_por_id("nombre", "tablas", "", $val_op);
$table = $table['nombre'];

//Obtengo las columnas de la tabla
$exclusiones = "id, fecha_creacion, fecha_modificacion, creado_por, modificado_por";
$columnas = show_columns($table, $exclusiones);
$lista_columnas = implode(",", $columnas);

$duplicate = "INSERT INTO $table ($lista_columnas) SELECT $lista_columnas FROM $table WHERE id = $id";
$run_d = mysqli_query($conexion, $duplicate) OR die($duplicate);

if ($run_d)
{
	$error = "Se ha duplicado el elemento exitosamente&tipo=exito";
}
else
{
	$error = "ERR: No se ha duplicado el elemento.&tipo=error";
}

$new_id = get_last($table);

header("location:../index.php?op=$val_op".'c'."&id=$new_id&error=$error");
	
?>