<?php
/********************************************************\
|  Moldeable CMS - Listado de entradas.		             |
|  Fecha Modificación: 4/04/2014		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

//Obtengo las opciones de la tabla
$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
$i = 0;
while($i <= (sizeof($opciones)-1))
{	
	$nombre = $opciones[$i][0];
	$opcion[$nombre] = $opciones[$i][1];
	$i++;
}
if(sizeof($opciones)>0)
extract($opcion);

if (isset($_GET[id]) and isset($_GET[r]))
{
    $submenu_table = consulta_bd_por_id("submenu_table","tablas","",$val_op);
    $submenu_table = $submenu_table['submenu_table'];
    $id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]):0;
    if(substr_count($submenu_table, ",")!=0){
        $tablas_menu = explode(",", $submenu_table);
        $tablas_menu_reverse_1 = array_reverse($tablas_menu);
        $abc = array('a','b','c','d','e');
        array_unshift($tablas_menu, $tabla);
        array_unshift($tablas_menu_reverse_1, $tabla);
        $tablas_menu_reverse = array_reverse($tablas_menu);
        for($c=0;$c<count($tablas_menu);$c++)
        {
           
           $tablas_consulta[] = $tablas_menu_reverse_1[$c]." ".$abc[$c];
           
           if ($c == (count($tablas_menu)-1))
           {
               $filtro .= $abc[$c].".id = ".$id;
           }
           else
           {
               $tabla_singular = singular(trim($tablas_menu_reverse[$c]));
               $filtro .= $abc[$c].".".$tabla_singular."_id = ".$abc[$c+1].".id";
               $filtro .= " and ";
           }
        }    
        $tablas_consulta = implode(',',$tablas_consulta);
        //c.id = 2 and c.id = b.edifio_id and b.id = a.tipo_id
    }
    else
    {
	//$submenu_table = (isset($_GET[r])) ? mysql_real_escape_string($_GET[r]):0;
	$filtro = singular($submenu_table)."_id = $id";
    }
    $tipo = consulta_bd_por_id("nombre",$_GET[r],"",$id);
    $tipo = $tipo['nombre'];

}
else
{
	$filtro_q = consulta_bd("filtro, save_in","tablas","id = $val_op","");
	$save_in = $filtro_q[0][1];
	$filtro_a = explode(" ",$filtro_q[0][0]);
	foreach ($filtro_a as $k => $f)
	{
		if ($f[0] == '$')
		{
			$resto_palabra = substr($f, 1);
			$filtro_a[$k] =  $$resto_palabra;
		}
	}
	$filtro = implode(" ", $filtro_a);
}

if ($sm_table[0][0] != '')
{
    $rid = "&rid=$id";
}

?>


    
  


<div id="botonera">
	<div id="titulo"><h3>Listado de <?php echo get_table_name($val_op); echo ($tipo) ? ' - '.$tipo : ''; ?></h3></div>
	<div id="botones">
		<?php
			$update = consulta_bd("valor","opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t","nombre = 'obligatorios' AND tabla_id = t.id","");
			$obligatorios = $update[0][0];
			if ($obligatorios == '')
			{
				$next = 'c';
			}
			else
			{
				$next = ($solo_update) ? 'c': 'b'.$rid;
			}
			
			if ($create != '0' OR !isset($create))
			{
				if ($save_in)
				{
					$filtro_url = str_replace(" ", '', $filtro);
					$next_vars = ($filtro) ? $save_in.$next."&".$filtro_url."&last=".$val_op : $save_in.$next."&last=".$val_op;
				}
				else
				{
					$next_vars = $val_op.$next;
				}
			
			?>
				<button type="button" class="boton3 agregar_nuevo" style="color:red" onclick="javascript:volver('index.php?op=<?php echo "$next_vars";?>')">Nueva entrada</button>
			<?php
			}
		?>
	</div>
</div>



<?php
	if($nombre=="filtro"){
		$letter = (isset($_GET[l])) ? mysqli_real_escape_string($conexion, $_GET[l]): 'a';
		?>
		<br />
		<a href="index.php?op=<?php echo $val_op; ?>a&l=a" <?php if ($letter == 'a') echo "style='color:red;'"?>>A</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=b" <?php if ($letter == 'b') echo "style='color:red;'"?>>B</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=c" <?php if ($letter == 'c') echo "style='color:red;'"?>>C</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=d" <?php if ($letter == 'd') echo "style='color:red;'"?>>D</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=e" <?php if ($letter == 'e') echo "style='color:red;'"?>>E</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=f" <?php if ($letter == 'f') echo "style='color:red;'"?>>F</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=g" <?php if ($letter == 'g') echo "style='color:red;'"?>>G</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=h" <?php if ($letter == 'h') echo "style='color:red;'"?>>H</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=i" <?php if ($letter == 'i') echo "style='color:red;'"?>>I</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=j" <?php if ($letter == 'j') echo "style='color:red;'"?>>J</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=k" <?php if ($letter == 'k') echo "style='color:red;'"?>>K</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=l" <?php if ($letter == 'l') echo "style='color:red;'"?>>L</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=m" <?php if ($letter == 'm') echo "style='color:red;'"?>>M</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=n" <?php if ($letter == 'n') echo "style='color:red;'"?>>N</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=ñ" <?php if ($letter == 'ñ') echo "style='color:red;'"?>>Ñ</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=o" <?php if ($letter == 'o') echo "style='color:red;'"?>>O</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=p" <?php if ($letter == 'p') echo "style='color:red;'"?>>P</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=q" <?php if ($letter == 'q') echo "style='color:red;'"?>>Q</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=r" <?php if ($letter == 'r') echo "style='color:red;'"?>>R</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=s" <?php if ($letter == 's') echo "style='color:red;'"?>>S</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=t" <?php if ($letter == 't') echo "style='color:red;'"?>>T</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=u" <?php if ($letter == 'u') echo "style='color:red;'"?>>U</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=v" <?php if ($letter == 'v') echo "style='color:red;'"?>>V</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=w" <?php if ($letter == 'w') echo "style='color:red;'"?>>W</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=x" <?php if ($letter == 'x') echo "style='color:red;'"?>>X</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=y" <?php if ($letter == 'y') echo "style='color:red;'"?>>Y</a> |
		<a href="index.php?op=<?php echo $val_op; ?>a&l=z" <?php if ($letter == 'z') echo "style='color:red;'"?>>Z</a>
		</br>
	<?php
	}
?>

<br />
<div id="loader"></div>
<div id="tablewrapper" style="display:none">
	<div id="tableheader">
    	<div class="search">
    		<em>Buscar por:</em><br />
            <select id="columns" class="select" onchange="sorter.search('query')"></select>
            <input type="text" id="query" onkeyup="sorter.search('query')" />
        </div>
        <span class="details">
			<div>Filas <span id="startrecord"></span>-<span id="endrecord"></span> de <span id="totalrecords"></span></div>
    		<div><a href="javascript:sorter.reset()">Quitar Filtros</a></div>
    	</span>
    </div>
    <table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
        <thead>
            <tr>
            	<?php 
            		$columnas = show_columns($tabla, $exclusiones_list);

                    if(substr_count($submenu_table, ",")!=0){
                        foreach ($columnas as $c)
                        {
                          $columnas_sm[] = "a.$c";  
                        }
                        $lista_columnas = implode(",", $columnas_sm);
                    }
                    else
                    {
                        $lista_columnas = implode(",", $columnas);
                    }
            		
					//Columnas con Autocompletar
					$columna_autocompletar_items = array();
					$tabla_autocompletar_items = array();
					$id_col_autocompletar = array();
					if ($autocompletar != ''){
						$autocompletar_array = explode(';',$autocompletar);
						$j=0;
						foreach ($autocompletar_array as $autocompletar_item)
						{
							$datos_autocompletar = explode(',', $autocompletar_item);
							$tabla_autocompletar_items[$j] = trim($datos_autocompletar[0]);
							$columna_autocompletar_items[$j] = trim($datos_autocompletar[1]);
							$j++;
						}
					}   
					//Fin columnas con autocompletar 
            		
            		$i = 0;
            		while($i <= (sizeof($columnas)-1))
            		{	
            		
            			$related[$i] = column_is_related($columnas[$i]);
            		
            			$colum = strtoupper(get_real_name($columnas[$i]));
            			if ($i == 0)
            			{
            				echo '<th class="nosort"><h3>'.$colum.'</h3></th>';
            			}
            			else
            			{
            				if ($related[$i])
            				{
            					
            					$belong_to_table = check_if_item_has_belongs_to($columnas[$i]);
            					if ($belong_to_table)
            					{
            						//Consulto si hay otra relacion
            						$columna_bt = singular($belong_to_table)."_id";
            						$belongs_to_parent = check_if_item_has_belongs_to($columna_bt);
            						if ($belongs_to_parent)
            						{
	            						//Consulto si hay otra relacion
	            						$columna_btp = singular($belongs_to_parent)."_id";
	            						$belongs_to_gran_parent = check_if_item_has_belongs_to($columna_btp);
	            						if ($belongs_to_gran_parent)
	            						{
		            						echo '<th><h3>'.strtoupper(get_real_name($belongs_to_gran_parent)).'</h3></th>';	
	            						}
	            						echo '<th><h3>'.strtoupper(get_real_name($belongs_to_parent)).'</h3></th>';	
            						}
	            					echo '<th><h3>'.strtoupper(get_real_name($belong_to_table)).'</h3></th>';
            					}
            					echo '<th><h3>'.strtoupper(get_real_name($related[$i])).'</h3></th>';
            				}
            				else
            				{
            					echo '<th><h3>'.$colum.'</h3></th>';
            				}
            			}           			
            			
						if ($columnas[$i] == 'modificado_por') {
	            			$id_modificado_por = $i;
	            		}
	            		elseif ($columnas[$i] == 'creado_por') {
	            			$id_creado_por = $i;
	            		}
	            		
						if (in_array($columnas[$i], $columna_autocompletar_items))
						$id_col_autocompletar[] .= $i;
	            		
            			$i++;
            		}
            		
            		if($asociados)
            		{
        				echo '<th><h3>'.strtoupper($asociados).' ASOCIADOS</h3></th>';
    				}
    				
            	?>
            	<?php if (!isset($edit) or $edit == 1) { ?>
                <th class="nosort" width="30"><h3>&nbsp;</h3></th>
                <th class="nosort" width="30"><h3>&nbsp;</h3></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
			<?php
				
				if ($order_list_by != '')
				{
					$order_by = $order_list_by;
				}
				else
				{
					$order_by = "id DESC";
				}

				if ($nombre == 'filtro')
				{
					$filtro = $opcion[$nombre]." LIKE '$letter%'";	
				}
				$sql = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro,$order_by) : consulta_bd($lista_columnas,$tabla,$filtro,$order_by);
			 	$cant = mysqli_affected_rows($conexion);
				$i = 0;
				while($i <= (sizeof($sql)-1))
				{
					$id = $sql[$i][0];
					
				 	if ($i%2!=0)
					{
					 	echo '<tr>';
					}
					else
					{
					 	echo '<tr bgcolor="#ddd">';
					}
					
					$m = $i+1;
					
					for($j = 0; $j <= sizeof($columnas)-1; $j++)
					{
						if (is_boolean($columnas[$j], $tabla))
						{
							//dejo editables por ajax los checkbox
							$val_checkbox = ($sql[$i][$j] == 1) ? "Si":"No";
							if ($sql[$i][$j] == 1){
								$marcado = 'checked="checked"';
							} else{
								$marcado = '';
							} 
							$val_rel = $columnas[$j].'-'.$sql[$i][0].'-'.$tabla;
							echo '<td><input type="checkbox" class="check_" name="" value="'.$sql[$i][$j].'" '.$marcado.' rel="'.$val_rel.'" /><span class="'.$val_rel.'">'.$val_checkbox.'</span></td>';
						}
						elseif(in_array($j, $id_col_autocompletar))
						{
							//Recorro los autocompletar
							foreach ($autocompletar_array as $autocompletar_item)
							{
								$datos_autocompletar = explode(',', $autocompletar_item);
								$tabla_autocompletar = trim($datos_autocompletar[0]);
								$columna_autocompletar = trim($datos_autocompletar[1]);
								
								if ($columnas[$j] == $columna_autocompletar)
								{
									$val_autocompletar = consulta_bd_por_id("nombre","$tabla_autocompletar","",$sql[$i][$j]);
									echo '<td>'.$val_autocompletar['nombre'].'</td>';
								}
							}	
						}
						elseif (is_date($sql[$i][$j]))
						{
							//se muestran las fechas de creacion y mofificacion
							$date = date("d/m/Y H:i:s", strtotime($sql[$i][$j]));
							echo '<td class="fechas">'.$date.'</td>';
						}
						else
						{           				
							if ($related[$j]) //Arituculos
							{
								$id = $sql[$i][$j];
								if ($id != null)
								{
									$belong_to_table = check_if_item_has_belongs_to($columnas[$j]);
									if ($belong_to_table) //cuestiones
									{
									
										//Valor del articulo
										$id_related = singular($belong_to_table)."_id";
										$val_related = consulta_bd_por_id("nombre, $id_related", plural($related[$j]), '', $id);
										
										if ($belongs_to_parent) //temas
										{								
										
											//Valor de la cuestion
											$id_related_padre = singular($belongs_to_parent)."_id";
											$val_related_padre = consulta_bd_por_id("nombre, $id_related_padre", $belong_to_table, '', $val_related["$id_related"]);
											$data_padre = ($val_related_padre['nombre'] == '') ? "&nbsp;": $val_related_padre['nombre'];
													
											if ($belongs_to_gran_parent) //partes
											{
												//Valor del tema
												$id_related_parent = singular($belongs_to_gran_parent)."_id";
												$val_related_parent = consulta_bd_por_id("nombre, $id_related_parent", $belongs_to_parent, '', $val_related_padre["$id_related_padre"]);
											
												//Valor del parte
												$val_related_gran_parent = consulta_bd_por_id("nombre", $belongs_to_gran_parent, '', $val_related_parent["$id_related_parent"]);
												$data = ($val_related_gran_parent['nombre'] == '') ? "&nbsp;": $val_related_gran_parent['nombre'];
												//Mostrar el parte
												echo '<td>'.ucwords($data).'</td>';
											}
											else
											{
												//Valor del tema
												$val_related_parent = consulta_bd_por_id("nombre", $belongs_to_parent, '', $val_related_padre["$id_related_padre"]);

											}
											
											//Mostrar el tema
											$data = ($val_related_parent['nombre'] == '') ? "&nbsp;": $val_related_parent['nombre'];
											echo '<td>'.ucwords($data).'</td>';
										}
										else
										{
											//Valor de la cuestion
											$val_related_padre = consulta_bd_por_id("nombre", $belong_to_table, '', $val_related["$id_related"]);
											$data_padre = ($val_related_padre['nombre'] == '') ? "&nbsp;": $val_related_padre['nombre'];
										}
										
										//Mostrar la cuestion
										echo '<td>'.ucwords($data_padre).'</td>';
										
										//Mostrar el articulo
										$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
										echo '<td>'.ucwords($data).'</td>';
										
									}
									else
									{
										// se muestran en la lista los select
										$val_related = consulta_bd_por_id("nombre", plural($related[$j]), '', $id);
										$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
										echo '<td>'.ucwords($data).'</td>';
									}
								}
								else
								{
									echo '<td>&nbsp;</td>';
								}
							}
							elseif  ((isset($id_modificado_por) AND $j == $id_modificado_por) OR (isset($id_creado_por) AND $j == $id_creado_por))
							{	
								$val_uid = (int)$sql[$i][$j];
								$user_modificado_por = consulta_bd_por_id("nombre, apellido_paterno","administradores","",$val_uid);
								$user_name = $user_modificado_por['nombre']." ".$user_modificado_por['apellido_paterno'];
								echo '<td class="usuarios">'.$user_name.'</td>';
							}
							else
							{
								//campos de textos con opcion de editar
								$data = ($sql[$i][$j] == '') ? "&nbsp;": $sql[$i][$j];
								if($colum == 'id'){
									echo '<td>'.$data.'</td>';
								} else {
									echo '<td rel="'.$columnas[$j].'-'.$sql[$i][0].'-'.$tabla.'" style="position:relative">
											<span class="'.$columnas[$j].'-'.$sql[$i][0].'-'.$tabla.'">'.$data.'</span>
											<form method="post" class="form_editar_en_linea">';
											//if (is_numeric($data)){ $tipo_dato = 'int'; } else { $tipo_dato = '';}
												
									echo 		'<input class="campo_editable_lista" id="" type="text" name="" value="'.$data.'" />';
												
									echo	   '<a href="javascript:void(0)" class="btn_editar_linea">Guardar</a>
											</form>
										  </td>';
								}
								/////////////////////////////
							}
						}
						
					}
					if($asociados)
					{
						$data = get_asociados($tabla, $asociados, $sql[$i][0]);
						echo '<td>'.ucwords($data).'</td>';
					}
				
					if (!isset($edit) or $edit == 1) {
						//con esto se edita y elimina desde la linea
						echo '<td>
								<a href="index.php?op='.$val_op.'c&id='.$sql[$i][0].$rid.'" class="boton4 btn_editar" target="_blank">Editar</a>
							  </td>
							  <td>'; 
						?>
								<a onclick="javascript:confirmar('<?php echo $sql[$i][0].$rid; ?>','del','','<?php echo $tabla; ?>','','')" class="boton4 btn_eliminar">Eliminar</a>
						<?php		
						echo  '</td>
							</tr>';
					}
					$i++;
				}
				
			  ?>
        </tbody>
    </table>
    <div id="tablefooter">
      <div id="tablenav">
        	<div>
                <img src="css/images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="css/images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="css/images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="css/images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div>
            	<select class="select" id="pagedropdown"></select>
			</div>
            <div>
            	<a class="ver_todo" href="javascript:sorter.showall()">Ver Todo</a>
            </div>
        </div>
		<div id="tablelocation">
        	<div>
                <select onchange="sorter.size(this.value)">
                <option value="5">5</option>
                    <option value="10" selected="selected">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span class="filas_por_pagina">Filas por página</span>
            </div>
            <div class="page">Página<span id="currentpage"></span> de <span id="totalpages"></span></div>
        </div>
    </div>
</div>

<script type="text/javascript">
//editar en linea los checkbox

    $(function(){
		
		$('.check_').click(function(){
           var variables = $(this).attr('rel');
           var valor = $(this).attr('checked');
           alertify.log("Realizando Modificacion");
			
			$.ajax({
				type: "POST",
				url: "app/partials/ajax_check_has_many.php",
				data: { campos: variables, marcado: valor },
				success: function(respuesta){
					//console.log(respuesta);
					alertify.success("Modificacion realizada con éxito");
					if(valor == "checked"){
						//marcar si
						$('span.'+variables).html('Si');
					} else {
						$('span.'+variables).html('No');
						//marcar no
					};
				}, 
				error: function(){
					alertify.error("No se pudo realizar la solicitud");
			  		return false;
				}
			});
			
		}); 
	   
    });
</script>


<script type="text/javascript">
//Editar campos en linea, no los select ni los checkbox ni ID
$(function(){
	$("td").dblclick(function(){
		$(this).find(".form_editar_en_linea").fadeIn(100);
	});
	$("a.btn_editar_linea").click(function(){
		var valor_campo = $(this).parent().find(".campo_editable_lista").val();
		var variables = $(this).parent().parent().attr("rel");
		alertify.log("Realizando Modificacion");
		
		$.ajax({
				type: "POST",
				url: "app/partials/ajax_campos_list.php",
				data: { campos:variables, val:valor_campo },
				success: function(respuesta){
					//console.log(respuesta);
					alertify.success("Modificacion realizada con éxito");
					$('span.'+variables).html(valor_campo);
					$(".form_editar_en_linea").fadeOut(100);
				}, 
				error: function(){
					alertify.error("No se pudo realizar la solicitud");
			  		return false;
				}
			});
	});
	
});
</script>

<script type="text/javascript" src="js/tinytable.js"></script>
<script type="text/javascript">
function cargado()
{
	$('#loader').hide();
	$('#tablewrapper').fadeIn(500);
}
<?php if ($cant > 0) { ?>
var sorter = new TINY.table.sorter('sorter','table',{
	headclass:'head',
	ascclass:'asc',
	descclass:'desc',
	evenclass:'evenrow',
	oddclass:'oddrow',
	evenselclass:'evenselected',
	oddselclass:'oddselected',
	paginate:true,
	size:10,
	colddid:'columns',
	currentid:'currentpage',
	totalid:'totalpages',
	startingrecid:'startrecord',
	endingrecid:'endrecord',
	totalrecid:'totalrecords',
	hoverid:'selectedrow',
	pageddid:'pagedropdown',
	navid:'tablenav',
	init:true
}, cargado);
<?php }else{ ?>
	cargado();
<?php } ?>
</script>
<p></p>


