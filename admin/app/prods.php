<?php 
date_default_timezone_set('America/Santiago');
include('../conf.php');
$query = consulta_bd('
	pd.sku, p.nombre, pd.precio, pd.descuento, pd.stock, pd.dias_de_fabricacion, p.descripcion_seo, pd.ancho, pd.alto, pd.largo, pd.origen, p.id',
	'productos p 
	JOIN productos_detalles pd ON p.id = pd.producto_id
	JOIN categorias_productos cp ON cp.producto_id = p.id
	JOIN categorias c ON c.id = cp.categoria_id
	JOIN subcategorias sb ON sb.id = cp.subcategoria_id',
	'p.publicado = 1 AND pd.publicado = 1', 'p.id');

header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=productos_".time()."BD.xls"); 
header("Pragma: no-cache" );
header("Expires: 0");

$hoy = date("d/m/Y");

?>
<h3>Base de datos de productos publicados, fecha de reporte <?php echo "$hoy";?></h3>
<table border="1">
	<thead>
		<tr>
			<td>sku</td>
			<td>nombre</td>
			<td>precio</td>
			<td>descuento</td>
			<td>stock</td>
			<td>dias_de_fabricacion</td>
			<td>descripcion_seo</td>
			<td>ancho</td>
			<td>alto</td>
			<td>largo</td>
			<td>origen</td>
			<td>subcategorias</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($query as $item) { 
			$subcategorias = consulta_bd('sb.id, sb.nombre, sb.categoria_id', "subcategorias sb join categorias_productos cp on cp.subcategoria_id = sb.id", "cp.producto_id = $item[11]", '');
		?>
		<tr <?= ($item[1] == 'Pagado') ? 'style="background: #66c878;"' : ''?>>
			<td><?= $item[0] ?></td>
			<td><?= $item[1] ?></td>
			<td><?= $item[2] ?></td>
			<td><?= $item[3] ?></td>
			<td><?= $item[4] ?></td>
			<td><?= $item[5] ?></td>
			<td><?= $item[6] ?></td>
			<td><?= $item[7] ?></td>
			<td><?= $item[8] ?></td>
			<td><?= $item[9] ?></td>
			<td><?= $item[10] ?></td>
			<td>
			<?php foreach ($subcategorias as $sb):
				$categorias = consulta_bd('nombre', 'categorias', "id = $sb[2]", '');
			?>
				<?= $categorias[0][0] . ' > ' . $sb[1] ?>,
			<?php endforeach ?>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>