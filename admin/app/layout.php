<?php
setcookie("cms",1,time()+5*24*60*60);

/********************************************************\
|  Moldeable CMS - Listado de entradas.		             |
|  Fecha Modificación: 22/08/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php 
include('includes/header.php');
if (is_numeric($val_op))
$sm_table = consulta_bd("submenu_table","tablas","id = $val_op","");

?>
<script type="text/javascript">
$(document).ready(function($){
    var ventana_ancho = $(window).width();
    //alert(ventana_ancho);
	var ancho_final = ventana_ancho - 226;
	$('#back').css("width", ancho_final + "px");
	
	var ancho_botonera = ventana_ancho - 258;
	$('#botonera').css("width", ancho_botonera + "px"); 
});

</script>
<script type="text/javascript">
$( window ).resize(function() {
  	var ventana_ancho2 = $(window).width();
    var ancho_final2 = ventana_ancho2 - 226;
	$('#back').css("width", ancho_final2 + "px");
	
	var ancho_botonera = ventana_ancho2 - 258;
	$('#botonera').css("width", ancho_botonera + "px");
});
</script>

<link rel="stylesheet" href="css/aristo/css/uniform.default.css" media="screen" />
<script src="js/jquery.uniform.js"></script>
<script>
	$(function() {
		$(":checkbox").uniform({checkboxClass: 'checker'});
		$(".select").uniform();
	});
</script>

<script type="text/javascript">
	$(window).load(function(){
		$('.oculto').css('display', 'none');
		var ventana_alto = $(window).height();
		var contenido_alto = $("#back").height();
		var alto_final = contenido_alto + 190;
		
		//alert(alto_final+", "+ ventana_alto);
		if (ventana_alto > alto_final){
			$("#footer").css("position", "absolute");
		} else {
			$("#footer").css("position", "relative");
		}
	});
	
</script>
<script type="text/javascript" src="js/addOnResize.js"></script>
<script type="text/javascript">
$(function(){
	$("#principal").addOnResizeEvent();
	$("#principal").resize( function() {
		console.log("Cambié de tamaño");
		var ventana_alto = $(window).height();
		var contenido_alto = $("#back").height();
		var alto_final = contenido_alto + 190;
		
		//alert(alto_final+", "+ ventana_alto);
		if (ventana_alto > alto_final){
			$("#footer").css("position", "absolute");
		} else {
			$("#footer").css("position", "relative");
		}
	});
});


</script>

<script type="text/javascript" src="js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#entero').numeric();
})
</script>



</head>

<body>
<!--<div id="loader_consulta" style="display:none">Cargando...</div> -->
	
    
<div id="header">
    <?php /*?><div id="fecha_reloj"><?php echo fecha();?></div>
    	<img src="http://www.moldeable.com/cms/logo_cms.jpg" /><?php */?>
    <?php 
		
		$val_uid = $_SESSION[$nombre_sitio."_admin"];
		$user_modificado_por = consulta_bd_por_id("nombre, apellido_paterno","administradores","",$val_uid);
		$user_name = $user_modificado_por['nombre']." ".$user_modificado_por['apellido_paterno'];
		
		$cliente = consulta_bd("valor","opciones","nombre='nombre_cliente'","");
		if($cliente[0][0] == ''){
			$nombre_cliente = 'Cliente Anonimo';
		} else {
			$nombre_cliente = $cliente[0][0];
		}
		
	?>
    <div class="center">
        <div class="logo">
        	<img src="pics/logo-moldeable-cms2.png" width="193" height="22" alt="Moldeable CMS" />
        </div>
         <?php
			$informe = consulta_bd("valor","opciones","nombre='informe_tienda'","");
			 if ($informe[0][0] == 1){ ?>
        	<a href="app/informes.php" class="informes" style="float:right; height:46px; padding:0 20px; text-transform:uppercase; line-height:46px;color:#fff; text-decoration:none; border:solid 1px #666; margin-left:20px;">Informe de ventas</a>
            <?php } ?>
            <div class="perfil">
                <div class="box">
                    <p class="nombre"><?php echo $user_name; ?></p>
                    <p class="empresa"><?php echo $nombre_cliente; ?></p>
                    <p class="salir"><a href="action/logout.php">Salir</a></p>
                </div>
                <div class="foto"><img src="pics/perfil.jpg" width="53" height="48" /></div>
            </div><!--Fin perfil -->
            <?php $popup = consulta_bd('activo', 'active_popup', '', ''); ?>
            <a href="javascript:void(0)" class="activar_popup btn-active <?= ($popup[0][0] == 1) ? 'activado' : '' ?>"><?= ($popup[0][0] == 0) ? 'Activar PopUp' : 'Desactivar PopUp' ?></a>
        </div><!--Fin center -->
    </div><!--fin header -->
    
    
    
<div id="menu">
	<?php include('includes/menu.php'); ?>
    <div style="clear:both"></div>
</div>



<div id="back">
	<div id="principal"><?php include("app/$page.php"); ?></div>
</div>




<!--cierre faltante -->
</div>



<div id="footer">
	Powered By Moldeable S.A. - 2015<br />
	Todos los derechos reservados. <?php echo $_SESSION['env'];?>
</div>







<?php  
	//$id_tabla = $_GET[op];
	//echo '<h1>'.$tabla.'</h1>';
	//$tabla_destino = consulta_bd("nombre","tablas","id=$id_tabla","");
	$idEntrada = $_GET[id];
	
?>
<script src="js/croppic.js"></script>
<script type="text/javascript">
var croppicHeaderOptions = {
			uploadUrl:'img_save_to_file.php',
			cropData:{
				"dummyData":1,
				"tabla":"nombre_tabla"
			},
			cropUrl:'img_crop_to_file.php',
			customUploadButtonId:'cropContainerHeaderButton',
			modal:false,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
			onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
			onImgDrag: function(){ console.log('onImgDrag') },
			onImgZoom: function(){ console.log('onImgZoom') },
			onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
			onAfterImgCrop:function(){ console.log('onAfterImgCrop') }
	}	
	var croppic = new Croppic('croppic', croppicHeaderOptions);
	
	
	
	
<?php 		
	$paso1 = consulta_bd("id","tablas","nombre='$tabla'","id desc");
	$paso4 = consulta_bd("valor","opciones_tablas","tabla_id=".$paso1[0][0]." and nombre ='crop'","id desc");
	$campos_separados2 = explode(",", $paso4[0][0]);
	for($k=0; $k<sizeof($campos_separados2); $k++){ ?>
		var croppicContainerModalOptions_<?php echo $campos_separados2[$k] ?> = {
			uploadUrl:'img_save_to_file.php',
			cropUrl:'img_crop_to_file.php?tabla=<?php echo $tabla; ?>&campo=<?php echo $campos_separados2[$k] ?>&id=<?php echo $idEntrada; ?>',
			modal:true,
			imgEyecandyOpacity:0.4,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onAfterImgCrop:function(){
				var imagen_nueva = $("#<?php echo $campos_separados2[$k] ?>_cropContainerModal").find("img.croppedImg").attr("src");
				$("img#cont_img_actual_<?php echo $campos_separados2[$k] ?>").attr("src", imagen_nueva)
			}
			};
	<?php
		echo "var ".$campos_separados2[$k]."_cropContainerModal = new Croppic('".$campos_separados2[$k]."_cropContainerModal', croppicContainerModalOptions_".$campos_separados2[$k].");";
		
	}//fin for
	?>
</script>
    
    

<!--alertas -->

<script type="text/javascript">
$(function(){
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'ventana'){ ?>
		alertify.alert("<?php echo $_GET[error]; ?>");
	<?php } ?>	
	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'notificacion'){ ?>
	alertify.log("<?php echo $_GET[error];?>");
	<?php } ?>
	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'exito'){ ?>
	alertify.success("<?php echo $_GET[error];?>");
	<?php } ?>
	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'error'){ ?>
	alertify.error("<?php echo $_GET[error];?>");
	<?php } ?>
});	
</script>

<script type="text/javascript">
	$('.activar_popup').click(function(){
		$.ajax({
			url: 'action/activar_popup.php',
			type: 'post',
			success: function(res){
				location.reload();
			}
		});
	});
</script>

<?php 

$real_op = substr($_GET[op], -1);
if(is_numeric($real_op)){
	$idTabla = $_GET[op];
	$table_name = consulta_bd('nombre', 'tablas', "id = $idTabla", '');	
	} else {
		$idTabla = substr($_GET[op], 0, -1);
		if($idTabla > 0){
			$table_name = consulta_bd('nombre', 'tablas', "id = $idTabla", '');	
			} else {
			$idTabla = opciones("first_table_id");
			if($idTabla > 0){
				$table_name = consulta_bd('nombre', 'tablas', "id = $idTabla", '');
				} else {
				$table_name = consulta_bd('nombre', 'tablas', "id = 1", '');	
				}
			
			}
		
		
		}
if ($table_name[0][0] == 'descuento_productos') { ?>
<script>
	$(function(){
		if ($('#descuento_opcion_id').val() == 0 || $('#descuento_opcion_id').val() == 3) {
			$("#donde_el_descuento").parent().parent().css('display', 'none');
		}else{
			$("#donde_el_descuento").parent().append('<select class="select" name="'+$("#donde_el_descuento").attr('name')+'" id="donde_el_descuento_select"></select>');
			$.ajax({
				url: 'action/buscar_tipo.php',
				type: 'post',
				dataType: 'json',
				data: {id_opcion: $('#descuento_opcion_id').val()}
			}).done(function(res){
				var input = $("#donde_el_descuento");
				console.log(res);
				// $('#donde_el_descuento_select').remove();

				for (var i = 0; i < res.length; i++) {
					if ($('#donde_el_descuento').val() == res[i][0]) {
						$('#donde_el_descuento_select').append('<option value="'+res[i][0]+'" selected>'+res[i][1]+'</option>');
					}else{
						$('#donde_el_descuento_select').append('<option value="'+res[i][0]+'">'+res[i][1]+'</option>');
					}
				}
			}).fail(function(res){
				console.log(res);
			})
		}
		$("#donde_el_descuento").css('display', 'none');
		$("#donde_el_descuento_select").addClass('select');
	})
</script>
<!-- Ecommerce con descuentos por Categorías, Marcas o Subcategorias -->
<script type="text/javascript">
	// var nombre_tabla = $('#descuento_opcion_id option:selected').text();
	$(function(){
		$('#descuento_opcion_id').change(function(){
			var id_opcion = $(this).val();
			var nombre_tabla = $('#descuento_opcion_id option:selected').text();
			$("#donde_el_descuento").parent().parent().css('display', 'table-row');

			if (id_opcion != 3 && id_opcion != 0) {
				$.ajax({
					url: 'action/buscar_tipo.php',
					type: 'post',
					dataType: 'json',
					data: {id_opcion: id_opcion}
				}).done(function(res){
					console.log(res);
					var input = $("#donde_el_descuento");
					$('#donde_el_descuento_select').remove();

					input.parent().append('<select class="select" name="'+input.attr('name')+'" id="donde_el_descuento_select"><option value="0">Seleccione '+nombre_tabla+'</option></select>');

					for (var i = 0; i < res.length; i++) {
						$('#donde_el_descuento_select').append('<option value="'+res[i][0]+'">'+res[i][1]+'</option>');
					}

					reset_select();
				}).fail(function(res){
					console.log(res);
				})
			}else{
				$("#donde_el_descuento").parent().parent().css('display', 'none');
				$('#donde_el_descuento_select').val(0);
				$('#donde_el_descuento').val(0);
			}
		});

		function reset_select(){
			$('#donde_el_descuento_select').on('change', function(){
				$("#donde_el_descuento").val($(this).val());
			});
		}
		
	});
</script>
<?php } ?>  <!-- END CONDIFICION DESCUENTO PRODUCTOS -->  

<?php if ($table_name[0][0] == 'descuento_despachos') { ?>
<script>
	$('#region_id').on('change', function(){
		var region_id = $(this).val();
		$.ajax({
			url: 'action/buscar_comunas.php',
			type: 'post',
			dataType: 'json',
			data: {region_id: region_id}
		}).done(function(res){
			$('#comuna_id').html('<option value="-1">Seleccione...</option>');
			$('#comuna_id').append('<option value="0">SIN COMUNA</option>');
			for (var i = 0; i < res.length; i++) {
				$('#comuna_id').append('<option value="'+res[i][0]+'">'+res[i][1]+'</option>');
			}
		}).fail(function(res){
			console.log(res);
		})
	})
</script>
<?php } ?>
    

</body>
</html>