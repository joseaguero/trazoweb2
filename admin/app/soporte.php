<?php 

/********************************************************\
|  Moldeable CMS - Envía mail de soporte.		         |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

function comprobar_mail($email)
{ 
    $mail_correcto = 0; 
    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {  
          if (substr_count($email,".")>= 1){ 
             $term_dom = substr(strrchr ($email, '.'),1); 
             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){  
                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
                if ($caracter_ult != "@" && $caracter_ult != "."){ 
                   $mail_correcto = 1; 
                } 
             } 
          } 
       } 
    } 
    if ($mail_correcto) 
       return 1; 
    else 
       return 0; 
} 

function enviar_mail($nombre, $apellido, $email, $tipo, $msg, $asunto)
{
	$para = 'soporte@moldeable.com';
	
	$header = "From: $nombre $apellido \r\n Reply-To:$mail\r\n";
	$header .= "X-Mailer:PHP/".phpversion()."\n";
	$header .= "Mime-Version: 1.0\n";
	$header .= "Content-Type: text/html";
	$msg2 = "El siguiente correo fue enviado desde el sitio web por el formulario de soporte:";
	$msg2 .= "<br /><br />";
	$msg2 .= "Nombre de quien contacto: $nombre $apellido";
	$msg2 .= "<br /><br />";
	$msg2 .= "Mail: $email";
	$msg2 .= "<br /><br />";
	$msg2 .= "Tipo de contacto: $tipo";
	$msg2 .= "<br /><br />";
	$msg2 .= "Mensaje:";
	$msg2 .= "<br /><br />";
	$msg2 .= $msg;
	$msg2 .= "<br /><br /><br /><br />";
	$msg2 .= "NOTA: Los acentos fueron omitidos intencionalmente.";
	$mail = mail($para, $asunto, utf8_decode($msg2) ,$header);
	if ($mail)
	{
		$error_registro = "Mensaje enviado.&tipo=exito";
	}
	else
	{
		$error_registro = "Error enviando el mensaje, por favor intentelo nuevamente, cod_011&tipo=error";
	}
	return $error_registro;
}

if ($_POST[soporte])
{
	if ($_POST[nombre] && $_POST[apellido] && $_POST[email] && $_POST[tipo] && $_POST[msg])
	{
		$nombre = $_POST[nombre];
		$apellido = $_POST[apellido];
		$cod_tipo = $_POST[tipo];
		$msg = $_POST[msg];
		$email = $_POST[email];
		
		switch ($cod_tipo) {
		case 0:
			$tipo = "No seleccionado";
		case 1:
			$tipo = "Error";
		case 2:
			$tipo = "Consulta";
		case 3:
			$tipo = "Otro";
		}
		
		$comp_mail = comprobar_mail($email);
		
		if ($comp_mail==1)
		{
			$mail = $email;
		}
		else
		{
			$error = "Correo Inválido&tipo=error";
			header ("location:../index.php?op=7&error_registro=$error_registro");
			break;
		}
		
		$asunto = "Solicitud de Soporte desde Clones.cl";
		$error = enviar_mail($nombre, $apellido, $email, $tipo, $msg, $asunto);
		header ("location:../index.php?op=7&error=$error");
	}
	else
	{
		$error = "Todos los campos son necesarios&tipo=notificacion";
		header ("location:../index.php?op=7&error=$error");
	}
}
else
{
	$error = "Error desconocido, por favor contáctese a traves de soporte@moldeable.com&tipo=error";
	header ("location:../index.php?op=7&error=$error");
}

?>

