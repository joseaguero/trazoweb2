<?php
/********************************************************\
|  Moldeable CMS - Editor de perfiles           		 |
|  Fecha Modificación: 19/08/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

include ('../conf.php');
$nombre_sitio = get_nombre_sitio($base);
if ($_SESSION[$nombre_sitio."_admin"]!='')
{
	$user = $_SESSION[$nombre_sitio."_admin"];
	$hash = $_SESSION[$nombre_sitio."_admin_sh"];
	$s_error = "Error de sesión.Otro usuario puede estar usando su nombre de usuario.";
	
	if(!check_admin_loged($hash, $user))
	die(header("location:adlogin.php?error=$s_error"));

	header("Content-Type: text/html;charset=utf-8");
	
	//Acciones generales de contenido 
	if ($_POST[tabla])
	{
		$tabla = $_POST[tabla];
		$id = (is_numeric($_POST[id])) ? $_POST[id]: 0;
		$val_op = get_val_op($_POST[op]); //Valor de la opción o id de la tabla, para devolverse a la misma página
		$sec_op = get_sec_op($_POST[op]);
		
		//Editar
		if ($_POST[update_."$tabla"])
		{

			$values = "fecha_modificacion = NOW()";
			$exception .= $_POST['exception'].", ";
					
			//Obtengo las opciones de la tabla
			$opciones = consulta_bd("nombre, valor","opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t","tabla_id = t.id","");
			$i = 0;
			while($i <= (sizeof($opciones)-1))
			{	
				$nombre = $opciones[$i][0];
				$opcion[$nombre] = $opciones[$i][1];
				$i++;
			}
			$archivos = $opcion['archivos'];
			
			//Verifico que los obligatorios estén ok.
	
			if ($opcion['obligatorios_edit'])
			{
				$obligatorios_edit = explode(',', $opcion['obligatorios_edit']);
				foreach ($obligatorios_edit as $obl)
				{
					$obl = trim($obl);
					if ($_POST["$obl"] == '')
					{
						$error = "Debe completar al menos los campos y ".$opcion['obligatorios_edit']."&tipo=notificacion";
						break;
					}
				}
			}
				
			//Elimino todos los permisos de la tabla relacionada
			$del = del_bd_generic("tablas_perfiles", "perfil_id", $id);
			if ($del)
			{	
				//Se ingresan los nuevos valores
				$columnas = consulta_bd('id, nombre',"tablas","",'posicion');
				$i = 0;
				while($i <= (sizeof($columnas)-1))
				{	
					$id_tabla = $columnas[$i][0];
					$nombre_tabla = $columnas[$i][1];
					if ($_POST[$nombre_tabla."_".$id_tabla] == 'on') 
					{
						$values = "$id_tabla, $id, 3";
						$insert = insert_bd("tablas_perfiles","tabla_id, perfil_id, permiso_id",$values);
						
						if ($insert) {
							//Selecciono los nombres de las tablas hijas 
							$childs = consulta_bd("id, nombre, is_sub_menu","tablas","parent = '$id_tabla'","");
							$cant = mysqli_affected_rows($conexion);
							if ($cant>0)
							{
								$j = 0;
								while($j <= (sizeof($childs)-1))
								{
									$id_tabla_hijo = $childs[$j][0];
									$nombre_tabla_hijo = $childs[$j][1];
									$is_sub_sub_menu = $childs[$j][2];
									if ($is_sub_sub_menu)
									{
										if ($_POST[$nombre_tabla_hijo."_".$id_tabla_hijo] == 'on') 
										{
											$id_child = $childs[$j][0];
											$values = "$id_child, $id, 3";
											//$insert = insert_bd("tablas_perfiles","tabla_id, perfil_id, permiso_id",$values);
											
											$grandsons = consulta_bd("id, nombre, is_sub_menu","tablas","parent = '$id_child'","");
											$z = 0;
											while($z <= (sizeof($grandsons)-1))
											{	
												$id_tabla_nieto = $grandsons[$z][0];
												$nombre_tabla_nieto = $grandsons[$z][1];
												$nombre_post = "radio_".$nombre_tabla_nieto."_".$id_tabla_nieto;
												$permiso_id = $_POST[$nombre_post];
												$values = "$id_tabla_nieto, $id, $permiso_id";
												//$insert = insert_bd("tablas_perfiles","tabla_id, perfil_id, permiso_id",$values);
												$z++;
											}
										}
									}
									else
									{
										$nombre_post = "radio_".$nombre_tabla_hijo."_".$id_tabla_hijo;
										if ($_POST[$nombre_post])
										{
											$permiso_id = $_POST[$nombre_post];
											$values = "$id_tabla_hijo, $id, $permiso_id";
											$insert = insert_bd("tablas_perfiles","tabla_id, perfil_id, permiso_id",$values);
										}
									}		
									$j++;
								}
							}
						}
						else
						{
							$error .= "Error al ingresar los permisos.&tipo=error";	
						}
					}
					$i++;
				}
			}
			else
			{
				$error .= "Error al actualizar las opciones del perfil&tipo=error";
			}
			//Fin many permisos perfil
			
			
			//Se actualiza el nombre del perfil
			if (!$error)
			{			
				$values = "nombre = '$_POST[nombre]', fecha_modificacion = NOW(), modificado_por = $user";
				$conditions = "id = $_POST[id]";
				$update = update_bd($tabla, $values, $conditions);
				if ($update)
				{
					$error .= "Se ha editado la entrada.&tipo=exito";
				}
				else
				{
					$error .= "Error al editado la entrada&tipo=error";
				}
			}
			header("location:../index.php?op=".$val_op."c&id=$id&error=$error");
		}
		elseif ($_POST['add_perfiles'])
		{
			$keys = "nombre, fecha_modificacion, creado_por, fecha_creacion";
			$values = "'$_POST[nombre]',NOW(),$user,NOW()";
			$crear = insert_bd('perfiles', $keys, $values);
			if ($crear)
			{
				$error .= "Se ha creado el perfil.&tipo=exito";
				$id = get_last('perfiles');
				header("location:../index.php?op=".$val_op."c&id=$id&error=$error");
			}
			else
			{
				$error .= "Error al crear el perfil&tipo=error";
				header("location:../index.php?op=".$val_op."c&error=$error");
			}
			
		}
	}
//Fin IF Session
}
else
{
	header("location:../adlogin.php");
}
?>