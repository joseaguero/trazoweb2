<?php 
ini_set('max_execution_time', 500);

session_cache_limiter('private, must-revalidate');
session_cache_expire(180);
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
/********************************************************\
|  Moldeable CMS - Configuración básica				 	 |
|  Fecha Modificación: 14/03/2011		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/
 	session_start();  
	require_once('includes/library_source.php');
	
	$die_start = "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Error de base de datos:</span> <br /><br />";
	$die_end = "</p>";

	
	//Por lo general no cambiar.
	$host = 'localhost';
	//Usuario de la base de datos.
	$user = 'root';	
	//Paswrod del usuario.
	$pass = 'moldeablelocal';		
	//Base de datos a trabajar.
	$base = 'trazofinal';
	
	$conexion = mysqli_connect("$host", "$user", "$pass", "$base");
	if (!$conexion) {
		echo "Error: No se pudo establecer una conexion con la base de datos:." . PHP_EOL;
		echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
		exit;
	} else {
		// echo "Éxito: Se realizó una conexión apropiada a MySQL! La base de datos mi_bd es genial." . PHP_EOL;
		// echo "Información del host: " . mysqli_get_host_info($conexion) . PHP_EOL;
	}
	mysqli_query ($conexion, "SET NAMES 'utf8'");

	$url_base = 'http://localhost/trazoweb2/';
	$url_global = "http://localhost/trazoweb2/";
	
?>