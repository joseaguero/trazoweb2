<?php

/********************************************************\
|  Moldeable CMS - System File.				             |
|  Fecha Modificación: 29/08/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

	require_once ("conf.php");
	include ("fecha.php");
	$nombre_sitio = get_nombre_sitio($base);
	$_SESSION['env'] = get_env();
	if ($_SESSION[$nombre_sitio."_admin"]!='' or $_SESSION['env'] != 'produccion')
	{
		$user = $_SESSION[$nombre_sitio."_admin"];
		$perfil_admin = ($_SESSION['env'] != 'produccion') ? 1 : $_SESSION[$nombre_sitio."_perfil_admin"];
		$hash = $_SESSION[$nombre_sitio."_admin_sh"];
		$s_error = "Error de sesión.Otro usuario puede estar usando su nombre de usuario.";
		
		if(!check_admin_loged($hash, $user) and $_SESSION['env'] == 'produccion' )
		{
			$_SESSION['before_log_in_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			die(header("location:adlogin.php?error=$s_error"));
		}
		else
		{
			$_SESSION['before_log_in_url'] = false;
		}
		
		if (!isset($_GET[op]))
		{
			$val_op = get_option('first_table_id');
			if ($val_op == '')
			{
	            $q_op = consulta_bd("id","tablas","nombre != 'administradores' AND show_in_menu != 0 AND is_sub_menu != 1 AND parent = 0","posicion ASC LIMIT 1");
	            $val_op = $q_op[0][0];
			}

            $op = $val_op."a";
            $action = get_sec_op($op);
            $tabla = obtener_tabla($val_op);

            if (!column_exists($tabla, 'id'))
            {
                $action = 'other';
                $q_op = consulta_bd_por_id("op","tablas","",$val_op);
                $op = $q_op['op'];
            }
		}
		else
		{
			$op = $_GET[op];	
			$val_op = get_val_op($op);
			if (is_numeric($_GET[id]) AND $_GET[id] != 0)
			{
				$id = $_GET[id];
				$tabla = obtener_tabla($val_op);
				$action = get_sec_op($op);
			}
			else
			{
				if (!is_numeric($val_op))
				{
					$action = 'other';
					$val_op_q = consulta_bd("id","tablas","op = '$op'","");
					$val_op = $val_op_q[0][0];
					$tabla = obtener_tabla($val_op);
				}
				else
				{
					$action = get_sec_op($op);
					$tabla = obtener_tabla($val_op);
				}
				
			}
		}
		
		if ($op == 'pdfview')
		{
			$page = $op;
			$tabla = $_GET[tabla];
			include("app/layout.php");
		}
		elseif (!get_permiso($val_op, $action, $perfil_admin))
		{
			$page = "not_allowed";
			include("app/layout.php");
		}
		else
		{
			switch ($action)
			{
				case 'a':
					$page = "list";
					include("app/layout.php");
					break;
				case 'b':
					$page = "new";
					include("app/layout.php");
					break;
				case 'c':
					$page = "edit";
					include("app/layout.php");
					break;
				default:
					$dos_primeras = substr(($op),0,2);
					if ($dos_primeras == 'bd')
					{
						$dos_ultimas = str_replace("bd_","",$op);
						$tabla = consulta_bd("nombre","tablas","id = '$dos_ultimas'","");
						$tabla = $tabla[0][0];
						header("location:app/bd.php?tabla=$tabla");
					}
					else
					{
						$dir_pag = "app/$op.php";			
						if(!file_exists($dir_pag))
						{
							$page = "not_allowed";
						}
						else
						{
							$page = $op;
						}
						include("app/layout.php");
					}
					break;
			}
		}
	}
	else
	{
		$_SESSION['before_log_in_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		header("location:adlogin.php");
	}
?>