<ul id="dropdown-menu" class="sf-menu">
	<?php
		$tablas = consulta_bd("t.id, t.is_sub_menu, t.op, t.parent, tp.permiso_id, t.submenu_table", "tablas t, tablas_perfiles tp", "t.parent = 0 AND tp.tabla_id = t.id AND tp.perfil_id = $perfil_admin AND t.show_in_menu = 1", "posicion");
		$i = 0;
		while($i <= (sizeof($tablas)-1))
		{	
			$access_menu = ($tablas[$i][4] != 1) ? true:false;

			if ($access_menu)
			{
				$is_sub_menu = $tablas[$i][1];
				if (!$is_sub_menu)
				{
					if ($tablas[$i][3] == 0)
					{
						$op_link = ($tablas[$i][2] != '') ? $tablas[$i][2]: $tablas[$i][0].'a';
						$nombre = get_table_name($tablas[$i][0]);
						echo '<li class="current"><a href="index.php?op='.$op_link.'" class="menu">'.$nombre.'</a></li>';
					}
				}
				elseif ($tablas[$i][5] != '')
				{
					$submenu_table = $tablas[$i][5];
                                        if(substr_count($submenu_table, ",")!=0){
                                            $tablas_menu = explode(",", $submenu_table);
                                            $submenu_table = $tablas_menu[0];
                                        }
                                        $menu_items = consulta_bd("nombre, id",$submenu_table,"","");
					$nombre = get_table_name($tablas[$i][0]);
					echo '<li><a class="menu">'.$nombre.'</a>';
					echo '<ul>';
					$sm = 0;
					while($sm <= (sizeof($menu_items)-1))
					{	
						$op_link_sm = $tablas[$i][0].'a&id='.$menu_items[$sm][1].'&r='.$submenu_table;
						echo '<li><a href="index.php?op='.$op_link_sm.'" class="menu">'.ucwords($menu_items[$sm][0]).'</a></li>';
						$sm++;
					}
					echo '</ul>';
				}
				else
				{
					$parent_id = $tablas[$i][0];
					$nombre = get_table_name($tablas[$i][0]);
					echo '<li><a class="menu">'.$nombre.'</a>';
					echo '<ul>';
					$childs = consulta_bd("t.id, t.op, tp.permiso_id, t.is_sub_menu","tablas t, tablas_perfiles tp","t.parent = $parent_id AND tp.tabla_id = t.id AND tp.perfil_id = $perfil_admin ","posicion");
					$j = 0;
					while($j <= (sizeof($childs)-1))
					{	
						$access = $childs[$j][2];
						if ($access != 1)
						{
							$nombre_child = get_table_name($childs[$j][0]);
							$is_sub_sub_menu = $childs[$j][3];
							if ($is_sub_sub_menu)
							{
								$child_id = $childs[$j][0];
								echo '<li><a class="menu">'.$nombre_child.'</a><ul>';
								$grandsons = consulta_bd("t.id, t.op, tp.permiso_id, t.is_sub_menu","tablas t, tablas_perfiles tp","t.parent = $child_id AND tp.tabla_id = t.id AND tp.perfil_id = $perfil_admin ","posicion");
								$z = 0;
								while($z <= (sizeof($grandsons)-1))
								{	
									$op_link = ($grandsons[$z][1] != '') ? $grandsons[$z][1]: $grandsons[$z][0].'a';
									$nombre_grandsons = get_table_name($grandsons[$z][0]);
									echo '<li><a href="index.php?op='.$op_link.'" class="menu">'.$nombre_grandsons.'</a></li>';
									$z++;
								}
								echo '</ul></li>';
							}
							else
							{
								$op_link = ($childs[$j][1] != '') ? $childs[$j][1]: $childs[$j][0].'a';
								if($op_link == "xlsPP"){
									echo '<li><a href="includes/productos_pedidos.php" class="menu">XLS Productos Compras</a></li>';
								}elseif($op_link == "xlsPr"){
									echo '<li><a href="app/prods.php" class="menu">XLS Productos publicados</a></li>';
								}elseif($op_link == "xlsPd"){
									echo '<li><a href="app/peds.php" class="menu">XLS Pedidos</a></li>';
								}else{
									echo '<li rel="'.$op_link.'"><a href="index.php?op='.$op_link.'" class="menu">'.$nombre_child.'</a></li>';
								}			
							}
						}
						$j++;
					}
					echo '</ul></li>';
				} 
			}
			$i++;
		}
	?>
	
    <li><a class="menu" href="javascript:void(0)">Actualizar Productos</a>
        <ul>
        	<li><a target="_blank" href="../ws/stock_y_precio.php">Stock y Precio</a></li>
            <li><a target="_blank" href="../ws/leeProductosMadre.php">Productos Madre</a></li>
            <li><a target="_blank" href="../ws/leeProductosHijo.php">Productos Hijo</a></li>
            <li><a target="_blank" href="../ws/leeRelaciones.php">Relaciones</a></li>
            <li><a target="_blank" href="../ws/leeCategorias.php">Categorías</a></li>
            <li><a target="_blank" href="../ws/leeSubCategorias.php">Subcategorias</a></li>
        </ul>
    </li>
    <li><a href="action/logout.php" class="menu">Salir</a></li>
</ul>