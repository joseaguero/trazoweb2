<?php
	include('../../../conf.php');
	include('../functions.php');
	$cart = $_SESSION['moldeable_cart'];
	$active_user = $_SESSION['active_user'];
	
	if ($cart) {
		
		$ok_url = get_option_tienda('ok_url');
		$fail_url = get_option_tienda('fail_url');
		$logo_url = get_option_tienda('logo_url');
	
		$direccion = mysql_real_escape_string($_POST[direccion]);
		$comuna = mysql_real_escape_string($_POST[comuna]);
		$ciudad = mysql_real_escape_string($_POST[ciudad]);
		$region = mysql_real_escape_string($_POST[region]);
		$regalo = mysql_real_escape_string($_POST[regalo]);
	
		$oc = create_oc($cart, $direccion, $comuna, $ciudad, $region, $active_user, '', '', $regalo);
		$_SESSION['oc'] = $oc;
	
		if (!$oc)
		die(header('location:../../../../index.php?error=Error l procesar el pedido, por favor inténtelo nuevamente.'));
	
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
?>
	<div style="margin: 0 auto; width: 980px;text-align:center;font-family:verdana;font-size: 12px;">
		<p style="line-height: 170px;">&nbsp</p>
		<img src="<?php echo $logo_url; ?>" border="0" /><br /><br /><br />
		<img src="359.gif" border="0" /><br /><br />
		Estamos procesando su compra, espere por favor...
	</div>
		<script type="text/javascript" src="../../../js/jquery-1.6.1.min.js"></script>
		<form id="form" action="https://checkout.dineromail.com/CheckOut" method="post" >
			 <!-- Variables Obligatorias -->
			<input type="hidden" name="merchant" value="dmclqa@gmail.com" />
			<input type="hidden" name="country_id" value="3" />
			<input type="hidden" name="payment_method_available" value="all" /> 
			<input type="hidden" name="tool" value="button">
			<input type="hidden" name="seller_name" value="Automoblox.cl" />
			<input type="hidden" name="transaction_id" value="<?php echo $oc;?>" />
			<input type="hidden" name="ok_url" value="<?php echo $ok_url;?>" />
			<input type="hidden" name="error_url" value="<?php echo $fail_url; ?>" />
			<input type="hidden" name="header_image" value="<?php echo $logo_url;?>" />
			<input type="hidden" name="button_color" value="D91C1D" />
			<input type="hidden" name="border_color" value="D91C1D" />
			<input type="hidden" name="links_color" value="D91C1D" />
			<input type="hidden" name="font_color" value="666666" />
			<input type="hidden" name="hover_step_color" value="DCDCDC" />
			<input type="hidden" name="url_redirect_enabled" value="1" />
			<input type="hidden" name="change_quantity" value="0" />
<!-- 			<input type="hidden" name="payment_method_1" value="cl_visa" /> -->
			<?php
				$i = 1;
				foreach ($contents as $prd_id=>$qty) {
					$producto = consulta_bd_por_id("nombre","productos","",$prd_id);
					$precio_producto = get_precio($prd_id, 'descuento');
				?>
					<input type="hidden" name="item_name_<?php echo $i; ?>" value="<?php echo $producto['nombre']; ?>" /> 
					<input type="hidden" name="item_quantity_<?php echo $i; ?>" value="<?php echo $qty; ?>" />
					<input type="hidden" name="item_ammount_<?php echo $i; ?>" value="<?php echo $precio_producto;?>00" />
					<input type="hidden" name="item_currency_<?php echo $i;?>" value="clp" />
			<?php 
					$i++;
				}
			?>
		</form>
		
		<script type="text/javascript">
		$(function(){
			$('#form').submit();
		})
		</script>

<?php
	} else {
		header('location:../../../../index.php?error=Su carro está vacío!');
	}
?>