<?php
// require_once 'paginador/paginator.class.php';
	function diasDespachoForastero($marca_id) {
		if($marca_id == null) {
			$res = null;
		}
		else {
			$cuenta = consulta_bd('dias_despacho','marcas','id = '.$marca_id);
			if(is_array($cuenta)){
				$res = intval($cuenta[0][0]);
			}
			else {
				$res = null;
			}
		}
		return $res;
		
	}
  
//date_default_timezone_set('Etc/UTC');
require "phpMailer/PHPMailerAutoload.php";

//carro resumen flotante
function miniCart() {
	global $db;
	$cart = isset($_SESSION['cart_alfa_cm']) ? $_SESSION['cart_alfa_cm'] : NULL;
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$output[] .= '<div class="resumenCarroHeader">
            	<div class="headCarroResumen ctndr100">
                	<a href="javascript:void(0)" class="cantItems">'.qty_pro().'</a>
                    <a href="javascript:void(0)" class="carro"><i class="material-icons">&#xE8CC;</i></a>
                    <a href="javascript:void(0)" class="totalHeader">$'.number_format(totalCart(),0,",",".").'</a>
                    <a class="cerrarMenuCart" href="javascript:void(0)"><i class="material-icons">&#xE5CD;</i></a>
                </div>';	

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}
			
			if($producto[0][3] > 0){
				$valor = $producto[0][3]*$qty;
			} else {
				$valor = $producto[0][2]*$qty;
			}
			
			$output[] = '
			<div class="filasProdCart ctndr100" id="fila_carro_'.$prd_id.'">
                	<a href="" class="imgThumbsCartHeader">
                   		<img src="'.$thumbs.'" width="100%" />
                	</a>
                    <div class="datosProdHeader">
                    	<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">'.$producto[0][1].'</a>
                        <a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="valorFilaHeader">$'.number_format($valor,0,",",".").'</a>
                    </div>
                    <div class="qtyHeader">
               	  		<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">'.$qty.'</a> 
                    </div>
                    <a class="quitarCarro" href="javascript:void(0)" rel="'.$prd_id.'"><i class="material-icons">&#xE5CD;</i></a> 
                </div><!--Fin filasProdCart -->
			';
			
		}
			
		$output[] .= '<div class="totalesCartHeader ctndr100">
                	TOTAL: <span>$'.number_format(totalCart(),0,",",".").'</span>
                </div>
                <a class="btnComprarHeader" href="'.$url_base.'identificacion">COMPRAR</a>
            </div>';
		
		
	} else {
		$output[] = '<div class="resumenCarroHeader">
						<div class="headCarroResumen ctndr100">
							<a href="javascript:void(0)" class="cantItems">'.qty_pro().'</a>
							<a href="javascript:void(0)" class="carro"><i class="material-icons">&#xE8CC;</i></a>
							<a href="javascript:void(0)" class="totalHeader">$'.number_format(totalCart(),0,",",".").'</a>
							<a class="cerrarMenuCart" href="javascript:void(0)"><i class="material-icons">&#xE5CD;</i></a>
						</div>';	
		$output[] .= 	'<p class="carroVacio">Su carro está vacío.</p><br />';
		$output[] .= '</div>';
	}
	return join('',$output);
	
}



//Funciones carro de compras
function previewDatos($dato){
	$datoCodificado = '';
	$caracteres = str_split($dato);
	
	 foreach ($caracteres as $key => $valor) {
		if($key > 3){
			if($valor == ' '){
				$datoF = ' ';
			} elseif($valor == '@'){
				$datoF = $valor;
			}else {
				$datoF = '*';
			}
		} else {
			$datoF = $valor;
		}
		
		$datoCodificado .= $datoF;
		
	} 
	
   //var_dump($caracteres);
   return $datoCodificado;
}


//cantidad de productos en el carro
function qty_pro(){
	global $db;
	$cart = isset($_SESSION['cart_alfa_cm']) ? $_SESSION['cart_alfa_cm'] : NULL;
	if ($cart){
		$items = explode(',',$cart);
		return count($items);
	}
	else
	{
		return 0;
	}
}
function totalCart(){
	// global $db;
	$cart = isset($_SESSION['cart_alfa_cm']) ? $_SESSION['cart_alfa_cm'] : NULL;
	$total = 0;
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$sql = consulta_bd("id, precio, descuento","productos_detalles","id=$prd_id","");
			if($sql[0][2] > 0){
				$total += $sql[0][2]*$qty;
			} else {
				$total += $sql[0][1]*$qty;
			}
		}
		
	}
	return round($total);
}


//resumen de productos en paso identificacion y envio
function resumenCompraShort() {
	global $db;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			if($productos[0][3] > 0){
				$precio_final = $productos[0][3] *$qty;
			}else{
				$precio_final = $productos[0][2] *$qty;
			}
			
			$output[] .= '
			<div class="fila-carro2">
				<div class="caja1-carro2">'.$productos[0][1].'</div>
				<div class="caja2-carro2 qtyCarroShort">'.$qty.'</div>
				<div class="precio-carro2">$'.number_format(round($precio_final),0,",",".").'</div>
			</div>
			';
			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			$despacho = $address[0][5];
		$output[] = '
		<div class="caja-total2">
			<div class="tot1">Sub Total:</div> <div class="tot2">$'.number_format($total,0,",",".").'</div>';
		
		if($_SESSION["precio"]){
			
			$output[] = '<div class="tot1">Envío</div> 
					 <div id="precio_bottom" class="tot2 valorEnvio">$0</div>';

			$output[] = '<div class="tot1 blue2">Total:</div><div class="tot2 blue2" id="totalConEnvio">$'.number_format($total_neto+$iva+(int)$_SESSION["precio"],0,",",".").'</div>';

			$output[] = '</div>';
		} else {
			
			$output[] = '<div class="tot1">envío:</div> <div id="precio_bottom" class="tot2 valorEnvio"><span class="rojo">$0</span></div>';
			
				$output[] = '<div class="tot1 blue2">Total:</div><div class="tot2 blue2" id="totalConEnvio">$'.number_format($total_neto+$iva,0,",",".").'</div>';
			
			$output[] = '</div>';
		}
		
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('',$output);
	
}

function get_total_price()
{
	global $db;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart){
	
		$items = explode(',',$cart);
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		foreach ($contents as $prd_id=>$qty) {
			$prod = consulta_bd("pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			if($prod[0][1] > 0){
				$precio_final = $prod[0][1];
			}else{
				$precio_final = $prod[0][0];
			}
			$total += $precio_final * $qty;
		}
	}
	else
	{
		$total = 0;
	}
	return $total;
}
//FUNCION PARA SABER TIPO DE PAGO DE WEB PAY Y NUMERO DE CUOTAS
function tipo_pago($tipo_pago,$num_cuotas){
    switch ($tipo_pago){
        case 'VN':
            $tipo_pago = "Crédito";
            $tipo_cuota = "Sin cuotas";
            $cuota = "00";
            break;

        case 'VC':
            $tipo_pago = "Crédito";
            $tipo_cuota = "Cuotas Normales";
            $cuota_valores = strlen($num_cuotas);
            if($cuota_valores==1){
                $cuota="0".$num_cuotas;
            }else{
                $cuota = $num_cuotas;
            }                
            break;

        case 'SI':
            $tipo_pago = "Crédito";
            $tipo_cuota = "Sin interés";
            $cuota_valores = strlen($num_cuotas);
            if($cuota_valores==1){
                $cuota="0".$num_cuotas;
            }else{
                $cuota = $num_cuotas;
            }
            break;

        case 'CI':
            $tipo_pago = "Crédito";
            $tipo_cuota = "Cuotas Comercio";
            $cuota_valores = strlen($num_cuotas);
            $cuota_valores = $num_cuotas ." cuotas";
            
            if($cuota_valores==1){
                $cuota="0".$num_cuotas ." cuotas";
            }else{
                $cuota = $num_cuotas ." cuotas";
            }
            break;

        case 'VD':
            $tipo_pago = "Débito";
            $tipo_cuota = "Venta Débito";
            $cuota = "00";
            break;
    }

    return array("tipo_pago" => $tipo_pago, "tipo_cuota" => $tipo_cuota, "cuota" => $cuota);
}


/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/ 
function fechaRetiroPorCarroActivo(){
	//$datos_cliente = consulta_bd("id","pedidos","oc='$oc'","");
	//$id_pedido = $datos_cliente[0][0];
	global $db;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$cantDias = 0;	

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
            
            $pD = consulta_bd("producto_id, nombre","productos_detalles","id=$prd_id","");
            $id_prod = $pD[0][0];
            $campos = "p.id, p.marca_id, m.dias_despacho";
            $tabla  = "productos p, marcas m";
            $where  = "p.id=$id_prod and p.marca_id = m.id";
            $productos = consulta_bd($campos,$tabla,$where,"");

            if($cantDias > $productos[0][2]){
                    $cantDias = $cantDias;
                } else {
                    $cantDias = $productos[0][2];
                }
            
    
        }
    }
    
    //return $cantDias;
	
	// dias a sumar
    $dias = $dias_origin = $cantDias;
    // dias que el programa ha contado
    $dias_contados = 0;
    // timestamp actual
    $time = time();
    // duracion (en segundos) que tiene un día
    $dia_time = 3600*24; //3600 segundos en una hora * 24 horas que tiene un dia.

	while($dias != 0) {
        $dias_contados++;
        $tiempoContado = $time+($dia_time*$dias_contados); // Sacamos el timestamp en la que estamos ahora mismo comprobando
        if(esFestivo($tiempoContado) == false)
            $dias--;
    }
    //return "El programa ha recorrido ".$dias_contados." (ha saltado ".($dias_contados-$dias_origin).") hasta llegar la fecha que deseabas:".PHP_EOL.date("D, d/m/Y",$tiempoContado);
    
	$fechaFinal = date("D, d/m/Y",$tiempoContado);
	$FEC = explode(",",$fechaFinal);
	if($FEC[0] == "Mon"){
		$disLiteral = "Lunes";
	} else if($FEC[0] == "Tue"){
		$disLiteral = "Martes";
	} else if($FEC[0] == "Wed"){
		$disLiteral = "Miércoles";
	} else if($FEC[0] == "Thu"){
		$disLiteral = "Jueves";
	} else if($FEC[0] == "Fri"){
		$disLiteral = "Viernes";
	} else if($FEC[0] == "Sat"){
		$disLiteral = "Sábado";
	} else if($FEC[0] == "Sun"){
		$disLiteral = "Domingo";
	}
	return $disLiteral." ".$FEC[1];
	
}

function fechaRetiro($oc){
	$datos_cliente = consulta_bd("id","pedidos","oc='$oc'","");
	$id_pedido = $datos_cliente[0][0];
	
	$detalle_pedido = consulta_bd("productos_detalle_id","productos_pedidos","pedido_id=$id_pedido","");
	$cantDias = 0;
	for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
		$pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
		$id_prod = $pD[0][0];
		$campos = "p.id, p.marca_id, m.dias_despacho";
		$tabla  = "productos p, marcas m";
		$where  = "p.id=$id_prod and p.marca_id = m.id";
		$productos = consulta_bd($campos,$tabla,$where,"");
		
		if($cantDias > $productos[0][2]){
				$cantDias = $cantDias;
			} else {
				$cantDias = $productos[0][2];
			}
	}
	
	// dias a sumar
    $dias = $dias_origin = $cantDias;
    // dias que el programa ha contado
    $dias_contados = 0;
    // timestamp actual
    $time = time();
    // duracion (en segundos) que tiene un día
    $dia_time = 3600*24; //3600 segundos en una hora * 24 horas que tiene un dia.

	while($dias != 0) {
        $dias_contados++;
        $tiempoContado = $time+($dia_time*$dias_contados); // Sacamos el timestamp en la que estamos ahora mismo comprobando
        if(esFestivo($tiempoContado) == false)
            $dias--;
    }
    //return "El programa ha recorrido ".$dias_contados." (ha saltado ".($dias_contados-$dias_origin).") hasta llegar la fecha que deseabas:".PHP_EOL.date("D, d/m/Y",$tiempoContado);
    
	$fechaFinal = date("D, d/m/Y",$tiempoContado);
	$FEC = explode(",",$fechaFinal);
	if($FEC[0] == "Mon"){
		$disLiteral = "Lunes";
	} else if($FEC[0] == "Tue"){
		$disLiteral = "Martes";
	} else if($FEC[0] == "Wed"){
		$disLiteral = "Miércoles";
	} else if($FEC[0] == "Thu"){
		$disLiteral = "Jueves";
	} else if($FEC[0] == "Fri"){
		$disLiteral = "Viernes";
	} else if($FEC[0] == "Sat"){
		$disLiteral = "Sábado";
	} else if($FEC[0] == "Sun"){
		$disLiteral = "Domingo";
	}
	return $disLiteral." ".$FEC[1];
	
}

//funcion para sumar dias sin contar fines de semana y festivos
function esFestivo($time) {
        $DF = consulta_bd("año, enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre, noviembre, diciembre","dias_festivos","","");
		
		$dias_festivos = array();
		for($i=0; $i<sizeof($DF); $i++) {
			$año = $DF[$i][0];
			$enero = explode(",",$DF[$i][1]);
			$febrero = explode(",",$DF[$i][2]);
			$marzo = explode(",",$DF[$i][3]);
			$abril = explode(",",$DF[$i][4]);
			$mayo = explode(",",$DF[$i][5]);
			$junio = explode(",",$DF[$i][6]);
			$julio = explode(",",$DF[$i][7]);
			$agosto = explode(",",$DF[$i][8]);
			$septiembre = explode(",",$DF[$i][9]);
			$octubre = explode(",", $DF[$i][10]);
			$noviembre = explode(",",$DF[$i][11]);
			$diciembre = explode(",",$DF[$i][12]);
			
			$año2 = array(
					1 => $enero,
					2 => $febrero,
					3 => $marzo,
					4 => $abril,
					5 => $mayo,
					6 => $junio,
					7 => $julio,
					8 => $agosto,
					9 => $septiembre,
					10 => $octubre,
					11 => $noviembre,
					12 => $diciembre								
					);
			$dias_festivos[$año] = $año2;
			}// fin for
	

				
        $dias_saltados = array(0,6); // 0: domingo, 1: lunes... 6:sabado

        $w = date("w",$time); // dia de la semana en formato 0-6
        if(in_array($w, $dias_saltados)) return true;
        $j = date("j",$time); // dia en formato 1 - 31
        $n = date("n",$time); // mes en formato 1 - 12
        $y = date("Y",$time); // año en formato XXXX
        if(isset($dias_festivos[$y]) && isset($dias_festivos[$y][$n]) && in_array($j,$dias_festivos[$y][$n])) return true;

        return false;
    }
  
  
  function enviarComprobanteCliente($oc){

    $nombre_sitio = "Trazo";
    $nombre_corto = "trazo";
    $noreply = "no-reply@".$nombre_corto.".cl";
    $url_sitio="http://www.".$nombre_corto.".cl";
    $logo = $url_sitio."/img/logo.jpg";
    $correo_venta = "ventas@trazo.cl";
	$color_logo = "#414141";
    $msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion, retiro_en_tienda, fecha_despacho","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_pedido = $datos_cliente[0][2];
	$retiroEnTienda = $datos_cliente[0][4];
    
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo","productos_pedidos","pedido_id=$id_pedido","");
    $despacho = $datos_cliente[0][3];
    
    $ex_despacho = explode('-', $datos_cliente[0][5]);

    if ($retiroEnTienda == 1) {
    	$fecha_despacho = calculoFestivos(1, strtotime($datos_cliente[0][5]));
    	$explode = explode(" ", $fecha_despacho);
    	$fecha_despacho = $explode[2];
    }else{
    	$fecha_despacho = date("d/m/Y", strtotime($datos_cliente[0][5]));
    }

    // $fecha_despacho = $dia.'/'.$ex_despacho[1].'/'.$ex_despacho[0];

    $tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc; color:#666;">
                    <tr>
                        <th align="center" width="20%">Imagen</th>
                        <th align="center" width="20%">Producto</th>
                        <th align="center" width="20%">SKU</th>
                        <th align="center" width="20%">Cantidad</th>
                        <th align="center" width="20%">Precio Unitario</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td  align="center" width="20%" style="border-bottom: 2px solid #ccc;">
												<img src="'.$productos[0][2].' " width="100"/>
											</td>';
                        $tabla_compra .= '  <td  align="center" width="20%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td  align="center" width="20%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][3].'</td>'; //codigo producto
                        $tabla_compra .= '  <td  align="center" width="20%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        $tabla_compra .= '  <td  align="center" width="20%" align="right" style="border-bottom: 2px solid #ccc;color:'.$color_logo.';">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';

                    }

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado","pedidos","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			/*if($totales[0][1] != '' || $totales[0][1] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}*/
			
			/*if($totales[0][2] != '' || $totales[0][2] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Giftcard:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][2],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}*/
            
            

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'; text-align:right;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                <p><a href="'.$url_sitio.'" style="color:#8CC63F;"><img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/></a></p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;">Estimado '.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;">Gracias por su compra</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0 0 10px 0;">Su número de compra es: <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                    <p style="color:#2f6c8e;float:right;width:100%;margin: 0px;">
                                        <a href="'.$url_sitio.'/tienda/boucher/boucherCompra.php?oc='.$oc.'">
                                            <img src="'.$url_sitio.'/tienda/btnBoucher.jpg" />
                                        </a>
                                    </p>
                                </th>
                            </tr>
                        </table>
                        <br/><br/>';
                        
			//franja de info de despacho o retiro en tienda
			  if($retiroEnTienda == 1){          
              $msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #999;">
							<tr>
								<td>
									<div style="float:left; padding:10px;"><img src="'.$url_sitio.'/img/iconoRetiroTienda.png" /></div>
									<div style="float:left; padding:10px; color:#fff;">
										Tu pedido estará disponible para retiro '.$fecha_despacho.'
									</div>
								</td>
							</tr>
						</table><br /><br />';
			  } else {
				 
			  	$msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #999;">
							<tr>
								<td>
									<div style="float:left; padding:10px;"><img src="'.$url_sitio.'/img/iconoDespacho.png" /></div>
									<div style="float:left; padding:10px; color:#fff;">
										Tu pedido será despachado el '.$fecha_despacho.'
									</div>
								</td>
							</tr>
						</table><br /><br />';
			  
			  }
						
						$msg2.='<table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                    <p style="color: #797979;">PRODUCTOS COMPRADOS</p>
									<p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;
	// 
	$to = "$nombre_cliente <$email_cliente>";
	$cc = '';

	return enviarCorreoJP($asunto, $msg2, $to, $cc);
      
    
	/*$mailCliente = new PHPMailer;
    $mailCliente->isSMTP();
    $mailCliente->SMTPDebug = 2;
    $mailCliente->Debugoutput = 'html';
    $mailCliente->Host = 'mail.forastero.life';
    $mailCliente->Port = 25;
    $mailCliente->SMTPSecure = 'tls';
    $mailCliente->SMTPAuth = true;
    $mailCliente->Username = "tiendaforasterolife@gmail.com";
    $mailCliente->Password = "Molde3938";
    $mailCliente->setFrom('no-responder@forastero.life', 'Forastero');
    $mailCliente->addAddress("$email_cliente", "$nombre_cliente");
    $mailCliente->CharSet = 'UTF-8';
    $mailCliente->Subject = $asunto;
    $mailCliente->msgHTML($msg2);
    $mailCliente->AltBody = "$msg2";

    if (!$mailCliente->send()) {
        return 'envio fallido Cliente' . $mailCliente->ErrorInfo;
    } else {
        return 'envio exitoso Cliente';
    }*/



}


function enviarComprobanteAdmin($oc){

    $nombre_sitio = "Trazo";
    $nombre_corto = "trazo";
    $noreply = "no-reply@".$nombre_corto.".cl";
    $url_sitio="http://www.".$nombre_corto.".cl";
    $logo = $url_sitio."/img/logo.jpg";
    //$email_admin = 'clientes@trazodiseno.cl, benjamin@trazodiseno.cl, rafa@trazodiseno.cl,
//contacto@forastero.life, isabel@amoble.cl, aguzman@trazodiseno.cl';
    //$email_admin = 'htorres@moldeable.com';
	$color_logo = "#414141";
    $msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion,retiro_en_tienda","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_pedido = $datos_cliente[0][2];
	$retiroEnTienda = $datos_cliente[0][4];
    
	$datos_cliente = consulta_bd("nombre,
    email,
    id,
    direccion, 
    region,
    ciudad,
    comuna,
    direccion,
    telefono,
    rut,
    factura,
    direccion_factura,
    giro,
    email_factura,
    rut_factura,
    fecha_creacion,
    telefono,
    regalo,
	razon_social,
	payment_type_code,
	shares_number, 
	transaction_date,amount,total_pagado,fecha_despacho","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    
    $tipo_pago = tipo_pago($datos_cliente[0][19],$datos_cliente[0][20]);
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo","productos_pedidos","pedido_id=$id_pedido","");
    $despacho = $datos_cliente[0][3];
    $amount = $datos_cliente[0][22];
   	$total_pagado = $datos_cliente[0][23];
    
    /*$header = "From: ".$nombre_sitio." <".$noreply.">\nReply-To:".$noreply."\n";
    $header .= "X-Mailer:PHP/".phpversion()."\n";
    $header .= "Mime-Version: 1.0\n";
    $header .= "Content-Type: text/html";*/

    $ex_despacho = explode('-', $datos_cliente[0][24]);

    if ($retiroEnTienda == 1) {
    	$fecha_despacho = calculoFestivos(1, strtotime($datos_cliente[0][24]));
    	$explode = explode(" ", $fecha_despacho);
    	$fecha_despacho = $explode[2];
    }else{
    	$fecha_despacho = date("d/m/Y", strtotime($datos_cliente[0][24]));
    }
   	
   	// $fecha_despacho = $dia.'/'.$ex_despacho[1].'/'.$ex_despacho[0];

    $tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;font-family: Trebuchet MS, sans-serif; color:#666;">
                    <tr>
                        <th align="center" width="20%">Imagen</th>
                        <th align="center" width="20%">Producto</th>
                        <th align="center" width="20%">SKU</th>
                        <th align="center" width="20%">Cantidad</th>
                        <th align="center" width="20%">Precio Unitario</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td  align="center" width="20%" style="border-bottom: 2px solid #ccc;">
												<img src="'.$productos[0][2].' " width="100"/>
											</td>';
                        $tabla_compra .= '  <td  align="center" width="20%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td  align="center" width="20%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][3].'</td>'; //codigo producto
                        $tabla_compra .= '  <td  align="center" width="20%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        $tabla_compra .= '  <td  align="center" width="20%" align="right" style="border-bottom: 2px solid #ccc;color:'.$color_logo.';">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';

                    }

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado","pedidos","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			/*if($totales[0][1] != '' || $totales[0][1] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}*/
			
			/*if($totales[0][2] != '' || $totales[0][2] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Giftcard:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][2],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}*/
            
            

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                <p><a href="'.$url_sitio.'" style="color:#8CC63F;"><img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/></a></p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;">'.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;">ha generado una compra web.</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0 0 10px 0;">Su número de compra es: <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
									<p style="color:#2f6c8e;float:right;width:100%;margin: 0px;">
                                        <a href="'.$url_sitio.'/tienda/boucher/boucherCompra.php?oc='.$oc.'">
                                            <img src="'.$url_sitio.'/tienda/btnBoucher.jpg" />
                                        </a>
                                    </p>
                                </th>
                            </tr>
                        </table>
                        <br/><br/>';
                        
						//franja de info de despacho o retiro en tienda
			  if($retiroEnTienda == 1){          
              $msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #999;">
							<tr>
								<td>
									<div style="float:left; padding:10px;"><img src="'.$url_sitio.'/img/iconoRetiroTienda.png" /></div>
									<div style="float:left; padding:10px; color:#fff;">
										Tu pedido estará disponible para retiro '.$fecha_despacho.'
									</div>
								</td>
							</tr>
						</table>';
			  } else {
				 
			  	$msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #999;">
							<tr>
								<td>
									<div style="float:left; padding:10px;"><img src="'.$url_sitio.'/img/iconoDespacho.png" /></div>
									<div style="float:left; padding:10px; color:#fff;">
										Tu pedido será despachado el '.$fecha_despacho.'
									</div>
								</td>
							</tr>
						</table>';
			  
			  }
			  
			 
                        
              $msg2 .= ' <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>
                                <td valign="top" width="50%">
                                    <h3>Dirección de entrega</h3>
                                    <ul>
                                        <li><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'</li>
                                        <li><strong style="color:'.$color_logo.';">Region: </strong>'.$datos_cliente[0][4].'</li>
                                        <li><strong style="color:'.$color_logo.';">Ciudad: </strong>'.$datos_cliente[0][5].'</li>
                                        <li><strong style="color:'.$color_logo.';">Comuna: </strong>'.$datos_cliente[0][6].'</li>
                                    </ul>
                                </td>
                                <td valign="top" width="50%">
                                    <h3>Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][0].'</li>
                                            <li><strong style="color:'.$color_logo.';">Correo: </strong>'.$datos_cliente[0][1].'</li>
                                            <li><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$datos_cliente[0][8].'</li>
                                            <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][9].'</li>
                                        </ul>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>
                                <td valign="top" width="50%">';
                                if($datos_cliente[0][14] != ''){
                            $msg2.='<h3>Datos empresa</h3>
                                    <ul>
                                        <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][18].'</li>
                                        <li><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][11].'</li>
                                        <li><strong style="color:'.$color_logo.';">Giro: </strong>'.$datos_cliente[0][12].'</li>
                                        <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][14].'</li>
                                        <li><strong style="color:'.$color_logo.';">Email: </strong>'.$datos_cliente[0][13].'</li>
                                        <li><strong style="color:'.$color_logo.';">Telefono: </strong>'.$datos_cliente[0][16].'</li>
                                    </ul>';
                                }
                            $msg2.='</td>
                                <td valign="top" width="50%">';
    
	
                            
							$msg2.='<h3>Datos Transbank</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                            <li><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$tipo_pago[tipo_pago].'</li>
                                            <li><strong style="color:'.$color_logo.';">Tipo de cuota: </strong>'.$tipo_pago[tipo_cuota].'</li>
											<li><strong style="color:'.$color_logo.';">Nº Cuotas: </strong>'.$tipo_pago[cuota].'</li>
											<li><strong style="color:'.$color_logo.';">Fecha: </strong>'.$datos_cliente[0][21].'</li>
                                        </ul>
                                    </p>';
                                   
                        $msg2.='</td>
                            </tr>
                        </table>
                        <br/><br/>
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #797979;">PRODUCTOS COMPRADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">Para ver el detalle de la compra y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'/admin/index.php?op=31c&id='.$id_pedido.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;
	// 
	$to = "Clientes <clientes@trazodiseno.cl>";
    $cc = 'Benjamin <benjamin@trazodiseno.cl>, Rafa <rafa@trazodiseno.cl>, Contacto <contacto@forastero.life>, Isabel <isabel@amoble.cl>, Andrea <aguzman@trazodiseno.cl>, Felipe <felipe@trazodiseno.cl>, Moldeable <ventas@moldeable.com>, Consuelo Dias <consuelodiazf@gmail.com>, Sofia <sofia@trazodiseno.cl>, Finanzas <asistentefinanzas@trazodiseno.cl>';
    
    return enviarCorreoJP($asunto, $msg2, $to, $cc);
    

// $mail = new PHPMailer;
// $mail->isSMTP();
// $mail->SMTPDebug = 0;
// $mail->Debugoutput = 'html';
// $mail->Host = 'smtp.gmail.com';
// $mail->Port = 587;
// $mail->SMTPSecure = 'tls';
// $mail->SMTPAuth = true;
// $mail->Username = "tiendaforasterolife@gmail.com";
// $mail->Password = "Molde3938";
// $mail->setFrom('no-responder@forastero.life', 'Forastero');
    
//     $mail->addAddress('clientes@trazodiseno.cl', 'Clientes');
//     $mail->addAddress('benjamin@trazodiseno.cl', 'Benjamin');
//     $mail->addAddress('rafa@trazodiseno.cl', 'Rafa');
//     $mail->addAddress('contacto@forastero.life', 'Contacto');
//     $mail->addAddress('isabel@amoble.cl', 'Isabel');
//     $mail->addAddress('aguzman@trazodiseno.cl', 'Andrea');
//     $mail->addAddress('felipe@trazodiseno.cl', 'Felipe');
//     $mail->addAddress('ventas@moldeable.com', 'Moldeable');
//     //$mail->addAddress('jpgalecio@moldeable.com', 'José Pedro');
//     $mail->addAddress('consuelodiazf@gmail.com', 'Consuelo Diaz');
//     $mail->addAddress('sofia@trazodiseno.cl', 'Sofia');
//     $mail->addAddress('asistentefinanzas@trazodiseno.cl', 'Finanzas');
    
// $mail->CharSet = 'UTF-8';
// $mail->Subject = $asunto;
// $mail->msgHTML($msg2);
// $mail->AltBody = "$msg2";

// if (!$mail->send()) {
//     return 'envio fallido' . $mail->ErrorInfo;
// } else {
//     return 'envio exitoso';
// }
    


	
}


function diasDespacho(){
	global $db;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$dias = 0;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.id, p.marca_id, m.dias_despacho","productos p, productos_detalles pd, marcas m","p.id=pd.producto_id and pd.id=$prd_id and m.id = p.marca_id","");
			
			if($dias  >= $producto[0][2]){
				$dias = $dias;
			} else {
				$dias = $producto[0][2];
			}
		}//fin foreach
			
		
		
		
	} else {
		$dias = 0;
	}
	return $dias;
}


function fechaDespacho($regionFuncion, $localidadFuncion){
	$cantDias = consulta_bd("id, dias_despacho","localidades","id=".$localidadFuncion,"");
	$cantDias = $cantDias[0][1];
		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( "+$cantDias day" , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'j/m/Y' , $nuevafecha );
		return $nuevafecha;
	
}




function showCartExito($oc){
	$pedido = consulta_bd("id, oc, total, valor_despacho, total_pagado","pedidos","oc='$oc'","");
	$productos_pedidos = consulta_bd("pp.cantidad, pp.precio_unitario, pp.precio_total, p.nombre","productos_pedidos pp, productos p, productos_detalles pd","pp.productos_detalle_id = pd.id and pd.producto_id=p.id and pp.pedido_id=".$pedido[0][0],"pp.id");
	
	$carro_exito = '<div class="t-paso3">Productos Asociados</div>
					<div class="t-carro">
						<div class="caja-top1">Producto</div>
						<div class="caja-top2">Precio unitario</div>
						<div class="caja-top4">Cantidad</div>
						<div class="caja-top5">Total Item</div>
					</div>';
	for($i=0; $i<sizeof($productos_pedidos); $i++) {
		$carro_exito .= '<div class="itemsExito">
							<div class="caja-top1">'.$productos_pedidos[$i][3].'</div>
							<div class="caja-top2">$'.number_format($productos_pedidos[$i][1],0,",",".").'</div>
							<div class="caja-top4">'.$productos_pedidos[$i][0].'</div>
							<div class="caja-top5">$'.number_format($productos_pedidos[$i][2],0,",",".").'</div>
						</div>';
	}
	//Fin ciclo
	$carro_exito .= '<div class="totalesExito">
						<div class="filaValoresExito">
							<span>$'.number_format($pedido[0][2],0,",",".").'</span>
							<span class="nomValor">Subtotal</span>
						</div>
						<div class="filaValoresExito">
							<span>$'.number_format($pedido[0][3],0,",",".").'</span>
							<span class="nomValor">Envío</span>
						</div>
						<div class="filaValoresExito filaTotal">
							<span>$'.number_format($pedido[0][4],0,",",".").'</span>
							<span class="nomValor">Total</span>
						</div>
					</div>';
		
	return $carro_exito;
	
}

function valorDespacho($id_localidad){
	global $db;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$valorEnvio = 0;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.id, cp.subcategoria_id","productos p, productos_detalles pd, categorias_productos cp, subcategorias sc","p.id=pd.producto_id and pd.id=$prd_id and cp.producto_id = p.id and sc.id = cp.subcategoria_id","");

			$subcategoriaProducto = $producto[0][1];
			
			$infoLocalidad = consulta_bd("base, tipo_zona","localidades","id=$id_localidad","");
			$cant = sizeof($infoLocalidad);
			if($cant > 0){
				$base = $infoLocalidad[0][0];
				$tipo_zona = $infoLocalidad[0][1];

			} else {
				$base = 0;
				$tipo_zona = 0;
			}
			$valor  = consulta_bd("valor, tiempo","valores_despachos","tipo_zona = '$tipo_zona' and base = '$base' and subcategoria_id = $subcategoriaProducto","");
			$cant2 = sizeof($valor);

			if($cant2 > 0){
			 $valorEnvio += $valor[0][0] * $qty;
			 //CalculoValor en santiago
			 if($valorEnvioSantiago > $valor[0][0]){
				 $valorEnvioSantiago = $valorEnvioSantiago;
				 }else{
					 $valorEnvioSantiago = $valor[0][0];
				}
			} else {
				$valorEnvio += 0;
			}

			// die($subcategoriaProducto);
			
			
		}//fin foreach
			
		
		
		
	} else {
		$valorEnvio = 0;
	}
	$region = consulta_bd("r.id","regiones r, ciudades ciu, comunas c, localidades l","r.id = ciu.region_id and ciu.id = c.ciudade_id and c.id = l.comuna_id and l.id = $id_localidad","");
	if($region[0][0] == 13){
		//valor envio en santiago
		return $valorEnvioSantiago;
	}else{
		//envio resto de las regiones
		return $valorEnvio;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function get_precio($id, $tipo) {
	$filas = consulta_bd("precio","productos", "id = $id", "");
	$precio = $filas[0][0];
	$descuento = $filas[0][1];
	if ($tipo == 'normal')
	{
		$valor = $precio;
	}
	else if ($tipo == 'oferta')
	{
		if ($descuento != 0 and $descuento > 0)
		{
			$valor = round($precio*(1-$descuento/100));
		}
		else
		{
			$valor = $precio;
		}	
	}
	return $valor;
}

function writeMiniCart() {
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return count($items).' producto'.$s;
	}
	else
	{
		return ' Mis compras';
	}
}
function writeShoppingCart(){
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return '<p>Ud tiene <strong>'.count($items).' producto'.$s.'</strong> en su carro de compras:</p>';
	}
}






function finalCart(){
	global $total_compra;
	global $db;
	global $oc;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '
			<table width="690">
				<tr class="titulo_tabla">
					<th align="left">Producto</th>
					<th align="right">Marca</th>
					<th align="right">Precio Unitario</th>
					<th align="right">Cantidad</th>
					<th align="right">Sub-Total</th>
				</tr>
		';
		
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$sql = 'SELECT id, nombre, precio FROM productos WHERE id = '.$prd_id.'';
			$result = $db->query($sql);
			$row = $result->fetch();
			extract($row);
			
			if ($i%2!=0)
			{
				$output[] = '<tr class="gris">';
			}
			else
			{
				$output[] = '<tr>';
			}			
			$precio_final = get_precio($prd_id, 'descuento');
			
			$output[] = '<td>'.$nombre.'</td>';
			$output[] = '<td align="right">$'.number_format($precio_final).'</td>';
			$output[] = '<td align="right">'.$qty.'</td>';
			$output[] = '<td align="right">$'.number_format(($precio_final * $qty)).'</td>';
			$total_compra += $precio_final * $qty;
			$output[] = '</tr>';
			$i++;
		}
		
		//Calcula costo de despacho
		if ($i%2!=0)
		{
			$output[] = '<tr class="gris">';
		}
		else
		{
			$output[] = '<tr>';
		}
		
		$region_id = $_SESSION['region_id'];
		// $costo = costo_por_despacho($region_id);
		
		
		$output[] = '<td>Cargo por despacho';
		if (!$_SESSION['active_user'])
		{
			$output[] = '*';
		}
		$output[] = '</td>';
		$output[] = '<td align="right"></td>';
		$output[] = '<td align="right">';
		$output[] = formato_moneda($costo, 'pesos', 0);
		$output[] = '</td>';
		$output[] = '<td align="right"></td>';
		$output[] = '<td align="right">';
		$output[] = formato_moneda($costo, 'pesos', 0);
		$output[] = '</td>';
		$output[] = '<td width="20"></td>';
		$total_compra += $costo;
		$output[] = '</tr>';

		$total_neto = $total_compra/1.19;
		$iva = $total_neto * 0.19;
		
		$output[] = '<tr>';
		$output[] = '<td colspan="6" >&nbsp;</td>';
		$output[] = '</tr>';
		$output[] = '<tr>';
		$output[] = '<td colspan="3" >&nbsp;</td>';
		$output[] = '<td align="left">Total Neto:</td>';
		$output[] = '<td align="right"><strong>$'.number_format($total_neto).'</strong></td>';

		$output[] = '</tr>';
		
		$output[] = '<tr>';
		$output[] = '<td colspan="3" >&nbsp;</td>';
		$output[] = '<td align="left">IVA:</td>';
		$output[] = '<td align="right"><strong>$'.number_format($iva).'</strong></td>';

		$output[] = '</tr>';

		$output[] = '<tr>';
		$output[] = '<td colspan="3" >&nbsp;</td>';
		$output[] = '<td align="left">Total:</td>';
		$output[] = '<td align="right"><strong>$'.number_format($total_compra).'</strong></td>';

		$output[] = '</tr>';
		$output[] = '<tr><td colspan="5">&nbsp;</td></tr>';
		$output[] = '<tr><td colspan="5" align="right">
						<form action="http://www.bikeoutlet.cl/cgi-bin/tbk_bp_pago.cgi" method="post">

							<input type="hidden" value="http://www.bikeoutlet.cl/index.php?op=success" name="TBK_URL_EXITO"/>
							<input type="hidden" value="http://www.bikeoutlet.cl/index.php?op=failure" name="TBK_URL_FRACASO"/>
							<input type="hidden" name="TBK_ORDEN_COMPRA" value="'.$oc.'">
							<input type="hidden" name="TBK_ID_SESION" value="'.$oc.'">
							<input type="hidden" name="TBK_TIPO_TRANSACCION" value="TR_NORMAL">
							<input type="hidden" name="TBK_MONTO" value="'.$total_compra.'00">
							
							<div id="confirmar" style="margin-left: 520px;float:left;">
								<div id="btns">
								      <div class="but_2"><button type="submit" >Finalizar Compra</button></div>
								</div>
							</div>
						</form>
							
					</td></tr>';
		$output[] = '</table>';
		
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('',$output);
}


// Retorna los días que estará disponible el producto.
function despachoDias($id){

	$producto = consulta_bd('pd.dias_de_fabricacion', 'productos p join productos_detalles pd on p.id = pd.producto_id', "p.id = {$id}", '');
	$dias_fabricacion = $producto[0][0];
	$dia_hoy = date("Y-m-d");

	$dias_despacho = $dias_fabricacion;

	// EN PROCESO
	// if (isCyber()) {
	// 	$dias_despacho += 5;
	// }

	return $dias_despacho;
}

/* Retorna la fecha en la que estará el pedido (Paso 3 carro)
[[ Se tomará la disponibilidad de retiro más alta según productos que estén en el carrito ]] */
function despachoDate($retiro, $comuna_id){
	global $db;
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$cantDias = 0;	

		$i = 1;
		$cantDias = 0;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd('p.id', 'productos p join productos_detalles pd on p.id = pd.producto_id', "pd.id = {$prd_id}", '');

			$id_prod = $producto[0][0];
			$dias_x_prod = despachoDias($id_prod);
			
            if($cantDias > $dias_x_prod){
                $cantDias = $cantDias;
            } else {
                $cantDias = $dias_x_prod;
            }
        }
        if ($retiro != NULL AND $retiro != 0) {
        	$cantDias += 1;
        }
    $dia_hoy = date("Y-m-d");
    $fecha_despacho = strtotime('+'.$cantDias.' day', strtotime($dia_hoy));
	$fecha_despacho = date('d/m/Y', $fecha_despacho);

   }

	$dia_de_despacho = validar_dia_despacho($cantDias, $comuna_id);

	$es_colina = ($comuna_id == 333) ? 1 : 0;

	return fecha_despacho_ws($dia_de_despacho, $es_colina);
}

function validar_dia_despacho($cantDias, $comuna_id){

	if ($comuna_id == 333) {

		$contador_dias = 0;

		$fecha_amoble = explode(" ", calculoFestivos($cantDias+$contador_dias, time()));

		$fecha_seteada = date("Y-m-d", strtotime($fecha_amoble[2]));

		$dia_de_la_semana = date("N", strtotime($fecha_seteada));

		if ($dia_de_la_semana == 2) {
			$fecha_seteada = $fecha_seteada;
		}else{
			return validar_dia_despacho($cantDias+1, $comuna_id);
		}

	}else{
   		$fecha_amoble = explode(" ", calculoFestivos($cantDias, time()));

   		$fecha_seteada = date("Y-m-d", strtotime($fecha_amoble[2]));
		
	}

	return date("d/m/Y", strtotime($fecha_seteada));
}

// Retorna una fecha dependiendo de los días que quieras avanzar
function calculoFestivos($cantDias, $fecha){
	// dias a sumar
	$dias = $dias_origin = $cantDias;
	// dias que el programa ha contado
	$dias_contados = 0;
	// timestamp actual
	// $time = time();
	// duracion (en segundos) que tiene un día
	$dia_time = 3600*24; //3600 segundos en una hora * 24 horas que tiene un dia.

	while($dias != 0) {
        $dias_contados++;
        $tiempoContado = $fecha+($dia_time*$dias_contados); // Sacamos el timestamp en la que estamos ahora mismo comprobando
        if(esFestivo($tiempoContado) == false){
            $dias--;
        }
   }

   $fechaFinal = date("D, Y-m-d",$tiempoContado);
   $FEC = explode(",",$fechaFinal);
	if($FEC[0] == "Mon"){
		$disLiteral = "Lunes";
	} else if($FEC[0] == "Tue"){
		$disLiteral = "Martes";
	} else if($FEC[0] == "Wed"){
		$disLiteral = "Miércoles";
	} else if($FEC[0] == "Thu"){
		$disLiteral = "Jueves";
	} else if($FEC[0] == "Fri"){
		$disLiteral = "Viernes";
	} else if($FEC[0] == "Sat"){
		$disLiteral = "Sábado";
	} else if($FEC[0] == "Sun"){
		$disLiteral = "Domingo";
	}

	return $disLiteral . ' ' . $FEC[1];
}

/* Retorna la fecha en la que estará listo el pedido para ser retiro o despachado (Exito y correos)
[[ Se cuenta como la fecha final la más alta de todos los productos dentro de esa orden de compra ]] */
function despachoDateExito($oc){
	$pedido_id = consulta_bd('id, retiro_en_tienda', 'pedidos', "oc='{$oc}'", '');
	$producto = consulta_bd('p.id, ped.fecha, ped.fecha_creacion, pd.id, ped.comuna', 'pedidos ped join productos_pedidos pp on ped.id = pp.pedido_id join productos_detalles pd on pd.id = pp.productos_detalle_id join productos p on p.id = pd.producto_id', "ped.id = {$pedido_id[0][0]}", '');
	$cant = mysqli_affected_rows($conexion);
	if ($cant > 0) {
		foreach ($producto as $prod) {
			$dias_x_prod = despachoDias($prod[0]);
			$cantDias = 0;
			if ($cantDias > $dias_x_prod) {
				$cantDias = $cantDias;
			}else{
				$cantDias = $dias_x_prod;
			}
		}
		if ($pedido_id[0][1] == 1) {
			$cantDias += 1;
		}

		$dia_pedido = date("Y/m/d", strtotime($producto[0][1]));
      	$fecha_despacho = strtotime('+'.$cantDias.' day', strtotime($dia_pedido));
		$fecha_despacho = date('d/m/Y', $fecha_despacho);
	}

	$dia_de_despacho = validar_dia_despacho($cantDias, $comuna_id[0][0]);

	$es_colina = ($comuna_id[0][0] == 333) ? 1 : 0;

	return fecha_despacho_ws($dia_de_despacho, $es_colina);
}

function productos($filtro){
	$campos = 'p.id, p.nombre, p.thumbs, pd.precio, pd.descuento, m.nombre, pd.stock, pd.id, IF(pd.descuento > 0, pd.descuento, pd.precio) as precio_final, pd.sku, p.fecha_modificacion';
	$tablas = 'productos p join categorias_productos cp on p.id = cp.producto_id join productos_detalles pd on pd.producto_id = p.id join marcas m on m.id = p.marca_id';
	$where = 'p.publicado = 1 AND pd.publicado = 1 AND pd.principal = 1';

	if (isset($filtro['orden']) AND $filtro['orden'] != null AND $filtro['orden'] != '') {
		$last .= $filtro['orden'];
	}else if (isset($filtro['random']) AND $filtro['random'] != false) {
		$last .= '';
	}else{
		$last .= 'p.id desc';
	}

	if (isset($filtro['random']) AND $filtro['random'] != false AND $filtro['random'] != '') {
		$last .= ' RAND()';
	}

	// Limite normal
	if (isset($filtro['limite']) AND $filtro['limite'] != null AND $filtro['limite'] != '') {
		$last .= ' LIMIT '.$filtro['limite'];
	}

	// Límite para paginado
	if (isset($filtro['limit']) AND $filtro['limit'] != null AND $filtro['limit'] != '') {
		$last .= ' '.$filtro['limit'];
	}

	if (isset($filtro['linea']) AND $filtro['linea'] != null AND $filtro['linea'] != '') {
		$where .= ' AND cp.linea_id = ' . $filtro['linea'];
	}

	if (isset($filtro['categoria']) AND $filtro['categoria'] != null AND $filtro['categoria'] != '') {
		$where .= ' AND cp.categoria_id = ' . $filtro['categoria'];
	}

	if (isset($filtro['subcategoria']) AND $filtro['subcategoria'] != null AND $filtro['subcategoria'] != '') {
		$where .= ' AND cp.subcategoria_id = ' . $filtro['subcategoria'];
	}

	if (isset($filtro['busqueda']) AND $filtro['busqueda'] != null AND $filtro['busqueda'] != '') {
		$where .= " AND p.nombre LIKE '%{$filtro['busqueda']}%' OR pd.sku LIKE '%{$filtro['busqueda']}%'";
	}

	if (isset($filtro['destacados']) AND $filtro['destacados'] != false AND $filtro['destacados'] != null) {
		$where .= " AND p.destacado = 1";
	}

	if (isset($filtro['oferta']) AND $filtro['oferta'] != false) {
		$where .= " AND pd.descuento > 0";
	}

	if (isset($filtro['relacionados']) AND $filtro['relacionados'] != '' AND $filtro['relacionados'] != NULL) {
		$where .= " AND p.id != " .$filtro['relacionados'];
	}

	// return "SELECT $campos FROM $tablas WHERE $where ORDER BY $last";

	$productos = consulta_bd($campos,$tablas,$where." GROUP BY p.id",$last);
	$index = 0;
	$count = 0;

	if (is_array($productos)) {
		foreach ($productos as $aux) {
			$out['productos']['producto'][$index]['id_producto'] = $aux[0]; // Madre
			$out['productos']['producto'][$index]['id_hijo'] = $aux[7]; // Hijo
			$out['productos']['producto'][$index]['nombre'] = $aux[1];
			// $out['productos']['producto'][$index]['imagen_grilla'] = 'imagenes/productos/'.$aux[2];
			$out['productos']['producto'][$index]['imagen_grilla'] = $aux[2];
			$out['productos']['producto'][$index]['precio'] = $aux[3];
			$out['productos']['producto'][$index]['descuento'] = $aux[4];
			$out['productos']['producto'][$index]['porcentaje'] = (($aux[3] - $aux[4]) / $aux[3]) * 100;
			$out['productos']['producto'][$index]['precio_final'] = $aux[8];
			$out['productos']['producto'][$index]['marca'] = $aux[5];
			$out['productos']['producto'][$index]['stock'] = $aux[6];
			$out['productos']['producto'][$index]['nombre_seteado'] = url_amigables($aux[1]);
			$out['productos']['producto'][$index]['sku'] = $aux[9];
			$out['productos']['producto'][$index]['fecha_modificacion'] = $aux[10];
			$index++;
			$count++;
		}
		$out['productos']['total'][] = $count;
	}else{
		return false;
	}


	return $out;
}

function recorrerCarro(){
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
   }

   return $contents;
}

function get_grilla($producto, $page, $ipp, $rutaRetorno, $filtro){
	$pagina = $page;
	$contPage = 1;
	$productos = $producto['productos']['producto'];
	$total_productos = $producto['productos']['total'][0];

	$pages = new Paginator;
	$pages->items_total = $total_productos;
	$pages->mid_range = 3;
	$pages->paginate($rutaRetorno);

	$filtro['limit'] = $pages->limit;
	$products = productos($filtro);
	$productos = $products['productos']['producto'];

	if (is_array($productos)) {
		for($i=0; $i<sizeof($productos); $i++){ 
		/*if($contPage > ($ipp*$pagina)){
			$pagina = $pagina + 1;
		} else {
		
		}
		$contPage = $contPage +1;*/
		
		
			if($productos[$i]['imagen_grilla'] == ''|| $productos[$i]['imagen_grilla'] == 'NULL'){
				$grilla = $url_base."img/sinImagenGrilla.jpg";
			} else {
				$grilla = $productos[$i]['imagen_grilla'];
			}

			$url = 'ficha/'.$productos[$i]['id_producto'].'/'.$productos[$i]['nombre_seteado'];

	   		$out .= '<div class="grillaProducto">
	        <div class="intGrilla">
	            <a href="'.$url.'" class="contImgGrilla">';
	            	if ($productos[$i]['descuento']) {
	            		$out .= '<span class="triangulo"><span>-'.round($productos[$i]['porcentaje']).'%</span></span>';
	            	}
	            $out .= '<img src="'.$grilla.'" width="100%" alt="'.$productos[$i]['nombre'].'" />
	            </a>';
	            // -----------------
	            // Si el producto tiene un solo hijo se muestra el botón de agregar desde la grilla
	            $productos_hijo = consulta_bd("count(id)", "productos_detalles", "producto_id = {$productos[$i]['id_producto']}", "");
				if ($productos_hijo[0][0] == 1) {
					$out .= '<a href="#" class="add-grilla mostrar" data-ide="'.$productos[$i]['id_hijo'].'" data-sku="'.$productos[$i]['sku'].'">Agregar al carro</a>';
				}
				// ----------------- 
	            $out .= '<a href="'.$url.'">'.$productos[$i]['nombre'].'</a>
	            <a href="'.$url.'" class="valorGrilla">';
	            if ($productos[$i]['descuento'] > 0) {
	            	$out .= '<span class="ahora">$'.number_format($productos[$i]['descuento'],0,",",".").'</span> - <span class="antes">$'.number_format($productos[$i]['precio'],0,",",".").'</span>';
	            }else{
	            	if ($productos_hijo[0][0] > 1) {
	            		$out .='Desde $'.number_format($productos[$i]['precio'],0,",",".").'';
	            	}else{
	            		$out .='$'.number_format($productos[$i]['precio'],0,",",".").'';
	            	}
	            }
	            $out .= '</a>'; 
	            $medidas = cantMedidas($productos[$i]['id_producto']);
	            if ($medidas > 1) {
	            	$out .= '<a href="'.$url.'" class="medidasGrilla">'. $medidas .' Tamaños disponibles</a>';
				} 

	            $stock = ultimasUnidades($productos[$i]['id_producto']);
	            if ($stock > 0) {
	           		$out .= '<a href="'.$url.'" class="unidadesGrilla">¡Últimas unidades!</a>';
	            }

	        $out .= '</div></div>';
	    }
	    $out .= '</div>';
	    $out .= '<div class="contBtnMas paginacion">'.$pages->display_pages().'</div>';
	}else{
		$out = 'No se encontraron productos </div>';
	}
	
    return $out;
}

function grilla_ofertas($producto, $page, $ipp, $rutaRetorno, $filtro){
	$productos = $producto['productos']['producto'];
	$total_productos = $producto['productos']['total'][0];
	$pages = new Paginator;
    $pages->items_total = $total_productos;
    $pages->mid_range = 3;

    $pages->paginate($rutaRetorno, $filtro_cat, $filtro_sub);

    $filtro['limit'] = $pages->limit;

    $productos = productos($filtro);
	$productos = $productos['productos']['producto'];

	if (is_array($productos)) {
		for($i=0; $i<sizeof($productos); $i++){ 

			if($productos[$i]['imagen_grilla'] == ''|| $productos[$i]['imagen_grilla'] == 'NULL'){
				$grilla = $url_base."img/sinImagenGrilla.jpg";
			} else {
				$grilla = $productos[$i]['imagen_grilla'];
			}

			$url = 'ficha/'.$productos[$i]['id_producto'].'/'.$productos[$i]['nombre_seteado'];

	   		$out .= '<div class="grillaProducto">
	        <div class="intGrilla">
	            <a href="'.$url.'" class="contImgGrilla">';
	            	if ($productos[$i]['descuento']) {
	            		$out .= '<span class="triangulo"><span>-'.round($productos[$i]['porcentaje']).'%</span></span>';
	            	}
	            $out .= '<img src="'.$grilla.'" width="100%" alt="'.$productos[$i]['nombre'].'" />
	            </a>';
	            // -----------------
	            // Si el producto tiene un solo hijo se muestra el botón de agregar desde la grilla
	            $productos_hijo = consulta_bd("count(id)", "productos_detalles", "producto_id = {$productos[$i]['id_producto']}", "");
				if ($productos_hijo[0][0] == 1) {
					$out .= '<a href="#" class="add-grilla mostrar" data-ide="'.$productos[$i]['id_hijo'].'" data-sku="'.$productos[$i]['sku'].'">Agregar al carro</a>';
				}
				// ----------------- 
	            $out .= '<a href="'.$url.'">'.$productos[$i]['nombre'].'</a>
	            <a href="'.$url.'" class="valorGrilla">';
	            if ($productos[$i]['descuento'] > 0) {
	            	$out .= '<span class="ahora">$'.number_format($productos[$i]['descuento'],0,",",".").'</span> - <span class="antes">$'.number_format($productos[$i]['precio'],0,",",".").'</span>';
	            }else{
	            	if ($productos_hijo[0][0] > 1) {
	            		$out .='Desde $'.number_format($productos[$i]['precio'],0,",",".").'';
	            	}else{
	            		$out .='$'.number_format($productos[$i]['precio'],0,",",".").'';
	            	}
	            }
	            $out .= '</a>'; 
	            $medidas = cantMedidas($productos[$i]['id_producto']);
	            if ($medidas > 1) {
	            	$out .= '<a href="'.$url.'" class="medidasGrilla">'. $medidas .' Tamaños disponibles</a>';
				} 

	            $stock = ultimasUnidades($productos[$i]['id_producto']);
	            if ($stock > 0) {
	           		$out .= '<a href="'.$url.'" class="unidadesGrilla">¡Últimas unidades!</a>';
	            }

	        $out .= '</div></div>';
	    }
	    $out .= '</div><!--Fin productosDestacados -->';
	    $out .= '<div class="contBtnMas paginacion">'.$pages->display_pages().'</div>';
	}else{
		$out = 'No se encontraron productos';
	}
	
       
    return $out;
}

function grilla_busqueda($producto){
	$productos = $producto['productos']['producto'];
	$total_productos = $producto['productos']['total'][0];

	for($i=0; $i<sizeof($productos); $i++){ 

		if($productos[$i]['imagen_grilla'] == ''|| $productos[$i]['imagen_grilla'] == 'NULL'){
			$grilla = $url_base."img/sinImagenGrilla.jpg";
		} else {
			$grilla = $productos[$i]['imagen_grilla'];
		}

		$url = 'ficha/'.$productos[$i]['id_producto'].'/'.$productos[$i]['nombre_seteado'];

   		$out .= '<div class="grillaProducto">
        <div class="intGrilla">
            <a href="'.$url.'" class="contImgGrilla">';
            	if ($productos[$i]['descuento']) {
            		$out .= '<span class="triangulo"><span>-'.round($productos[$i]['porcentaje']).'%</span></span>';
            	}
            $out .= '<img src="'.$grilla.'" width="100%" alt="'.$productos[$i]['nombre'].'" />
            </a>';
            // -----------------
            // Si el producto tiene un solo hijo se muestra el botón de agregar desde la grilla
            $productos_hijo = consulta_bd("count(id)", "productos_detalles", "producto_id = {$productos[$i]['id_producto']}", "");
			if ($productos_hijo[0][0] == 1) {
				$out .= '<a href="#" class="add-grilla mostrar" data-ide="'.$productos[$i]['id_hijo'].'" data-sku="'.$productos[$i]['sku'].'">Agregar al carro</a>';
			}
			// ----------------- 
            $out .= '<a href="'.$url.'">'.$productos[$i]['nombre'].'</a>
            <a href="'.$url.'" class="valorGrilla">';
            if ($productos[$i]['descuento'] > 0) {
            	$out .= '<span class="ahora">$'.number_format($productos[$i]['descuento'],0,",",".").'</span> - <span class="antes">$'.number_format($productos[$i]['precio'],0,",",".").'</span>';
            }else{
            	if ($productos_hijo[0][0] > 1) {
            		$out .='Desde $'.number_format($productos[$i]['precio'],0,",",".").'';
            	}else{
            		$out .='$'.number_format($productos[$i]['precio'],0,",",".").'';
            	}
            }
            $out .= '</a>'; 
            $medidas = cantMedidas($productos[$i]['id_producto']);
            if ($medidas > 1) {
            	$out .= '<a href="'.$url.'" class="medidasGrilla">'. $medidas .' Tamaños disponibles</a>';
			} 

            $stock = ultimasUnidades($productos[$i]['id_producto']);
            if ($stock > 0) {
           		$out .= '<a href="'.$url.'" class="unidadesGrilla">¡Últimas unidades!</a>';
            }

        $out .= '</div></div>';
    }
       
    return $out;
}

function grilla_destacados($arreglo){
	$productos = $arreglo['productos']['producto'];

	for ($i=0; $i < sizeof($productos); $i++) { 
		if($productos[$i]['imagen_grilla'] == ''|| $productos[$i]['imagen_grilla'] == 'NULL'){
			$grilla = $url_base."img/sinImagenGrilla.jpg";
		} else {
			$grilla = $productos[$i]['imagen_grilla'];
		}

		$url = 'ficha/'.$productos[$i]['id_producto'].'/'.$productos[$i]['nombre_seteado'];
		$dest_ocultos = ($i > 4) ? 'destacadosOcultos' : '';

		$out .= '<div class="grillaProducto">
		<div class="intGrilla '.$dest_ocultos.'">
        <a href="'.$url.'" class="contImgGrilla">';
        	if ($productos[$i]['descuento']) {
        		$out .= '<span class="triangulo"><span>-'.round($productos[$i]['porcentaje']).'%</span></span>';
        	}
        $out .= '<img src="'.$grilla.'" width="100%" alt="'.$productos[$i]['nombre'].'" />
        </a>';
        // -----------------
        // Si el producto tiene un solo hijo se muestra el botón de agregar desde la grilla
        $productos_hijo = consulta_bd("count(id)", "productos_detalles", "producto_id = {$productos[$i]['id_producto']}", "");
		if ($productos_hijo[0][0] == 1) {
			$out .= '<a href="#" class="add-grilla mostrar" data-ide="'.$productos[$i]['id_hijo'].'" data-sku="'.$productos[$i]['sku'].'">Agregar al carro</a>';
		}
		// ----------------- 
        $out .= '<a href="'.$url.'">'.$productos[$i]['nombre'].'</a>
        <a href="'.$url.'" class="valorGrilla">';
        if ($productos[$i]['descuento'] > 0) {
        	$out .= '<span class="ahora">$'.number_format($productos[$i]['descuento'],0,",",".").'</span> - <span class="antes">$'.number_format($productos[$i]['precio'],0,",",".").'</span>';
        }else{
        	if ($productos_hijo[0][0] > 1) {
        		$out .='Desde $'.number_format($productos[$i]['precio'],0,",",".").'';
        	}else{
        		$out .='$'.number_format($productos[$i]['precio'],0,",",".").'';
        	}
        }
        $out .= '</a>'; 
        $medidas = cantMedidas($productos[$i]['id_producto']);
        if ($medidas > 1) {
        	$out .= '<a href="'.$url.'" class="medidasGrilla">'. $medidas .' Tamaños disponibles</a>';
		} 

        $stock = ultimasUnidades($productos[$i]['id_producto']);
        if ($stock > 0) {
       		$out .= '<a href="'.$url.'" class="unidadesGrilla">¡Últimas unidades!</a>';
        }

		$out .= '</div></div>';
	}

	return $out;
}

function lista_carro(){
	$contents = recorrerCarro();

	$index = 0;
	foreach ($contents as $prd_id => $qty) {
		$producto = consulta_bd('p.thumbs, p.nombre, IF(pd.descuento > 0, pd.descuento, pd.precio), pd.sku, pd.nombre, pd.terminacion_id', 'productos p join productos_detalles pd on pd.producto_id = p.id', "pd.id = $prd_id", '');

		$out['producto'][$index]['id'] = $prd_id;
		$out['producto'][$index]['nombre'] = $producto[0][1];
		$out['producto'][$index]['nombre_hijo'] = $producto[0][4];
		$out['producto'][$index]['imagen_grilla'] = $producto[0][0];
		$out['producto'][$index]['precio'] = $producto[0][2];
		$out['producto'][$index]['cantidad'] = $qty;
		$out['producto'][$index]['nombre_seteado'] = url_amigables($producto[0][1]);
		$out['producto'][$index]['sku'] = $producto[0][3];
		$out['producto'][$index]['terminacion'] = $producto[0][5];
		$out['totales']['subtotal'] += $producto[0][2]*$qty;
		$index++;
	}



	return $out;
}

function fecha_despacho_ws($fecha, $colina){
	// Envío la fecha calculada al ws para ver si está disponible
 	if ($colina == 1) {
 		$url = "https://trazo.cl/consultar_dia_despacho_colina/".$fecha.'/1';
 	}else{
 		$url = "https://trazo.cl/consultar_dia_despacho/".$fecha;
 	}
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

	$str = curl_exec($curl); 
	curl_close($curl);
	$fecha_ws = json_decode($str, true);

	$fecha_ws = $fecha_ws['fecha'];

	return $fecha_ws;
}

function enviarCorreoJP($asunto, $mensaje, $to, $cc){

	$website_key = "TTYxMlE1YVFiamUxTFpPWWVPNjAvVzdxd0hQcjNNMU83TE1ZWnFTTjdzbz0tLUtDOEo1c1RFOGFMVHczYlQ4MTduMVE9PQ==--79f57b2956cfb77d8e7776642cfcc304cd54552e";

	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => "http://142.93.196.220/api/messages",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"asunto\"\r\n\r\n$asunto\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mensaje\"\r\n\r\n$mensaje\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to\"\r\n\r\n$to\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cc\"\r\n\r\n$cc\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"website_key\"\r\n\r\n$website_key\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
		CURLOPT_HTTPHEADER => array(
		"Cache-Control: no-cache",
		"Postman-Token: 98658c76-cbfd-41dc-9a8d-80c83ed3571f",
		"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  return 0;
	} else {
	  return 1;
	}

}

function producto_dificil(){
	$cart = $_SESSION['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
	}

	foreach ($contents as $prd_id => $qty) {
		$producto = consulta_bd('p.dificil', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $prd_id", '');

		$pr_dificil = $producto[0][0];

		if ($pr_dificil == 1) {
			return false;
		}
	}

	return true;
}

function codigo_descuento($codigo, $localidad_id){
	/* Consultamos si el codigo de descuento es válido */
	$fecha = date('Ymd');
	$consulta = consulta_bd('id, valor, porcentaje, codigo, descuento_opcion_id, donde_el_descuento', 'descuento_productos',"codigo = '{$codigo}' COLLATE utf8_bin and activo = 1 and $fecha >= fecha_desde and $fecha <= fecha_hasta and (cantidad - usados > 0)", "");

	if (is_array($consulta)) {
		$opcion_descuento = $consulta[0][4];
		$valor = $consulta[0][1];
		$porcentaje = $consulta[0][2];

		$total_carro = totalCart();

		if ($opcion_descuento == 3) { // Opción aplicar a todo (Total del carro)
			if ($valor > 0) {
				if ($valor > $total_carro) {
					return 0;
				}else{
					$descuento = $valor;
				}
			}else{
				$descuento = round(($total_carro * $porcentaje) / 100);
			}
			$_SESSION['descuento']['codigo'] = $codigo;
			$_SESSION['descuento']['valor'] = $descuento;
			return 1;
		}elseif($opcion_descuento == 2 OR $opcion_descuento == 1){ // Opción subcategorias y categorías
			$carro = recorrer_carro();
			$donde = $consulta[0][5];

			$opcion = ($opcion_descuento == 2) ? "sb.id = $donde" : "c.id = $donde";

			$descuento = 0;
			foreach ($carro as $prd => $qty) {
				$subcategoria_producto = consulta_bd('sb.id, pd.precio, pd.descuento', 'categorias_productos cp JOIN subcategorias sb ON sb.id = cp.subcategoria_id JOIN categorias c ON c.id = cp.categoria_id JOIN productos p ON cp.producto_id = p.id JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $prd and $opcion", '');
				
				if (is_array($subcategoria_producto)) {
					$precio = $subcategoria_producto[0][1];
					$descuento_producto = $subcategoria_producto[0][2];
					if ($descuento_producto > 0) {
						$descuento = $descuento;
					}else{
						if ($valor > 0) {
							if ($valor > $total_carro) {
								return 0;
							}else{
								$descuento += $valor * $qty;
							}
						}else{
							$descuento += round(($precio * $porcentaje) / 100) * $qty;
						}
					}
				}else{
					$descuento = $descuento;
				}
			}

			if ($descuento == 0) {
				unset($_SESSION['descuento']);
				return 0;
			}else{
				$_SESSION['descuento']['codigo'] = $codigo;
				$_SESSION['descuento']['valor'] = $descuento;
				return 1;
			}

		}
	}else{
		if ($localidad_id != 0) {
			// Revisamos si el codigo pertenece a uno de despacho
			$fecha = date('Ymd');
			$consulta_despacho = consulta_bd('id, codigo, monto, region_id, comuna_id', 'descuento_despachos',"codigo = '{$codigo}' COLLATE utf8_bin and activo = 1 and $fecha >= fecha_desde and $fecha <= fecha_hasta", "");

			$comuna = consulta_bd('c.id', 'comunas c join localidades l on c.id = l.comuna_id', "l.id = $localidad_id", '');
			$region = consulta_bd('r.id', 'regiones r join ciudades ci on ci.region_id = r.id join comunas c on c.ciudade_id = ci.id join localidades l on c.id = l.comuna_id', "l.id = $localidad_id", '');

			if (is_array($consulta_despacho)) {
				if ($consulta_despacho[0][4] != 0) {
					if ($consulta_despacho[0][4] == $comuna[0][0]) {
						$_SESSION['descuento']['codigo'] = $consulta_despacho[0][1];
						$_SESSION['descuento']['valor'] = $consulta_despacho[0][2];

						$_SESSION['descuento_despacho'] = true;
						return 2;
					}else{
						unset($_SESSION['descuento']);
						unset($_SESSION['descuento_despacho']);
						return 0;
					}
				}elseif($consulta_despacho[0][3] == $region[0][0]){
					$_SESSION['descuento']['codigo'] = $consulta_despacho[0][1];
					$_SESSION['descuento']['valor'] = $consulta_despacho[0][2];

					$_SESSION['descuento_despacho'] = true;
					return 2;
				}else{
					unset($_SESSION['descuento']);
					unset($_SESSION['descuento_despacho']);
					return 0;
				}
			}else{
				unset($_SESSION['descuento']);
				unset($_SESSION['descuento_despacho']);
				return 0;
			}
		}else{
			unset($_SESSION['descuento']);
			unset($_SESSION['descuento_despacho']);
			return 0;
		}
	}
	mysqli_close($conexion);
}

function info_user($cliente_id, $dato){

	$cliente = consulta_bd('nombre, rut, telefono, email, apellido', 'clientes', "id = $cliente_id", '');

	if (is_array($cliente)) {
		if ($cliente_id > 0) {

			switch ($dato) {
				case 'nombre':
					return $cliente[0][0];
				break;

				case 'apellido':
					return $cliente[0][4];
				break;
				
				case 'rut':
					return $cliente[0][1];
				break;

				case 'telefono':
					return $cliente[0][2];
				break;

				case 'email':
					return $cliente[0][3];
				break;

				case 'fullname':
					return $cliente[0][0] . ' ' . $cliente[0][4];
				break;
			}

		}else{
			// Todos los datos (Para el dash)
		}
	}

	return null;

}

function get_pedidos($user_id, $limit){
	if (is_null($limit)) {
		$query = consulta_bd('DISTINCT(id), oc, fecha, fecha_despacho, total_pagado', 'pedidos', "cliente_id = $user_id AND estado_id = 2", 'id desc');
	}else{
		$query = consulta_bd('DISTINCT(id), oc, fecha, fecha_despacho, total_pagado', 'pedidos', "cliente_id = $user_id AND estado_id = 2", "id desc $limit");
	}
	$i = 0;
	$x = 0;
	foreach ($query as $item) {
		$out[$i]['id'] = $item[0];
		$out[$i]['oc'] = $item[1];
		$out[$i]['fecha'] = $item[2];
		$out[$i]['fecha_entrega'] = $item[3];
		$out[$i]['total_pagado'] = $item[4];

		$productos = consulta_bd('p.thumbs, p.nombre, pd.sku, pp.cantidad, pp.precio_total, pd.id', 'productos_pedidos pp JOIN productos_detalles pd ON pd.id = pp.productos_detalle_id JOIN productos p ON p.id = pd.producto_id', "pp.pedido_id = $item[0]", '');

		foreach ($productos as $aux) {
			$out[$i]['productos'][$x]['nombre'] = $productos[0][1];
			$out[$i]['productos'][$x]['imagen'] = $productos[0][0];
			$out[$i]['productos'][$x]['sku'] = $productos[0][2];
			$out[$i]['productos'][$x]['cantidad'] = $productos[0][3];
			$out[$i]['productos'][$x]['total'] = $productos[0][4];
			$out[$i]['productos'][$x]['id'] = $productos[0][5];

			$x++;
		}

		$i++;
	}

	return $out;

}

function get_pedido($oc){
	$query = consulta_bd('DISTINCT(id), oc, fecha, fecha_despacho, total_pagado, valor_despacho, card_number, shares_number, payment_type_code, authorization_code, retiro_en_tienda, direccion, comuna, region, ciudad, transaction_date, total, descuento', 'pedidos', "oc = '$oc'", '');

	if (is_array($query)) {

		$out[0]['id'] = $query[0][0];
		$out[0]['oc'] = $query[0][1];
		$out[0]['fecha'] = $query[0][2];
		$out[0]['fecha_entrega'] = $query[0][3];
		$out[0]['total_pagado'] = $query[0][4];
		$out[0]['valor_despacho'] = $query[0][5];
		$out[0]['card_number'] = $query[0][6];
		$out[0]['cuotas'] = $query[0][7];
		$out[0]['tipo_pago'] = $query[0][8];
		$out[0]['codigo_auth'] = $query[0][9];
		$out[0]['retiro_tienda'] = $query[0][10];
		$out[0]['direccion'] = $query[0][11];
		$out[0]['comuna'] = $query[0][12];
		$out[0]['region'] = $query[0][13];
		$out[0]['ciudad'] = $query[0][14];
		$out[0]['fecha_webpay'] = $query[0][15];
		$out[0]['total'] = $query[0][16];
		$out[0]['descuento'] = $query[0][17];

		$productos = consulta_bd('p.thumbs, p.nombre, pd.sku, pp.cantidad, pp.precio_total, pd.id', 'productos_pedidos pp JOIN productos_detalles pd ON pd.id = pp.productos_detalle_id JOIN productos p ON p.id = pd.producto_id', "pp.pedido_id = {$query[0][0]}", '');

		$x = 0;
		foreach ($productos as $aux) {
			$out[0]['productos'][$x]['nombre'] = $productos[0][1];
			$out[0]['productos'][$x]['imagen'] = $productos[0][0];
			$out[0]['productos'][$x]['sku'] = $productos[0][2];
			$out[0]['productos'][$x]['cantidad'] = $productos[0][3];
			$out[0]['productos'][$x]['total'] = $productos[0][4];
			$out[0]['productos'][$x]['id'] = $productos[0][5];

			$x++;
		}

		return $out;

	}

}

function producto_guardado($producto_id, $cliente_id){

	$query = consulta_bd('id', 'productos_guardados', "cliente_id = $cliente_id AND productos_detalle_id = $producto_id", '');

	return is_array($query);

}

function productos_guardados($cliente_id){
	$query = consulta_bd('pd.id, p.nombre, pd.sku, IF(pd.descuento > 0, pd.descuento, pd.precio), p.thumbs', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN productos_guardados pg ON pd.id = pg.productos_detalle_id', "pg.cliente_id = $cliente_id", 'pg.id desc');

	return $query;
}

function cacheo(){
	return rand(999999999, 99999999999);
}

function validar_stock_carro($carro){
	$items = explode(',',$carro);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}

	$errores = 0;
	$contador = 0;
	foreach ($contents as $prd => $qty) {
		$query = consulta_bd('pd.sku, p.pack, p.id', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $prd", '');

		$sku = $query[0][0];
		$pack = $query[0][1];
		$cantidad = $qty;

		$errores = 0;
		if ($pack == 0) {
		
			$file = file_get_contents('http://trazo.cl/ws_elias/product_size/sku/'.$sku);
			$xml = simplexml_load_string($file);
			$stock = $xml->items->stock_disponible; // Stock disponible trazo sin procesar
			$transito = $xml->items->stock_en_transito;

			$data = consulta_bd('pd.id', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.sku = '$sku'", '');

			if ($file) {
				
				$stock_disponible = $stock - $transito;
				if ($stock_disponible > 0) {

					if ($stock_disponible < $cantidad) {
						$errores++;
					}

				}else{
					$errores++;
				}

			}else{
				$errores++;
			}

			$out['errores'] = $errores;
			$out['productos'][$contador]['id'] = (int)$data[0][0];
			$out['productos'][$contador]['error'] = ($errores > 0) ? 1 : 0;

			$contador++;

		}else{
			$productos_pack = consulta_bd('p.codigos_pack', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.sku = '$sku'", '');
			$explode_pack = explode(',', $productos_pack[0][0]);
			foreach ($explode_pack as $item) {
				$sku_pack = trim($item);
				$file = file_get_contents('http://trazo.cl/ws_elias/product_size/sku/'.$sku_pack);
				$xml = simplexml_load_string($file);
				$stock = $xml->items->stock_disponible; // Stock disponible trazo sin procesar
				$transito = $xml->items->stock_en_transito;

				$data = consulta_bd('pd.id', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.sku = '$sku_pack'", '');

				if ($file) {
					
					$stock_disponible = $stock - $transito;
					if ($stock_disponible > 0) {

						if ($stock_disponible < $cantidad) {
							$errores++;
						}

					}else{
						$errores++;
					}

				}else{
					$errores++;
				}

				$out['errores'] = $errores;
				$out['productos'][$contador]['id'] = (int)$data[0][0];
				$out['productos'][$contador]['error'] = ($errores > 0) ? 1 : 0;

				$contador++;
			}
		}

	}

	if ($out['errores'] > 0) {
		$_SESSION['error_stock'] = $out['productos'];
	}

	// Creo una sesión para saber que el cliente dio click en el botón de mi-carro
	$_SESSION['stock_validado'] = true;

	return $out;

}

function opciones($campo){
		$opciones = consulta_bd("valor","opciones","nombre = '$campo'","");
		$valor = $opciones[0][0];
		return $valor;	
		}

?>