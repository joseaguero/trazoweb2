<?php	
include_once("../conx.php");
include_once("../admin/includes/library.php");

	$tbk_respuesta = $_POST[TBK_RESPUESTA];
	$monto_total = $_POST[TBK_MONTO];
	$oc = $_POST[TBK_ORDEN_COMPRA];
	$tbk_auth = $_POST[TBK_CODIGO_AUTORIZACION];
	$tbk_final_tar = $_POST[TBK_FINAL_NUMERO_TARJETA];
	$tbk_num_cuotas = $_POST[TBK_NUMERO_CUOTAS];
	$tbk_tipo_pago = $_POST[TBK_TIPO_PAGO];
	

//Obtengo el id del pedido
$pedido_id_q = consulta_bd("id, despacho","pedidos","oc = '$oc'","");
$pedido_id = $pedido_id_q[0][0];
$costo_despacho = $pedido_id_q[0][1];

//Obtengo el carro para envío por correo por si todo está ok y obtengo el monto de la compra
/* $q_carro = "SELECT producto_id, cantidad, precio, descuento, nombre FROM detalle_pedidos d, (SELECT id,precio,descuento,nombre FROM productos) p WHERE pedido_id = '$pedido_id' AND p.id = d.producto_id"; */

$q_carro = "SELECT producto_id, cantidad, valor_unitario_pagado, descuento, nombre, sku FROM detalle_pedidos d, (SELECT id,precio,descuento,nombre FROM productos) p WHERE pedido_id = '$pedido_id' AND p.id = d.producto_id";

//Genero la tabla de compras
$run_carro = mysqli_query($conexion, $q_carro);
$i = 0;
$carro = array();
$tabla_compra = '
		<table width="500" border="0">
			<tr>
				<td><strong>Producto</strong></td>
				<td align="right">SKU</td>
				<td align="right"><strong>Precio</strong></td>
				<td align="right"><strong>Cantidad</strong></td>
				<td align="right"><strong>Sub-Total</strong></td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>';
$total_carro = 0;
while($filas = mysqli_fetch_array($run_carro))
{
	$id_producto = $filas[0];
	$producto = $filas[4];
	$qty = $filas[1];
	$precio = $filas[2];
	$sub_total = $precio*$qty;
	$sku_producto = $filas[5];

	//Reviso el stock del producto, si es menor rechazo la transaccion.
	if ($id_producto != '') {
		$stock = consulta_bd_por_id("stock","productos","",$id_producto);
		if ($stock['stock'] < $qty)
		{
			echo 'RECHAZADO 0';
			alfa_log("RECHAZADO STOCK", $oc);
			die();
		}
	}
	
	//Agrego la venta al producto y le descuento el stock.
	$num_vtas = "UPDATE productos SET vtas = vtas+1, stock = stock-$qty WHERE id = '$id_producto'";
	$run = mysqli_query($conexion, $num_vtas);
	
	$tabla_compra .= '
			<tr>
				<td>'.$producto.'</td>
				<td>'.$sku_producto.'</td>
				<td align="right">$ '.number_format($precio).'</td>
				<td align="right">'.$qty.'</td>
				<td align="right">$ '.number_format($precio*$qty).'</td>
			</tr>
	';
	
	$total_carro = $total_carro+$sub_total;
	$i++;
}

//Se agrega el costo por despacho.
$tabla_compra .= '<tr><td>Cargo por despacho</td>';
$tabla_compra .= '<td align="right">';
$tabla_compra .= formato_moneda($costo_despacho, 'pesos', 0);
$tabla_compra .= '</td>';
$tabla_compra .= '<td align="right"></td>';
$tabla_compra .= '<td align="right">';
$tabla_compra .= formato_moneda($costo_despacho, 'pesos', 0);
$tabla_compra .= '</td>';
$tabla_compra .= '<td width="20"></td>';
$tabla_compra .= '</tr>';

$total_carro += $costo_despacho;

$val_iva = (int)get_option('iva');

$total_neto = $total_carro/(1+$val_iva/100);
$iva = $total_neto*($val_iva/100);
$tabla_compra .= '
<tr><td colspan="4">&nbsp;</td></tr>
<tr>
	<td colspan="2">&nbsp;</td>
	<td><strong>Total Neto</strong></td>
	<td align="right">$ '.number_format($total_neto).'</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
	<td><strong>IVA</strong></td>
	<td align="right">$ '.number_format($iva).'</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
	<td><strong>Total</strong></td>
	<td align="right">$ '.number_format($total_carro).'</td>
</tr>
';
$tabla_compra .= '</table>';


$filas = "SELECT direccion, comuna, ciudad,region_id, cliente_id FROM pedidos WHERE id = '$pedido_id'";
$run = mysqli_query($conexion, $filas);
$res = mysqli_fetch_array($run);
$region_id = $res[3];

if ($res[0] == '')
{
	$filas2 = "SELECT direccion, comuna, ciudad, region_id FROM clientes WHERE id = '$res[4]'";
	$run2 = mysqli_query($conexion, $filas2);
	$res2 = mysqli_fetch_array($run2);
	$region_id = $res2[3];
}

$reg = "SELECT nombre FROM regiones WHERE id = '$region_id'";
$run_reg = mysqli_query($conexion, $reg);
$res_reg = mysqli_fetch_array($run_reg);
$region = $res_reg[0];

$direccion = ($res[0] != '') ? $res[0]: $res2[0];
$comuna = ($res[1] != '') ? $res[1] : $res2[1];
$ciudad = ($res[2] != '') ? $res[2] : $res2[2];

if (check_mac($oc))
{	
	if ($tbk_respuesta == 0)
	{
		$total_carro_tbk = $total_carro."00";
		if ($total_carro_tbk == $monto_total)
		{
			//Se actualiza el estado del pedido y se confirma la compra
			if ($oc != '')
			{
				//Se valida el estado de la Orden de Compra
				$consulta_estado = "SELECT estado_vta_id FROM pedidos WHERE oc = '$oc'";
				$run_ce = mysqli_query($conexion, $consulta_estado);
				$res = mysqli_fetch_array($run_ce);
				if ($res[0] == 1)
				{
					//Se actualiza el estado de la orden de compra
					$update = "UPDATE pedidos SET estado_vta_id = '2', tbk_final_tar = '$tbk_final_tar', tbk_num_cuotas = '$tbk_num_cuotas', auth_tbk = '$tbk_auth', tbk_tipo_pago = '$tbk_tipo_pago' WHERE oc = '$oc'";
					$run = mysqli_query($conexion, $update);
					if ($run)
					{		
							//Envío correo con los datos de la compra al usuario
							$consulta = "SELECT p.id, p.cliente_id, p.comentarios, c.email, c.nombre, c.apellidos, c.rut, c.telefono1, c.telefono2, p.fecha, p.tbk_final_tar, p.tbk_num_cuotas FROM pedidos p, clientes c WHERE oc = '$oc' AND c.id = p.cliente_id";
							$run = mysqli_query($conexion, $consulta) or die(mysqli_error($conexion)."<br /><br />$sql");;
							$res = mysqli_fetch_array($run);
							$pedido_id = $res[0];
							$cliente_id = $res[1];
							$comentarios = $res[2];
							$email = $res[3];
							$nombre_completo = "$res[4] $res[5]";
							$rut_cliente = $res[6];
							$telefono_cliente = $res[8];
							$celular_cliente = $res[7];
							$fecha = $res[8];
							$tbk_final_tar = $res[10];
							$tbk_num_cuotas = $res[11];
							
							$datos_entrega = '
								Nombre Cliente: '.$nombre_completo.'<br />
								Rut: '.$rut_cliente.'<br />
								Dirección: '.$direccion.'<br />
								Comuna: '.$comuna.'<br />
								Ciudad: '.$ciudad.'<br />
								Región: '.$region.'<br />
								E-mail: '.$email.'<br />
								Celular: '.$celular_cliente.'<br />
								Teléfono: '.$telefono_cliente.'<br /><br />
							';
							
							$datos_pago = '
								Tipo de transacción: Venta<br />
								Tarjeta de crédito terminada en: XXXX-XXXX-XXXX-'.$tbk_final_tar.'<br />
								Cuotas: '.$tbk_num_cuotas.'<br />
								Fecha y hora de transacción: '.$fecha.'<br />
								Código de autorización de la transacción: '.$auth_tbk.'<br /><br />
							';
							
							//Envío correo al cliente y administrador
							//Obtengo los destinatarios
							$destinatarios_mail_ventas = get_option('destinatarios_mail_ventas');
							$para = "$email, $destinatarios_mail_ventas";
			
							$comentarios = ($comentarios != '') ? $comentarios : "Sin Comentarios";
																
							$destinatarios = explode(',', $para);
							foreach ($destinatarios as $d)
							{
								$dest = trim($d);
								if ($d == $email)
								{
									$asunto = "Detalle de tu compra en Outlet Bike";
									$reply_to = "no-reply@bike-outlet.cl";
			
									//Genero el mensaje
									$mensaje = get_option('mail_confirmacion_compra');
									$mensaje = str_replace("@carro", $tabla_compra, $mensaje);
									$mensaje = str_replace("@oc", $oc, $mensaje);
									$mensaje = str_replace("@nombre_completo", $nombre_completo, $mensaje);
									$mensaje = str_replace("@comentarios", $comentarios, $mensaje);	
									$mensaje = str_replace("@datos_entrega", $datos_entrega, $mensaje);
									$mensaje = str_replace("@datos_pago", $datos_pago, $mensaje);
									
									$send_mail = enviar_comprobante($d, $reply_to, $mensaje, $asunto);
								}
								else
								{
									
									//Genero el mensaje para el administrador
									$mensaje = get_option('mail_confirmacion_compra_interno');
									$mensaje = str_replace("@carro", $tabla_compra, $mensaje);
									$mensaje = str_replace("@oc", $oc, $mensaje);
									$mensaje = str_replace("@datos_entrega", $datos_entrega, $mensaje);
									$mensaje = str_replace("@datos_pago", $datos_pago, $mensaje);
									$mensaje = str_replace("@comentarios", $comentarios, $mensaje);	
									
									$asunto = "Información de compra en Outlet Bike";
									$reply_to = $email;
									$send_mail = enviar_comprobante($d, $reply_to, $mensaje, $asunto);
								}
							}
							
							//Se vacía el carro de compras.
							$_SESSION['cart_alfa_cm'] = false;
							
/* 							$error = "Tu pedido ha sido confirmado, te enviaremos un correo con el resumen.<br /> Muchas Gracias."; */
/* 							header('location:../index.php?error='.$error); */
							echo "ACEPTADO";
							alfa_log("ACEPTADO", $oc);
					}
					else //run
					{
						$error = "Tuvimos problemas al confirmar tu pedido, por favor inténtalo nuevamente.&tipo=ventana";
/* 						header('location:../index.php?op=cart&error='.$error); */
						echo "RECHAZADO 1"; //Run
						alfa_log("RECHAZADO 1", $oc);
					}
				}
				else
				{
					$error = "Tuvimos problemas al validar tu pedido, por favor inténtalo nuevamente.&tipo=ventana";
/* 					header('location:../index.php?op=cart&error='.$error); */
					echo "RECHAZADO 2 $error"; //Estado
					alfa_log("RECHAZADO 2 $error", $oc);
				}
			}
			else
			{
				$error = "Tuvimos problemas al procesar tu orden de compra, por favor inténtalo nuevamente.&tipo=ventana";
/* 				header('location:../index.php?op=cart&error='.$error); */
				echo "RECHAZADO 3 $error"; //OC
				alfa_log("RECHAZADO 3 $error", $oc);
			}
		}
		else
		{
			echo "RECHAZADO 4 $total_carro_tbk == $monto_total"; //Totales diferentes
			alfa_log("RECHAZADO 4 $total_carro_tbk == $monto_total", $oc);
		}
	}
	else
	{
		echo "ACEPTADO"; //Se acepta el rechazo del banco.
		alfa_log("RECHAZADO BANCO", $oc);
	}
}
else
{
	echo "RECHAZADO 5"; //Check Mac
	alfa_log("RECHAZADO 5", $oc);
}


function alfa_log($resultado, $oc){
	$carpetalogs = "/home/ab7405/public_html/bikeoutlet.cl/cgi-bin/log";
	$archivo_tmp = "$carpetalogs/log_alfa_$oc.txt";
	$file = fopen($archivo_tmp,'w');
	fwrite($file, $resultado); 
	fclose($file);
}

function check_mac($oc)
{

	$carpetalogs = "/home/ab7405/public_html/bikeoutlet.cl/cgi-bin/log";
	
	$archivo_tmp = "$carpetalogs/datos_checkmac_$oc.txt";
	
	reset($_POST);
	$i = 0;
	foreach ($_POST as $key=>$val)
	{
		if ($key != 'submit')
		{
			if ($i==0)
			{
				$variables .= "$key=$val";
			}
			else
			{
				$variables .= "&$key=$val";
			}
		}
		$i++;
	}
	$file = fopen($archivo_tmp,'w');
	fwrite($file, $variables); 
	fclose($file);
	
    $cmdline = "/home/ab7405/public_html/bikeoutlet.cl/cgi-bin/tbk_check_mac.cgi $archivo_tmp";
	exec($cmdline,$result,$retint);
	
	if ($result[0]=="CORRECTO")
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

?>
