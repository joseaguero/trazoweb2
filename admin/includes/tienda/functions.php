<?php

/********************************************************\
|  Funciones Tienda V0.1  								 |
|  Fecha Modificación: 01/02/2013						 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  	        		 |
|  http://www.moldeable.com/                             |
\********************************************************/

if (file_exists("admin/conf.php")){
	include_once("admin/conf.php");	
}


if ($active_user != '')
{
	$usuario = consulta_bd("nombre","clientes","id = '$active_user'","");
	$nombre_usuario = $usuario[0][0];
}

function get_option_tienda($val) {
	$res = consulta_bd("valor","opciones_tienda","nombre = '$val'","");
	return $res[0][0];
}

function get_adjusted_image($max_height, $max_width, $img_url)
{
	$size=getimagesize($img_url);
	$img_width=$size[0];
	$img_height=$size[1];
	$line_height = ($max_height-$img_height)/2;
	
	$p = $max_width/$img_width;
	$new_width = $p*$img_width;
	$new_height = $p*$img_height;	
		
	if ($img_width > $max_width) {
		if ($new_height > $max_height) {
			$p2 = $new_height/$max_height;
			$new_width = $p2*$new_width;
			$new_height = $p2*$new_height;
			return '<img src="'.$img_url.'" height="'.$new_height.'" border="0">';
		}
		else {
			return '<img src="'.$img_url.'" width="'.$new_width.'" border="0">';
		}
	}
	elseif ($img_height > $max_height) {
		return '<img src="'.$img_url.'" height="'.$new_height.'" border="0">';
	}
	else {
		return '<img src="'.$img_url.'" width="'.$max_width.'" height="'.$max_height.'" border="0"  style="line-height:'.$line_height.'px">';
	}
}

function change_cart($action, $id, $cart) {
	switch ($action) {
		case 'add':
			if ($cart) {
				//Reviso el stock del producto
				$stock = consulta_bd_por_id("nombre, stock","productos","",mysqli_real_escape_string($conexion, $id));
				
				$items = explode(',',$cart);
				$newcart = '';
				$qty[$id] = 0;
				foreach ($items as $item) {
					//Si el que se agrega es diferente al que se proceso no hay verificacion.
					if ($id != $item) {
						if ($newcart != '') {
							$newcart .= ','.$item;
						} else {
							$newcart = $item;
						}
					}
					else
					{
						//Se agrega el item solo una vez verificando stock
						$qty[$item]++;
						if ($qty[$item] <= (int)$stock['stock'])
						{
							if ($newcart != '') {
								$newcart .= ','.$item;
							} else {
								$newcart = $item;
							}
						}
					}
				}
				
				//Se agrega el item si este no existía en el carro o si el stock es suficiente.
				$qty[$id]++;
				if ($qty[$id] <= (int)$stock['stock'])
				{
					if ($newcart != '') {
						$newcart .= ','.$id;
					} else {
						$newcart = $id;
					}
				}
				else
				{
					$error = "No existe stock suficiente para ".$stock['nombre'].", sólo se ha considerado el stock disponible.&tipo=ventana";
				}
				
				$cart = $newcart;
			} else {
				$cart = $id;
			}
			break;
		case 'delete':
			if ($cart) {
				$items = explode(',',$cart);
				$newcart = '';
				foreach ($items as $item) {
					if ($id != $item) {
						if ($newcart != '') {
							$newcart .= ','.$item;
						} else {
							$newcart = $item;
						}
					}
				}
				$cart = $newcart;
			}
			break;
		case 'update':
			if ($cart) {
				foreach ($_POST as $key=>$value) {
					if (stristr($key,'qty')) {
						$id = str_replace('qty','',$key);
						//Reviso el stock del producto
						$stock = consulta_bd_por_id("nombre, stock","productos","",mysqli_real_escape_string($conexion, $id));
						
						$items = ($newcart != '') ? explode(',',$newcart) : explode(',',$cart);
						$newcart = '';
						foreach ($items as $item) {
							if ($id != $item) {
								if ($newcart != '') {
									$newcart .= ','.$item;
								} else {
									$newcart = $item;
								}
							}
						}
						for ($i=1;$i<=$value;$i++) {
							if ($i <= $stock['stock'])
							{
								if ($newcart != '') {
									$newcart .= ','.$id;
								} else {
									$newcart = $id;
								}
							}
							else
							{
								$error = "No existe stock suficiente para ".$stock['nombre'].", sólo se ha considerado el stock disponible.&tipo=ventana";
								break;							
							}
						}	
					}
				}
			}
			$cart = $newcart;
			break;	
		case 'up':
			if ($cart) {
				$items = explode(',',$cart);
				$newcart = '';
				$count = 0;
				foreach ($items as $item) {
					if ($id != $item) {
						if ($newcart != '') {
							$newcart .= ','.$item;
						} else {
							$newcart = $item;
						}
					}
					else
					{
						$count++;
					}
				}
				if ($count>0)
				{
					for ($j=1;$j<=($count+1);$j++)
					{
						if ($newcart != '') {
							$newcart .= ','.$id;
						} else {
							$newcart = $id;
						}
					}
				}
				$cart = $newcart;
			}
			break;
		case 'down':
			if ($cart) {
				$items = explode(',',$cart);
				$newcart = '';
				$count = 0;
				foreach ($items as $item) {
					if ($id != $item) {
						if ($newcart != '') {
							$newcart .= ','.$item;
						} else {
							$newcart = $item;
						}
					}
					else
					{
						$count++;
					}
				}
				if ($count>0)
				{
					for ($j=1;$j<=($count-1);$j++)
					{
						if ($newcart != '') {
							$newcart .= ','.$id;
						} else {
							$newcart = $id;
						}
					}
				}
				$cart = $newcart;
			}
			break;		
	}
	return $cart;
}

function writeMiniCart() {
	$cart = $_SESSION['moldeable_cart'];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return count($items).' producto'.$s;
	}
	else
	{
		return ' Mis compras';
	}
}

function writeShoppingCart() {
	$cart = $_SESSION['moldeable_cart'];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return '<p>Ud tiene <strong>'.count($items).' producto'.$s.'</strong> en su carro de compras:</p>';
	}
}

function get_items_qty($cart)
{
	if ($cart){
		$items = explode(',',$cart);
		return count($items);
	}
	else
	{
		return 0;
	}
}

function get_total_price()
{
	global $db;
	$cart = $_SESSION['moldeable_cart'];
	if ($cart){
	
		$items = explode(',',$cart);
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		foreach ($contents as $prd_id=>$qty) {
			$filas = consulta_bd_por_id("precio, descuento","productos","",$prd_id);
			extract($filas);
			$precio_final = get_precio($prd_id, 'descuento');
			$total += $precio_final * $qty;
		}
	}
	else
	{
		$total = 0;
	}
	return $total;
}

function showCart() {
	global $db;
	$cart = $_SESSION['moldeable_cart'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}

	    $output = '<div id="grilla-carro">';
    	$output .= '<div id="titulo-producto">PRODUCTO</div>';
		$output .= '<div id="titulo-descrip">DESCRIPCIÓN</div>';
		$output .= '<div id="titulo-code">CÓDIGO</div>';
		$output .= '<div id="titulo-precio">PRECIO</div>';
		$output .= '<div id="titulo-cantidad">CANTIDAD</div>';
		$output .= '<div id="titulo-total">TOTAL</div>';
        
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$filas = consulta_bd_por_id("id, nombre, precio, descuento, imagen_grilla, sku","productos","","$prd_id");
			extract($filas);

			$precio_producto = get_precio($id, 'descuento');

	        $output .= '<div class="fila-carro">';
	        $output .= '	<div class="caja-carro-producto">';
	        $output .= '    	&nbsp<img src="imagenes/productos/'.$imagen_grilla.'" width="103" />';
	        $output .= '    </div>';
	            
	        $output .= '    <div class="caja-carro-descrip">'.$nombre.'</div>';
	        $output .= '    <div class="caja-carro-codigo">ITEM #'.$sku.'</div>';
	        $output .= '    <div class="caja-carro-precio">';
	        $output .= 		formato_moneda($precio_producto, 'pesos', 0);
	        $output .= '	 </div>';
	        $output .= '    <div class="caja-carro-cantidad">';
	        $output .= '    	<div class="cont-cantidad">';
	        $output .= '            <div class="masmenos">';
	        $output .= '            	<a href="index.php?op=c1&action=up&id='.$prd_id.'"><img src="img/mas.jpg" width="25" height="16" /></a>';
	        $output .= '            	<a href="index.php?op=c1&action=down&id='.$prd_id.'"><img src="img/menos.jpg" width="25" height="11" /></a>';
	        $output .= '            </div>';
	        $output .= '            <div class="cantidad"><input type="text" value="'.$qty.'"/></div>';
	        $output .= '            <div class="borrar-cantidad">';
	        $output .= '            	<a href="index.php?op=c1&action=delete&id='.$prd_id.'">';
		    $output .= '                	<img src="img/x.jpg" width="23" height="27" />';
		    $output .= '                </a>';
	        $output .= '            </div>';
	        $output .= '    	</div>';
	        $output .= '    </div>';
	        $output .= '   <div class="caja-carro-total">';
	        $output .= 		formato_moneda($precio_producto*$qty, 'pesos', 0);
	        $output .= '   </div>';
	            
	        $output .= '</div>';
	        
	        $total += $precio_producto*$qty; 
	    }
	    
	    $output .= '	<div id="fila-total-carro1">';
	    $output .= '		<div id="valor-total">VALOR TOTAL:</div>';
	    $output .= '		<div id="caja-total-2">'.formato_moneda($total, 'pesos', 0).'</div>';
	    $output .= '	</div>';
	    $output .= '</div>';
    }
    return $output;
}

function showFinalCart() {
	global $db;
	$cart = $_SESSION['moldeable_cart'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}

	    $output = '<div id="grilla-carro">';
    	$output .= '<div id="titulo-producto">PRODUCTO</div>';
		$output .= '<div id="titulo-descrip">DESCRIPCIÓN</div>';
		$output .= '<div id="titulo-code">CÓDIGO</div>';
		$output .= '<div id="titulo-precio">PRECIO</div>';
		$output .= '<div id="titulo-cantidad">CANTIDAD</div>';
		$output .= '<div id="titulo-total">TOTAL</div>';
        
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$filas = consulta_bd_por_id("id, nombre, precio, descuento, imagen_grilla, sku","productos","","$prd_id");
			extract($filas);

			$precio_producto = get_precio($id, 'descuento');

	        $output .= '<div class="fila-carro">';
	        $output .= '	<div class="caja-carro-producto">';
	        $output .= '    	&nbsp<img src="imagenes/productos/'.$imagen_grilla.'" width="103" />';
	        $output .= '    </div>';
	            
	        $output .= '    <div class="caja-carro-descrip">'.$nombre.'</div>';
	        $output .= '    <div class="caja-carro-codigo">ITEM #'.$sku.'</div>';
	        $output .= '    <div class="caja-carro-precio">';
	        $output .= 		formato_moneda($precio_producto, 'pesos', 0);
	        $output .= '	 </div>';
	        $output .= '    <div class="caja-carro-cantidad">';
	        $output .= '    	<div class="cont-cantidad_final">';
	        $output .= '            <div class="cantidad">'.$qty.'</div>';
	        $output .= '    	</div>';
	        $output .= '    </div>';
	        $output .= '   <div class="caja-carro-total">';
	        $output .= 		formato_moneda($precio_producto*$qty, 'pesos', 0);
	        $output .= '   </div>';
	            
	        $output .= '</div>';
	        
	        $total += $precio_producto*$qty; 
	    }
	    
	    $output .= '	<div id="fila-total-carro1">';
	    $output .= '		<div id="valor-total">SUB TOTAL:</div>';
	    $output .= '		<div id="caja-total-2">'.formato_moneda($total, 'pesos', 0).'</div>';
	    $output .= '	</div>';
	    $output .= '</div>';
    }
    return $output;
}

function get_precio($id, $tipo) {
	$filas = consulta_bd("precio, descuento","productos", "id = $id", "");
	$precio = $filas[0][0];
	$descuento = $filas[0][1];
	if ($tipo == 'normal')
	{
		$valor = $precio;
	}
	else if ($tipo == 'descuento')
	{
		if ($descuento != 0 and $descuento > 0)
		{
			$valor = round($precio*(1-$descuento/100));
		}
		else
		{
			$valor = $precio;
		}	
	}
	return $valor;
}

function mail_bienvenida($email, $mensaje)
{

	$dir_logo = "http://www.bikeoutlet.cl/img/logo.png";
	$dominio_tienda = get_option('dominio_tienda');
	$nombre_tienda = get_option('nombre_tienda');
	$nombre_completo = "$nombre $apellidos";
		
	$header = "From: $nombre_tienda <no-reply@$dominio_tienda>\nReply-To:$reply_to\n";
	$header .= "X-Mailer:PHP/".phpversion()."\n";
	$header .= "Mime-Version: 1.0\n";
	$header .= "Content-Type: text/html";
	
	$msg2 = '
	<html>
		<head>
		<title>'.$nombre_tienda.'</title>
			<style type="text/css">
				body,td { 
					color:#000000; 
					font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; 
					background-color: #ffffff; 
				}
				a {
					color: #33ccff;
				}
			</style>
		</head>
		<body>
		<div style="font:12px/1.35em Verdana, Arial, Helvetica, sans-serif;">
		    <table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
		        <tr>
		            <td align="center" valign="top">
		                <table cellspacing="0" cellpadding="0" border="0" width="650">
		                    <tr>
		                        <td valign="top">
		                            <p><a href="http://www.'.$dominio_tienda.'" style="color:#8CC63F;"><img src="'.$dir_logo.'" alt="Ir a la tieda" border="0"/></a></p></td>
		                    </tr>
		                </table>
						<br/><br/>
						'.$mensaje.'
		            </td>
		        </tr>
		    </table>
		</div>
		</body>
	</html>
	';
	
	$asunto = "Bienvenido a $nombre_tienda";
	$send_mail = mail($email, $asunto, utf8_decode($msg2) ,$header);
	
	if ($send_mail)
	{
		return true;
	}
	else
	{
		return false;
	}

}

function generate_oc() {
	$rand = rand(1000, 9999);	
	$date = date('Ymds');
	$oc = "OC_$date$rand";
	return $oc;
}

function cantidad_productos_carro() {
	$cart = $_SESSION['cart_alfa_cm'];
	$items = explode(',',$cart);
	if($cart)
	{
		$cant = count($items);
		$s = ($cant > 0 ) ? 'items' : 'item';
	}
	else
	{
		$cant = 0;
	}
	echo $cant." ".$s;
}
	
function create_oc($cart, $direccion, $comuna, $ciudad, $region, $active_user, $codigo_promocion, $comentarios, $regalo){
	$oc = generate_oc();
	$sub_total = get_total_price();
	$iva = $sub_total/1.19;
	$envio = 0;
	$total = $sub_total + $envio;
	
	//Se genera el header del carro
	$campos = "oc, cliente_id, sub_total, envio, iva, total, fecha, estado_venta_id, regalo, direccion, comuna, ciudad, region, codigo_promocion, comentarios, fecha_creacion";
	$values = "'$oc', '$active_user', $sub_total, $envio, $iva, $total, NOW(), 1, $regalo, '$direccion', '$comuna', '$ciudad', '$region', '$codigo_promocion', '$comentarios', NOW()";
	$insert = insert_bd('ventas', $campos, $values);
	$venta_id = get_last('ventas');
	
	//Se genera el detalle del carro
	$items = explode(',',$cart);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}
	
	foreach ($contents as $prd_id=>$qty) {
		if ($prd_id != '') {
			$precio_producto = get_precio($prd_id, 'descuento');
			$campos = "venta_id, producto_id, cantidad, precio_venta";
			$values = "'$venta_id', '$prd_id', '$qty', '$precio_producto'";
			$insert = insert_bd('detalle_ventas', $campos, $values);
		}
	}
	
	return ($insert) ? $oc : false;
}


	
?>