<?php

/********************************************************\
|  Moldeable CMS - Excel generator.			             |
|  Fecha Modificación: 16/06/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');
include('../conf.php');
mysqli_query ($conexion, "SET NAMES 'latin1'");


header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=productosPedidosBD.xls"); 
header("Pragma: no-cache" );
header("Expires: 0"); 

$hoy = date("d/m/Y");

$productos = consulta_bd("pp.id, pp.pedido_id, pp.productos_detalle_id, pp.cantidad, pp.precio_unitario, pp.codigo, pp.fecha_creacion, p.oc","productos_pedidos pp join pedidos p on pp.pedido_id = p.id","","id desc");
?>

<h3>Base de datos de productos pedidos, fecha de reporte <?= $hoy; ?></h3>
<table width="700" border="1">
        <thead>
            <tr>
            	<th><h3>ID</h3></th>
                <th><h3>pedido_id</h3></th>
                <th><h3>OC</h3></th>
                <th><h3>productos_detalle_id</h3></th>
                <th><h3>Cantidad</h3></th>
                <th><h3>Precio Unitario</h3></th>
                <th><h3>Codigo</h3></th>
                <th><h3>Fecha creacion</h3></th>
				
            </tr>
        </thead>
        <tbody>
        	<?php for($i=0; $i<sizeof($productos); $i++) {?>
			<tr>
            	<td><?= $productos[$i][0]; ?></td>
                <td><?= $productos[$i][1]; ?></td>
                <td><?= $productos[$i][7]; ?></td>
                <td><?= $productos[$i][2]; ?></td>
                <td><?= $productos[$i][3]; ?></td>
                <td><?= $productos[$i][4]; ?></td>
                <td><?= $productos[$i][5]; ?></td>
                <td><?= $productos[$i][6]; ?></td>
            </tr>
            <?php } ?>
        </tbody>

</table>