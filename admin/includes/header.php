<?php
	date_default_timezone_set('America/Santiago');
	$tiny_mce = get_option("tiny_mce");
	$datepicker = get_option("datepicker");	
	$tienda = get_option('tienda');
	
	$cliente = consulta_bd("valor","opciones","nombre='nombre_cliente'","");
	if($cliente[0][0] == ''){
		$nombre_cliente = 'Cliente Anonimo';
	} else {
		$nombre_cliente = $cliente[0][0];
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8 " />
<title>CMS <?php echo $nombre_cliente;?></title>
<link href="css/style.css?v=881291891821" rel="stylesheet" type="text/css" />
<link href="css/layout.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/tinytable.css" />

<script type="text/javascript" src="js/jquery-1.8.0.js"></script>
<script language="Javascript" type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.2.custom.css">

<!-- TinyMCE -->
<script type="text/javascript" src="js/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		// General options
		language : "es",
		selector: "textarea",
		//mode : "textareas",
		theme : "modern",
		width: 800,
    	height: 150,
		elements : "<?php echo $tiny_mce; if ($tienda) {echo ",valor_opcion_tienda";} ?>",
		content_css: "css/content.css",
		plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "js/tiny_mce/skins/lightgray/content.min.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
	});
	
	function activar_tiny_mc(){
		tinymce.init({
			// General options
			language : "es",
			selector: "textarea",
			//mode : "textareas",
			theme : "modern",
			width: 800,
			height: 150,
			elements : "<?php echo $tiny_mce; if ($tienda) {echo ",valor_opcion_tienda";} ?>",
			content_css: "css/content.css",
			plugins: [
			 "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			 "save table contextmenu directionality emoticons template paste textcolor"
	   ],
	   content_css: "js/tiny_mce/skins/lightgray/content.min.css",
	   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
	   style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		]
		});
	};
</script>

<!-- /TinyMCE -->

<!-- Menu -->
		<link rel="stylesheet" type="text/css" href="css/superfish.css" media="screen">
		
		<script type="text/javascript" src="js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish.js"></script>
		<script type="text/javascript" src="js/supersubs.js"></script> 
		<script type="text/javascript"> 
		 
		    $(document).ready(function(){ 
		        $("ul.sf-menu").supersubs({ 
		            minWidth:    12,   // minimum width of sub-menus in em units 
		            maxWidth:    27,   // maximum width of sub-menus in em units 
		            extraWidth:  1     // extra width can ensure lines don't sometimes turn over 
		                               // due to slight rounding differences and font-family 
		        }).superfish({
		        	speed:       'fast'
		        	}
		        );  // call supersubs first, then superfish, so that subs are 
		                         // not display:none when measuring. Call before initialising 
		                         // containing tabs for same reason. 
		    }); 
		 
		</script>
<!-- Fin menu -->

<!-- TABS -->
<?php
	if (get_sec_op($op) == 'c') 
	{
		$datepicker = get_option("datepicker");
?>
	<!-- Tabs -->
	<script type="text/javascript">
		$(function() {
			$("#tabs").tabs();
		});
	</script>
	<!-- Fin Tabs -->
	


	
	<!-- CheckBox Tree -->
	<script type="text/javascript" src="js/jquery.checkboxtree.js"></script>
	<link rel="stylesheet" type="text/css" href="css/checkboxtree.css" charset="utf-8">
	<!-- Fin CheckBox Tree -->
	
	<!-- Calendarios para campos de fecha -->
<script>
$(function() {
	$("<?php echo $datepicker;?>").datepicker({
		dateFormat: 'yy-mm-dd',
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dayNamesMin: ['Do','Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		firstDay: 1
	});
});
</script>
	<!-- fin calendario -->
	
<!-- related selects -->
<script type="text/javascript" src="js/jquery.relatedselects.min.js"></script>
<!-- Fin related Select -->
<?php
	}
	elseif (get_sec_op($op) != 'a') 
	{	
?>
<!-- related selects -->
<script type="text/javascript" src="js/jquery.relatedselects.min.js"></script>
<!-- Fin related Select -->
<?php
	}
?>
<!-- FIN TABS -->

<!-- FLOT -->
<?php 
	if ($op == 0)
	{
?>
	<!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <!-- <script language="javascript" type="text/javascript" src="js/jquery.flot.min.js"></script> -->
<?php
	}
?>
<!-- FIN FLOT -->



<!--Activar mensaje de bienvenida -->
<?php 
if(isset($_COOKIE['cms'])) {
} else {
		
?>
<script>
	$(function(){
		//$('.lightbox').fadeIn(300);
	});
</script>
<?php
	include("pags/bienvenida.php");
	setcookie("cms",1,time()+30*24*60*60);
	}
?>

<link href="css/assets/css/main.css" rel="stylesheet" />
<link href="css/assets/css/croppic.css" rel="stylesheet" />

<script src="js/alertify.js"></script>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />



<!-- Colorbox -->
<link media="screen" rel="stylesheet" href="css/colorbox.css" />
<script src="js/jquery.colorbox.js"></script>
<script type="text/javascript">
    $(function(){
		$(".ajax").colorbox({
			//onOpen:function(){ alert('onOpen: colorbox is about to open'); },
			//onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
			onComplete:function(){
					activar_tiny_mc();
					$(":checkbox").uniform({checkboxClass: 'checker'});
					$("select").uniform();	
				},
				height:"75%"
			//onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
			//onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
		});
        $(".color_box").colorbox({
            iframe:true, 
            innerWidth:800,
            innerHeight:550, 
            transition:"fade",
            onClosed: function() {
                reload_img_content("<?php echo $tabla;?>", "<?php echo $id;?>", "<?php echo $val_op;?>");
            }
        });
        
        $(".informes").colorbox({
            iframe:true, 
            innerWidth:'90%',
            innerHeight:'90%', 
            transition:"fade",
        });
    });
</script>
<!-- Fin Colorbox -->



<!--Permite mostarr solo numeros en el campo, si se intenta dejar letras no lo permite -->
<script type="text/javascript" src="js/jquery.numeric.js"></script>
<script type="text/javascript">
$(function(){
	$("<?php echo get_option("numericos"); ?>").numeric();
})
</script>


<script type="text/javascript" src="js/jquery.Rut.min.js"></script>
<script type="text/javascript">
$(function(){
	$("<?php echo get_option("rut"); ?>").Rut({
		format_on: 'keyup',
	   on_error: function(){ 
	   		alertify.error('Rut incorrecto'); 
			$("#run").css("border", "solid 1px #FE1A00");
		},
	   on_success: function(){ 
	   		alertify.log('El rut es correcto');
			$("#run").css("border", "solid 1px #ccc");
	   }
	});
})

</script>