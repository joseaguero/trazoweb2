<?php include('conf.php');
	$cliente = consulta_bd("valor","opciones","nombre='nombre_cliente'","");
	if($cliente[0][0] == ''){
		$nombre_cliente = 'Cliente Anonimo';
	} else {
		$nombre_cliente = $cliente[0][0];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Moldeable CMS</title>

<link href="css/config.css" rel="stylesheet" type="text/css" />
<link href="css/style_login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>

<link rel="stylesheet" href="css/aristo/css/uniform.default.css" media="screen" />
<script src="js/jquery.uniform.js"></script>
<script type="text/javascript">
$(function(){
	$(":checkbox").uniform({checkboxClass: 'checker'});
});
</script>
    
<script language="Javascript" type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.2.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.2.custom.css">

<script type="text/javascript">
	function fwp(){
		$('#logeo').css('display', 'none');
		$('#fwp').fadeIn(200);
		//document.getElementById('log_in').style.display = 'none';
		//document.getElementById('fwp').style.display = 'block';
	}
	function cancel(){
		$('#fwp').css('display', 'none');
		$('#logeo').fadeIn(200);
	};
</script>
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js">IE7_PNG_SUFFIX=".png"</script>
<![endif]-->

<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
</head>

<body>


    
    
    

<div class="wrap">
	<?php
		if ($_GET[op] != 'fgp')
		{
	?>

        <!-- login -->
		<div class="login" id="logeo">
            <!-- logotipo modeable cms-->
        	<div class="logo">
            	<img src="pics/logo-moldeable-cms.png" width="276" height="32" alt="moldeable ">
            </div><!-- / fin logotipo modeable cms-->
            <form action="action/login.php" method="POST" name="login">
            	<input name="username" type="text" id="usuario" class="textfield user" placeholder="Usuario"/>
                
                <input name="pass" type="password" id="contra" class="textfield pass" placeholder="Contraseña" />
                <input type="submit" class="boton accede" name="Submit" value="Acceder" />
                <div><a href="javascript:fwp();">¿Olvidaste tu usuario o Password?</a></div>
            </form>
            <div class="footer2">
            	<p class="para">Sistema de administración desarrollado exclusivamente para <strong class="ncam"><?php echo $nombre_cliente; ?></strong></p>
                <p class="de">S.A. @2015 www.moldeable.com</p>
            </div>
         </div><!-- / login -->

    
    
    <div id="fwp" style="display:none">
        <!-- login -->
		<div class="login">
            <!-- logotipo modeable cms -->
        	<div class="logo">
            	<img src="pics/logo-moldeable-cms.png" width="276" height="32" alt="moldeable ">
            </div><!-- / fin logotipo modeable cms-->
            <form action="action/fwp.php" method="POST" name="login">
            	<div class="msj_recuperar">Ingresa tu correo para recuperar tu password:	</div>
                <input name="mail" type="text" id="mail" class="textfield user" placeholder="Correo" />
                <a class="boton accede boton2 cancelar" onclick="javascript:cancel()">Cancelar</a>
		        <input type="submit" class="boton accede boton2" name="fwp" value="Enviar" style="margin-right:0; margin-left:20px!important;" />
                
            </form>
            
         </div><!-- / login -->
    </div>
    
	<?php
		}
		else
		{
			if ($_GET[hash]!='' AND $_GET[e]!='')
			{
	?>
    
    
    
    <div id="recuperar" >
        <!-- login -->
		<div class="login">
            <!-- logotipo modeable cms -->
        	<div class="logo">
            	<img src="pics/logo-moldeable-cms.png" width="276" height="32" alt="moldeable ">
            </div><!-- / fin logotipo modeable cms-->
            <form action="action/chp.php" method="POST">
            	<div class="msj_recuperar">Por favor ingrese se nueva password:</div>
                <input name="password" type="password" id="pass" class="textfield user"/>
                
                <div class="msj_recuperar">Nueva Password</div>
                <input name="password2" type="password" id="repass" class="textfield user" />
                
                <input type="submit" class="boton accede" name="update_pass" value="Enviar" />
                <input type="hidden" name="hash" value="<?php echo $_GET[hash];?>" />
                <input type="hidden" name="e" value="<?php echo $_GET[e];?>" />
                
                
            </form>
            
         </div><!-- / login -->
    </div>
    
    
    
    
	<?php
			}
			else
			{
				echo "<br /><br />Por favor inicie sesion <a href='adlogin.php' class='error'>aquí</a>.";
			}
		}
	?>
</div>

<!--alertas -->
<script src="js/alertify.min.js"></script>
<script>
	function reset () {
		$("#toggleCSS").attr("href", "css/alertify.default.css");
		alertify.set({
			labels : {
				ok     : "Aceptar",
				cancel : "Cancel"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'ventana'){ ?>
		alertify.set({ labels: { ok: "Aceptar", cancel: "Cancelar" } });
		alertify.alert("<?php echo $_GET[error]; ?>");
	<?php } ?>	
	<?php if (isset($_GET[tipo]) and $_GET[tipo] == 'error'){ ?>
		alertify.error("<?php echo $_GET[error];?>");
	<?php } ?>
</script>
    
    
</body>
</html>
