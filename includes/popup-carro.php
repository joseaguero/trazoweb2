<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$id = isset($_GET['id']) ? mysqli_real_escape_string($conexion, $_GET['id']) : NULL;
// $id = 10934;
$producto = consulta_bd('p.nombre, p.thumbs, IF(pd.descuento > 0, pd.descuento, pd.precio)', 'productos p join productos_detalles pd on pd.producto_id = p.id', "pd.id = {$id}", '');
?>

<h3>Has agregado este producto al carro</h3>
<div class="pop-cont">
	<img src="<?=$producto[0][1]?>" alt="<?=$producto[0][0]?>" width="150">
	<div class="pop-data">
		<?= $producto[0][0] ?> <br><br>
		$<?= number_format($producto[0][2], 0, ",", ".") ?>
	</div>
	<div class="pop-subtotal">
		Subtotal (<?= qty_pro() ?> Productos): $<?= number_format(totalCart(), 0, ",", "."); ?>
	</div>
	<a href="carro">IR AL CARRO</a>
</div>