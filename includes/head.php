
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
<base href="<?php echo $url_base; ?>" />

<LINK REL="SHORTCUT ICON" HREF="favicon.png">
<META NAME="Title" CONTENT="Trazo">
<META NAME="DC.Title" CONTENT="Trazo">
<META http-equiv="title" CONTENT="Trazo">

<META NAME="DESCRIPTION" CONTENT="Trazo es una tienda abierta en septiembre del 2016 en Vitacura cuyo fin es traer lo mejor de la moda en muebles y decoración a través de ambientes que conmuevan, inspiren y conecten.
Una tienda viajada, sensible e inquita con productos elegidos con cariño y que se renueven de manera permanente.">
<META NAME="Author" CONTENT="Moldeable S.A.">
<META NAME="DC.Author" CONTENT="Moldeable S.A.">
<META NAME="DC.Creator" CONTENT="Moldeable S.A.">
<META NAME="copyright" CONTENT="Idea Láser">
<META NAME="Generator" CONTENT="Moldeable CMS">
<META NAME="authoring.tool" CONTENT="Moldeable CMS">
<META NAME="Keywords" CONTENT="Tags separados por coma">
<META http-equiv="keywords" CONTENT="Tags separados por coma">
<META NAME="VW96.objecttype" CONTENT="Homepage">
<META NAME="resource-type" CONTENT="Homepage">
<META NAME="doc-type" CONTENT="Homepage">
<META NAME="Classification" CONTENT="General">
<META NAME="RATING" CONTENT="General">
<META NAME="Distribution" CONTENT="Global">
<META NAME="Language" CONTENT="Spanish">
<META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="Revisit-after" CONTENT="15 days">
<META NAME="CREDITS" CONTENT="Diseño y Prgramación: Moldeable S.A., www.moldeable.com">

<meta property="og:title" content="Trazo" />
<meta property="og:description" content="Trazo es una tienda abierta en septiembre del 2016 en Vitacura cuyo fin es traer lo mejor de la moda en muebles y decoración a través de ambientes que conmuevan, inspiren y conecten.
Una tienda viajada, sensible e inquita con productos elegidos con cariño y que se renueven de manera permanente." />
<meta property="og:image" content="thumbnail_image" />
<meta property="og:site_name" content="https://forastero.life/" />
<link rel="canonical" href="https://forastero.life">